#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QProcess, QPropertyAnimation, QSettings, QString, QTimer, QThread, QVariant, SLOT
from PyQt4.QtGui import QAction, QApplication, QDialog, QFileDialog, QMainWindow
from commands import getoutput
from time import sleep

# Imports (Custom Stuff)
import systray, ui_cadence, ui_cadence_rwait, ui_cadence_tb_jack, ui_cadence_tb_a2j, ui_cadence_tb_pa
from shared_jack import *

DEFAULT_LADSPA_PATH = ["/usr/lib/ladspa", "/usr/local/lib/ladspa", "/usr/lib32/ladspa", HOME+"/.ladspa" ]
DEFAULT_DSSI_PATH = ["/usr/lib/dssi", "/usr/local/lib/dssi", "/usr/lib32/dssi", HOME+"/.dssi" ]
DEFAULT_LV2_PATH  = ["/usr/lib/lv2", "/usr/local/lib/lv2", HOME+"/.lv2" ]
DEFAULT_VST_PATH  = ["/usr/lib/vst", "/usr/local/lib/vst", "/usr/lib32/vst", HOME+"/.vst" ]

WINEASIO_PREFIX = "HKEY_CURRENT_USER\Software\Wine\WineASIO"

XDG_APPLICATIONS_PATH = ["/usr/share/applications", "/usr/local/share/applications"]

# *.desktop files used for 'Default Applications'
DESKTOP_X_IMAGE = [
  "eog.desktop",
  "kde4/digikam.desktop",
  "kde4/gwenview.desktop",
  ]
DESKTOP_X_MUSIC = [
  "audacious.desktop",
  "clementine.desktop",
  "smplayer.desktop",
  "totem.desktop",
  "vlc.desktop",
  "kde4/amarok.desktop",
  ]
DESKTOP_X_VIDEO = [
  "smplayer.desktop",
  "totem.desktop",
  "vlc.desktop",
  ]
DESKTOP_X_TEXT = [
  "gedit.desktop",
  "kde4/kate.desktop",
  "kde4/kwrite.desktop",
  ]
DESKTOP_X_BROWSER = [
  "chrome.desktop",
  "firefox.desktop",
  "firefox-4.0.desktop",
  "kde4/konqbrowser.desktop",
  ]
# TODO - Office (needs some hacks)

# ALSA Bridge
AlsaJackFile = (""
"pcm.!default {\n"
"    type plug\n"
"    slave { pcm \"jack\" }\n"
"}\n"
"\n"
"pcm.jack {\n"
"    type jack\n"
"    playback_ports {\n"
"        0 system:playback_1\n"
"        1 system:playback_2\n"
"    }\n"
"    capture_ports {\n"
"        0 system:capture_1\n"
"        1 system:capture_2\n"
"    }\n"
"}\n"
"\n"
"ctl.mixer0 {\n"
"    type hw\n"
"    card 0\n"
"}\n")

AlsaPulseAudioFile = (""
"pcm.!default {\n"
"    type plug\n"
"    slave { pcm \"pulse\" }\n"
"}\n"
"\n"
"pcm.pulse {\n"
"    type pulse\n"
"}\n"
"\n"
"ctl.mixer0 {\n"
"    type hw\n"
"    card 0\n"
"}\n")

# Global Settings
GlobalSettings = QSettings("Cadence", "GlobalSettings")

# Imports (DBus)
try:
  import dbus
  from dbus.mainloop.qt import DBusQtMainLoop
  haveDBus = True
except:
  haveDBus = False

havePulseAudio = os.path.exists("/usr/bin/pulseaudio")

def refreshDBus():
  try:
    DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
    jacksettings.initBus(DBus.bus)
  except:
    DBus.jack = None

  try:
    DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
    a2j_client_name = DBus.a2j.get_jack_client_name()
  except:
    DBus.a2j = None

if (haveDBus):
  DBus.loop = DBusQtMainLoop(set_as_default=True)
  DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
  refreshDBus()
else:
  DBus.jack = None
  DBus.a2j = None

def isDesktopInstalled(desktop):
  for X_PATH in XDG_APPLICATIONS_PATH:
    if (os.path.exists(os.path.join(X_PATH, desktop))):
      return True
  else:
    return False

def get_xdg_property(file_dump, key):
  file_dump_split = file_dump.split(key)
  if (len(file_dump_split) > 1 and "=" in file_dump_split[1]):
    return file_dump_split[1].split("=")[1].split(";\n")[0]
  else:
    return None

# Get smart hexadecimal string
def smartHex(value, len_):
  hex_str = hex(value).replace("0x","")
  if (len(hex_str) < len_):
    hex_str = (len_-(len(hex_str)))*"0" + hex_str
  return hex_str

def searchAndSetComboBoxValue(combo_box, value):
  for i in range(combo_box.count()):
    if (combo_box.itemText(i).replace("/","-") == value):
      combo_box.setCurrentIndex(i)
      return True
  else:
    return False

def check_config():
  # Test 01 - User in audio group
  test_x_audio_group = ("audio" in getoutput("groups").split(" "))

  # Test 02 - Kernel
  uname2 = str(os.uname()[2])

  if ("-" in uname2):
    test_x_kernel_version = uname2.split("-",1)[0]

    if ("-pae" in uname2):
      split_str = uname2.rsplit("-",2)
      test_x_kernel_str = split_str[1]+"-"+split_str[2]
    else:
      test_x_kernel_str = uname2.rsplit("-",1)[1]

  else:
    test_x_kernel_str = "'Vanilla'"
    test_x_kernel_version = uname2

  config_dict = {
   'audio_group': test_x_audio_group,
   'kernel_str': test_x_kernel_str,
   'kernel_version': test_x_kernel_version
  }

  return config_dict

def PA_is_started():
  if ("pulseaudio" in getProcList()):
    return True
  else:
    return False

def PA_is_bridged():
  if (jack.client):
    port_list = jacklib.get_ports(jack.client, "", "", 0)
    if ("PulseAudio JACK Sink:front-left" in port_list):
      return True
    else:
      return False
  else:
    return False

# Get Linux distro name
def getDistroName():
    from commands import getoutput
    distro = ""

    if (os.path.exists("/etc/lsb-release")):
      distro = getoutput(". /etc/lsb-release && echo $DISTRIB_DESCRIPTION")
    # TODO - add more checks

    if (not distro):
      distro = os.uname()[0]

    return distro

# Get Wine stuff from registry file
def getWineKeyValue(WINEPREFIX, prefix, key, default):
  if ("HKEY_CURRENT_USER" in prefix):
    wine_file = os.path.join(WINEPREFIX, "user.reg")
  elif ("HKEY_LOCAL_MACHINE" in prefix):
    wine_file = os.path.join(WINEPREFIX, "system.reg")
  else:
    return default

  if (os.path.exists(wine_file)):
    wine_reg_prefix = "[" + prefix.replace("HKEY_CURRENT_USER\\","").replace("HKEY_LOCAL_MACHINE\\","").replace("\\","\\\\") + "]"
    wine_dump = open(wine_file, "r").read()
    wine_dump_split = wine_dump.split(wine_reg_prefix)
    if (len(wine_dump_split) > 1):
      wine_dump_small = wine_dump_split[1].split("[")[0]
      key_dump_split = wine_dump_small.split('"'+key+'"')
      if (len(key_dump_split) > 1):
        key_dump_small = key_dump_split[1].split(":")[1].split("\n")[0]
        return key_dump_small
      else:
        return default
    else:
      return default
  else:
    return default

# Get Process list
def getProcList():
    procs = []

    process = QProcess()
    process.start("ps", ["-e"]) # FIXME - linux only
    process.waitForFinished()

    procs_dump = QStringStr(process.readAllStandardOutput()).split("\n")

    for i in range(len(procs_dump)):
      if (i == 0): continue
      proc_test = procs_dump[i].rsplit(":", 1)[-1].split(" ")
      if (len(proc_test) > 1):
        procs.append(proc_test[1])

    return procs

# Force Restart Dialog
class ForceWaitDialog(QDialog, ui_cadence_rwait.Ui_Dialog):
    def __init__(self, parent=None):
        super(ForceWaitDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(Qt.Dialog|Qt.WindowCloseButtonHint)

# Wait while JACK restarts
class ForceRestartThread(QThread):
    def __init__(self, parent=None):
        super(ForceRestartThread, self).__init__(parent)

        self._started = False

    def wasJackStarted(self):
        return self._started

    def run(self):
        # Not started yet
        self._started = False
        process = QProcess(self.parent())

        # Kill All
        procs_term = ["a2j", "a2jmidid", "artsd", "jackd", "jackdmp", "knotify4", "lash", "ladishd", "ladiappd", "ladiconfd", "jmcore"]
        procs_kill = ["jackdbus", "pulseaudio"]
        tries = 30

        process.start("killall", procs_term)
        process.waitForFinished()

        for x in range(tries):
          procs_list = getProcList()
          for i in range(len(procs_term)):
            if (procs_term[i] in procs_list):
              break
            else:
              sleep(0.1)
          else:
            break

        process.start("killall", ["-KILL"] + procs_kill)
        process.waitForFinished()

        for x in range(tries):
          procs_list = getProcList()
          for i in range(len(procs_kill)):
            if (procs_kill[i] in procs_list):
              break
            else:
              sleep(0.1)
          else:
            break

        refreshDBus()

        for x in range(tries):
          procs_dump = getProcList()
          if (QString(u"jackdbus\n") in procs_dump):
            break
          else:
            sleep(0.1)

        DBus.jack.StartServer()

        # If we made it this far, then JACK is started
        self._started = True

# Additional JACK options
class ToolBarJackDialog(QDialog, ui_cadence_tb_jack.Ui_Dialog):
    def __init__(self, parent=None):
        super(ToolBarJackDialog, self).__init__(parent)
        self.setupUi(self)

        self.ladish_loaded = False

        if (haveDBus):
          if (GlobalSettings.value("JACK/AutoLoadLadishStudio", False).toBool()):
            self.rb_ladish.setChecked(True)
            self.ladish_loaded = True
          elif ("org.ladish" in DBus.bus.list_names()):
            self.ladish_loaded = True
        else:
          self.rb_ladish.setEnabled(False)
          self.rb_jack.setChecked(True)

        if (self.ladish_loaded):
          self.fillStudioNames()

        self.connect(self, SIGNAL("accepted()"), self.setOptions)
        self.connect(self.rb_ladish, SIGNAL("clicked()"), self.maybeFillStudioNames)

    def maybeFillStudioNames(self):
        if (not self.ladish_loaded):
          self.fillStudioNames()
          self.ladish_loaded = True

    def fillStudioNames(self):
        default_studio = GlobalSettings.value("JACK/LadishStudioName", "").toString()
        DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")

        studio_list_dump = DBus.ladish_control.GetStudioList()

        if (len(studio_list_dump) == 0):
          self.rb_ladish.setEnabled(False)
          self.rb_jack.setChecked(True)
        else:
          for i in range(len(studio_list_dump)):
            name = QString(studio_list_dump[i][0])
            self.cb_studio_name.addItem(name)

            if (not default_studio.isEmpty() and default_studio == name):
              self.cb_studio_name.setCurrentIndex(i)

    def setOptions(self):
        GlobalSettings.setValue("JACK/AutoLoadLadishStudio", self.rb_ladish.isChecked())
        GlobalSettings.setValue("JACK/LadishStudioName", self.cb_studio_name.currentText())

# Additional A2J MIDI options
class ToolBarA2JDialog(QDialog, ui_cadence_tb_a2j.Ui_Dialog):
    def __init__(self, parent=None):
        super(ToolBarA2JDialog, self).__init__(parent)
        self.setupUi(self)

        self.cb_export_hw.setChecked(GlobalSettings.value("A2J/ExportHW", True).toBool())

        self.connect(self, SIGNAL("accepted()"), self.setOptions)

    def setOptions(self):
        GlobalSettings.setValue("A2J/ExportHW", self.cb_export_hw.isChecked())

# Additional Pulse-JACK options
class ToolBarPADialog(QDialog, ui_cadence_tb_pa.Ui_Dialog):
    def __init__(self, parent=None):
        super(ToolBarPADialog, self).__init__(parent)
        self.setupUi(self)

        self.cb_playback_only.setChecked(GlobalSettings.value("Pulse2JACK/PlaybackModeOnly", False).toBool())

        self.connect(self, SIGNAL("accepted()"), self.setOptions)

    def setOptions(self):
        GlobalSettings.setValue("Pulse2JACK/PlaybackModeOnly", self.cb_playback_only.isChecked())

# Main Window
class CadenceMainW(QMainWindow, ui_cadence.Ui_CadenceMainW):
    def __init__(self, parent=None):
        super(CadenceMainW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Cadence")
        self.loadSettings()

        self.timer = QTimer()
        self.timer.setInterval(250)

        # TODO - Not Implemented Yet
        self.b_checks_fixes.setEnabled(False)
        self.tb_alsa.setEnabled(False)
        self.act_help_doc.setEnabled(False)

        self.l_distro.setText(getDistroName())
        self.l_arch.setText(os.uname()[4])
        self.l_kernel.setText(os.uname()[2])
        self.l_realtime.setText("")

        self.act_jack_start.setIcon(getIcon("media-playback-start"))
        self.act_jack_stop.setIcon(getIcon("media-playback-stop"))
        self.act_jack_clear_xruns.setIcon(getIcon("edit-clear"))
        self.act_jack_configure.setIcon(getIcon("configure"))
        self.act_a2j_start.setIcon(getIcon("media-playback-start"))
        self.act_a2j_stop.setIcon(getIcon("media-playback-stop"))
        self.act_pulse_start.setIcon(getIcon("media-playback-start"))
        self.act_pulse_stop.setIcon(getIcon("media-playback-stop"))

        self.act_quit.setIcon(getIcon("application-exit"))
        self.act_help_about.setIcon(getIcon("jack"))

        self.icon_apply = QIcon(getIcon("dialog-ok-apply", 16)).pixmap(16, 16)
        self.icon_cancel = QIcon(getIcon("dialog-cancel", 16)).pixmap(16, 16)
        self.icon_error = QIcon(getIcon("dialog-error", 16)).pixmap(16, 16)
        self.icon_warning = QIcon(getIcon("dialog-warning", 16)).pixmap(16, 16)

        for i in range(self.tw_tweaks.rowCount()):
          self.tw_tweaks.item(0, i).setTextAlignment(Qt.AlignCenter)

        self.tw_tweaks.setCurrentCell(0, 0)

        # Do a system check if first run
        if (self.settings.value("FirstRun", True).toBool()):
          self.refreshChecks()

        self.settings_changed_types = []
        self.frame_settings.setVisible(False)

        self.b_remove_ladspa.setEnabled(False)
        self.b_edit_ladspa.setEnabled(False)
        self.b_remove_dssi.setEnabled(False)
        self.b_edit_dssi.setEnabled(False)
        self.b_remove_lv2.setEnabled(False)
        self.b_edit_lv2.setEnabled(False)
        self.b_remove_vst.setEnabled(False)
        self.b_edit_vst.setEnabled(False)

        for ppath in DEFAULT_LADSPA_PATH:
          self.list_LADSPA.addItem(ppath)

        for ppath in DEFAULT_DSSI_PATH:
          self.list_DSSI.addItem(ppath)

        for ppath in DEFAULT_LV2_PATH:
          self.list_LV2.addItem(ppath)

        for ppath in DEFAULT_VST_PATH:
          self.list_VST.addItem(ppath)

        EXTRA_LADSPA_DIRS = GlobalSettings.value("AudioPlugins/EXTRA_LADSPA_PATH", "").toString()
        EXTRA_DSSI_DIRS = GlobalSettings.value("AudioPlugins/EXTRA_DSSI_PATH", "").toString()
        EXTRA_LV2_DIRS = GlobalSettings.value("AudioPlugins/EXTRA_LV2_PATH", "").toString()
        EXTRA_VST_DIRS = GlobalSettings.value("AudioPlugins/EXTRA_VST_PATH", "").toString()

        for ppath in EXTRA_LADSPA_DIRS.split(":"):
          if (os.path.exists(ppath)):
            self.list_LADSPA.addItem(ppath)

        for ppath in EXTRA_DSSI_DIRS.split(":"):
          if (os.path.exists(ppath)):
            self.list_DSSI.addItem(ppath)

        for ppath in EXTRA_LV2_DIRS.split(":"):
          if (os.path.exists(ppath)):
            self.list_LV2.addItem(ppath)

        for ppath in EXTRA_VST_DIRS.split(":"):
          if (os.path.exists(ppath)):
            self.list_VST.addItem(ppath)

        self.list_LADSPA.sortItems(Qt.AscendingOrder)
        self.list_DSSI.sortItems(Qt.AscendingOrder)
        self.list_LV2.sortItems(Qt.AscendingOrder)
        self.list_VST.sortItems(Qt.AscendingOrder)

        for desktop in DESKTOP_X_IMAGE:
          if (isDesktopInstalled(desktop)):
            self.cb_app_image.addItem(desktop)

        for desktop in DESKTOP_X_MUSIC:
          if (isDesktopInstalled(desktop)):
            self.cb_app_music.addItem(desktop)

        for desktop in DESKTOP_X_VIDEO:
          if (isDesktopInstalled(desktop)):
            self.cb_app_video.addItem(desktop)

        for desktop in DESKTOP_X_TEXT:
          if (isDesktopInstalled(desktop)):
            self.cb_app_text.addItem(desktop)

        for desktop in DESKTOP_X_BROWSER:
          if (isDesktopInstalled(desktop)):
            self.cb_app_browser.addItem(desktop)

        if (self.cb_app_image.count() == 0):
          self.ch_image.setEnabled(False)

        if (self.cb_app_music.count() == 0):
          self.ch_music.setEnabled(False)

        if (self.cb_app_video.count() == 0):
          self.ch_video.setEnabled(False)

        if (self.cb_app_text.count() == 0):
          self.ch_text.setEnabled(False)

        if (self.cb_app_office.count() == 0):
          self.ch_office.setEnabled(False)

        if (self.cb_app_browser.count() == 0):
          self.ch_browser.setEnabled(False)

        local_xdg_mimeapps = os.path.join(HOME, ".local", "share", "applications", "mimeapps.list")
        if (os.path.exists(local_xdg_mimeapps)):
          mimeapps_read = open(local_xdg_mimeapps, "r").read()
          x_image = get_xdg_property(mimeapps_read, "image/x-bitmap")
          x_music = get_xdg_property(mimeapps_read, "audio/x-wav")
          x_video = get_xdg_property(mimeapps_read, "video/x-ogg")
          x_text  = get_xdg_property(mimeapps_read, "application/x-zerosize")
          #x_office = get_xdg_property(mimeapps_read, "")
          x_browser = get_xdg_property(mimeapps_read, "text/html")

          if (x_image != None):
            if (searchAndSetComboBoxValue(self.cb_app_image, x_image)):
              self.ch_image.setChecked(True)

          if (x_music != None):
            if (searchAndSetComboBoxValue(self.cb_app_music, x_music)):
              self.ch_music.setChecked(True)

          if (x_video != None):
            if (searchAndSetComboBoxValue(self.cb_app_video, x_video)):
              self.ch_video.setChecked(True)

          if (x_text != None):
            if (searchAndSetComboBoxValue(self.cb_app_text, x_text)):
              self.ch_text.setChecked(True)

          if (x_browser != None):
            if (searchAndSetComboBoxValue(self.cb_app_browser, x_browser)):
              self.ch_browser.setChecked(True)

        else: # ~/.local/share/applications/mimeapps.list doesn't exist
          if (not os.path.exists(os.path.join(HOME, ".local", "share", "applications"))):
            if (not os.path.exists(os.path.join(HOME, ".local", "share"))):
              if (not os.path.exists(os.path.join(HOME, ".local"))):
                os.mkdir(os.path.join(HOME, ".local"))
              os.mkdir(os.path.join(HOME, ".local", "share"))
            os.mkdir(os.path.join(HOME, ".local", "share", "applications"))

        # Check for Wine
        for i in range(len(PATH)):
          if (os.path.exists(os.path.join(PATH[i], "regedit"))):
            haveWine = True
            break
        else:
          haveWine = False

        if (haveWine):
          self.WINEPREFIX = os.getenv("WINEPREFIX")
          if (not self.WINEPREFIX):
            self.WINEPREFIX = os.path.join(HOME,".wine")

          self.cb_wineprefix.addItem(self.WINEPREFIX)

          w_ins = int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Number of inputs", "00000010"), 16)
          w_outs = int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Number of outputs", "00000010"), 16)
          w_hw = bool(int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Connect to hardware", "00000001")))

          w_autostart = bool(int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Autostart server", "00000000")))
          w_fixed_bsize = bool(int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Fixed buffersize", "00000001")))
          w_prefer_bsize = int(getWineKeyValue(self.WINEPREFIX, WINEASIO_PREFIX, "Preferred buffersize", "00000400"), 16)

          for bsize in buffer_sizes:
            self.cb_wineasio_bsizes.addItem(str(bsize))
            if (bsize == w_prefer_bsize):
              self.cb_wineasio_bsizes.setCurrentIndex(self.cb_wineasio_bsizes.count()-1)

          self.sb_wineasio_ins.setValue(w_ins)
          self.sb_wineasio_outs.setValue(w_outs)
          self.cb_wineasio_hw.setChecked(w_hw)

          self.cb_wineasio_autostart.setChecked(w_autostart)
          self.cb_wineasio_fixed_bsize.setChecked(w_fixed_bsize)

        else:
          # No Wine
          self.tw_tweaks.hideRow(2)

        if (not havePulseAudio):
          self.group_pulse.setEnabled(False)

        self.buffer_size = 0
        self.sample_rate = 0
        self.realtime = None
        self.xruns = -1

        self.act_jack_bf_list = (self.act_jack_bf_16, self.act_jack_bf_32, self.act_jack_bf_64, self.act_jack_bf_128, self.act_jack_bf_256,
                                 self.act_jack_bf_512, self.act_jack_bf_1024, self.act_jack_bf_2048, self.act_jack_bf_4096, self.act_jack_bf_8192)

        # Global Systray
        self.systray = systray.GlobalSysTray("Cadence", "cadence")
        self.systray.setQtParent(self)

        self.systray.addAction("jack_start", QStringStr(self.tr("Start JACK")))
        self.systray.addAction("jack_stop", QStringStr(self.tr("Stop JACK")))
        self.systray.addAction("jack_configure", QStringStr(self.tr("Configure JACK")))
        self.systray.addSeparator("sep1")
        self.systray.addMenu("a2j", QStringStr(self.tr("A2J Bridge")))
        self.systray.addMenuAction("a2j", "a2j_start", QStringStr(self.tr("Start")))
        self.systray.addMenuAction("a2j", "a2j_stop", QStringStr(self.tr("Stop")))
        self.systray.addMenuAction("a2j", "a2j_export_hw", QStringStr(self.tr("Export Hardware Ports...")))
        self.systray.addMenu("pulse", QStringStr(self.tr("Pulse2JACK Bridge")))
        self.systray.addMenuAction("pulse", "pulse_start", QStringStr(self.tr("Start")))
        self.systray.addMenuAction("pulse", "pulse_stop", QStringStr(self.tr("Stop")))
        self.systray.addMenu("tools", QStringStr(self.tr("Tools")))
        self.systray.addMenuAction("tools", "app_catia", "Catia")
        self.systray.addMenuAction("tools", "app_claudia", "Claudia")
        self.systray.addMenuAction("tools", "app_carla", "Carla")
        self.systray.addMenuSeparator("tools", "tools_sep")
        self.systray.addMenuAction("tools", "app_logs", "Logs")
        self.systray.addMenuAction("tools", "app_render", "Render")
        self.systray.addMenuAction("tools", "app_xy-controller", "XY-Controller")
        self.systray.addSeparator("sep2")
        self.systray.addAction("show", QStringStr(self.tr("Minimize")))
        self.systray.addAction("quit", QStringStr(self.tr("Quit")))

        self.systray.setActionIcon("jack_start", "media-playback-start")
        self.systray.setActionIcon("jack_stop", "media-playback-stop")
        self.systray.setActionIcon("jack_configure", "configure")
        self.systray.setActionIcon("a2j_start", "media-playback-start")
        self.systray.setActionIcon("a2j_stop", "media-playback-stop")
        self.systray.setActionIcon("pulse_start", "media-playback-start")
        self.systray.setActionIcon("pulse_stop", "media-playback-stop")
        self.systray.setActionIcon("quit", "application-exit")

        self.systray.connect("show", self.systray_clicked_callback)
        self.systray.connect("jack_start", self.JackServerStart)
        self.systray.connect("jack_stop", self.JackServerStop)
        self.systray.connect("jack_configure", lambda: jack_configure(self))
        self.systray.connect("a2j_start", self.A2JBridgeStart)
        self.systray.connect("a2j_stop", self.A2JBridgeStop)
        self.systray.connect("a2j_export_hw", self.A2JBridgeExportHW)
        self.systray.connect("pulse_start", self.PABridgeStart)
        self.systray.connect("pulse_stop", self.PABridgeStop)
        self.systray.connect("app_catia", lambda app_="catia": self.func_start_app(app_))
        self.systray.connect("app_claudia", lambda app_="claudia": self.func_start_app(app_))
        self.systray.connect("app_carla", lambda app_="carla": self.func_start_app(app_))
        self.systray.connect("app_logs", lambda app_="jack_logs": self.func_start_app(app_))
        self.systray.connect("app_render", lambda app_="jack_render": self.func_start_app(app_))
        self.systray.connect("app_xy-controller", lambda app_="jack_xycontroller": self.func_start_app(app_))
        self.systray.connect("quit", self.systray_closed)

        if (self.systray.getTrayEngine() == "AppIndicator"):
          self.gtk_timer_helper = self.startTimer(1000)
        else:
          self.gtk_timer_helper = None

        self.systray.show()

        # ALSA Bridge
        if (os.path.exists(HOME+"/.asoundrc")):
          AlsaConf = open(HOME+"/.asoundrc", "r").read()
          if ("slave { pcm \"jack\" }" in AlsaConf):
            self.co_alsa_bridge.setCurrentIndex(1)
          elif ("slave { pcm \"pulse\" }" in AlsaConf) or ("slave { pcm \"pulseaudio\" }" in AlsaConf):
            self.co_alsa_bridge.setCurrentIndex(2)
          else:
            self.co_alsa_bridge.addItem(self.tr("Custom"))
            self.co_alsa_bridge.setCurrentIndex(3)

        # DBus Stuff
        if (haveDBus):
          if (DBus.jack and DBus.jack.IsStarted()):
            self.jackStarted(first_run=True)
          else:
            self.jackStopped()

          if (DBus.a2j):
            if (DBus.a2j.is_started()):
              self.a2jStarted()
            else:
              self.a2jStopped()
          else:
            self.menu_A2J.setEnabled(False)
            self.group_a2j.setEnabled(False)
            self.cb_a2j_autostart.setChecked(False)
            self.l_a2j.setText("A2J is not installed")
            self.settings.setValue("A2J/AutoStart", False)

        else: #No DBus
          self.act_jack_start.setEnabled(False)
          self.act_jack_stop.setEnabled(False)
          self.act_jack_restart.setEnabled(False)
          self.act_jack_configure.setEnabled(False)
          self.b_jack_start.setEnabled(False)
          self.b_jack_stop.setEnabled(False)
          self.b_jack_restart.setEnabled(False)
          self.b_jack_configure.setEnabled(False)
          self.systray.setActionEnabled("jack_start", False)
          self.systray.setActionEnabled("jack_stop", False)
          self.systray.setActionEnabled("jack_configure", False)
          self.menu_A2J.setEnabled(False)
          self.group_a2j.setEnabled(False)

          self.jackStarted(auto_stop=True, first_run=True)

        self.checkPA()

        setJackConnections(self, ["buffer-size"])

        self.connect(self.b_checks_refresh, SIGNAL("clicked()"), self.refreshChecks)

        self.connect(self.act_jack_start, SIGNAL("triggered()"), self.JackServerStart)
        self.connect(self.act_jack_stop, SIGNAL("triggered()"), self.JackServerStop)
        self.connect(self.act_jack_restart, SIGNAL("triggered()"), self.JackServerForceRestart)
        self.connect(self.act_jack_configure, SIGNAL("triggered()"), lambda: jack_configure(self))
        self.connect(self.act_jack_clear_xruns, SIGNAL("triggered()"), self.jack_clear_xruns)
        self.connect(self.b_jack_start, SIGNAL("clicked()"), self.JackServerStart)
        self.connect(self.b_jack_stop, SIGNAL("clicked()"), self.JackServerStop)
        self.connect(self.b_jack_restart, SIGNAL("clicked()"), self.JackServerForceRestart)
        self.connect(self.b_jack_configure, SIGNAL("clicked()"), lambda: jack_configure(self))
        self.connect(self.tb_jack, SIGNAL("clicked()"), self.func_tb_jack)

        self.connect(self.act_a2j_start, SIGNAL("triggered()"), self.A2JBridgeStart)
        self.connect(self.act_a2j_stop, SIGNAL("triggered()"), self.A2JBridgeStop)
        self.connect(self.act_a2j_export_hw, SIGNAL("triggered()"), self.A2JBridgeExportHW)
        self.connect(self.b_a2j_start, SIGNAL("clicked()"), self.A2JBridgeStart)
        self.connect(self.b_a2j_stop, SIGNAL("clicked()"), self.A2JBridgeStop)
        self.connect(self.b_a2j_export_hw, SIGNAL("clicked()"), self.A2JBridgeExportHW)
        self.connect(self.tb_a2j, SIGNAL("clicked()"), self.func_tb_a2j)

        self.connect(self.co_alsa_bridge, SIGNAL("currentIndexChanged(int)"), self.func_changed_alsa_bridge)
        #self.connect(self.tb_alsa, SIGNAL("clicked()"), self.func_tb_alsa)

        self.connect(self.act_pulse_start, SIGNAL("triggered()"), self.PABridgeStart)
        self.connect(self.act_pulse_stop, SIGNAL("triggered()"), self.PABridgeStop)
        self.connect(self.b_pulse_start, SIGNAL("clicked()"), self.PABridgeStart)
        self.connect(self.b_pulse_stop, SIGNAL("clicked()"), self.PABridgeStop)
        self.connect(self.tb_pulse, SIGNAL("clicked()"), self.func_tb_pulse)

        self.connect(self.act_tools_catia, SIGNAL("triggered()"), lambda app_="catia": self.func_start_app(app_))
        self.connect(self.act_tools_claudia, SIGNAL("triggered()"), lambda app_="claudia": self.func_start_app(app_))
        self.connect(self.act_tools_carla, SIGNAL("triggered()"), lambda app_="carla": self.func_start_app(app_))
        self.connect(self.act_tools_logs, SIGNAL("triggered()"), lambda app_="jack_logs": self.func_start_app(app_))
        self.connect(self.act_tools_render, SIGNAL("triggered()"), lambda app_="jack_render": self.func_start_app(app_))
        self.connect(self.act_tools_xycontroller, SIGNAL("triggered()"), lambda app_="jack_xycontroller": self.func_start_app(app_))
        self.connect(self.pic_catia, SIGNAL("clicked()"), lambda app_="catia": self.func_start_app(app_))
        self.connect(self.pic_claudia, SIGNAL("clicked()"), lambda app_="claudia": self.func_start_app(app_))
        self.connect(self.pic_carla, SIGNAL("clicked()"), lambda app_="carla": self.func_start_app(app_))
        self.connect(self.pic_logs, SIGNAL("clicked()"), lambda app_="jack_logs": self.func_start_app(app_))
        self.connect(self.pic_render, SIGNAL("clicked()"), lambda app_="jack_render": self.func_start_app(app_))
        self.connect(self.pic_xycontroller, SIGNAL("clicked()"), lambda app_="jack_xycontroller": self.func_start_app(app_))

        self.connect(self.b_apply_now, SIGNAL("clicked()"), self.func_settings_save_now)

        self.connect(self.b_add_ladspa, SIGNAL("clicked()"), lambda ptype="ladspa": self.func_plugins_add(ptype))
        self.connect(self.b_add_dssi, SIGNAL("clicked()"), lambda ptype="dssi": self.func_plugins_add(ptype))
        self.connect(self.b_add_lv2, SIGNAL("clicked()"), lambda ptype="lv2": self.func_plugins_add(ptype))
        self.connect(self.b_add_vst, SIGNAL("clicked()"), lambda ptype="vst": self.func_plugins_add(ptype))
        self.connect(self.b_remove_ladspa, SIGNAL("clicked()"), lambda ptype="ladspa": self.func_plugins_remove(ptype))
        self.connect(self.b_remove_dssi, SIGNAL("clicked()"), lambda ptype="dssi": self.func_plugins_remove(ptype))
        self.connect(self.b_remove_lv2, SIGNAL("clicked()"), lambda ptype="lv2": self.func_plugins_remove(ptype))
        self.connect(self.b_remove_vst, SIGNAL("clicked()"), lambda ptype="vst": self.func_plugins_remove(ptype))
        self.connect(self.b_edit_ladspa, SIGNAL("clicked()"), lambda ptype="ladspa": self.func_plugins_edit(ptype))
        self.connect(self.b_edit_dssi, SIGNAL("clicked()"), lambda ptype="dssi": self.func_plugins_edit(ptype))
        self.connect(self.b_edit_lv2, SIGNAL("clicked()"), lambda ptype="lv2": self.func_plugins_edit(ptype))
        self.connect(self.b_edit_vst, SIGNAL("clicked()"), lambda ptype="vst": self.func_plugins_edit(ptype))
        self.connect(self.list_LADSPA, SIGNAL("currentRowChanged(int)"), self.func_plugins_changed_LADSPA)
        self.connect(self.list_DSSI, SIGNAL("currentRowChanged(int)"), self.func_plugins_changed_DSSI)
        self.connect(self.list_LV2, SIGNAL("currentRowChanged(int)"), self.func_plugins_changed_LV2)
        self.connect(self.list_VST, SIGNAL("currentRowChanged(int)"), self.func_plugins_changed_VST)

        self.connect(self.ch_image, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.ch_music, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.ch_video, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.ch_text, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.ch_office, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.ch_browser, SIGNAL("clicked()"), self.func_settings_changed_apps)
        self.connect(self.cb_app_image, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_image)
        self.connect(self.cb_app_music, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_music)
        self.connect(self.cb_app_video, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_video)
        self.connect(self.cb_app_text, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_text)
        self.connect(self.cb_app_office, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_office)
        self.connect(self.cb_app_browser, SIGNAL("currentIndexChanged(int)"), self.func_app_changed_browser)

        self.connect(self.cb_wineprefix, SIGNAL("currentIndexChanged(int)"), self.func_settings_changed_wineasio)
        self.connect(self.sb_wineasio_ins, SIGNAL("valueChanged(int)"), self.func_settings_changed_wineasio)
        self.connect(self.sb_wineasio_outs, SIGNAL("valueChanged(int)"), self.func_settings_changed_wineasio)
        self.connect(self.cb_wineasio_hw, SIGNAL("clicked()"), self.func_settings_changed_wineasio)
        self.connect(self.cb_wineasio_autostart, SIGNAL("clicked()"), self.func_settings_changed_wineasio)
        self.connect(self.cb_wineasio_fixed_bsize, SIGNAL("clicked()"), self.func_settings_changed_wineasio)
        self.connect(self.cb_wineasio_bsizes, SIGNAL("currentIndexChanged(int)"), self.func_settings_changed_wineasio)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCadence)

        self.connect(self.timer, SIGNAL("timeout()"), self.refreshJackStatus)

        self.connect(self, SIGNAL("XRunCallback"), self._XRunCallback)
        self.connect(self, SIGNAL("BufferSizeCallback"), self._BufferSizeCallback)
        self.connect(self, SIGNAL("SampleRateCallback"), self._SampleRateCallback)
        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)

        self.refreshJackStatus()

        if (DBus.jack or DBus.a2j):
          DBus.bus.add_signal_receiver(self.DBusSignalReceiver, destination_keyword='dest', path_keyword='path',
                          member_keyword='member', interface_keyword='interface', sender_keyword='sender', )

    def getYesNo(self, yesno):
        if (yesno):
          return self.tr("Yes")
        else:
          return self.tr("No")

    def refreshChecks(self):
        system_check = check_config()

        self.l_check_audiogroup.setText(self.getYesNo(system_check['audio_group']))
        self.l_ico_audiogroup.setPixmap(self.icon_apply if system_check['audio_group'] else self.icon_warning)

        self.l_check_kernel.setText(system_check['kernel_str'].title())

        kernel_f_str = system_check['kernel_version'].replace("2.","")
        if (len(kernel_f_str.split(".")) >= 3):
          kernel_f_str = kernel_f_str.rsplit(".")[0]
        kernel_f = float(kernel_f_str)
        if (system_check['kernel_str'] == "generic"):
          if (kernel_f >= 6.39):
            self.l_ico_kernel.setPixmap(self.icon_warning)
          else:
            self.l_ico_kernel.setPixmap(self.icon_error)
        elif (system_check['kernel_str'] == "preempt"):
          self.l_ico_kernel.setPixmap(self.icon_error)
        elif (system_check['kernel_str'] == "lowlatency"):
          if (kernel_f >= 6.39):
            self.l_ico_kernel.setPixmap(self.icon_apply)
          else:
            self.l_ico_kernel.setPixmap(self.icon_warning)
        elif (system_check['kernel_str'] in ("realtime", "rt")):
          self.l_ico_kernel.setPixmap(self.icon_apply)
        else:
          self.l_ico_kernel.setPixmap(self.icon_warning)

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()
        elif (kwds['interface'] == "org.gna.home.a2jmidid.control"):
          if (kwds['member'] == "bridge_started"):
            self.a2jStarted()
          elif (kwds['member'] == "bridge_stopped"):
            self.a2jStopped()

    def jackStarted(self, auto_stop=False, first_run=False):
        if (haveDBus):
          refreshDBus()

        if (not jack.client):
          jack.client = jacklib.client_open("cadence", jacklib.NullOption, 0)
          if (auto_stop):
            self.jackStopped()
            return

        self.timer.start()
        self.act_tools_render.setEnabled(canRender)
        self.menuJackServer(True)

        if (haveDBus):
          self.act_jack_start.setEnabled(False)
          self.act_jack_stop.setEnabled(True)
          self.act_jack_configure.setEnabled(True)
          self.b_jack_start.setEnabled(False)
          self.b_jack_stop.setEnabled(True)
          self.b_jack_configure.setEnabled(True)
          self.systray.setActionEnabled("jack_start", False)
          self.systray.setActionEnabled("jack_stop", True)
          self.systray.setActionEnabled("jack_configure", True)

        if (jack.client):
          jacklib.set_buffer_size_callback(jack.client, self.JackBufferSizeCallback)
          jacklib.set_sample_rate_callback(jack.client, self.JackSampleRateCallback)
          jacklib.set_xrun_callback(jack.client, self.JackXRunCallback)
          jacklib.on_shutdown(jack.client, self.JackShutdownCallback)
          jacklib.activate(jack.client)

          self.buffer_size = int(jacklib.get_buffer_size(jack.client))
          self.sample_rate = int(jacklib.get_sample_rate(jack.client))
          self.realtime = int(jacklib.is_realtime(jack.client))
          self.xruns = 0

        if (not first_run):
          if (self.settings.value("A2J/AutoStart", True).toBool() and DBus.a2j and not DBus.a2j.is_started()):
            DBus.a2j.start()

          if (havePulseAudio and self.settings.value("Pulse2JACK/AutoStart", True).toBool() and not PA_is_bridged()):
            if (GlobalSettings.value("Pulse2JACK/PlaybackModeOnly", False).toBool()):
              os.system("pulse2jack -p")
            else:
              os.system("pulse2jack")

        self.refreshJackStatus(True)
        self.checkPA()

    def jackStopped(self):
        if (haveDBus):
          refreshDBus()

        if (jack.client):
          # client already closed
          jack.client = None

        self.timer.stop()
        self.act_tools_render.setEnabled(False)
        self.menuJackServer(False)

        if (haveDBus):
          self.act_jack_start.setEnabled(True)
          self.act_jack_stop.setEnabled(False)
          self.act_jack_configure.setEnabled(True)
          self.b_jack_start.setEnabled(True)
          self.b_jack_stop.setEnabled(False)
          self.b_jack_configure.setEnabled(True)
          self.systray.setActionEnabled("jack_start", True)
          self.systray.setActionEnabled("jack_stop", False)
          self.systray.setActionEnabled("jack_configure", True)

        self.buffer_size = 0
        self.sample_rate = 0
        self.xruns = -1

        self.refreshJackStatus(True)
        self.checkPA()

    def a2jStarted(self):
        self.l_a2j.setText(self.tr("A2J is running"))
        self.menuA2JBridge(True)

    def a2jStopped(self):
        self.l_a2j.setText(self.tr("A2J is stopped"))
        self.menuA2JBridge(False)

    def checkPA(self):
        b_start = False
        b_stop = False

        if (havePulseAudio):
          if (PA_is_started()):
            if (PA_is_bridged()):
              self.l_pulse.setText(self.tr("PulseAudio is started and bridged to JACK"))
              b_start = False
              b_stop = True
            else:
              self.l_pulse.setText(self.tr("PulseAudio is started but not bridged"))
              b_start = True
              b_stop = False

          else:
            self.l_pulse.setText(self.tr("PulseAudio is not started"))

            if (jack.client):
              b_start = True
              b_stop = False
            else:
              b_start = False
              b_stop = False

          self.act_pulse_start.setEnabled(b_start)
          self.act_pulse_stop.setEnabled(b_stop)
          self.b_pulse_start.setEnabled(b_start)
          self.b_pulse_stop.setEnabled(b_stop)
          self.systray.setActionEnabled("pulse_start", b_start)
          self.systray.setActionEnabled("pulse_stop", b_stop)

        else:
          self.l_pulse.setText(self.tr("PulseAudio is not installed"))
          self.group_pulse.setEnabled(False)
          self.menu_Pulse.setEnabled(False)
          self.systray.setActionEnabled("pulse_start", False)
          self.systray.setActionEnabled("pulse_stop", False)

    def JackServerStart(self):
        if (DBus.jack):
          try:
            DBus.jack.StartServer()
          except:
            QMessageBox.critical(self, self.tr("Error"),
                  self.tr("Failed to Start JACK, please check the JACK settings.\nIf this keeps happening, try a force restart."))

    def JackServerStop(self):
        if (DBus.jack):
          try:
            DBus.jack.StopServer()
          except:
            QMessageBox.critical(self, self.tr("Error"),
                  self.tr("Failed to Stop JACK."))

    def JackServerForceRestart(self):
        if (jack.client):
          ask_r = CustomMessageBox(self, QMessageBox.Warning, self.tr("Warning"),
                  self.tr("This will force kill all JACK applications!<br>Make sure to save your projects before continue."),
                  self.tr("Are you sure you want to force the restart of JACK?"))
          if (ask_r != QMessageBox.Yes):
            return

        self.timer.stop()

        self.wait_dialog = ForceWaitDialog(self)
        self.animation = QPropertyAnimation(self.wait_dialog.progressBar, "value")
        self.animation.setDuration(5500)
        self.animation.setStartValue(0)
        self.animation.setEndValue(100)
        self.animation.start()

        fr_thread = ForceRestartThread()
        fr_thread.start()
        self.connect(fr_thread, SIGNAL("finished()"), self.JackServerForceRestart_Finished)
        self.wait_dialog.exec_()

    def JackServerForceRestart_Finished(self):
        self.wait_dialog.close()

        if (self.sender().wasJackStarted()):
          if (self.settings.value("A2J/AutoStart", True).toBool() and DBus.a2j and not DBus.a2j.is_started()):
            DBus.a2j.start()

          if (self.settings.value("Pulse2JACK/AutoStart", True).toBool() and not PA_is_bridged()):
            if (GlobalSettings.value("Pulse2JACK/PlaybackModeOnly", False).toBool()):
              os.system("pulse2jack -p")
            else:
              os.system("pulse2jack")

          QMessageBox.information(self, self.tr("Info"), self.tr("JACK was re-started sucessfully"))
        else:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not start JACK!"))

        self.refreshJackStatus()
        self.timer.start()

    def A2JBridgeStart(self):
        DBus.a2j.start()

    def A2JBridgeStop(self):
        DBus.a2j.stop()

    def A2JBridgeExportHW(self):
        ask = QMessageBox.question(self, self.tr("A2J Hardware Export"), self.tr("Enable Hardware Export on the A2J Bridge?"),
            QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel, QMessageBox.No)
        if (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(True)
        elif (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(False)

    def PABridgeStart(self):
        if (jack.client):
          port_list = jacklib.get_ports(jack.client, "", "", 0)
          if ("system:capture_2" in port_list):
            os.system("pulse2jack")
          else:
            os.system("pulse2jack -p")
        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Cannot start Pulse-JACK bridge while JACK is stopped"))

        QTimer.singleShot(250, self.checkPA)

    def PABridgeStop(self):
        os.system("killall -KILL pulseaudio")
        QTimer.singleShot(250, self.checkPA)

    def menuJackServer(self, started):
        if (DBus.jack):
          self.act_jack_start.setEnabled(not started)
          self.act_jack_stop.setEnabled(started)
          self.b_jack_start.setEnabled(not started)
          self.b_jack_stop.setEnabled(started)
          self.systray.setActionEnabled("jack_start", not started)
          self.systray.setActionEnabled("jack_stop", started)
          self.menuA2JBridge(False)

    def menuA2JBridge(self, started):
        if (DBus.jack and DBus.jack.IsStarted()):
          self.act_a2j_start.setEnabled(not started)
          self.act_a2j_stop.setEnabled(started)
          self.act_a2j_export_hw.setEnabled(not started)
          self.b_a2j_start.setEnabled(not started)
          self.b_a2j_stop.setEnabled(started)
          self.b_a2j_export_hw.setEnabled(not started)
          self.systray.setActionEnabled("a2j_start", not started)
          self.systray.setActionEnabled("a2j_stop", started)
          self.systray.setActionEnabled("a2j_export_hw", not started)
        else:
          self.act_a2j_start.setEnabled(False)
          self.act_a2j_stop.setEnabled(False)
          self.b_a2j_start.setEnabled(False)
          self.b_a2j_stop.setEnabled(False)
          self.systray.setActionEnabled("a2j_start", False)
          self.systray.setActionEnabled("a2j_stop", False)
          self.systray.setActionEnabled("a2j_export_hw", False)

    def setBufferSize(self, buffer_size):
        if (buffer_size):
          for i in range(len(self.act_jack_bf_list)):
            act_i = QStringStr(self.act_jack_bf_list[i].text().replace("&",""))
            self.act_jack_bf_list[i].setEnabled(True)

            if (act_i == str(buffer_size)):
              #if (not self.act_jack_bf_list[i].isChecked()):
                self.act_jack_bf_list[i].setChecked(True)

            else:
              if (self.act_jack_bf_list[i].isChecked()):
                self.act_jack_bf_list[i].setChecked(False)

          latency = float(buffer_size)/self.sample_rate*1000
          if (latency < 10):
            latD = 3
          elif (latency < 100):
            latD = 4
          elif (latency < 1000):
            latD = 5
          elif (latency < 10000):
            latD = 6
          str_latency = str(latency)[0:latD]

          str_latency += " " + self.tr("ms")
          str_buffersize = str(buffer_size) + " " + self.tr("Samples")

        else:
          for i in range(len(self.act_jack_bf_list)):
            act_i = QStringStr(self.act_jack_bf_list[i].text().replace("&",""))
            self.act_jack_bf_list[i].setEnabled(False)

            if (self.act_jack_bf_list[i].isChecked()):
              self.act_jack_bf_list[i].setChecked(False)

          str_latency = "---"
          str_buffersize = "---"

        self.buffer_size = buffer_size
        self.l_latency.setText(str_latency)
        self.l_buffersize.setText(str_buffersize)

    def setSampleRate(self, sample_rate):
        if (sample_rate):
          str_sample_rate = str(sample_rate) + " " + self.tr("Hz")
        else:
          str_sample_rate = "---"

        self.sample_rate = sample_rate
        self.l_samplerate.setText(str_sample_rate)

    def setRealtime(self, realtime):
        if (realtime):
          self.l_realtime.setText(self.tr("Yes"))
          self.l_ico_realtime.setPixmap(self.icon_apply)
        else:
          self.l_realtime.setText(self.tr("No"))
          self.l_ico_realtime.setPixmap(self.icon_cancel)

    def setXruns(self, xruns):
        if (xruns >= 0):
          str_xruns = str(xruns)
        else:
          str_xruns = "---"

        self.l_xruns.setText(str_xruns)

    def refreshJackStatus(self, force_check=False):
        if (force_check):
          if (jack.client):
            self.l_jackserver.setText(self.tr("Running"))
            self.l_ico_jackserver.setPixmap(self.icon_apply)
            self.buffer_size = int(jacklib.get_buffer_size(jack.client))
            self.sample_rate = int(jacklib.get_sample_rate(jack.client))
            self.realtime = bool(jacklib.is_realtime(jack.client))
          else:
            self.l_jackserver.setText(self.tr("Stopped"))
            self.l_ico_jackserver.setPixmap(self.icon_cancel)
            self.buffer_size = 0
            self.sample_rate = 0
            self.xruns = -1

          if (DBus.jack and bool(DBus.jack.IsStarted())):
            if (jack.client):
              self.l_jack.setText(self.tr("JACK is Running"))
            else:
              self.l_jack.setText(self.tr("JACK is Sick"))
          else:
            self.l_jack.setText(self.tr("JACK is Stopped"))

          self.setBufferSize(self.buffer_size)
          self.setSampleRate(self.sample_rate)
          self.setRealtime(self.realtime)
          self.setXruns(self.xruns)

        if (jack.client):
          str_dsp_load = str(jacklib.cpu_load(jack.client))[0:4] + "%"
        else:
          str_dsp_load = "---"

        self.l_dspload.setText(str_dsp_load)

        systrayText = QStringStr(self.tr("<table>"
                       #"<tr><td align='center' colspan='2'><h4>Cadence</h4></td></tr>"
                       "<tr><td align='right'>JACK Status:</td><td>%1</td></tr>"
                       "<tr><td align='right'>Realtime:</td><td>%2</td></tr>"
                       "<tr><td align='right'>DSP Load:</td><td>%3</td></tr>"
                       "<tr><td align='right'>Xruns:</td><td>%4</td></tr>"
                       "<tr><td align='right'>Buffer Size:</td><td>%5</td></tr>"
                       "<tr><td align='right'>Sample Rate:</td><td>%6</td></tr>"
                       "<tr><td align='right'>Latency:</td><td>%7</td></tr>"
                       "</table>").arg(self.l_jackserver.text()).arg(
                       self.l_realtime.text()).arg(self.l_dspload.text()).arg(self.l_xruns.text()).arg(
                       self.l_buffersize.text()).arg(self.l_samplerate.text()).arg(self.l_latency.text()))
        self.systray.setToolTip(systrayText)

    def jack_clear_xruns(self):
        self.xruns = 0
        self.setXruns(self.xruns)

    def func_tb_jack(self):
        ToolBarJackDialog(self).exec_()

    def func_tb_a2j(self):
        ToolBarA2JDialog(self).exec_()

    def func_tb_pulse(self):
        ToolBarPADialog(self).exec_()

    def func_changed_alsa_bridge(self, index):
        if (index > 0):
          if (index == 1):
            content = AlsaJackFile
          elif (index == 2):
            content = AlsaPulseAudioFile
          else:
            return

          fileW = open(HOME+"/.asoundrc", "w")
          fileW.write(content)
          fileW.close()

        else:
          if (os.path.exists(HOME+"/.asoundrc")):
            os.remove(HOME+"/.asoundrc")

    def func_start_app(self, app_):
        os.system(app_+" &")

    def func_settings_changed(self, stype):
        self.settings_changed_types += [stype]
        self.frame_settings.setVisible(True)

    def func_settings_changed_apps(self):
        self.func_settings_changed("apps")

    def func_settings_changed_wineasio(self):
        self.func_settings_changed("wineasio")

    def func_settings_save_now(self):
        for stype in self.settings_changed_types:
          if (stype == "plugins"):
            EXTRA_LADSPA_DIRS = ""
            EXTRA_DSSI_DIRS = ""
            EXTRA_LV2_DIRS = ""
            EXTRA_VST_DIRS = ""

            for i in range(self.list_LADSPA.count()):
              ppath = self.list_LADSPA.item(i).text()
              if (not ppath in DEFAULT_LADSPA_PATH):
                if (EXTRA_LADSPA_DIRS != ""):
                  EXTRA_LADSPA_DIRS += ":"
                EXTRA_LADSPA_DIRS += ppath

            for i in range(self.list_DSSI.count()):
              ppath = self.list_DSSI.item(i).text()
              if (not ppath in DEFAULT_DSSI_PATH):
                if (EXTRA_DSSI_DIRS != ""):
                  EXTRA_DSSI_DIRS += ":"
                EXTRA_DSSI_DIRS += ppath

            for i in range(self.list_LV2.count()):
              ppath = self.list_LV2.item(i).text()
              if (not ppath in DEFAULT_LV2_PATH):
                if (EXTRA_LV2_DIRS != ""):
                  EXTRA_LV2_DIRS += ":"
                EXTRA_LV2_DIRS += ppath

            for i in range(self.list_VST.count()):
              ppath = self.list_VST.item(i).text()
              if (not ppath in DEFAULT_VST_PATH):
                if (EXTRA_VST_DIRS != ""):
                  EXTRA_VST_DIRS += ":"
                EXTRA_VST_DIRS += ppath

            GlobalSettings.setValue("AudioPlugins/EXTRA_LADSPA_PATH", EXTRA_LADSPA_DIRS)
            GlobalSettings.setValue("AudioPlugins/EXTRA_DSSI_PATH", EXTRA_DSSI_DIRS)
            GlobalSettings.setValue("AudioPlugins/EXTRA_LV2_PATH", EXTRA_LV2_DIRS)
            GlobalSettings.setValue("AudioPlugins/EXTRA_VST_PATH", EXTRA_VST_DIRS)

          elif (stype == "apps"):
            MIMEFILE = ""
            REAL_MIMEFILE = ""

            MIMEFILE += "application/x-designer=designer-qt4.desktop;\n"
            MIMEFILE += "application/x-ms-dos-executable=wine.desktop;\n"
            MIMEFILE += "audio/x-minipsf=audacious.desktop;\n"
            MIMEFILE += "audio/x-psf=audacious.desktop;\n"

            if (self.ch_image.isChecked()):
              MIMEFILE += "image/bmp=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/gif=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/jp2=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/jpeg=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/png=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/svg+xml=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/svg+xml-compressed=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/tiff=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-canon-cr2=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-canon-crw=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-eps=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-kodak-dcr=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-kodak-k25=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-kodak-kdc=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-nikon-nef=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-olympus-orf=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-panasonic-raw=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-pcx=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-pentax-pef=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-portable-anymap=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-portable-bitmap=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-portable-graymap=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-portable-pixmap=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-sony-arw=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-sony-sr2=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-sony-srf=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-tga=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-xbitmap=X-IMAGE-X.desktop;\n"
              MIMEFILE += "image/x-xpixmap=X-IMAGE-X.desktop;\n"
              MIMEFILE = MIMEFILE.replace("X-IMAGE-X.desktop", QStringStr(self.cb_app_image.currentText()).replace("/","-"))

            if (self.ch_music.isChecked()):
              MIMEFILE += "application/vnd.apple.mpegurl=X-MUSIC-X.desktop;\n"
              MIMEFILE += "application/xspf+xml=X-MUSIC-X.desktop;\n"
              MIMEFILE += "application/x-smaf=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/AMR=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/AMR-WB=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/aac=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/ac3=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/basic=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/flac=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/m3u=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/mp2=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/mp4=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/mpeg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/ogg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/vnd.rn-realaudio=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/vorbis=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/webm=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/wav=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-adpcm=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-aifc=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-aiff=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-aiffc=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-ape=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-cda=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-flac=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-flac+ogg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-gsm=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-m4b=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-matroska=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-mp2=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-mpegurl=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-ms-asx=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-ms-wma=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-musepack=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-ogg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-oggflac=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-pn-realaudio-plugin=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-riff=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-scpls=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-speex=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-speex+ogg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-tta=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-vorbis+ogg=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-wav=X-MUSIC-X.desktop;\n"
              MIMEFILE += "audio/x-wavpack=X-MUSIC-X.desktop;\n"
              MIMEFILE = MIMEFILE.replace("X-MUSIC-X.desktop", QStringStr(self.cb_app_music.currentText()).replace("/","-"))

            if (self.ch_video.isChecked()):
              MIMEFILE += "application/mxf=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/ram=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-asf=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-wpl=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/vnd.rn-realmedia=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/x-ms-wmp=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/x-ms-wms=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/x-netshow-channel=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/x-ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "application/x-quicktime-media-link=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/3gpp=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/3gpp2=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/divx=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/dv=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/flv=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/mp2t=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/mp4=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/mpeg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/quicktime=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/vivo=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/vnd.rn-realvideo=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/webm=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-anim=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-flic=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-flv=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-m4v=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-matroska=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ms-asf=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ms-wm=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ms-wmp=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ms-wmv=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ms-wvx=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-msvideo=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-nsv=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ogm=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-ogm+ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-theora=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-theora+ogg=X-VIDEO-X.desktop;\n"
              MIMEFILE += "video/x-wmv=X-VIDEO-X.desktop;\n"
              MIMEFILE = MIMEFILE.replace("X-VIDEO-X.desktop", QStringStr(self.cb_app_video.currentText()).replace("/","-"))

            if (self.ch_text.isChecked()):
              # TODO - more mimetypes
              MIMEFILE += "application/rdf+xml=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/xml=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/xml-dtd=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/xml-external-parsed-entity=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/xsd=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/xslt+xml=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/x-trash=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/x-wine-extension-inf=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/x-wine-extension-ini=X-TEXT-X.desktop;\n"
              MIMEFILE += "application/x-zerosize=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/css=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/plain=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-authors=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-c++-hdr=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-c++-src=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-changelog=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-chdr=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-cmake=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-copying=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-credits=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-csharp=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-csrc=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-install=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-log=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-lua=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-makefile=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-ms-regedit=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-nfo=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-objchdr=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-objcsrc=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-pascal=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-patch=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-python=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-readme=X-TEXT-X.desktop;\n"
              MIMEFILE += "text/x-vhdl=X-TEXT-X.desktop;\n"
              MIMEFILE = MIMEFILE.replace("X-TEXT-X.desktop", QStringStr(self.cb_app_text.currentText()).replace("/","-"))

            if (self.ch_office.isChecked()):
              MIMEFILE += "application/msword=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/msword-template=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/rtf=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.lotus-1-2-3=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.msaccess=X-OFFDB-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-excel=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-powerpoint=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-word=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.ms-works=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.chart=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.chart-template=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.database=X-OFFDB-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.formula=X-OFFMATH-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.formula-template=X-OFFMATH-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.graphics=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.graphics-flat-xml=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.graphics-xml=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.presentation=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.presentation-flat-xml=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.presentation-template=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.spreadsheet=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.spreadsheet-flat-xml=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.spreadsheet-template=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.text=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.text-flat-xml=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.text-master=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.text-template=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.oasis.opendocument.text-web=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.presentationml.presentation=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.presentationml.slideshow=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.presentationml.template=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.spreadsheetml.template=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.wordprocessingml.document=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.openxmlformats-officedocument.wordprocessingml.template=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.calc=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.chart=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.draw=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.impress=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.math=X-OFFMATH-X.desktop;\n"
              MIMEFILE += "application/vnd.stardivison.writer=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.base=X-OFFDB-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.calc=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.calc.template=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.draw=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.draw.template=X-OFFDRAW-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.impress=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.impress.template=X-OFFPRES-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.math=X-OFFMATH-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.writer=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.writer.global=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.sun.xml.writer.template=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/vnd.wordperfect=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/x-abiword=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "application/x-mswrite=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "text/richtext=X-OFFWORD-X.desktop;\n"
              MIMEFILE += "text/spreadsheet=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "text/tab-separated-values=X-OFFCALC-X.desktop;\n"
              MIMEFILE += "text/vnd.rn-realtext=X-OFFWORD-X.desktop;\n"
              #MIMEFILE = MIMEFILE.replace("X--X.desktop", QStringStr(self.cb_app_text.currentText()).replace("/","-"))

            if (self.ch_browser.isChecked()):
              MIMEFILE += "application/atom+xml=X-WEB-X.desktop;\n"
              MIMEFILE += "application/rss+xml=X-WEB-X.desktop;\n"
              MIMEFILE += "application/vnd.mozilla.xul+xml=X-WEB-X.desktop;\n"
              MIMEFILE += "application/x-mozilla-bookmarks=X-WEB-X.desktop;\n"
              MIMEFILE += "application/x-mswinurl=X-WEB-X.desktop;\n"
              MIMEFILE += "application/x-xbel=X-WEB-X.desktop;\n"
              MIMEFILE += "application/xhtml+xml=X-WEB-X.desktop;\n"
              MIMEFILE += "text/html=X-WEB-X.desktop;\n"
              MIMEFILE += "text/opml+xml=X-WEB-X.desktop;\n"
              MIMEFILE = MIMEFILE.replace("X-WEB-X.desktop", QStringStr(self.cb_app_browser.currentText()).replace("/","-"))
              # TODO - needs something else for default browser...

            local_xdg_mimeapps = os.path.join(HOME, ".local", "share", "applications", "mimeapps.list")
            local_xdg_defaults = os.path.join(HOME, ".local", "share", "applications", "defaults.list")

            REAL_MIMEFILE += "[Default Applications]\n"
            REAL_MIMEFILE += MIMEFILE
            REAL_MIMEFILE += "\n"
            REAL_MIMEFILE += "[Added Associations]\n"
            REAL_MIMEFILE += MIMEFILE
            REAL_MIMEFILE += "\n"

            write_file = open(local_xdg_mimeapps, "w")
            write_file.write(REAL_MIMEFILE)
            write_file.close()

            write_file = open(local_xdg_defaults, "w")
            write_file.write(REAL_MIMEFILE)
            write_file.close()

          elif (stype == "wineasio"):
            REGFILE = 'REGEDIT4\n\n[HKEY_CURRENT_USER\Software\Wine\WineASIO]\n'
            REGFILE += '"Autostart server"=dword:0000000%i\n' % (1 if (self.cb_wineasio_autostart.isChecked()) else 0)
            REGFILE += '"Connect to hardware"=dword:0000000%i\n' % (1 if (self.cb_wineasio_hw.isChecked()) else 0)
            REGFILE += '"Fixed buffersize"=dword:0000000%i\n' % (1 if (self.cb_wineasio_fixed_bsize.isChecked()) else 0)
            REGFILE += '"Number of inputs"=dword:000000%s\n' % (smartHex(self.sb_wineasio_ins.value(), 2))
            REGFILE += '"Number of outputs"=dword:000000%s\n' % (smartHex(self.sb_wineasio_outs.value(), 2))
            REGFILE += '"Preferred buffersize"=dword:0000%s\n' % (smartHex(self.cb_wineasio_bsizes.currentText().toInt()[0], 4))

            write_file = open("/tmp/cadence-wineasio.reg", "w")
            write_file.write(REGFILE)
            write_file.close()

            os.system("regedit /tmp/cadence-wineasio.reg")

        self.settings_changed_types = []
        self.frame_settings.setVisible(False)

    def func_plugins_add(self, ptype):
        new_path = QFileDialog.getExistingDirectory(self, self.tr("Add Path"), "", QFileDialog.ShowDirsOnly)
        if (new_path.isEmpty() or not os.path.exists(QStringStr(new_path))):
          return

        if (ptype == "ladspa"):
          self.list_LADSPA.addItem(new_path)
        elif (ptype == "dssi"):
          self.list_DSSI.addItem(new_path)
        elif (ptype == "lv2"):
          self.list_LV2.addItem(new_path)
        elif (ptype == "vst"):
          self.list_VST.addItem(new_path)

        self.func_settings_changed("plugins")

    def func_plugins_remove(self, ptype):
        if (ptype == "ladspa"):
          self.list_LADSPA.takeItem(self.list_LADSPA.currentRow())
        elif (ptype == "dssi"):
          self.list_DSSI.takeItem(self.list_DSSI.currentRow())
        elif (ptype == "lv2"):
          self.list_LV2.takeItem(self.list_LV2.currentRow())
        elif (ptype == "vst"):
          self.list_VST.takeItem(self.list_VST.currentRow())

        self.func_settings_changed("plugins")

    def func_plugins_edit(self, ptype):
        if (ptype == "ladspa"):
          cur_path = self.list_LADSPA.item(self.list_LADSPA.currentRow()).text()
        elif (ptype == "dssi"):
          cur_path = self.list_DSSI.item(self.list_DSSI.currentRow()).text()
        elif (ptype == "lv2"):
          cur_path = self.list_LV2.item(self.list_LV2.currentRow()).text()
        elif (ptype == "vst"):
          cur_path = self.list_VST.item(self.list_VST.currentRow()).text()

        new_path = QFileDialog.getExistingDirectory(self, self.tr("Edit Path"), cur_path, QFileDialog.ShowDirsOnly)

        if (new_path.isEmpty() or new_path == cur_path or not os.path.exists(QStringStr(new_path))):
          return

        if (ptype == "ladspa"):
          self.list_LADSPA.item(self.list_LADSPA.currentRow()).setText(new_path)
        elif (ptype == "dssi"):
          self.list_DSSI.item(self.list_DSSI.currentRow()).setText(new_path)
        elif (ptype == "lv2"):
          self.list_LV2.item(self.list_LV2.currentRow()).setText(new_path)
        elif (ptype == "vst"):
          self.list_VST.item(self.list_VST.currentRow()).setText(new_path)

        self.func_settings_changed("plugins")

    def func_plugins_changed_LADSPA(self, index):
        non_removable = (index >= 0 and self.list_LADSPA.item(index).text() not in DEFAULT_LADSPA_PATH)
        self.b_remove_ladspa.setEnabled(non_removable)
        self.b_edit_ladspa.setEnabled(non_removable)

    def func_plugins_changed_DSSI(self, index):
        non_removable = (index >= 0 and self.list_DSSI.item(index).text() not in DEFAULT_DSSI_PATH)
        self.b_remove_dssi.setEnabled(non_removable)
        self.b_edit_dssi.setEnabled(non_removable)

    def func_plugins_changed_LV2(self, index):
        non_removable = (index >= 0 and self.list_LV2.item(index).text() not in DEFAULT_LV2_PATH)
        self.b_remove_lv2.setEnabled(non_removable)
        self.b_edit_lv2.setEnabled(non_removable)

    def func_plugins_changed_VST(self, index):
        non_removable = (index >= 0 and self.list_VST.item(index).text() not in DEFAULT_VST_PATH)
        self.b_remove_vst.setEnabled(non_removable)
        self.b_edit_vst.setEnabled(non_removable)

    def func_app_changed_image(self, index):
        desktop = QStringStr(self.cb_app_image.itemText(index))
        self.func_settings_changed("apps")

    def func_app_changed_music(self, index):
        desktop = QStringStr(self.cb_app_music.itemText(index))
        self.func_settings_changed("apps")

    def func_app_changed_video(self, index):
        desktop = QStringStr(self.cb_app_video.itemText(index))
        self.func_settings_changed("apps")

    def func_app_changed_text(self, index):
        desktop = QStringStr(self.cb_app_text.itemText(index))
        self.func_settings_changed("apps")

    def func_app_changed_office(self, index):
        # TODO
        self.func_settings_changed("apps")

    def func_app_changed_browser(self, index):
        desktop = QStringStr(self.cb_app_browser.itemText(index))
        self.func_settings_changed("apps")

    def JackBufferSizeCallback(self, buffer_size, arg=None):
        if (DEBUG): print "JackBufferSizeCallback", buffer_size
        self.emit(SIGNAL("BufferSizeCallback"), buffer_size)
        return 0

    def JackSampleRateCallback(self, sample_rate, arg=None):
        if (DEBUG): print "JackSampleRateCallback", sample_rate
        self.emit(SIGNAL("SampleRateCallback"), sample_rate)
        return 0

    def JackXRunCallback(self, arg=None):
        if (DEBUG): print "JackXRunCallback", self.xruns+1
        self.emit(SIGNAL("XRunCallback"))
        return 0

    def JackShutdownCallback(self, arg=None):
        if (DEBUG): print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _BufferSizeCallback(self, buffer_size):
        print "jack bufsize callback", buffer_size
        self.setBufferSize(buffer_size)

    def _SampleRateCallback(self, sample_rate):
        self.setSampleRate(sample_rate)

    def _XRunCallback(self):
        self.xruns += 1
        self.setXruns(self.xruns)

    def _ShutdownCallback(self):
        if (jack.client):
          self.jackStopped()

    def aboutCadence(self):
        QMessageBox.about(self, self.tr("About Cadence"), self.tr("<h3>Cadence</h3>"
            "<br>Version %1"
            "<br>Cadence is a set of tools useful for Audio-related tasks, using Python and Qt.<br>"
            "<br>Copyright (C) 2010-2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))

        GlobalSettings.setValue("JACK/AutoStart", self.cb_jack_autostart.isChecked())
        GlobalSettings.setValue("A2J/AutoStart", self.cb_a2j_autostart.isChecked())
        GlobalSettings.setValue("Pulse2JACK/AutoStart", (havePulseAudio and self.cb_pulse_autostart.isChecked()))

        #self.settings.setValue("FirstRun", False)

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

        self.cb_jack_autostart.setChecked(GlobalSettings.value("JACK/AutoStart", True).toBool())
        self.cb_a2j_autostart.setChecked(GlobalSettings.value("A2J/AutoStart", True).toBool())
        self.cb_pulse_autostart.setChecked(GlobalSettings.value("Pulse2JACK/AutoStart", havePulseAudio).toBool())

    def systray_closed(self):
        self.hide()
        self.close()

    def systray_clicked_callback(self):
        if (self.isVisible()):
          self.systray.setActionText("show", QStringStr(self.tr("Restore")))
          self.hide()
        else:
          self.systray.setActionText("show", QStringStr(self.tr("Minimize")))
          showWindow(self)

    def timerEvent(self, event):
        if (self.gtk_timer_helper and event.timerId() == self.gtk_timer_helper):
          self.repaint()
        QMainWindow.timerEvent(self, event)

    def closeEvent(self, event):
        self.saveSettings()
        if (self.systray.isTrayAvailable() and self.isVisible()):
          self.hide()
          self.systray.setActionText("show", QStringStr(gui.tr("Restore")))
          event.ignore()
          return
        self.systray.close()
        QMainWindow.closeEvent(self, event)


#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Cadence")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/svg/cadence.svg"))

    # Show GUI
    gui = CadenceMainW()

    if ("--minimized" in app.arguments()):
      gui.hide()
      gui.systray.setActionText("show", QStringStr(gui.tr("Restore")))
    else:
      gui.show()

    # Set-up custom signal handling
    set_up_signals(gui)

    # App-Loop
    ret = gui.systray.exec_(app)

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
