#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os
from PyQt4.QtCore import Qt, QFile, QIODevice, QTextStream, QTimer, SIGNAL
from PyQt4.QtGui import QDialog, QIcon, QPalette, QSyntaxHighlighter

# Imports (Custom Stuff)
import icons_rc, ui_logs

# Get Icon from user theme, using our own as backup (Oxygen)
def getIcon(icon, size=16):
    return QIcon.fromTheme(icon, QIcon(":/%ix%i/%s.png" % (size, size, icon)))

# JACK Syntax Highlighter
class SyntaxHighligher_JACK(QSyntaxHighlighter):
    def __init__(self, parent):
        super(SyntaxHighligher_JACK, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if (": ERROR: " in text):
        self.setFormat(text.indexOf(" ERROR: "), text.count(), Qt.red)
      elif (": WARNING: " in text):
        self.setFormat(text.indexOf(" WARNING: "), text.count(), Qt.darkRed)
      elif (": ------------------" in text):
        self.setFormat(text.indexOf(" ------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))
      elif (": Connecting " in text):
        self.setFormat(text.indexOf(" Connecting "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))
      elif (": Disconnecting " in text):
        self.setFormat(text.indexOf(" Disconnecting "), text.count(), self.palette.color(QPalette.Active, QPalette.LinkVisited))
      elif (": New client " in text):
        self.setFormat(text.indexOf(" New client "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))

# A2J Syntax Highlighter
class SyntaxHighligher_A2J(QSyntaxHighlighter):
    def __init__(self, parent):
        super(SyntaxHighligher_A2J, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if (": error: " in text):
        self.setFormat(text.indexOf(" error: "), text.count(), Qt.red)
      elif (": WARNING: " in text):
        self.setFormat(text.indexOf(" WARNING: "), text.count(), Qt.darkRed)
      elif (": ----------------------------" in text):
        self.setFormat(text.indexOf("----------------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))
      elif (": port created: " in text):
        self.setFormat(text.indexOf(" port created: "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))
      elif (": port deleted: " in text):
        self.setFormat(text.indexOf(" port deleted: "), text.count(), self.palette.color(QPalette.Active, QPalette.LinkVisited))

# LASH Syntax Highlighter
class SyntaxHighligher_LASH(QSyntaxHighlighter):
    def __init__(self, parent):
        super(SyntaxHighligher_LASH, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if (": ERROR: " in text):
        self.setFormat(text.indexOf(" ERROR: "), text.count(), Qt.red)
      elif (": WARNING: " in text):
        self.setFormat(text.indexOf(" WARNING: "), text.count(), Qt.darkRed)
      elif (": ------------------" in text):
        self.setFormat(text.indexOf(" ------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))

# LADISH Syntax Highlighter
class SyntaxHighligher_LADISH(QSyntaxHighlighter):
    def __init__(self, parent):
        super(SyntaxHighligher_LADISH, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if (": ERROR: " in text):
        self.setFormat(text.indexOf(" ERROR: "), text.count(), Qt.red)
      elif (": WARNING: " in text):
        self.setFormat(text.indexOf(" WARNING: "), text.count(), Qt.darkRed)
      elif (": -------" in text):
        self.setFormat(text.indexOf(" -------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))

# Render Window
class LogsW(QDialog, ui_logs.Ui_LogsW):

    HOME = os.getenv("HOME")
    LOG_PATH = os.path.join(HOME, ".log")

    LOG_FILE_JACK   = os.path.join(LOG_PATH, "jack", "jackdbus.log")
    LOG_FILE_A2J    = os.path.join(LOG_PATH, "a2j", "a2j.log")
    LOG_FILE_LASH   = os.path.join(LOG_PATH, "lash", "lash.log")
    LOG_FILE_LADISH = os.path.join(LOG_PATH, "ladish", "ladish.log")

    def __init__(self, parent=None):
        super(LogsW, self).__init__(parent)
        self.setupUi(self)

        self.b_close.setIcon(getIcon("dialog-close"))
        self.b_purge.setIcon(getIcon("user-trash"))

        self.logTimer = self.startTimer(1000)

        self.connect(self.b_purge, SIGNAL("clicked()"), self.purgeLogs)

        tab_index = 0

        if (not os.path.exists(self.LOG_FILE_JACK)):
          self.LOG_FILE_JACK = None
          self.tabWidget.removeTab(0-tab_index)
          tab_index += 1

        if (not os.path.exists(self.LOG_FILE_A2J)):
          self.LOG_FILE_A2J = None
          self.tabWidget.removeTab(1-tab_index)
          tab_index += 1

        if (not os.path.exists(self.LOG_FILE_LASH)):
          self.LOG_FILE_LASH = None
          self.tabWidget.removeTab(2-tab_index)
          tab_index += 1

        if (not os.path.exists(self.LOG_FILE_LADISH)):
          self.LOG_FILE_LADISH = None
          self.tabWidget.removeTab(3-tab_index)
          tab_index += 1

        QTimer.singleShot(100, self.initApp)

    def initApp(self):
        if (self.LOG_FILE_JACK):
          self.syntax_jack = SyntaxHighligher_JACK(self.pte_jack)
          self.syntax_jack.setDocument(self.pte_jack.document())
          self.log_jack_file = QFile(self.LOG_FILE_JACK)
          self.log_jack_file.open(QIODevice.ReadOnly)
          self.log_jack_stream = QTextStream(self.log_jack_file)
          self.log_jack_stream.setCodec("UTF-8")

        if (self.LOG_FILE_A2J):
          self.syntax_a2j = SyntaxHighligher_A2J(self.pte_a2j)
          self.syntax_a2j.setDocument(self.pte_a2j.document())
          self.log_a2j_file = QFile(self.LOG_FILE_A2J)
          self.log_a2j_file.open(QIODevice.ReadOnly)
          self.log_a2j_stream = QTextStream(self.log_a2j_file)
          self.log_a2j_stream.setCodec("UTF-8")

        if (self.LOG_FILE_LASH):
          self.syntax_lash = SyntaxHighligher_LASH(self.pte_lash)
          self.syntax_lash.setDocument(self.pte_lash.document())
          self.log_lash_file = QFile(self.LOG_FILE_LASH)
          self.log_lash_file.open(QIODevice.ReadOnly)
          self.log_lash_stream = QTextStream(self.log_lash_file)
          self.log_lash_stream.setCodec("UTF-8")

        if (self.LOG_FILE_LADISH):
          self.syntax_ladish = SyntaxHighligher_LADISH(self.pte_ladish)
          self.syntax_ladish.setDocument(self.pte_ladish.document())
          self.log_ladish_file = QFile(self.LOG_FILE_LADISH)
          self.log_ladish_file.open(QIODevice.ReadOnly)
          self.log_ladish_stream = QTextStream(self.log_ladish_file)
          self.log_ladish_stream.setCodec("UTF-8")

        self.updateLogs(True)

    def purgeLogs(self):
        self.killTimer(self.logTimer)
        self.logTimer = None

        if (self.LOG_FILE_JACK):
          self.pte_jack.clear()
          self.log_jack_stream.flush()
          self.log_jack_file.close()
          self.log_jack_file.open(QIODevice.WriteOnly)
          self.log_jack_file.close()
          self.log_jack_file.open(QIODevice.ReadOnly)

        if (self.LOG_FILE_A2J):
          self.pte_a2j.clear()
          self.log_a2j_stream.flush()
          self.log_a2j_file.close()
          self.log_a2j_file.open(QIODevice.WriteOnly)
          self.log_a2j_file.close()
          self.log_a2j_file.open(QIODevice.ReadOnly)

        if (self.LOG_FILE_LASH):
          self.pte_lash.clear()
          self.log_lash_stream.flush()
          self.log_lash_file.close()
          self.log_lash_file.open(QIODevice.WriteOnly)
          self.log_lash_file.close()
          self.log_lash_file.open(QIODevice.ReadOnly)

        if (self.LOG_FILE_LADISH):
          self.pte_ladish.clear()
          self.log_ladish_stream.flush()
          self.log_ladish_file.close()
          self.log_ladish_file.open(QIODevice.WriteOnly)
          self.log_ladish_file.close()
          self.log_ladish_file.open(QIODevice.ReadOnly)

        self.logTimer = self.startTimer(1000)

    def fixReadText(self, text):
        return text.replace("[1m[31m","").replace("[1m[33m","").replace("[31m","").replace("[33m","").replace("[0m","")

    def updateLogs(self, firstRun=False):
        if (firstRun):
          self.pte_jack.clear()
          self.pte_a2j.clear()
          self.pte_lash.clear()
          self.pte_ladish.clear()

        if (self.LOG_FILE_JACK):
          text = self.fixReadText(self.log_jack_stream.readAll())
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_jack.appendPlainText(text)

        if (self.LOG_FILE_A2J):
          text = self.fixReadText(self.log_a2j_stream.readAll())
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_a2j.appendPlainText(text)

        if (self.LOG_FILE_LASH):
          text = self.fixReadText(self.log_lash_stream.readAll())
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_lash.appendPlainText(text)

        if (self.LOG_FILE_LADISH):
          text = self.fixReadText(self.log_ladish_stream.readAll())
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_ladish.appendPlainText(text)

        if (firstRun):
          self.pte_jack.horizontalScrollBar().setValue(0)
          self.pte_jack.verticalScrollBar().setValue(self.pte_jack.verticalScrollBar().maximum())
          self.pte_a2j.horizontalScrollBar().setValue(0)
          self.pte_a2j.verticalScrollBar().setValue(self.pte_a2j.verticalScrollBar().maximum())
          self.pte_lash.horizontalScrollBar().setValue(0)
          self.pte_lash.verticalScrollBar().setValue(self.pte_lash.verticalScrollBar().maximum())
          self.pte_ladish.horizontalScrollBar().setValue(0)
          self.pte_ladish.verticalScrollBar().setValue(self.pte_ladish.verticalScrollBar().maximum())

    def timerEvent(self, event):
        if (self.logTimer and event.timerId() == self.logTimer):
          self.updateLogs()

        return QDialog.timerEvent(self, event)

    def closeEvent(self, event):
        self.killTimer(self.logTimer)
        self.logTimer = None

        if (self.LOG_FILE_JACK):
          self.log_jack_file.close()

        if (self.LOG_FILE_A2J):
          self.log_a2j_file.close()

        if (self.LOG_FILE_LASH):
          self.log_lash_file.close()

        if (self.LOG_FILE_LADISH):
          self.log_ladish_file.close()

        return QDialog.closeEvent(self, event)


# Allow to use this as a standalone app
if __name__ == '__main__':

    # Additional imports
    import sys
    from PyQt4.QtGui import QApplication

    if not os.getenv("HOME"):
      print "HOME variable not set, cannot continue"
      sys.exit(1)

    if not os.getenv("PATH"):
      print "PATH variable not set, cannot continue"
      sys.exit(1)

    # App initialization
    app = QApplication(sys.argv)

    # Show GUI
    gui = LogsW()
    gui.show()

    # App-Loop
    sys.exit(app.exec_())
