#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import dbus
from dbus.mainloop.qt import DBusQtMainLoop
from commands import getoutput
from random import randint
from time import ctime
from PyQt4.QtCore import QFile, QSettings, QString, QVariant, SLOT
from PyQt4.QtGui import QAction, QApplication, QCursor, QDialog, QDialogButtonBox, QFileDialog, QMainWindow, QMenu, QTableWidgetItem, QTreeWidgetItem, QWizard

# Imports (Custom Stuff)
import systray, ui_settings_app
import ui_claudia, ui_claudia_createroom, ui_claudia_addnew, ui_claudia_addnew_klaudia, ui_claudia_runcustom
import ui_claudia_saveproject, ui_claudia_projectproperties, ui_claudia_studiolist, ui_claudia_studioname
from shared_canvas import *
from shared_jack import *
from klaudia_launcher import KlaudiaLauncher

try:
  from PyQt4.QtOpenGL import QGLWidget
  hasGL = True
except:
  hasGL = False

# Defines
ITEM_TYPE_STUDIO     = 0
ITEM_TYPE_STUDIO_APP = 1
ITEM_TYPE_ROOM       = 2
ITEM_TYPE_ROOM_APP   = 3

GRAPH_DICT_OBJECT_TYPE_GRAPH  = 0
GRAPH_DICT_OBJECT_TYPE_CLIENT = 1
GRAPH_DICT_OBJECT_TYPE_PORT   = 2
GRAPH_DICT_OBJECT_TYPE_CONNECTION = 3

URI_CANVAS_WIDTH   = "http://ladish.org/ns/canvas/width"
URI_CANVAS_HEIGHT  = "http://ladish.org/ns/canvas/height"
URI_CANVAS_X       = "http://ladish.org/ns/canvas/x"
URI_CANVAS_Y       = "http://ladish.org/ns/canvas/y"
URI_CANVAS_X_SPLIT = "http://kxstudio.sourceforge.net/ns/x_split"
URI_CANVAS_Y_SPLIT = "http://kxstudio.sourceforge.net/ns/y_split"
URI_CANVAS_SPLIT   = "http://kxstudio.sourceforge.net/ns/split"
URI_A2J_PORT       = "http://ladish.org/ns/a2j"

RECENT_PROJECTS_STORE_MAX_ITEMS = 50

DEFAULT_CANVAS_WIDTH  = 3100
DEFAULT_CANVAS_HEIGHT = 2400

DBus.loop = DBusQtMainLoop(set_as_default=True)
DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")
DBus.ladish_studio = None
DBus.ladish_room = None
DBus.ladish_graph = None
DBus.ladish_app_iface = None
DBus.patchbay = None

try:
  DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
except:
  DBus.a2j = None

try:
  DBus.ladish_app_daemon = DBus.bus.get_object("org.ladish.appdb", "/")
except:
  DBus.ladish_app_daemon = None

jacksettings.initBus(DBus.bus)

# QLineEdit and QPushButtom combo
def getAndSetPath(parent, cur_path, lineEdit):
    new_path = QFileDialog.getExistingDirectory(parent, parent.tr("Set Path"), cur_path, QFileDialog.ShowDirsOnly)
    if not new_path.isEmpty():
      lineEdit.setText(new_path)
    return new_path

# Configure Claudia Dialog
class ClaudiaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent=None):
        super(ClaudiaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.settings = self.parent().settings
        self.loadSettings()

        self.lw_page.setCurrentCell(0, 0)
        self.lw_page.item(0, 0).setIcon(QIcon(":/48x48/claudia.png"))
        self.lw_page.item(3, 0).setIcon(QIcon.fromTheme("application-x-executable", QIcon(":/48x48/exec.png")))

        self.tw_apps.removeTab(1)
        self.tw_apps.removeTab(1)

        self.cb_jack_port_alias.setVisible(False)
        self.label_jack_port_alias.setVisible(False)

        # Disabled for Alpha release
        self.cb_canvas_eyecandy.setEnabled(False)

        if not hasGL:
          self.cb_canvas_use_opengl.setChecked(False)
          self.cb_canvas_use_opengl.setEnabled(False)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

        self.connect(self.b_main_def_folder_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_main_def_folder.text(),
                                                       lineEdit=self.le_main_def_folder: getAndSetPath(parent, current_path, lineEdit))

    def saveSettings(self):
        self.settings.setValue("Main/DefaultProjectFolder", self.le_main_def_folder.text())
        self.settings.setValue("Main/UseSystemTray", self.cb_tray_enable.isChecked())
        self.settings.setValue("Main/CloseToTray", self.cb_tray_close_to.isChecked())
        self.settings.setValue("Main/RefreshInterval", self.sb_gui_refresh.value())
        self.settings.setValue("Canvas/Theme", self.cb_canvas_theme.currentText())
        self.settings.setValue("Canvas/BezierLines", self.cb_canvas_bezier_lines.isChecked())
        self.settings.setValue("Canvas/AutoHideGroups", self.cb_canvas_hide_groups.isChecked())
        self.settings.setValue("Canvas/FancyEyeCandy", self.cb_canvas_eyecandy.isChecked())
        self.settings.setValue("Canvas/UseOpenGL", self.cb_canvas_use_opengl.isChecked())
        self.settings.setValue("Canvas/Antialiasing", self.cb_canvas_render_aa.checkState())
        self.settings.setValue("Canvas/TextAntialiasing", self.cb_canvas_render_text_aa.isChecked())
        self.settings.setValue("Canvas/HighQualityAntialiasing", self.cb_canvas_render_hq_aa.isChecked())
        self.settings.setValue("Apps/Database", "LADISH" if self.rb_database_ladish.isChecked() else "Klaudia")

        try:
          ladish_config = DBus.bus.get_object("org.ladish.conf", "/org/ladish/conf")
        except:
          ladish_config = None

        if (ladish_config):
          ladish_config.set('/org/ladish/daemon/notify', "true" if (self.cb_ladish_notify.isChecked()) else "false")
          ladish_config.set('/org/ladish/daemon/studio_autostart', "true" if (self.cb_ladish_studio_autostart.isChecked()) else "false")
          ladish_config.set('/org/ladish/daemon/shell', QStringStr(self.le_ladish_shell.text()))
          ladish_config.set('/org/ladish/daemon/terminal', QStringStr(self.le_ladish_terminal.text()))

    def loadSettings(self):
        self.le_main_def_folder.setText(self.settings.value("Main/DefaultProjectFolder", os.path.join(os.getenv("HOME"),"ladish-projects")).toString())
        self.cb_tray_enable.setChecked(self.settings.value("Main/UseSystemTray", True).toBool())
        self.cb_tray_close_to.setChecked(self.settings.value("Main/CloseToTray", False).toBool())
        self.sb_gui_refresh.setValue(self.settings.value("Main/RefreshInterval", 100).toInt()[0])
        self.cb_canvas_bezier_lines.setChecked(self.settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(self.settings.value("Canvas/AutoHideGroups", True).toBool())
        self.cb_canvas_eyecandy.setChecked(self.settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_use_opengl.setChecked(self.settings.value("Canvas/UseOpenGL", False).toBool())
        self.cb_canvas_render_aa.setCheckState(self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(self.settings.value("Canvas/TextAntialiasing", True).toBool())
        self.cb_canvas_render_hq_aa.setChecked(self.settings.value("Canvas/HighQualityAntialiasing", False).toBool())

        theme_name = self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme())).toString()

        for i in range(patchcanvas.Theme.THEME_MAX):
          this_theme_name = patchcanvas.getThemeName(i)
          self.cb_canvas_theme.addItem(this_theme_name)
          if (this_theme_name == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

        try:
          ladish_config = DBus.bus.get_object("org.ladish.conf", "/org/ladish/conf")
        except:
          ladish_config = None

        if (ladish_config):
          try:
            self.cb_ladish_notify.setChecked(True if (ladish_config.get('/org/ladish/daemon/notify')[0] == "true") else False)
          except:
            self.cb_ladish_notify.setChecked(True)

          try:
            self.cb_ladish_studio_autostart.setChecked(True if (ladish_config.get('/org/ladish/daemon/studio_autostart')[0] == "true") else False)
          except:
            self.cb_ladish_studio_autostart.setChecked(True)

          try:
            self.le_ladish_shell.setText(ladish_config.get('/org/ladish/daemon/shell')[0])
          except:
            self.le_ladish_shell.setText("sh")

          try:
            self.le_ladish_terminal.setText(ladish_config.get('/org/ladish/daemon/terminal')[0])
          except:
            self.le_ladish_terminal.setText("xterm")

          database = self.settings.value("Apps/Database", "LADISH").toString()
          if (database == "LADISH"):
            self.rb_database_ladish.setChecked(True)
          elif (database == "Klaudia"):
            self.rb_database_kxstudio.setChecked(True)

    def resetSettings(self):
        self.le_main_def_folder.setText(os.path.join(os.getenv("HOME"),"ladish-projects"))
        self.cb_tray_enable.setChecked(True)
        self.cb_tray_close_to.setChecked(False)
        self.sb_gui_refresh.setValue(100)
        self.cb_ladish_notify.setChecked(True)
        self.cb_ladish_studio_autostart.setChecked(True)
        self.le_ladish_shell.setText("sh")
        self.le_ladish_terminal.setText("xterm")
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(True)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_use_opengl.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)
        self.cb_canvas_render_hq_aa.setChecked(False)
        self.rb_database_ladish.setChecked(True)
        #self.rb_database_kxstudio.setChecked(False)

# Studio List Dialog
class StudioListW(QDialog, ui_claudia_studiolist.Ui_StudioListW):
    def __init__(self, parent=None):
        super(StudioListW, self).__init__(parent)
        self.setupUi(self)

        self.studio_to_return = QString("")
        self.tableWidget.setColumnWidth(0, 125)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        studio_list_dump = DBus.ladish_control.GetStudioList()
        for i in range(len(studio_list_dump)):
          name = QString(studio_list_dump[i][0])
          date = QString(ctime(float(studio_list_dump[i][1][u'Modification Time'])))

          w_name = QTableWidgetItem(name)
          w_date = QTableWidgetItem(date)
          self.tableWidget.insertRow(0)
          self.tableWidget.setItem(0, 0, w_name)
          self.tableWidget.setItem(0, 1, w_date)

        self.connect(self, SIGNAL("accepted()"), self.setName)
        self.connect(self.tableWidget, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection)
        self.connect(self.tableWidget, SIGNAL("cellDoubleClicked(int, int)"), self.accept)

        if (self.tableWidget.rowCount() > 0):
          self.tableWidget.setCurrentCell(0, 0)

    def setName(self):
        self.studio_to_return = self.tableWidget.item(self.tableWidget.currentRow(), 0).text()

    def checkSelection(self, row, column, prev_row, prev_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Studio Name Dialog
class StudioNameW(QDialog, ui_claudia_studioname.Ui_StudioNameW):
    def __init__(self, parent=None, setName=True, block=True):
        super(StudioNameW, self).__init__(parent)
        self.setupUi(self)

        self.studio_list = []
        self.studio_to_return = QString("")
        self.block = block
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not block)

        if (bool(DBus.ladish_control.IsStudioLoaded()) and setName):
          current_name = QString(DBus.ladish_studio.GetName())
          self.studio_list.append(current_name)
          self.le_name.setText(current_name)

        studio_list_dump = DBus.ladish_control.GetStudioList()
        for i in range(len(studio_list_dump)):
          self.studio_list.append(QString(studio_list_dump[i][0]))

        self.connect(self, SIGNAL("accepted()"), self.setName)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText)

    def setName(self):
        self.studio_to_return = self.le_name.text()

    def checkText(self, text):
        if (self.block): false = (text.isEmpty() or text in self.studio_list)
        else: false = text.isEmpty()
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not false)

# Create Room Dialog
class CreateRoomW(QDialog, ui_claudia_createroom.Ui_CreateRoomW):
    def __init__(self, parent=None):
        super(CreateRoomW, self).__init__(parent)
        self.setupUi(self)

        self.room_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        room_templates_list_dump = DBus.ladish_control.GetRoomTemplateList()
        for i in range(len(room_templates_list_dump)):
          self.lw_templates.addItem(QString(room_templates_list_dump[i][0]))

        self.connect(self, SIGNAL("accepted()"), self.setRoom)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText)

        if (self.lw_templates.count() > 0):
          self.lw_templates.setCurrentRow(0)

    def setRoom(self):
        self.room_to_return = [unicode(self.le_name.text()), unicode(self.lw_templates.currentItem().text())]

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.lw_templates.currentRow() < 0) else True)

# Save Project Dialog
class SaveProjectW(QDialog, ui_claudia_saveproject.Ui_SaveProjectW):
    def __init__(self, parent=None, path="", name="", setName=True):
        super(SaveProjectW, self).__init__(parent)
        self.setupUi(self)

        self.project_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (path and name) else False)

        if (setName):
          self.le_path.setText(QString(path))
          self.le_name.setText(QString(name))

        self.connect(self, SIGNAL("accepted()"), self.setProject)
        self.connect(self.le_path, SIGNAL("textChanged(QString)"), self.checkText_path)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText_name)
        self.connect(self.b_open, SIGNAL("clicked()"), self.checkFolder)

    def setProject(self):
        self.project_to_return = [self.le_path.text(), self.le_name.text()]

    def checkFolder(self):
        proj_folder = self.parent().saved_settings["Main/DefaultProjectFolder"]

        # Create default project folder if the project has not been set yet 
        if (self.le_path.text().isEmpty()):
          if (not os.path.exists(proj_folder)):
            os.mkdir(proj_folder)

        new_path = getAndSetPath(self, proj_folder if (self.le_path.text().isEmpty()) else self.le_path.text(), self.le_path)
        #if (new_path.isEmpty()):
          #pass
        #elif (not QFile.exists(new_path+"/ladish-project.xml")):
          #QMessageBox.warning(self, self.tr("Error"), self.tr("The selected folder does not contain a ladish project!"))

    def checkText_name(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.le_path.text().isEmpty()) else True)

    def checkText_path(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.le_name.text().isEmpty() or not QFile.exists(text+"/ladish-project.xml")) else True)

# Project Properties Dialog
class ProjectPropertiesW(QDialog, ui_claudia_projectproperties.Ui_ProjectPropertiesW):
    def __init__(self, parent=None, name="", description="", notes=""):
        super(ProjectPropertiesW, self).__init__(parent)
        self.setupUi(self)

        self.default_name = name
        self.last_name = name
        self.data_to_return = [False, name, description, notes]
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (path and name) else False)

        self.connect(self, SIGNAL("accepted()"), self.setData)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText_name)
        self.connect(self.cb_save_now, SIGNAL("clicked(bool)"), self.checkSaveNow)

        self.le_name.setText(QString(name))
        self.le_description.setText(QString(description))
        self.le_notes.setPlainText(QString(notes))

    def setData(self):
        if ("toPlainText" in dir(self.le_notes)): #PyQt Bug
          notes = self.le_notes.toPlainText()
        else:
          notes = self.le_notes.plainText()

        self.data_to_return = [self.cb_save_now.isChecked(), self.le_name.text(), self.le_description.text(), notes]

    def checkSaveNow(self, save):
        if (save):
          self.le_name.setText(self.last_name)
        else:
          self.last_name = self.le_name.text()
          self.le_name.setText(self.default_name)

    def checkText_name(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not text.isEmpty())

# Add New App Dialog
class AddNewW(QWizard, ui_claudia_addnew.Ui_AddNewW):
    def __init__(self, parent=None):
        super(AddNewW, self).__init__(parent)
        self.setupUi(self)

        self.app_to_return = []
        self.tw_list_normal.setColumnWidth(0, 22)
        self.tw_list_normal.setColumnWidth(1, self.tw_list_normal.width()/4)
        self.tw_list_normal.setColumnWidth(2, self.tw_list_normal.width()/2)
        self.tw_list_normal.setColumnWidth(3, 40)
        self.rb_level_2.setEnabled(False)
        self.rb_level_3.setEnabled(False)

        i_normal = i_bristol = i_wine = i_vst = 0
        app_list = DBus.ladish_app_daemon.get_app_list()

        for i in range(len(app_list)):
          name = app_list[i]['name']
          generic_name = app_list[i]['generic_name']
          comment = app_list[i]['comment']
          icon  = app_list[i]['icon']
          exec_ = app_list[i]['exec']
          terminal = app_list[i]['terminal']
          type_ = app_list[i]['type']

          if (type_ == "normal"):
            w_name = QTableWidgetItem(name)
            w_comment = QTableWidgetItem(comment)
            w_terminal = QTableWidgetItem(self.tr("Yes") if (terminal == "true") else self.tr("No"))

            self.tw_list_normal.insertRow(i_normal)
            self.tw_list_normal.setItem(i_normal, 1, w_name)
            self.tw_list_normal.setItem(i_normal, 2, w_comment)
            self.tw_list_normal.setItem(i_normal, 3, w_terminal)

            i_normal += 1

        self.tw_list_normal.sortItems(1)

        self.connect(self, SIGNAL("currentIdChanged(int)"), self.changedPage)
        self.connect(self, SIGNAL("accepted()"), self.setApp)
        self.connect(self.rb_app_normal, SIGNAL("clicked()"), lambda index=0: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_bristol, SIGNAL("clicked()"), lambda index=1: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_wine, SIGNAL("clicked()"), lambda index=2: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_vst, SIGNAL("clicked()"), lambda index=3: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.tw_list_normal, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_normal)
        self.connect(self.tw_list_bristol, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_bristol)
        self.connect(self.tw_list_wine, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_wine)
        self.connect(self.tw_list_vst, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_vst)

    def changedPage(self, index):
        if (index == 1):
          self.checkSelection()
        elif (index == 2):
          self.checkApp()

    def setApp(self):
        if (self.rb_level_0.isChecked()):
          level = 0
        elif (self.rb_level_1.isChecked()):
          level = 1
        elif (self.rb_level_2.isChecked()):
          level = 2
        elif (self.rb_level_3.isChecked()):
          level = 3
        else:
          level = 0

        self.app_to_return = [unicode(self.le_command.text()), unicode(self.le_name.text()), True if (self.cb_terminal.isChecked()) else False, level]

    def checkApp(self):
        if (self.stackedWidget.currentIndex() == 0): #normal
          item = self.tw_list_normal.item(self.tw_list_normal.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 1): #bristol
          item = self.tw_list_bristol.item(self.tw_list_bristol.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 2): #wine
          item = self.tw_list_wine.item(self.tw_list_wine.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 3): #vst
          item = self.tw_list_vst.item(self.tw_list_vst.currentRow(), 1)
        else:
          print "invalid index of stackedWidget"
          return

        if not item:
          print "invalid item"
          return

        app_list = DBus.ladish_app_daemon.get_app_list()
        for i in range(len(app_list)):
          name = app_list[i]['name']
          generic_name = app_list[i]['generic_name']
          comment = app_list[i]['comment']
          icon  = app_list[i]['icon']
          exec_ = app_list[i]['exec']
          terminal = app_list[i]['terminal']
          type_ = app_list[i]['type']

          if (name == item.text()):
            break

        else:
          print "could not find app"
          return

        if (type_ == "normal"):
          self.le_command.setText(exec_)
          self.le_name.setText(name)
          self.cb_terminal.setChecked(True if (terminal == "true") else False)
          self.rb_level_0.setChecked(True)

    def checkSelection(self):
        if (self.rb_app_normal.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_normal.currentItem()) else False)
        elif (self.rb_app_bristol.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_bristol.currentItem()) else False)
        elif (self.rb_app_wine.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_wine.currentItem()) else False)
        elif (self.rb_app_vst.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_vst.currentItem()) else False)

    def checkSelection_normal(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_bristol(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_wine(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_vst(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

# Add New App Dialog (Klaudia)
class AddNewKlaudiaW(QDialog, ui_claudia_addnew_klaudia.Ui_AddNewKlaudiaW):
    def __init__(self, parent, appBus, url_folder, is_room, sample_rate, bpm):
        super(AddNewKlaudiaW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Claudia-Launcher")
        self.launcher = KlaudiaLauncher(self, self.settings, True)

        self.appBus = appBus
        self.url_folder = url_folder
        self.is_room = is_room
        self.sample_rate = sample_rate
        self.bpm = bpm

        self.test_url = True
        self.test_selected = False

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.connect(self, SIGNAL("accepted()"), self.launcher.addAppToLADISH)

    def checkGUI(self, test_selected=None):
        if (test_selected != None):
          self.test_selected = test_selected

        if (self.test_url and self.test_selected):
          self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        else:
          self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

    def isRoom(self):
        return self.is_room

    def getAppBus(self):
        return self.appBus

    def getSampleRate(self):
        return self.sample_rate

    def getBPM(self):
        if (self.bpm <= 0):
          return 120.0
        else:
          return self.bpm

    def getProjectFolder(self):
        return self.url_folder

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.launcher.saveSettings()

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

    def closeEvent(self, event):
        self.saveSettings()
        return QDialog.closeEvent(self, event)

# Run Custom App Dialog
class RunCustomW(QDialog, ui_claudia_runcustom.Ui_RunCustomW):
    def __init__(self, parent=None, command="", name="", terminal=False, level=0, active=False):
        super(RunCustomW, self).__init__(parent)
        self.setupUi(self)

        self.app_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if command else False)

        self.le_command.setText(command)
        self.le_name.setText(name)
        self.cb_terminal.setChecked(terminal)

        if (level == 0):
          self.rb_level_0.setChecked(True)
        elif (level == 1):
          self.rb_level_1.setChecked(True)
        elif (level == 2):
          self.rb_level_2.setChecked(True)
        elif (level == 3):
          self.rb_level_3.setChecked(True)
        else:
          self.rb_level_0.setChecked(True)

        if (active):
          self.le_command.setEnabled(False)
          self.cb_terminal.setEnabled(False)
          self.rb_level_0.setEnabled(False)
          self.rb_level_1.setEnabled(False)
          self.rb_level_2.setEnabled(False)
          self.rb_level_3.setEnabled(False)

        ### Not yet supported
        self.rb_level_2.setEnabled(False)
        self.rb_level_3.setEnabled(False)
        ### Not yet supported

        self.connect(self, SIGNAL("accepted()"), self.setApp)
        self.connect(self.le_command, SIGNAL("textChanged(QString)"), self.checkText)

    def setApp(self):
        if (self.rb_level_0.isChecked()):
          level = 0
        elif (self.rb_level_1.isChecked()):
          level = 1
        elif (self.rb_level_2.isChecked()):
          level = 2
        elif (self.rb_level_3.isChecked()):
          level = 3
        else:
          level = 0

        self.app_to_return = [unicode(self.le_command.text()), unicode(self.le_name.text()), True if (self.cb_terminal.isChecked()) else False, level]

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if text.isEmpty() else True)

# Main Window
class ClaudiaMainW(QMainWindow, ui_claudia.Ui_ClaudiaMainW):
    def __init__(self, parent=None):
        super(ClaudiaMainW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Claudia")
        self.loadSettings()

        self.timer100  = QTimer()
        self.timer1000 = QTimer()
        self.timer100.setInterval(self.saved_settings["Main/RefreshInterval"])
        self.timer1000.setInterval(1000)
        self.timer1000.start()

        # Global Systray
        if (self.saved_settings["Main/UseSystemTray"]):
          self.systray = systray.GlobalSysTray("Claudia", "claudia")
          self.systray.setQtParent(self)

          self.systray.addAction("studio_new", QStringStr(self.tr("New Studio...")))
          self.systray.addSeparator("sep1")
          self.systray.addAction("studio_start", QStringStr(self.tr("Start Studio")))
          self.systray.addAction("studio_stop", QStringStr(self.tr("Stop Studio")))
          self.systray.addSeparator("sep2")
          self.systray.addAction("studio_save", QStringStr(self.tr("Save Studio")))
          self.systray.addAction("studio_save_as", QStringStr(self.tr("Save Studio As...")))
          self.systray.addAction("studio_rename", QStringStr(self.tr("Rename Studio...")))
          self.systray.addAction("studio_unload", QStringStr(self.tr("Unload Studio")))
          self.systray.addSeparator("sep3")
          self.systray.addMenu("tools", QStringStr(self.tr("Tools")))
          self.systray.addMenuAction("tools", "tools_configure_jack", QStringStr(self.tr("Configure JACK")))
          self.systray.addMenuAction("tools", "tools_render", QStringStr(self.tr("JACK Render")))
          self.systray.addMenuAction("tools", "tools_logs", QStringStr(self.tr("Logs")))
          self.systray.addMenuSeparator("tools", "tools_sep")
          self.systray.addMenuAction("tools", "tools_clear_xruns", QStringStr(self.tr("Clear Xruns")))
          self.systray.addAction("configure", QStringStr(self.tr("Configure Claudia")))
          self.systray.addSeparator("sep4")
          self.systray.addAction("show", QStringStr(self.tr("Minimize")))
          self.systray.addAction("quit", QStringStr(self.tr("Quit")))

          self.systray.setActionIcon("studio_new", "document-new")
          self.systray.setActionIcon("studio_start", "media-playback-start")
          self.systray.setActionIcon("studio_stop", "media-playback-stop")
          self.systray.setActionIcon("studio_save", "document-save")
          self.systray.setActionIcon("studio_save_as", "document-save-as")
          self.systray.setActionIcon("studio_rename", "edit-rename")
          self.systray.setActionIcon("studio_unload", "dialog-close")
          self.systray.setActionIcon("tools_configure_jack", "configure")
          self.systray.setActionIcon("tools_render", "media-record")
          self.systray.setActionIcon("tools_clear_xruns", "edit-clear")
          self.systray.setActionIcon("configure", "configure")
          self.systray.setActionIcon("quit", "application-exit")

          self.systray.connect("show", self.systray_clicked_callback)
          self.systray.connect("studio_new", self.func_studio_new)
          self.systray.connect("studio_start", self.func_studio_start)
          self.systray.connect("studio_stop", self.func_studio_stop)
          self.systray.connect("studio_save", self.func_studio_save)
          self.systray.connect("studio_save_as", self.func_studio_save_as)
          self.systray.connect("studio_rename", self.func_studio_rename)
          self.systray.connect("studio_unload", self.func_studio_unload)
          self.systray.connect("tools_configure_jack", lambda: jack_configure(self))
          self.systray.connect("tools_render", lambda: jack_render(self))
          self.systray.connect("tools_logs", lambda: show_logs(self))
          self.systray.connect("tools_clear_xruns", self.jack_clear_xruns)
          self.systray.connect("configure", self.configureClaudia)
          self.systray.connect("quit", self.systray_closed)

          self.systray.setToolTip("LADISH Frontend")
          self.systray.show()

        else:
          self.systray = None

        setIcons(self, ["canvas", "jack", "transport"])

        self.act_studio_new.setIcon(getIcon("document-new"))
        self.menu_studio_load.setIcon(getIcon("document-open"))
        self.act_studio_start.setIcon(getIcon("media-playback-start"))
        self.act_studio_stop.setIcon(getIcon("media-playback-stop"))
        self.act_studio_rename.setIcon(getIcon("edit-rename"))
        self.act_studio_save.setIcon(getIcon("document-save"))
        self.act_studio_save_as.setIcon(getIcon("document-save-as"))
        self.act_studio_unload.setIcon(getIcon("dialog-close"))
        self.menu_studio_delete.setIcon(getIcon("edit-delete"))
        self.b_studio_new.setIcon(getIcon("document-new"))
        self.b_studio_load.setIcon(getIcon("document-open"))
        self.b_studio_save.setIcon(getIcon("document-save"))
        self.b_studio_save_as.setIcon(getIcon("document-save-as"))

        self.act_room_create.setIcon(getIcon("list-add"))
        self.menu_room_delete.setIcon(getIcon("edit-delete"))

        self.act_project_new.setIcon(getIcon("document-new"))
        self.menu_project_load.setIcon(getIcon("document-open"))
        self.act_project_save.setIcon(getIcon("document-save"))
        self.act_project_save_as.setIcon(getIcon("document-save-as"))
        self.act_project_unload.setIcon(getIcon("dialog-close"))
        self.act_project_properties.setIcon(getIcon("edit-rename"))
        self.b_project_new.setIcon(getIcon("document-new"))
        self.b_project_load.setIcon(getIcon("document-open"))
        self.b_project_save.setIcon(getIcon("document-save"))
        self.b_project_save_as.setIcon(getIcon("document-save-as"))

        self.act_app_add_new.setIcon(getIcon("list-add"))
        self.act_app_run_custom.setIcon(getIcon("system-run"))

        self.act_tools_reactivate_ladishd.setIcon(getIcon("view-refresh"))
        self.act_quit.setIcon(getIcon("application-exit"))
        self.act_settings_configure.setIcon(getIcon("configure"))

        self.last_connection_id = 1
        self.group_list = []
        self.group_split_list = []
        self.connection_list = []

        self.buffer_size = 0
        self.sample_rate = 0
        self.last_buffer_size = 0
        self.last_sample_rate = 0
        self.next_sample_rate = 0

        self.last_bpm = None
        self.last_transport_state = None

        self.last_item_type = None
        self.last_room_path = None

        self.cb_buffer_size.clear()
        self.cb_sample_rate.clear()

        for i in range(len(buffer_sizes)):
          self.cb_buffer_size.addItem(str(buffer_sizes[i]))

        for i in range(len(sample_rates)):
          self.cb_sample_rate.addItem(str(sample_rates[i]))

        self.scene = patchcanvas.PatchScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        if (self.saved_settings["Canvas/UseOpenGL"] and hasGL):
          self.graphicsView.setViewport(QGLWidget(self.graphicsView))
          self.graphicsView.setRenderHint(QPainter.HighQualityAntialiasing, self.saved_settings["Canvas/HighQualityAntialiasing"])

        p_options = patchcanvas.options_t()
        p_options.theme_name       = QString(self.saved_settings["Canvas/Theme"])
        p_options.bezier_lines     = self.saved_settings["Canvas/BezierLines"]
        p_options.antialiasing     = self.saved_settings["Canvas/Antialiasing"]
        p_options.auto_hide_groups = self.saved_settings["Canvas/AutoHideGroups"]
        p_options.fancy_eyecandy   = self.saved_settings["Canvas/FancyEyeCandy"]

        p_features = patchcanvas.features_t()
        p_features.group_info       = False
        p_features.group_rename     = False
        p_features.port_info        = True
        p_features.port_rename      = False
        p_features.handle_group_pos = False

        patchcanvas.set_options(p_options)
        patchcanvas.set_features(p_features)
        patchcanvas.init(self.scene, self.canvas_callback, DEBUG)

        patchcanvas.setInitialPos(DEFAULT_CANVAS_WIDTH/2, DEFAULT_CANVAS_HEIGHT/2)
        patchcanvas.setCanvasSize(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT)
        self.graphicsView.setSceneRect(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT)

        self.miniCanvasPreview.setRealParent(self)
        self.miniCanvasPreview.init(self.scene, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT)

        if (self.saved_settings['Apps/Database'] == "LADISH" and not DBus.ladish_app_daemon):
          self.act_app_add_new.setEnabled(False)

        if (bool(DBus.jack.IsStarted())):
          self.jackStarted()
        else:
          self.jackStopped()

        if (bool(DBus.ladish_control.IsStudioLoaded())):
          self.studioLoaded()
          if (bool(DBus.ladish_studio.IsStarted())):
            self.studioStarted()
            self.init_ports()
          else:
            self.studioStopped()
        else:
          self.studioUnloaded()

        setCanvasConnections(self)
        setJackConnections(self, ["jack", "transport", "misc"])

        self.connect(self.act_studio_new, SIGNAL("triggered()"), self.func_studio_new)
        self.connect(self.act_studio_start, SIGNAL("triggered()"), self.func_studio_start)
        self.connect(self.act_studio_stop, SIGNAL("triggered()"), self.func_studio_stop)
        self.connect(self.act_studio_save, SIGNAL("triggered()"), self.func_studio_save)
        self.connect(self.act_studio_save_as, SIGNAL("triggered()"), self.func_studio_save_as)
        self.connect(self.act_studio_rename, SIGNAL("triggered()"), self.func_studio_rename)
        self.connect(self.act_studio_unload, SIGNAL("triggered()"), self.func_studio_unload)
        self.connect(self.b_studio_new, SIGNAL("clicked()"), self.func_studio_new)
        self.connect(self.b_studio_load, SIGNAL("clicked()"), self.func_studio_load_b)
        self.connect(self.b_studio_save, SIGNAL("clicked()"), self.func_studio_save)
        self.connect(self.b_studio_save_as, SIGNAL("clicked()"), self.func_studio_save_as)

        self.connect(self.act_room_create, SIGNAL("triggered()"), self.func_room_create)

        self.connect(self.act_project_new, SIGNAL("triggered()"), self.func_project_new)
        self.connect(self.act_project_save, SIGNAL("triggered()"), self.func_project_save)
        self.connect(self.act_project_save_as, SIGNAL("triggered()"), self.func_project_save_as)
        self.connect(self.act_project_unload, SIGNAL("triggered()"), self.func_project_unload)
        self.connect(self.act_project_properties, SIGNAL("triggered()"), self.func_project_properties)
        self.connect(self.b_project_new, SIGNAL("clicked()"), self.func_project_new)
        self.connect(self.b_project_load, SIGNAL("clicked()"), self.func_project_load)
        self.connect(self.b_project_save, SIGNAL("clicked()"), self.func_project_save)
        self.connect(self.b_project_save_as, SIGNAL("clicked()"), self.func_project_save_as)

        self.connect(self.act_app_add_new, SIGNAL("triggered()"), self.func_app_add_new)
        self.connect(self.act_app_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)

        self.connect(self.graphicsView.horizontalScrollBar(), SIGNAL("valueChanged(int)"), self.minicanvas_hs_changed)
        self.connect(self.graphicsView.verticalScrollBar(), SIGNAL("valueChanged(int)"), self.minicanvas_vs_changed)

        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshBufferSize(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshSampleRate(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshDSPLoad(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshTransport(self))
        self.connect(self.timer100, SIGNAL("timeout()"), self.refreshXruns)
        self.connect(self.timer1000, SIGNAL("timeout()"), self.refreshDBusFix)

        self.connect(self.menu_studio_load, SIGNAL("aboutToShow()"), lambda menu=self.menu_studio_load,
                     function=self.func_studio_load_m: self.updateMenuStudioList(menu, function))
        self.connect(self.menu_studio_delete, SIGNAL("aboutToShow()"), lambda menu=self.menu_studio_delete,
                     function=self.func_studio_delete_m: self.updateMenuStudioList(menu, function))
        self.connect(self.menu_room_delete, SIGNAL("aboutToShow()"), self.updateMenuRoomList)
        self.connect(self.menu_project_load, SIGNAL("aboutToShow()"), self.updateMenuProjectList)

        self.connect(self.treeWidget, SIGNAL("itemSelectionChanged()"), self.checkCurrentRoom)
        #self.connect(self.treeWidget, SIGNAL("itemPressed(QTreeWidgetItem*, int)"), self.checkCurrentRoom)
        self.connect(self.treeWidget, SIGNAL("itemDoubleClicked(QTreeWidgetItem*, int)"), self.doubleClickedAppList)
        self.connect(self.treeWidget, SIGNAL("customContextMenuRequested(QPoint)"), self.showAppListCustomMenu)

        self.connect(self.scene, SIGNAL("sceneGroupMoved(int, int, QPointF)"), self.canvas_item_moved)
        self.connect(self.scene, SIGNAL("scaleChanged(double)"), self.canvas_scale_changed)
        self.connect(self.miniCanvasPreview, SIGNAL("miniCanvasMoved(float, float)"), self.minicanvas_moved)

        self.connect(self.act_settings_configure, SIGNAL("triggered()"), self.configureClaudia)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutClaudia)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)
        self.connect(self, SIGNAL("QueueExecutionHalted"), self.signal_QueueExecutionHalted_exec)

        # DBus Stuff
        DBus.bus.add_signal_receiver(self.DBusSignalReceiver, destination_keyword='dest', path_keyword='path',
                        member_keyword='member', interface_keyword='interface', sender_keyword='sender', )

        QTimer.singleShot(10, self.minicanvas_recheck_init)

    def minicanvas_moved(self, xp, yp):
        self.graphicsView.horizontalScrollBar().setValue(xp*DEFAULT_CANVAS_WIDTH)
        self.graphicsView.verticalScrollBar().setValue(yp*DEFAULT_CANVAS_HEIGHT)

    def minicanvas_hs_changed(self, value):
        maximum = self.graphicsView.horizontalScrollBar().maximum()
        if (maximum == 0):
          xp = 0
        else:
          xp = float(value)/maximum
        self.miniCanvasPreview.setViewPosX(xp)

    def minicanvas_vs_changed(self, value):
        maximum = self.graphicsView.verticalScrollBar().maximum()
        if (maximum == 0):
          yp = 0
        else:
          yp = float(value)/maximum
        self.miniCanvasPreview.setViewPosY(yp)

    def minicanvas_recheck_size(self):
        self.miniCanvasPreview.setViewSize(float(self.graphicsView.width())/DEFAULT_CANVAS_WIDTH, float(self.graphicsView.height())/DEFAULT_CANVAS_HEIGHT)

    def minicanvas_recheck_all(self):
        self.minicanvas_recheck_size()
        self.minicanvas_hs_changed(self.graphicsView.horizontalScrollBar().value())
        self.minicanvas_vs_changed(self.graphicsView.verticalScrollBar().value())
        self.miniCanvasPreview.update()

    def minicanvas_recheck_init(self):
        #self.scene.zoom_fit()
        #self.scene.zoom_reset()
        self.miniCanvasPreview.setViewPosX(0.33)
        self.miniCanvasPreview.setViewPosY(0.33)
        self.minicanvas_moved(0.33, 0.33)

    def canvas_scale_changed(self, scale):
        self.miniCanvasPreview.setViewScale(scale)

    def canvas_item_moved(self, group_id, split_mode, pos):
        if (split_mode == patchcanvas.PORT_MODE_INPUT):
          canvas_x = URI_CANVAS_X_SPLIT
          canvas_y = URI_CANVAS_Y_SPLIT
        else:
          canvas_x = URI_CANVAS_X
          canvas_y = URI_CANVAS_Y

        DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, canvas_x, str(pos.x()))
        DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, canvas_y, str(pos.y()))

        self.miniCanvasPreview.update()

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          if (DEBUG): print "jack signal", kwds['member']
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()

        elif (kwds['interface'] == "org.jackaudio.JackPatchbay"):
          if (kwds['path'] == DBus.patchbay.object_path):
            if (DEBUG): print "patchbay signal", kwds['member']
            if (kwds['member'] == "ClientAppeared"):
              self.signal_ClientAppeared(args)
            elif (kwds['member'] == "ClientDisappeared"):
              self.signal_ClientDisappeared(args)
            elif (kwds['member'] == "PortAppeared"):
              self.signal_PortAppeared(args)
            elif (kwds['member'] == "PortDisappeared"):
              self.signal_PortDisappeared(args)
            elif (kwds['member'] == "PortRenamed"):
              self.signal_PortRenamed(args)
            elif (kwds['member'] == "PortsConnected"):
              self.signal_PortsConnected(args)
            elif (kwds['member'] == "PortsDisconnected"):
              self.signal_PortsDisconnected(args)

        elif (kwds['interface'] == "org.ladish.Control"):
          if (DEBUG): print "ladish_control signal", kwds['member']
          if (kwds['member'] == "StudioAppeared"):
            self.studioLoaded()
            if (bool(DBus.ladish_studio.IsStarted())):
              self.studioStarted()
            else:
              self.studioStopped()
          elif (kwds['member'] == "StudioDisappeared"):
            self.studioUnloaded()
          elif (kwds['member'] == "QueueExecutionHalted"):
            self.signal_QueueExecutionHalted()
          elif (kwds['member'] == "CleanExit"):
            self.signal_CleanExit(args)

        elif (kwds['interface'] == "org.ladish.Studio"):
          if (DEBUG): print "ladish_studio signal", kwds['member']
          if (kwds['member'] == "StudioStarted"):
            self.studioStarted()
          elif (kwds['member'] == "StudioStopped"):
            self.studioStopped()
          elif (kwds['member'] == "StudioRenamed"):
            self.signal_StudioRenamed(args)
          elif (kwds['member'] == "RoomAppeared"):
            self.signal_RoomAppeared(args)
          elif (kwds['member'] == "RoomDisappeared"):
            self.signal_RoomDisappeared(args)
          elif (kwds['member'] == "RoomChanged"):
            self.signal_RoomChanged(args)

        elif (kwds['interface'] == "org.ladish.Room"):
          if (DEBUG): print "ladish_room signal", kwds['member']
          if (kwds['member'] == "ProjectPropertiesChanged"):
            self.signal_ProjectPropertiesChanged(kwds['path'], args)

        elif (kwds['interface'] == "org.ladish.AppSupervisor"):
          if (DEBUG): print "ladish_app_iface signal", kwds['member']
          if (kwds['member'] == "AppAdded"):
            self.signal_AppAdded(kwds['path'], args)
          elif (kwds['member'] == "AppRemoved"):
            self.signal_AppRemoved(kwds['path'], args)
          elif (kwds['member'] == "AppStateChanged"):
            self.signal_AppStateChanged(kwds['path'], args)

    def DBusReconnect(self):
        DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
        DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")

    def jackStarted(self):
        #self.DBusReconnect()

        if (not jack.client):
          jack.client = jacklib.client_open("claudia", jacklib.NullOption, None)
          jacklib.on_shutdown(jack.client, self.JackShutdownCallback)
          jacklib.activate(jack.client)

        self.timer100.start()
        self.act_jack_render.setEnabled(canRender)
        self.b_jack_render.setEnabled(canRender)
        self.menu_Transport.setEnabled(True)
        self.group_transport.setEnabled(True)

        if (self.systray):
          self.systray.setActionEnabled("tools_render", canRender)

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(100)
        self.pb_dsp_load.update()

        self.init_jack()

    def jackStopped(self):
        #self.DBusReconnect()

        if (jack.client):
          #jacklib.deactivate(jack.client)
          #jacklib.client_close(jack.client)
          jack.client = None

        self.timer100.stop()
        self.act_jack_render.setEnabled(False)
        self.b_jack_render.setEnabled(False)
        self.menu_Transport.setEnabled(False)
        self.group_transport.setEnabled(False)

        if (self.systray):
          self.systray.setActionEnabled("tools_render", False)

        setBufferSize(self, jacksettings.getBufferSize())
        setSampleRate(self, jacksettings.getSampleRate())
        setRealTime(self, jacksettings.isRealtime())
        setXruns(self, -1)

        if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
          self.label_time.setText("00:00:00")
        elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
          self.label_time.setText("000|0|0000")
        elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
          self.label_time.setText("000'000'000")

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(0)
        self.pb_dsp_load.update()

        if (self.next_sample_rate):
          jack_sample_rate(self, self.next_sample_rate)

    def studioLoaded(self):
        DBus.ladish_studio = DBus.bus.get_object("org.ladish", "/org/ladish/Studio")
        DBus.ladish_graph = dbus.Interface(DBus.ladish_studio, 'org.ladish.GraphDict')
        DBus.ladish_app_iface = dbus.Interface(DBus.ladish_studio, 'org.ladish.AppSupervisor')
        DBus.patchbay = dbus.Interface(DBus.ladish_studio, 'org.jackaudio.JackPatchbay')

        self.label_first_time.setVisible(False)
        self.graphicsView.setVisible(True)
        self.miniCanvasPreview.setVisible(True)
        #if (self.miniCanvasPreview.is_initiated):
          #self.minicanvas_recheck_size()

        self.menu_Room.setEnabled(True)
        self.menu_Project.setEnabled(False)
        self.menu_Application.setEnabled(True)
        self.group_project.setEnabled(False)

        self.act_studio_rename.setEnabled(True)
        self.act_studio_unload.setEnabled(True)

        if (self.systray):
          self.systray.setActionEnabled("studio_rename", True)
          self.systray.setActionEnabled("studio_unload", True)

        self.init_studio()

    def studioUnloaded(self):
        DBus.patchbay = None
        DBus.ladish_studio = None
        DBus.ladish_graph = None
        DBus.ladish_app_iface = None

        self.last_item_type = None
        self.last_room_path = None

        self.label_first_time.setVisible(True)
        self.graphicsView.setVisible(False)
        self.miniCanvasPreview.setVisible(False)

        self.menu_Room.setEnabled(False)
        self.menu_Project.setEnabled(False)
        self.menu_Application.setEnabled(False)
        self.group_project.setEnabled(False)

        self.act_studio_start.setEnabled(False)
        self.act_studio_stop.setEnabled(False)
        self.act_studio_rename.setEnabled(False)
        self.act_studio_save.setEnabled(False)
        self.act_studio_save_as.setEnabled(False)
        self.act_studio_unload.setEnabled(False)

        if (self.systray):
          self.systray.setActionEnabled("studio_start", False)
          self.systray.setActionEnabled("studio_stop", False)
          self.systray.setActionEnabled("studio_rename", False)
          self.systray.setActionEnabled("studio_save", False)
          self.systray.setActionEnabled("studio_save_as", False)
          self.systray.setActionEnabled("studio_unload", False)

        self.b_studio_save.setEnabled(False)
        self.b_studio_save_as.setEnabled(False)

        self.treeWidget.clear()

        patchcanvas.clear()

    def studioStarted(self):
        self.act_studio_start.setEnabled(False)
        self.act_studio_stop.setEnabled(True)
        self.act_studio_save.setEnabled(True)
        self.act_studio_save_as.setEnabled(True)

        if (self.systray):
          self.systray.setActionEnabled("studio_start", False)
          self.systray.setActionEnabled("studio_stop", True)
          self.systray.setActionEnabled("studio_save", True)
          self.systray.setActionEnabled("studio_save_as", True)

        self.b_studio_save.setEnabled(True)
        self.b_studio_save_as.setEnabled(True)

    def studioStopped(self):
        self.act_studio_start.setEnabled(True)
        self.act_studio_stop.setEnabled(False)
        self.act_studio_save.setEnabled(False)
        self.act_studio_save_as.setEnabled(False)

        if (self.systray):
          self.systray.setActionEnabled("studio_start", True)
          self.systray.setActionEnabled("studio_stop", False)
          self.systray.setActionEnabled("studio_save", False)
          self.systray.setActionEnabled("studio_save_as", False)

        self.b_studio_save.setEnabled(False)
        self.b_studio_save_as.setEnabled(False)

    def canvas_callback(self, action, value1, value2, value_str):
        if (action == patchcanvas.ACTION_GROUP_INFO):
          pass

        elif (action == patchcanvas.ACTION_GROUP_RENAME):
          pass

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = value1
          DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_SPLIT, "true")

          patchcanvas.splitGroup(group_id)
          self.miniCanvasPreview.update()

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = value1
          DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_SPLIT, "false")

          patchcanvas.joinGroup(group_id)
          self.miniCanvasPreview.update()

        elif (action == patchcanvas.ACTION_PORT_INFO):
          this_port_id = value1

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          # Graph Ports
          for i in range(len(graph_ports)):
            group_id    = graph_ports[i][0]
            group_name  = graph_ports[i][1]
            group_ports = graph_ports[i][2]

            for j in range(len(group_ports)):
              port_id    = group_ports[j][0]
              port_name  = group_ports[j][1]
              port_flags = group_ports[j][2]
              port_type  = group_ports[j][3]
              if (this_port_id == port_id):
                break

            if (this_port_id == port_id):
              break

          if (DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_PORT, this_port_id, URI_A2J_PORT) == "yes"):
            port_flags_ = self.get_real_a2j_port_flags(group_name, port_name, jacklib.PortIsInput if (port_flags & jacklib.PortIsInput) else jacklib.PortIsOutput)
            if (port_flags_ != 0):
              port_flags = port_flags_

          flags = []
          if (port_flags & jacklib.PortIsInput):
            flags.append(self.tr("Input"))
          if (port_flags & jacklib.PortIsOutput):
            flags.append(self.tr("Output"))
          if (port_flags & jacklib.PortIsPhysical):
            flags.append(self.tr("Physical"))
          if (port_flags & jacklib.PortCanMonitor):
            flags.append(self.tr("Can Monitor"))
          if (port_flags & jacklib.PortIsTerminal):
            flags.append(self.tr("Terminal"))

          flags_text = ""
          for i in range(len(flags)):
            if (flags_text): flags_text+= " | "
            flags_text += flags[i]

          if (port_type == jacklib.AUDIO):
            type_text = self.tr("Audio")
          elif (port_type == jacklib.MIDI):
            type_text = self.tr("MIDI")
          else:
            type_text = self.tr("Unknown")

          port_full_name = group_name+":"+port_name

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Group ID:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%3</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%4</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Flags:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%7</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(group_id).arg(port_name).arg(port_id).arg(port_full_name).arg(flags_text).arg(type_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORT_RENAME):
          pass

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          port_a = value1
          port_b = value2
          DBus.patchbay.ConnectPortsByID(port_a, port_b)

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = value1

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][0]):
              port_a = self.connection_list[i][1]
              port_b = self.connection_list[i][2]
              break
          else:
            return

          DBus.patchbay.DisconnectPortsByID(port_a, port_b)

    def canvas_add_group(self, group_id, group_name, do_pos=True):
        room_list_names = self.get_room_list_names()

        split_try = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_SPLIT)

        if (split_try == "true"):
          split = patchcanvas.SPLIT_YES
        elif (split_try == "false"):
          split = patchcanvas.SPLIT_NO
        else:
          split = patchcanvas.SPLIT_UNDEF

        if (group_name in room_list_names or group_name in ("Capture", "Playback")):
          icon = patchcanvas.ICON_LADISH_ROOM
        else:
          icon = patchcanvas.ICON_APPLICATION

        patchcanvas.addGroup(group_id, group_name, split, icon)
        self.group_list.append([group_id, group_name])

        if (do_pos):
          x  = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_X)
          y  = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_Y)
          x2 = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_X_SPLIT)
          y2 = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_Y_SPLIT)

          if (x != None and y != None):
            if (x2 == None): x2 = float(x)+50
            if (y2 == None): y2 = float(y)+50
            patchcanvas.setGroupPos(group_id, float(x), float(y), float(x2), float(y2))

        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_remove_group(self, group_id, group_name):
        for i in range(len(self.group_list)):
          if (self.group_list[i][0] == group_id):
            self.group_list.pop(i)
            break
        else:
          return

        patchcanvas.removeGroup(group_id)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_add_port(self, port_id, port_name, port_mode, port_type, group_id, group_name, split, do_pos=True):
        for i in range(len(self.group_list)):
          if (group_id == self.group_list[i][0]):
            break
        else: #Hack for clients started with no ports
          self.canvas_add_group(group_id, group_name, do_pos)

        if (DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_PORT, port_id, URI_A2J_PORT) == "yes"):
          port_type = patchcanvas.PORT_TYPE_MIDI_A2J

          flags = self.get_real_a2j_port_flags(group_name, port_name, port_mode)
          if (flags & jacklib.PortIsPhysical):
            split = True

        patchcanvas.addPort(group_id, port_id, port_name, port_mode, port_type)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

        if not group_id in self.group_split_list:
          if (split):
            patchcanvas.splitGroup(group_id)
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          elif ( group_name == "Hardware Capture" or
                 group_name == "Hardware Playback" or
                 group_name == "Capture" or
                 group_name == "Playback"
               ):
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          self.group_split_list.append(group_id)

    def canvas_remove_port(self, port_id):
        patchcanvas.removePort(port_id)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_rename_port(self, port_id, port_name):
        patchcanvas.renamePort(port_id, port_name.split(":")[-1])
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_connect_ports(self, port_a, port_b):
        connection_id = self.last_connection_id
        patchcanvas.connectPorts(connection_id, port_a, port_b)
        self.connection_list.append([connection_id, port_a, port_b])
        self.last_connection_id += 1
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_disconnect_ports(self, port_a, port_b):
        for i in range(len(self.connection_list)):
          if ( (self.connection_list[i][1] == port_a and self.connection_list[i][2] == port_b) or
               (self.connection_list[i][2] == port_a and self.connection_list[i][1] == port_b) ):
            patchcanvas.disconnectPorts(self.connection_list[i][0])
            self.connection_list.pop(i)
            break
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def get_room_list_names(self):
        room_list_names = []
        room_list_dump = DBus.ladish_studio.GetRoomList()
        for i in range(len(room_list_dump)):
          room_path = room_list_dump[i][0]
          ladish_room = DBus.bus.get_object("org.ladish", room_path)
          room_name = QString(ladish_room.GetName())
          room_list_names.append(room_name)

        return room_list_names

    def get_real_a2j_port_flags(self, group_name, port_name, port_mode):
        str1 = "a2j:"+group_name+" "
        str2 = " (%s): " % ("capture" if (port_mode == patchcanvas.PORT_MODE_INPUT) else "playback") + port_name

        port_list = jacklib.get_ports(jack.client, None, None, 0)
        for i in range(len(port_list)):
            if (port_list[i].split("[")[0] == str1 and port_list[i].split("]")[-1] == str2):
              port_name = port_list[i]
              break
        else:
          return 0

        port_id = jacklib.port_by_name(jack.client, port_name)
        return jacklib.port_flags(port_id)

    def jack_clear_xruns(self):
        DBus.jack.ResetXruns()

    def init_jack(self):
        if (not jack.client): # Jack Crash/Bug ?
          self.menu_Transport.setEnabled(False)
          self.group_transport.setEnabled(False)

        buffer_size = int(DBus.jack.GetBufferSize())
        realtime = bool(DBus.jack.IsRealtime())
        sample_rate = int(DBus.jack.GetSampleRate())

        setBufferSize(self, buffer_size)
        setRealTime(self, realtime)
        setSampleRate(self, sample_rate)

        refreshDSPLoad(self)
        refreshTransport(self)
        self.refreshXruns()

    def init_studio(self):
        self.treeWidget.clear()

        studio_item = QTreeWidgetItem(ITEM_TYPE_STUDIO)
        studio_item.setText(0, QString(DBus.ladish_studio.GetName()))
        self.treeWidget.insertTopLevelItem(0, studio_item)

        self.treeWidget.setCurrentItem(studio_item)
        self.last_item_type = ITEM_TYPE_STUDIO
        self.last_room_path = None

        self.init_apps()

    def init_ports_prepare(self):
        pass

    def init_ports(self):
        # Refuse to init ports if we cant connect to jack
        if (not jack.client): return

        self.last_connection_id = 1
        self.group_list = []
        self.group_split_list = []
        self.connection_list = []

        graph_dump = DBus.patchbay.GetGraph(0)
        graph_version = graph_dump[0]
        graph_ports = graph_dump[1]
        graph_conns = graph_dump[2]

        # Graph Ports
        for i in range(len(graph_ports)):
          idx   = graph_ports[i][0]
          name  = graph_ports[i][1]
          ports = graph_ports[i][2]

          for j in range(len(ports)):
            port_id    = int(ports[j][0])
            port_name  = QStringStr(ports[j][1])
            port_flags = int(ports[j][2])
            port_type  = patchcanvas.PORT_TYPE_AUDIO_JACK if (int(ports[j][3]) == jacklib.AUDIO) else patchcanvas.PORT_TYPE_MIDI_JACK

            if (port_flags & jacklib.PortIsInput):
              port_mode = jacklib.PortIsInput
            elif (port_flags & jacklib.PortIsOutput):
              port_mode = jacklib.PortIsOutput
            else:
              print "Invalid port mode for", port_name
              port_mode = None

            self.canvas_add_port(port_id, port_name, port_mode, port_type, idx, name, (port_flags & jacklib.PortIsPhysical), False)

        # Graph Connections
        for i in range(len(graph_conns)):
          conn_source_id   = int(graph_conns[i][0])
          conn_source_name = QStringStr(graph_conns[i][1])
          conn_source_port_id   = int(graph_conns[i][2])
          conn_source_port_name = QStringStr(graph_conns[i][3])
          conn_target_id   = int(graph_conns[i][4])
          conn_target_name = QStringStr(graph_conns[i][5])
          conn_target_port_id   = int(graph_conns[i][6])
          conn_target_port_name = QStringStr(graph_conns[i][7])

          self.canvas_connect_ports(conn_source_port_id, conn_target_port_id)

        # Recheck all group positions
        for i in range(len(self.group_list)):
          group_id = self.group_list[i][0]

          x  = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_X)
          y  = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_Y)
          x2 = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_X_SPLIT)
          y2 = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_Y_SPLIT)

          if (x != None and y != None):
            if (x2 == None): x2 = float(x)+50
            if (y2 == None): y2 = float(y)+50
            patchcanvas.setGroupPos(group_id, float(x), float(y), float(x2), float(y2))

        QTimer.singleShot(1000 if (self.saved_settings['Canvas/FancyEyeCandy']) else 0, self.miniCanvasPreview.update)

    def init_apps(self):
        studio_iface = dbus.Interface(DBus.ladish_studio, 'org.ladish.AppSupervisor')
        studio_app_list_dump = DBus.ladish_app_iface.GetAll()
        studio_item = self.treeWidget.topLevelItem(0)

        studio_graph_version = studio_app_list_dump[0]
        for i in range(len(studio_app_list_dump[1])):
          number   = studio_app_list_dump[1][i][0]
          name     = studio_app_list_dump[1][i][1]
          active   = studio_app_list_dump[1][i][2]
          terminal = studio_app_list_dump[1][i][3]
          level    = studio_app_list_dump[1][i][4]
          text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
          item = QTreeWidgetItem(ITEM_TYPE_STUDIO_APP)
          item.setText(0, text)
          item.properties = [number, name, active, terminal, level]
          studio_item.addChild(item)

        room_list_dump = DBus.ladish_studio.GetRoomList()
        for i in range(len(room_list_dump)):
          room_path = room_list_dump[i][0]
          ladish_room = DBus.bus.get_object("org.ladish", room_path)
          room_name = ladish_room.GetName()

          room_iface = dbus.Interface(ladish_room, 'org.ladish.AppSupervisor')
          room_app_list_dump = room_iface.GetAll()
          room_item = self.addNewRoom(room_path, room_name)

          room_graph_version = studio_app_list_dump[0]
          for j in range(len(room_app_list_dump[1])):
            number   = room_app_list_dump[1][j][0]
            name     = room_app_list_dump[1][j][1]
            active   = room_app_list_dump[1][j][2]
            terminal = room_app_list_dump[1][j][3]
            level    = room_app_list_dump[1][j][4]
            text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
            item = QTreeWidgetItem(ITEM_TYPE_ROOM_APP)
            item.setText(0, text)
            item.properties = [number, name, active, terminal, level]
            room_item.addChild(item)

        self.treeWidget.expandAll()

    def func_studio_new(self):
        dialog = StudioNameW(self, False)
        if (dialog.exec_()):
          DBus.ladish_control.NewStudio(dbus.String(dialog.studio_to_return))

    def func_studio_load_m(self, studio_name):
        DBus.ladish_control.LoadStudio(studio_name)

    def func_studio_load_b(self):
        dialog = StudioListW(self)
        if (dialog.exec_()):
          DBus.ladish_control.LoadStudio(dbus.String(dialog.studio_to_return))

    def func_studio_start(self):
        DBus.ladish_studio.Start()

    def func_studio_stop(self):
        DBus.ladish_studio.Stop()

    def func_studio_rename(self):
        dialog = StudioNameW(self, True)
        if (dialog.exec_()):
          DBus.ladish_studio.Rename(dbus.String(dialog.studio_to_return))

    def func_studio_save(self):
        DBus.ladish_studio.Save()

    def func_studio_save_as(self):
        dialog = StudioNameW(self, True, False)
        if (dialog.exec_()):
          DBus.ladish_studio.SaveAs(dbus.String(dialog.studio_to_return))

    def func_studio_unload(self):
        DBus.ladish_studio.Unload()

    def func_studio_delete_m(self, studio_name):
        DBus.ladish_control.DeleteStudio(dbus.String(studio_name))

    def func_room_create(self):
        dialog = CreateRoomW(self)
        if (dialog.exec_()):
          new_room = dialog.room_to_return
          name     = new_room[0]
          template = new_room[1]
          DBus.ladish_studio.CreateRoom(name, template)

    def func_room_delete_m(self, room_name):
        DBus.ladish_studio.DeleteRoom(dbus.String(room_name))

    def func_project_new(self):
        dialog = SaveProjectW(self)
        dialog.setWindowTitle(self.tr("New Project"))

        if (dialog.exec_()):
          path = dbus.String(dialog.project_to_return[0])
          name = dbus.String(dialog.project_to_return[1])
          DBus.ladish_room.SaveProject(path, name)

    def func_project_load(self):
        project_path = QFileDialog.getExistingDirectory(self, self.tr("Open Project"), self.saved_settings["Main/DefaultProjectFolder"])
        if (project_path.isEmpty()):
          return
        else:
          if (QFile.exists(project_path+"/ladish-project.xml")):
            DBus.ladish_room.LoadProject(unicode(project_path))
          else:
            QMessageBox.warning(self, self.tr("Warning"), self.tr("The selected folder does not contain a ladish project!"))

    def func_project_save(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          path = dbus.String(project_properties[u'dir'])
          name = dbus.String(project_properties[u'name'])
          DBus.ladish_room.SaveProject(path, name)
        else:
          dialog = SaveProjectW(self)
          if (dialog.exec_()):
            path = dbus.String(dialog.project_to_return[0])
            name = dbus.String(dialog.project_to_return[1])
            DBus.ladish_room.SaveProject(path, name)

    def func_project_save_as(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          path = dbus.String(project_properties[u'dir'])
          name = dbus.String(project_properties[u'name'])
          dialog = SaveProjectW(self, path, name, True)
        else:
          dialog = SaveProjectW(self)

        if (dialog.exec_()):
          path = dbus.String(dialog.project_to_return[0])
          name = dbus.String(dialog.project_to_return[1])
          DBus.ladish_room.SaveProject(path, name)

    def func_project_unload(self):
        DBus.ladish_room.UnloadProject()

    def func_project_properties(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        name = project_properties[u'name']
        dir_ = project_properties[u'dir']

        if ("description" in project_properties.keys()):
          description = project_properties[u'description']
        else:
          description = ""

        if ("notes" in project_properties.keys()):
          notes = project_properties[u'notes']
        else:
          notes = ""

        dialog = ProjectPropertiesW(self, name, description, notes)

        if (dialog.exec_()):
          save_now = dialog.data_to_return[0]
          new_name = dbus.String(dialog.data_to_return[1])
          description = dbus.String(dialog.data_to_return[2])
          notes = dbus.String(dialog.data_to_return[3])

          DBus.ladish_room.SetProjectDescription(description)
          DBus.ladish_room.SetProjectNotes(notes)

          if (save_now):
            DBus.ladish_room.SaveProject(dbus.String(dir_), new_name)

    def func_app_add_new(self):
        if (self.saved_settings['Apps/Database'] == "LADISH"):
          dialog = AddNewW(self)
          if (dialog.exec_()):
            new_app = dialog.app_to_return
            command  = new_app[0]
            name     = new_app[1]
            terminal = new_app[2]
            level    = new_app[3]
            DBus.ladish_app_iface.RunCustom(terminal, command, name, level)

        elif (self.saved_settings['Apps/Database'] == "Klaudia"):
          url_folder = ""
          if (self.last_item_type == ITEM_TYPE_STUDIO or self.last_item_type == ITEM_TYPE_STUDIO_APP):
            url_folder = self.saved_settings['Main/DefaultProjectFolder']
            is_room = False

          elif (self.last_item_type == ITEM_TYPE_ROOM or self.last_item_type == ITEM_TYPE_ROOM_APP):
            project_properties_dump = DBus.ladish_room.GetProjectProperties()
            graph_version = project_properties_dump[0]
            project_properties = project_properties_dump[1]

            if len(project_properties):
              url_folder = QString(project_properties[u'dir'])
              is_room = True
            else:
              url_folder = self.saved_settings['Main/DefaultProjectFolder']
              is_room = False

          else:
            print "Invalid last_item_type"
            return

          sample_rate = self.cb_sample_rate.currentText().replace("*","").toInt()[0]
          bpm = self.sb_bpm.cleanText().toInt()[0]
          dialog = AddNewKlaudiaW(self, DBus.ladish_app_iface, url_folder, is_room, sample_rate, bpm)
          dialog.exec_()

        else:
          print "Unsupported database"
          return

    def func_app_run_custom(self):
        dialog = RunCustomW(self)
        if (dialog.exec_()):
          new_app = dialog.app_to_return
          command  = new_app[0]
          name     = new_app[1]
          terminal = new_app[2]
          level    = new_app[3]
          DBus.ladish_app_iface.RunCustom(terminal, command, name, level)

    def addNewRoom(self, room_path, room_name):
        room_object = DBus.bus.get_object("org.ladish", room_path)
        project_properties_dump = room_object.GetProjectProperties()

        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          project_path = project_properties[u'dir']
          project_name = project_properties[u'name']
          item_string = "("+project_name+")"
        else:
          project_path = None
          project_name = None
          item_string = ""

        item = QTreeWidgetItem(ITEM_TYPE_ROOM)
        item.properties = [room_path, room_name]
        item.setText(0, QString("%1 %2").arg(room_name).arg(item_string))
        index = int(room_path.replace("/org/ladish/Room",""))

        itemx = self.treeWidget.topLevelItem(index)
        if (itemx != None):
          if (not itemx.isVisible()):
            self.treeWidget.takeTopLevelItem(index)
          return

        for i in range(index):
          if (not self.treeWidget.topLevelItem(i)):
            fake_item = QTreeWidgetItem(-1)
            self.treeWidget.insertTopLevelItem(i, fake_item)
            fake_item.setHidden(True)

        self.treeWidget.insertTopLevelItem(index, item)
        return item

    def updateMenuStudioList(self, menu, function):
        menu.clear()
        studio_list_dump = DBus.ladish_control.GetStudioList()
        if (len(studio_list_dump) == 0):
            act_no_studio = QAction(self.tr("Empty studio list"), menu)
            act_no_studio.setEnabled(False)
            menu.addAction(act_no_studio)
        else:
          for i in range(len(studio_list_dump)):
            studio_name = studio_list_dump[i][0]
            act_x_studio = QAction(studio_name, menu)
            menu.addAction(act_x_studio)
            self.connect(act_x_studio, SIGNAL("triggered()"), lambda name=studio_name: function(name))

    def updateMenuRoomList(self):
        self.menu_room_delete.clear()
        if (bool(DBus.ladish_control.IsStudioLoaded())):
          room_list_dump = DBus.ladish_studio.GetRoomList()
          if (len(room_list_dump) == 0):
            self.createEmptyMenuRoomActon()
          else:
            for i in range(len(room_list_dump)):
              ladish_room = DBus.bus.get_object("org.ladish", room_list_dump[i][0])
              room_name = ladish_room.GetName()
              act_x_room = QAction(room_name, self.menu_room_delete)
              self.menu_room_delete.addAction(act_x_room)
              self.connect(act_x_room, SIGNAL("triggered()"), lambda name=room_name: self.func_room_delete_m(name))
        else:
          self.createEmptyMenuRoomActon()

    def createEmptyMenuRoomActon(self):
        act_no_room = QAction(self.tr("Empty room list"), self.menu_room_delete)
        act_no_room.setEnabled(False)
        self.menu_room_delete.addAction(act_no_room)

    def updateMenuProjectList(self):
        self.menu_project_load.clear()
        act_project_load = QAction(self.tr("Load from folder..."), self.menu_project_load)
        self.menu_project_load.addAction(act_project_load)
        self.connect(act_project_load, SIGNAL("triggered()"), self.func_project_load)

        ladish_recent_iface = dbus.Interface(DBus.ladish_room, "org.ladish.RecentItems")
        proj_list_dump = ladish_recent_iface.get(RECENT_PROJECTS_STORE_MAX_ITEMS)

        if (len(proj_list_dump) > 0):
          self.menu_project_load.addSeparator()
          for i in range(len(proj_list_dump)):
            proj_path = proj_list_dump[i][0]
            if ("name" in proj_list_dump[i][1].keys()):
              proj_name = proj_list_dump[i][1]['name']
            else:
              continue

            act_x_proj = QAction(proj_name, self.menu_project_load)
            self.menu_project_load.addAction(act_x_proj)
            self.connect(act_x_proj, SIGNAL("triggered()"), lambda path=proj_path: self.func_x_load(path))

    def func_x_load(self, path):
        DBus.ladish_room.LoadProject(unicode(path))

    def checkCurrentRoom(self):
        item = self.treeWidget.currentItem()
        room_path = None

        if not item:
          return

        if (item.type() == ITEM_TYPE_STUDIO or item.type() == ITEM_TYPE_STUDIO_APP):
          self.menu_Project.setEnabled(False)
          self.group_project.setEnabled(False)
          self.menu_Application.setEnabled(True)
          DBus.ladish_room = None
          DBus.ladish_app_iface = dbus.Interface(DBus.ladish_studio, "org.ladish.AppSupervisor")
          ITEM_TYPE = ITEM_TYPE_STUDIO

        elif (item.type() == ITEM_TYPE_ROOM or item.type() == ITEM_TYPE_ROOM_APP):
          self.menu_Project.setEnabled(True)
          self.group_project.setEnabled(True)
          if (item.type() == ITEM_TYPE_ROOM):
            room_path = item.properties[0]
          elif (item.type() == ITEM_TYPE_ROOM_APP):
            room_path = item.parent().properties[0]
          else:
            print "Couldn't find room_path"
            return

          DBus.ladish_room = DBus.bus.get_object("org.ladish", room_path)
          DBus.ladish_app_iface = dbus.Interface(DBus.ladish_room, "org.ladish.AppSupervisor")
          ITEM_TYPE = ITEM_TYPE_ROOM

          project_properties_dump = DBus.ladish_room.GetProjectProperties()
          has_project = (len(project_properties_dump[1]) > 0) #project_properties

          self.act_project_save.setEnabled(has_project)
          self.act_project_save_as.setEnabled(has_project)
          self.act_project_unload.setEnabled(has_project)
          self.act_project_properties.setEnabled(has_project)
          self.b_project_save.setEnabled(has_project)
          self.b_project_save_as.setEnabled(has_project)
          self.menu_Application.setEnabled(has_project)

        else:
          print "Invalid ITEM_TYPE"
          return

        if (ITEM_TYPE != self.last_item_type or room_path != self.last_room_path):
          if (ITEM_TYPE == ITEM_TYPE_STUDIO):
            object_path = DBus.ladish_studio
          elif (ITEM_TYPE == ITEM_TYPE_ROOM):
            object_path = DBus.ladish_room
          else:
            print "Invalid ITEM_TYPE (2)"
            return

          patchcanvas.clear()
          DBus.patchbay = dbus.Interface(object_path, 'org.jackaudio.JackPatchbay')
          DBus.ladish_graph = dbus.Interface(object_path, 'org.ladish.GraphDict')
          self.init_ports()

        self.last_item_type = ITEM_TYPE
        self.last_room_path = room_path

    def doubleClickedAppList(self, item, row):
        if (item.type() == ITEM_TYPE_STUDIO_APP or item.type() == ITEM_TYPE_ROOM_APP):
          if (not item.properties[2]):
            DBus.ladish_app_iface.StartApp(item.properties[0])

    def showAppListCustomMenu(self, pos):
        item = self.treeWidget.currentItem()
        if (item):
          cMenu = QMenu()
          if (item.type() == ITEM_TYPE_STUDIO):
            act_x_add_new = cMenu.addAction(self.tr("Add New..."))
            act_x_run_custom = cMenu.addAction(self.tr("Run Custom..."))
            cMenu.addSeparator()
            act_x_create_room = cMenu.addAction(self.tr("Create Room..."))

            act_x_add_new.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_run_custom.setIcon(QIcon.fromTheme("system-run", QIcon(":/16x16/system-run.png")))
            act_x_create_room.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_add_new.setEnabled(self.act_app_add_new.isEnabled())

            self.connect(act_x_add_new, SIGNAL("triggered()"), self.func_app_add_new)
            self.connect(act_x_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)
            self.connect(act_x_create_room, SIGNAL("triggered()"), self.func_room_create)

          elif (item.type() == ITEM_TYPE_ROOM):
            act_x_add_new = cMenu.addAction(self.tr("Add New..."))
            act_x_run_custom = cMenu.addAction(self.tr("Run Custom..."))
            cMenu.addSeparator()
            act_x_new = cMenu.addAction(self.tr("New Project..."))
            cMenu.addMenu(self.menu_project_load)
            act_x_save = cMenu.addAction(self.tr("Save Project"))
            act_x_save_as = cMenu.addAction(self.tr("Save Project As..."))
            act_x_unload = cMenu.addAction(self.tr("Unload Project"))
            cMenu.addSeparator()
            act_x_properties = cMenu.addAction(self.tr("Project Properties..."))
            cMenu.addSeparator()
            act_x_delete_room = cMenu.addAction(self.tr("Delete Room"))

            act_x_add_new.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_run_custom.setIcon(QIcon.fromTheme("system-run", QIcon(":/16x16/system-run.png")))
            act_x_new.setIcon(QIcon.fromTheme("document-new", QIcon(":/16x16/document-new.png")))
            act_x_save.setIcon(QIcon.fromTheme("document-save", QIcon(":/16x16/document-save.png")))
            act_x_save_as.setIcon(QIcon.fromTheme("document-save-as", QIcon(":/16x16/document-save-as.png")))
            act_x_unload.setIcon(QIcon.fromTheme("dialog-close", QIcon(":/16x16/dialog-close.png")))
            act_x_properties.setIcon(QIcon.fromTheme("edit-rename", QIcon(":/16x16/edit-rename.png")))
            act_x_delete_room.setIcon(QIcon.fromTheme("edit-delete", QIcon(":/16x16/edit-delete.png")))

            act_x_add_new.setEnabled(self.menu_Application.isEnabled() and self.act_app_add_new.isEnabled())

            project_properties_dump = DBus.ladish_room.GetProjectProperties()
            if (len(project_properties_dump[1]) <= 0):
              act_x_run_custom.setEnabled(False)
              act_x_save.setEnabled(False)
              act_x_save_as.setEnabled(False)
              act_x_unload.setEnabled(False)
              act_x_properties.setEnabled(False)

            self.connect(act_x_add_new, SIGNAL("triggered()"), self.func_app_add_new)
            self.connect(act_x_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)
            self.connect(act_x_new, SIGNAL("triggered()"), self.func_project_new)
            self.connect(act_x_save, SIGNAL("triggered()"), self.func_project_save)
            self.connect(act_x_save_as, SIGNAL("triggered()"), self.func_project_save_as)
            self.connect(act_x_unload, SIGNAL("triggered()"), self.func_project_unload)
            self.connect(act_x_properties, SIGNAL("triggered()"), self.func_project_properties)
            self.connect(act_x_delete_room, SIGNAL("triggered()"), lambda room_name=DBus.ladish_room.GetName(): self.func_room_delete_m(room_name))

          elif (item.type() == ITEM_TYPE_STUDIO_APP or item.type() == ITEM_TYPE_ROOM_APP):
            number = item.properties[0]
            name   = item.properties[1]
            active = item.properties[2]
            terminal = item.properties[3]
            level  = item.properties[4]

            if (active):
              act_x_stop = cMenu.addAction(self.tr("Stop"))
              act_x_kill = cMenu.addAction(self.tr("Kill"))
              act_x_stop.setIcon(QIcon.fromTheme("media-playback-stop", QIcon(":/16x16/media-playback-stop.png")))
              act_x_kill.setIcon(QIcon.fromTheme("dialog-close", QIcon(":/16x16/dialog-close.png")))
              self.connect(act_x_stop, SIGNAL("triggered()"), lambda: self.func_x_stop(number))
              self.connect(act_x_kill, SIGNAL("triggered()"), lambda: self.func_x_kill(number))
            else:
              act_x_start = cMenu.addAction(self.tr("Start"))
              act_x_start.setIcon(QIcon.fromTheme("media-playback-start", QIcon(":/16x16/media-playback-start.png")))
              self.connect(act_x_start, SIGNAL("triggered()"), lambda: self.func_x_start(number))
            act_x_properties = cMenu.addAction(self.tr("Properties"))
            cMenu.addSeparator()
            act_x_remove = cMenu.addAction(self.tr("Remove"))
            act_x_properties.setIcon(QIcon.fromTheme("edit-rename", QIcon(":/16x16/edit-rename.png")))
            act_x_remove.setIcon(QIcon.fromTheme("edit-delete", QIcon(":/16x16/edit-delete.png")))
            self.connect(act_x_properties, SIGNAL("triggered()"), lambda: self.func_x_properties(number))
            self.connect(act_x_remove, SIGNAL("triggered()"), lambda: self.func_x_remove(number))

        cMenu.exec_(QCursor.pos())

    def func_x_start(self, number):
        DBus.ladish_app_iface.StartApp(number)

    def func_x_stop(self, number):
        DBus.ladish_app_iface.StopApp(number)

    def func_x_kill(self, number):
        DBus.ladish_app_iface.KillApp(number)

    def func_x_properties(self, number):
        info_dump = DBus.ladish_app_iface.GetAppProperties(number)
        name     = info_dump[0]
        command  = info_dump[1]
        active   = info_dump[2]
        terminal = info_dump[3]
        level    = info_dump[4]
        dialog = RunCustomW(self, command, name, terminal, level, active)
        dialog.setWindowTitle(self.tr("App properties"))
        if (dialog.exec_()):
          ret = dialog.app_to_return
          command  = ret[0]
          name     = ret[1]
          terminal = ret[2]
          level    = ret[3]
          DBus.ladish_app_iface.SetAppProperties(number, name, command, terminal, level)

    def func_x_remove(self, number):
        DBus.ladish_app_iface.RemoveApp(number)

    def signal_StudioRenamed(self, args):
        new_studio_name = args[0]
        self.treeWidget.topLevelItem(0).setText(0, new_studio_name)

    def signal_ClientAppeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        self.canvas_add_group(group_id, group_name)

    def signal_ClientDisappeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        self.canvas_remove_group(group_id, group_name)

    def signal_PortAppeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        port_flags = args[5]
        port_type  = patchcanvas.PORT_TYPE_AUDIO_JACK if (int(args[6]) == jacklib.AUDIO) else patchcanvas.PORT_TYPE_MIDI_JACK

        if (port_flags & jacklib.PortIsInput):
          port_mode = jacklib.PortIsInput
        elif (port_flags & jacklib.PortIsOutput):
          port_mode = jacklib.PortIsOutput
        else:
          print "Invalid port mode for", port_name
          port_mode = None

        self.canvas_add_port(port_id, port_name, port_mode, port_type, group_id, group_name, (port_flags & jacklib.PortIsPhysical))

    def signal_PortDisappeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        self.canvas_remove_port(port_id)

    def signal_PortRenamed(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        new_name   = args[5]
        self.canvas_rename_port(port_id, new_name)

    def signal_PortsConnected(self, args):
        graph_version = args[0]
        source_group_id   = args[1]
        source_group_name = args[2]
        source_port_id    = args[3]
        source_port_name  = args[4]
        target_group_id   = args[5]
        target_group_name = args[6]
        target_port_id    = args[7]
        target_port_name  = args[8]
        self.canvas_connect_ports(source_port_id, target_port_id)

    def signal_PortsDisconnected(self, args):
        graph_version = args[0]
        source_group_id   = args[1]
        source_group_name = args[2]
        source_port_id    = args[3]
        source_port_name  = args[4]
        target_group_id   = args[5]
        target_group_name = args[6]
        target_port_id    = args[7]
        target_port_name  = args[8]
        self.canvas_disconnect_ports(source_port_id, target_port_id)

    def signal_AppStateChanged(self, path, args):
        graph_version = args[0]
        number   = args[1]
        name     = args[2]
        active   = args[3]
        terminal = args[4]
        level    = args[5]

        if (path == "/org/ladish/Studio"):
          index = 0
        else:
          index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)
        for i in range(top_level_item.childCount()):
          if (top_level_item.child(i).properties[0] == number):
            if (top_level_item.child(i).properties[1] != name):
              patchcanvas.renameGroup(top_level_item.child(i).properties[1], name)
            top_level_item.child(i).properties = [number, name, active, terminal, level]
            top_level_item.child(i).setText(0, QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name))
            break

    def signal_AppAdded(self, path, args):
        graph_version = args[0]
        number   = args[1]
        name     = args[2]
        active   = args[3]
        terminal = args[4]
        level    = args[5]

        if (path == "/org/ladish/Studio"):
          index = 0
          ITEM_TYPE = ITEM_TYPE_STUDIO_APP
        else:
          index = int(path.replace("/org/ladish/Room",""))
          ITEM_TYPE = ITEM_TYPE_ROOM_APP

        top_level_item = self.treeWidget.topLevelItem(index)

        for i in range(top_level_item.childCount()):
          if (top_level_item.child(i).properties[0] == number):
            # App was added before, probably during reload/init
            return

        text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
        item = QTreeWidgetItem(ITEM_TYPE)
        item.setText(0, text)
        item.properties = [number, name, active, terminal, level]
        top_level_item.addChild(item)

    def signal_AppRemoved(self, path, args):
        graph_version = args[0]
        number   = args[1]

        if (path == "/org/ladish/Studio"):
          index = 0
        else:
          index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)
        for i in range(top_level_item.childCount()):
          if (top_level_item.child(i).properties[0] == number):
            top_level_item.takeChild(i)
            break

    def signal_RoomAppeared(self, args):
        path = args[0]
        name = args[1][u'name']
        #if (u'template' in args[1].keys()):
          #template = args[1][u'template']
        #else:
          #template = None
        index = int(path.replace("/org/ladish/Room",""))

        item = self.treeWidget.topLevelItem(index)
        if (item != None):
          if (not item.isVisible()):
            self.treeWidget.takeTopLevelItem(index)
          return

        for i in range(index):
          if (not self.treeWidget.topLevelItem(i)):
            fake_item = QTreeWidgetItem(-1)
            self.treeWidget.insertTopLevelItem(i, fake_item)
            fake_item.setHidden(True)

        room_item = QTreeWidgetItem(ITEM_TYPE_ROOM)
        room_item.properties = [path, name]
        room_item.setText(0, QString(name))
        self.treeWidget.insertTopLevelItem(index, room_item)

    def signal_RoomDisappeared(self, args):
        path = args[0]
        name = args[1][u'name']
        #if (u'template' in args[1].keys()):
          #template = args[1][u'template']
        #else:
          #template = None
        index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)

        if not top_level_item:
          while (True):
            index -= 1
            top_level_item = self.treeWidget.topLevelItem(index)
            if (top_level_item != None):
              break
          else:
            print "Error, invalid index"
            return

        for i in range(top_level_item.childCount()):
          top_level_item.takeChild(i)

        self.treeWidget.takeTopLevelItem(index)

    def signal_RoomChanged(self, args):
        print "TODO", args

    def signal_ProjectPropertiesChanged(self, path, args):
        graph_version = args[0]
        properties = args[1]
        has_project = (len(properties) > 0)

        if (has_project):
          room_path = properties[u'dir']
          room_name = properties[u'name']
          item_string = "("+room_name+")"
        else:
          room_path = None
          room_name = None
          item_string = ""

        self.act_project_save.setEnabled(has_project)
        self.act_project_save_as.setEnabled(has_project)
        self.act_project_unload.setEnabled(has_project)
        self.act_project_properties.setEnabled(has_project)
        self.b_project_save.setEnabled(has_project)
        self.b_project_save_as.setEnabled(has_project)
        self.menu_Application.setEnabled(has_project)

        index = int(path.replace("/org/ladish/Room",""))
        item = self.treeWidget.topLevelItem(index)
        item.setText(0, QString("%1 %2").arg(item.properties[1]).arg(item_string))

    def signal_QueueExecutionHalted(self):
        self.emit(SIGNAL("QueueExecutionHalted"))

    def signal_QueueExecutionHalted_exec(self):
        log_path = os.path.join(os.getenv("HOME"), ".log", "ladish", "ladish.log")
        if (os.path.exists(log_path)):
          log_file = open(log_path)
          log_text = log_file.read().split("ERROR: ")[-1].split("\n")[0].replace("[31m","").replace("[33m","").replace("[0m","")
          log_file.close()
        else:
          log_text = None

        msgbox = QMessageBox(QMessageBox.Critical, self.tr("Execution Halted"), self.tr(
                             "Something went wrong with ladish so the last action was not sucessful.\n"), QMessageBox.Ok, self)
        msgbox.setInformativeText(self.tr("You can check the ladish log file (or click in the 'Show Details' button) to find out what went wrong."))
        if (log_text): msgbox.setDetailedText(log_text)
        msgbox.show()

    def signal_CleanExit(self, args):
        self.timer1000.stop()
        QTimer.singleShot(1000, self.DBusReconnect)
        QTimer.singleShot(1500, self.timer1000.start)

    def JackShutdownCallback(self, arg=None):
        if (DEBUG): print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _ShutdownCallback(self):
        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

    def refreshXruns(self):
        xruns = int(DBus.jack.GetXruns())
        setXruns(self, xruns)

    def refreshDBusFix(self):
        # Dummy function to force refresh of ladish DBus
        DBus.ladish_control.GetStudioList()

    def configureClaudia(self):
        ClaudiaSettingsW(self).exec_()

    def aboutClaudia(self):
        QMessageBox.about(self, self.tr("About Claudia"), self.tr("<h3>Claudia</h3>"
            "<br>Version %1"
            "<br>Claudia is a Graphical User Interface to LADISH.<br>"
            "<br>Copyright (C) 2010-2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.settings.setValue("SplitterSizes", self.splitter.saveState())
        self.settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())
        self.settings.setValue("ShowStatusbar", self.frame_statusbar.isVisible())
        self.settings.setValue("TransportView", self.selected_transport_view)

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

        splitter_sizes = self.settings.value("SplitterSizes").toByteArray()
        if (splitter_sizes):
          self.splitter.restoreState(splitter_sizes)
        else:
          self.splitter.setSizes((100, 400))

        show_toolbar = self.settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        show_statusbar = self.settings.value("ShowStatusbar", True).toBool()
        self.act_settings_show_statusbar.setChecked(show_statusbar)
        self.frame_statusbar.setVisible(show_statusbar)

        transport_set_view(self, self.settings.value("TransportView", TRANSPORT_VIEW_HMS).toInt()[0])

        self.saved_settings = {
          "Main/DefaultProjectFolder": self.settings.value("Main/DefaultProjectFolder", os.getenv("HOME")+"/ladish-projects").toString(),
          "Main/UseSystemTray": self.settings.value("Main/UseSystemTray", True).toBool(),
          "Main/CloseToTray": self.settings.value("Main/CloseToTray", False).toBool(),
          "Main/RefreshInterval": self.settings.value("Main/RefreshInterval", 100).toInt()[0],
          "Canvas/Theme": self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme)).toString(),
          "Canvas/BezierLines": self.settings.value("Canvas/BezierLines", True).toBool(),
          "Canvas/AutoHideGroups": self.settings.value("Canvas/AutoHideGroups", True).toBool(),
          "Canvas/FancyEyeCandy": self.settings.value("Canvas/FancyEyeCandy", False).toBool(),
          "Canvas/UseOpenGL": self.settings.value("Canvas/UseOpenGL", False).toBool(),
          "Canvas/Antialiasing": self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": self.settings.value("Canvas/TextAntialiasing", True).toBool(),
          "Canvas/HighQualityAntialiasing": self.settings.value("Canvas/HighQualityAntialiasing", False).toBool(),
          "Apps/Database": self.settings.value("Apps/Database", "LADISH").toString()
        }

    def systray_closed(self):
        self.hide()
        self.close()

    def systray_clicked_callback(self):
        if (self.isVisible()):
          self.systray.setActionText("show", QStringStr(self.tr("Restore")))
          self.hide()
        else:
          self.systray.setActionText("show", QStringStr(self.tr("Minimize")))
          showWindow(self)

    def timerEvent(self, event):
        if (self.gtk_timer_helper and event.timerId() == self.gtk_timer_helper):
          self.repaint()
        QMainWindow.timerEvent(self, event)

    def resizeEvent(self, event):
        QTimer.singleShot(0, self.minicanvas_recheck_size)
        QMainWindow.resizeEvent(self, event)

    def closeEvent(self, event):
        self.saveSettings()
        if (self.systray):
          if (self.saved_settings["Main/CloseToTray"] and self.systray.isTrayAvailable() and self.isVisible()):
            self.hide()
            self.systray.setActionText("show", QStringStr(gui.tr("Restore")))
            event.ignore()
            return
          self.systray.close()
        QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Claudia")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/svg/claudia.svg"))

    # Show GUI
    gui = ClaudiaMainW()

    if (gui.systray and "--minimized" in app.arguments()):
      gui.hide()
      gui.systray.setActionText("show", QStringStr(gui.tr("Restore")))
    else:
      gui.show()

    # Set-up custom signal handling
    set_up_signals(gui)

    # App-Loop
    if (gui.systray):
      ret = gui.systray.exec_(app)
    else:
      ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
