/* Code for OSS related tasks */

#include "carla-bridge.h"
#include "osc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

// Static global objects
static UiData* ui = nullptr;
static const char* plugin_name = nullptr;

// Global OSC stuff
lo_server_thread global_osc_server_thread = nullptr;
const char* global_osc_server_path = nullptr;
OscData global_osc_data = { nullptr, nullptr };

void osc_init(UiData* ui_, const char* plugin_name_, const char* osc_url)
{
    ui = ui_;
    plugin_name = plugin_name_;

    const char* host = lo_url_get_hostname(osc_url);
    const char* port = lo_url_get_port(osc_url);

    global_osc_data.path   = lo_url_get_path(osc_url);
    global_osc_data.target = lo_address_new(host, port);

    free((void*)host);
    free((void*)port);

    global_osc_server_thread = lo_server_thread_new(nullptr, osc_error_handler);

    const char* osc_thread_path = lo_server_thread_get_url(global_osc_server_thread);

    // Replace ' ' by '_' on plugin_name
    char* fixed_name = strdup(plugin_name);
    for (unsigned int i=0; i<strlen(fixed_name); i++)
    {
        if (fixed_name[i] == ' ')
            fixed_name[i] = '_';
    }

    char osc_path_tmp[strlen(osc_thread_path)+strlen(fixed_name)+1];
    sprintf(osc_path_tmp, "%s%s", osc_thread_path, fixed_name);
    global_osc_server_path = strdup(osc_path_tmp);

    free((void*)fixed_name);
    free((void*)osc_thread_path);

    lo_server_thread_add_method(global_osc_server_thread, nullptr, nullptr, osc_message_handler, nullptr);
    lo_server_thread_start(global_osc_server_thread);
}

void osc_close()
{
    osc_clear_data(&global_osc_data);

    lo_server_thread_stop(global_osc_server_thread);
    lo_server_thread_del_method(global_osc_server_thread, nullptr, nullptr);
    lo_server_thread_free(global_osc_server_thread);

    free((void*)global_osc_server_path);
}

void osc_clear_data(OscData* osc_data)
{
    if (osc_data->path)
        free((void*)osc_data->path);

    if (osc_data->target)
        lo_address_free(osc_data->target);

    osc_data->path = nullptr;
    osc_data->target = nullptr;
}

void osc_error_handler(int num, const char* msg, const char* path)
{
    std::cerr << "osc_error_handler(" << num << ", " << msg << ", " << path << ")" << std::endl;
}

int osc_message_handler(const char* path, const char* /* types */, lo_arg** argv, int /* argc */, void* /* data */, void* /* user_data */)
{
    char method[32];
    memset(method, 0, sizeof(char)*24);

    unsigned int mindex = strlen(plugin_name)+2;

    for (unsigned int i=mindex; i<strlen(path) && i-mindex<24; i++)
        method[i-mindex] = path[i];

    if (strcmp(method, "control") == 0)
        return osc_control_handler(argv);
    else if (strcmp(method, "program") == 0)
        return osc_program_handler(argv);
    else if (strcmp(method, "midi_program") == 0)
        return osc_midi_program_handler(argv);
    else if (strcmp(method, "note_on") == 0)
        return osc_note_on_handler(argv);
    else if (strcmp(method, "note_off") == 0)
        return osc_note_off_handler(argv);
    else if (strcmp(method, "show") == 0)
        return osc_show_handler();
    else if (strcmp(method, "hide") == 0)
        return osc_hide_handler();
    else if (strcmp(method, "quit") == 0)
        return osc_quit_handler();
#ifdef WANT_EXTRA_OSC_SUPPORT
    else if (strcmp(method, "set_parameter_midi_channel") == 0)
        return osc_set_parameter_midi_channel_handler(argv);
    else if (strcmp(method, "set_parameter_midi_cc") == 0)
        return osc_set_parameter_midi_channel_handler(argv);
#endif
    else
        std::cerr << "Got unsupported OSC method '" << method << "' on '" << path << "'" << std::endl;

    return 1;
}

int osc_control_handler(lo_arg** argv)
{
    int index    = argv[0]->i;
    double value = argv[1]->f;

    if (ui)
      ui->queque_message(BRIDGE_MESSAGE_PARAMETER, index, 0, value);

    return 0;
}

int osc_program_handler(lo_arg** argv)
{
    int index = argv[0]->i;

    if (ui && index >= 0)
      ui->queque_message(BRIDGE_MESSAGE_PROGRAM, index, 0, 0.0);

    return 0;
}

int osc_midi_program_handler(lo_arg** argv)
{
    int bank    = argv[0]->i;
    int program = argv[1]->i;

    if (ui && bank >= 0 && program >= 0)
      ui->queque_message(BRIDGE_MESSAGE_MIDI_PROGRAM, bank, program, 0.0);

    return 0;
}

int osc_note_on_handler(lo_arg** argv)
{
    int note     = argv[1]->i;
    int velocity = argv[2]->i;

    if (ui && note >= 0 && velocity >= 0)
      ui->queque_message(BRIDGE_MESSAGE_NOTE_ON, note, velocity, 0.0);

    return 0;
}

int osc_note_off_handler(lo_arg** argv)
{
    int note     = argv[1]->i;
    int velocity = argv[2]->i;

    if (ui && note >= 0 && velocity >= 0)
      ui->queque_message(BRIDGE_MESSAGE_NOTE_OFF, note, velocity, 0.0);

    return 0;
}

int osc_show_handler()
{
    if (ui)
      ui->queque_message(BRIDGE_MESSAGE_SHOW_GUI, 1, 0, 0.0);

    return 0;
}

int osc_hide_handler()
{
    if (ui)
      ui->queque_message(BRIDGE_MESSAGE_SHOW_GUI, 0, 0, 0.0);

    return 0;
}

int osc_quit_handler()
{
    if (ui)
      ui->queque_message(BRIDGE_MESSAGE_QUIT, 0, 0, 0.0);

    return 0;
}

void osc_send_update()
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+8];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/update");
        lo_send(global_osc_data.target, target_path, "s", global_osc_server_path);
    }
}

void osc_send_control(int index, double value)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/control");
        lo_send(global_osc_data.target, target_path, "if", index, value);
    }
}

void osc_send_program(int index)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/program");
        lo_send(global_osc_data.target, target_path, "i", index);
    }
}

void osc_send_midi_program(int bank, int program)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+14];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/midi_program");
        lo_send(global_osc_data.target, target_path, "ii", bank, program);
    }
}

void osc_send_exiting()
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/exiting");
        lo_send(global_osc_data.target, target_path, "");
    }
}

void osc_send_bridge_audio_count(int ains, int aouts, int total)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+20];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_audio_count");
        lo_send(global_osc_data.target, target_path, "iii", ains, aouts, total);
    }
}

void osc_send_bridge_midi_count(int mins, int mouts, int total)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+19];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_midi_count");
        lo_send(global_osc_data.target, target_path, "iii", mins, mouts, total);
    }
}

void osc_send_bridge_param_count(int pins, int pouts, int total)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+20];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_param_count");
        lo_send(global_osc_data.target, target_path, "iii", pins, pouts, total);
    }
}

void osc_send_bridge_program_count(int count)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+22];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_program_count");
        lo_send(global_osc_data.target, target_path, "i", count);
    }
}

void osc_send_bridge_plugin_info(int hints)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+20];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_plugin_info");
        lo_send(global_osc_data.target, target_path, "i", hints);
    }
}

void osc_send_bridge_param_info(int param_index, const char* name, const char* label)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+19];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_param_info");
        lo_send(global_osc_data.target, target_path, "iss", param_index, name, label);
    }
}

void osc_send_bridge_param_data(int param_index, int ptype, int hints, int index, int rindex, int midi_channel, int midi_cc)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+19];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_param_data");
        lo_send(global_osc_data.target, target_path, "iiiiiii", param_index, ptype, hints, index, rindex, midi_channel, midi_cc);
    }
}

void osc_send_bridge_param_ranges(int param_index, double def, double min, double max, double step, double step_small, double step_large)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+21];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_param_ranges");
        lo_send(global_osc_data.target, target_path, "iffffff", param_index, def, min, max, step, step_small, step_large);
    }
}

void osc_send_bridge_program_name(int index, const char* name)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+21];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_program_name");
        lo_send(global_osc_data.target, target_path, "is", index, name);
    }
}

void osc_send_bridge_ains_peak(int index, double value)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+18];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_ains_peak");
        lo_send(global_osc_data.target, target_path, "if", index, value);
    }
}

void osc_send_bridge_aouts_peak(int index, double value)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+19];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_aouts_peak");
        lo_send(global_osc_data.target, target_path, "if", index, value);
    }
}

void osc_send_bridge_update()
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+15];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/bridge_update");
        lo_send(global_osc_data.target, target_path, "");
    }
}
