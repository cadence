/* Code for OSS related tasks */

#include <lo/lo.h>

// public ---------------------------------------------------------------------------
void osc_init(UiData* ui, const char* plugin_name, const char* osc_url);
void osc_close();

void osc_send_update();
void osc_send_control(int index, double value);
void osc_send_program(int index);
void osc_send_midi_program(int bank, int program);
void osc_send_exiting();

void osc_send_bridge_audio_count(int ains, int aouts, int total);
void osc_send_bridge_midi_count(int mins, int mouts, int total);
void osc_send_bridge_param_count(int pins, int pouts, int total);
void osc_send_bridge_program_count(int count);
void osc_send_bridge_plugin_info(int hints);
void osc_send_bridge_param_info(int param_index, const char* name, const char* label);
void osc_send_bridge_param_data(int param_index, int ptype, int hints, int index, int rindex, int midi_channel, int midi_cc);
void osc_send_bridge_param_ranges(int param_index, double def, double min, double max, double step, double step_small, double step_large);
void osc_send_bridge_program_name(int index, const char* name);
void osc_send_bridge_ains_peak(int index, double value);
void osc_send_bridge_aouts_peak(int index, double value);
void osc_send_bridge_update();

// private ---------------------------------------------------------------------------
struct OscData {
    char* path;
    lo_address target;
};

void osc_clear_data(OscData* osc_data);
void osc_error_handler(int num, const char* msg, const char* path);
int osc_message_handler(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data);
int osc_control_handler(lo_arg** argv);
int osc_program_handler(lo_arg** argv);
int osc_midi_program_handler(lo_arg** argv);
int osc_note_on_handler(lo_arg** argv);
int osc_note_off_handler(lo_arg** argv);
int osc_show_handler();
int osc_hide_handler();
int osc_quit_handler();

#ifdef WANT_EXTRA_OSC_SUPPORT
int osc_set_parameter_midi_channel_handler(lo_arg** argv);
int osc_set_parameter_midi_cc_handler(lo_arg** argv);
#endif
