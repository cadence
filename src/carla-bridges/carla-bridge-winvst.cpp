/* Carla WinVST Bridge Code */

#include "carla-bridge-plugin.h"
#include "../carla/vst_shared.h"

#include <iostream>

#define APPLICATION_CLASS_NAME "Carla"

// ---------------------------------------------------------------------------
// Carla compatible functions

// Global variables
static pthread_mutex_t carla_proc_lock_var = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t carla_midi_lock_var = PTHREAD_MUTEX_INITIALIZER;

// Global variables (shared)
AudioPlugin* AudioPlugins[MAX_PLUGINS] = { nullptr };
ExternalMidiNote ExternalMidiNotes[MAX_MIDI_EVENTS];

volatile double ains_peak[MAX_PLUGINS*2]  = { 0.0 };
volatile double aouts_peak[MAX_PLUGINS*2] = { 0.0 };

const char* bool2str(bool yesno)
{
    if (yesno)
        return "true";
    else
        return "false";
}

void carla_proc_lock()
{
    pthread_mutex_lock(&carla_proc_lock_var);
}

void carla_proc_unlock()
{
    pthread_mutex_unlock(&carla_proc_lock_var);
}

void carla_midi_lock()
{
    pthread_mutex_lock(&carla_midi_lock_var);
}

void carla_midi_unlock()
{
    pthread_mutex_unlock(&carla_midi_lock_var);
}

void send_plugin_midi_note(unsigned short plugin_id, bool onoff, uint8_t note, uint8_t velo, bool, bool, bool)
{
    carla_midi_lock();
    for (unsigned int i=0; i<MAX_MIDI_EVENTS; i++)
    {
        if (ExternalMidiNotes[i].valid == false)
        {
            ExternalMidiNotes[i].valid = true;
            ExternalMidiNotes[i].plugin_id = plugin_id;
            ExternalMidiNotes[i].onoff = onoff;
            ExternalMidiNotes[i].note = note;
            ExternalMidiNotes[i].velo = velo;
            break;
        }
    }
    carla_midi_unlock();
}

// ---------------------------------------------------------------------------
// Custom VstAudioPlugin class

class WinVstPlugin : public VstAudioPlugin
{
public:
    WinVstPlugin()
    {
        window = nullptr;

        for (unsigned short i=0; i<MAX_MIDI_EVENTS; i++)
            ExternalMidiNotes[i].valid = false;
    }

    ~WinVstPlugin()
    {
        qDebug("~WinVstPlugin()");

        // Safely disable plugin
        carla_proc_lock();
        id = -1;
        jack_deactivate(jack_client);
        carla_proc_unlock();

        // delete window?

        qDebug("~WinVstPlugin() - end");
    }

    static int winvst_jack_process_callback(jack_nframes_t nframes, void* arg)
    {
        if (arg)
        {
            WinVstPlugin* plugin = (WinVstPlugin*)arg;
            if (plugin->id >= 0)
            {
                carla_proc_lock();
                plugin->process(nframes);
                carla_proc_unlock();
            }
        }
        return 0;
    }

    static int winvst_jack_bufsize_callback(jack_nframes_t new_buffer_size, void* arg)
    {
        if (arg)
        {
            WinVstPlugin* plugin = (WinVstPlugin*)arg;
            plugin->buffer_size_changed(new_buffer_size);
        }
        return 0;
    }

    bool init(HINSTANCE& hInst_, const char* name, const char* title_)
    {
        hInst = hInst_;
        title = title_;

        VST_Function vstfn = (VST_Function)lib_symbol("VSTPluginMain");

        if (! vstfn)
        {
            vstfn = (VST_Function)lib_symbol("main");
#ifdef TARGET_API_MAC_CARBON
            if (! vstfn)
                vstfn = (VST_Function)lib_symbol("main_macho");
#endif
        }

        if (vstfn)
        {
            effect = vstfn(VstHostCallback);

            if (effect)
            {
                jack_client = jack_client_open(name, JackNullOption, 0);

                if (jack_client)
                {
                    effect->dispatcher(effect, effOpen, 0, 0, nullptr, 0.0f);
                    effect->dispatcher(effect, effSetSampleRate, 0, 0, nullptr, jack_get_sample_rate(jack_client));
                    effect->dispatcher(effect, effSetBlockSize, 0, jack_get_buffer_size(jack_client), nullptr, 0.0f);
                    effect->dispatcher(effect, 77 /* effSetProcessPrecision */, 0, 0 /* float 32bit */, nullptr, 0.0f);
                    effect->user = this;

                    if (effect->dispatcher(effect, effGetVstVersion, 0, 0, nullptr, 0.0f) <= 2)
                        is_oldsdk = true;

                    jack_set_process_callback(jack_client, winvst_jack_process_callback, this);
                    jack_set_buffer_size_callback(jack_client, winvst_jack_bufsize_callback, this);

                    reload();

                    id = 0;
                    AudioPlugins[0] = this;

                    return true;
                }
            }
        }

        return false;
    }

    void ui_show()
    {
        if (! window && effect->flags & effFlagsHasEditor)
        {
            window = CreateWindow(APPLICATION_CLASS_NAME, title,
                                  WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX,
                                  CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                  0, 0, hInst, 0);

            effect->dispatcher(effect, effEditOpen, 0, 0, window, 0.0f);

#ifndef ERect
            struct ERect {
                short top;
                short left;
                short bottom;
                short right;
            };
#endif
            ERect* vst_rect;

            if (effect->dispatcher(effect, effEditGetRect, 0, 0, &vst_rect, 0.0f))
            {
                SetWindowPos(window, 0, 0, 0,
                             vst_rect->right - vst_rect->left + 6,
                             vst_rect->bottom - vst_rect->top + 25,
                             SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
            }

            hints |= PLUGIN_HAS_GUI;
        }

        if (window)
        {
            ShowWindow(window, SW_SHOWNORMAL);
            UpdateWindow(window);
        }

        show_gui(true);
    }

    void ui_hide()
    {
        show_gui(false);

        if (window)
            ShowWindow(window, SW_HIDE);
    }

    void ui_resize(int width, int height)
    {
        if (window)
            SetWindowPos(window, 0, 0, 0,
                         width + 6,
                         height + 25,
                         SWP_NOACTIVATE | SWP_NOMOVE |
                         SWP_NOOWNERZORDER | SWP_NOZORDER);
    }

    void update_parameter(int index, double value)
    {
        switch(index)
        {
            case PARAMETER_ACTIVE:
                set_active(value, false, false);
                break;
            case PARAMETER_DRYWET:
                set_drywet(value, false, false);
                break;
            case PARAMETER_VOLUME:
                set_vol(value, false, false);
                break;
            case PARAMETER_BALANCE_LEFT:
                set_balance_left(value, false, false);
                break;
            case PARAMETER_BALANCE_RIGHT:
                set_balance_right(value, false, false);
                break;
            default:
                set_parameter_value(index, value, true, false, false);
                break;
        }
    }

    void update_program(int index)
    {
        set_program(index, true, false, false, true);
    }

    void update_midi_program(int, int)
    {
    }

    void send_note_on(int note, int velocity)
    {
        send_plugin_midi_note(id, true, note, velocity, false, false, false);
    }

    void send_note_off(int note, int velocity)
    {
        send_plugin_midi_note(id, false, note, velocity, false, false, false);
    }

    void* get_widget()
    {
        return window;
    }

    bool is_resizable()
    {
        return false;
    }

private:
    HWND window;

    HINSTANCE hInst;
    const char* title;
};

// ---------------------------------------------------------------------------
// Global objects

static bool close_now = false;
static WinVstPlugin* winvst;

// ---------------------------------------------------------------------------
// Extra OSC support

int osc_set_parameter_midi_channel_handler(lo_arg** argv)
{
    int parameter_id = argv[1]->i;
    int midi_channel = argv[2]->i;

    if (! winvst || midi_channel > 15)
        return 1;

    if (parameter_id < (int32_t)winvst->param.count)
        winvst->param.data[parameter_id].midi_channel = midi_channel;

    return 0;
}

int osc_set_parameter_midi_cc_handler(lo_arg** argv)
{
    int parameter_id = argv[1]->i;
    int midi_cc      = argv[2]->i;

    if (! winvst || midi_cc > 0x5F)
        return 1;
    else if (midi_cc < -1)
        midi_cc = -1;

    if (parameter_id < (int32_t)winvst->param.count)
        winvst->param.data[parameter_id].midi_cc = midi_cc;

    return 0;
}

// ---------------------------------------------------------------------------
// Bridge specfic code

void toolkit_init()
{
}

void toolkit_loop(WinVstPlugin* winvst_, const char* plugin_name, bool reparent)
{
    winvst = winvst_;

    osc_send_update();

    MSG msg;
    while (close_now == false)
    {
        while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
            DispatchMessage(&msg);

        if (close_now) break;

        if (winvst->ain.count > 0)
        {
            osc_send_bridge_ains_peak(1, ains_peak[0]);
            osc_send_bridge_ains_peak(2, ains_peak[1]);
        }

        if (close_now) break;

        if (winvst->aout.count > 0)
        {
            osc_send_bridge_aouts_peak(1, aouts_peak[0]);
            osc_send_bridge_aouts_peak(2, aouts_peak[1]);
        }

        if (close_now) break;

        winvst->run_messages();

        if (close_now) break;

        winvst->idle_gui();

        if (close_now == false)
            usleep(50000); // 50 ms
    }
}

void toolkit_quit()
{
    close_now = true;
}

void toolkit_window_show()
{
    winvst->ui_show();
}

void toolkit_window_hide()
{
    winvst->ui_hide();
}

void toolkit_window_resize(int width, int height)
{
    winvst->ui_resize(width, height);
}

LRESULT WINAPI MainProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
    case WM_DESTROY:
        toolkit_quit();
        return 0;
    default:
        if (close_now)
            return 0;
        else
            return DefWindowProc(hWnd, msg, wParam, lParam);
    }
}

// ---------------------------------------------------------------------------
// Windows main

//int main(int argc, char* argv[])
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR cmdline, int cmdshow)
{
#define MAXCMDTOKENS 128
    int argc;
    LPSTR argv[MAXCMDTOKENS];
    LPSTR p = GetCommandLine();

    char command[256];
    char *args;
    char *d, *e;

    argc = 0;
    args = (char *)malloc(lstrlen(p)+1);
    if (args == (char *)NULL) {
        fprintf(stdout, "Insufficient memory in WinMain()\n");
        return 1;
    }

    // Parse command line handling quotes.
    d = args;
    while (*p)
    {
        // for each argument
        if (argc >= MAXCMDTOKENS - 1)
            break;

        e = d;
        while ((*p) && (*p != ' ')) {
            if (*p == '\042') {
                // Remove quotes, skipping over embedded spaces.
                // Doesn't handle embedded quotes.
                p++;
                while ((*p) && (*p != '\042'))
                    *d++ =*p++;
            }
            else 
                *d++ = *p;
            if (*p)
                p++;
        }
        *d++ = '\0';
        argv[argc++] = e;

        while ((*p) && (*p == ' '))
            p++;        // Skip over trailing spaces
    }
    argv[argc] = 0;

    if (strlen(argv[0]) == 0)
    {
        GetModuleFileName(hInst, command, sizeof(command)-1);
        argv[0] = command;
    }

    if (argc != 5)
    {
       std::cerr << argv[0] << " :: bad arguments" << std::endl;
       return 1;
    }

    const char* binary  = argv[1];
    const char* name    = argv[2];
    const char* title   = argv[3];
    const char* osc_url = argv[4];

    WNDCLASSEX wclass;
    wclass.cbSize = sizeof(WNDCLASSEX);
    wclass.style = 0;
    wclass.lpfnWndProc = MainProc;
    wclass.cbClsExtra = 0;
    wclass.cbWndExtra = 0;
    wclass.hInstance = hInst;
    wclass.hIcon = LoadIcon(hInst, APPLICATION_CLASS_NAME);
    wclass.hCursor = LoadCursor(0, IDI_APPLICATION);
    wclass.lpszMenuName = "MENU_DSSI_VST";
    wclass.lpszClassName = APPLICATION_CLASS_NAME;
    wclass.hIconSm = 0;

    if (! RegisterClassEx(&wclass))
    {
        std::cerr << "Failed to register Wine application" << std::endl;
        return 1;
    }

    // Init toolkit
    toolkit_init();

    // Init Plugin
    WinVstPlugin winvst;

    // Init OSC
    osc_init(&winvst, title, osc_url);

    if (winvst.lib_open(binary))
    {
        if (winvst.init(hInst, name, title) == false)
        {
            std::cerr << "Failed to load Windows VST" << std::endl;
            winvst.lib_close();
            osc_close();
            toolkit_quit();
            return 1;
        }
    }
    else
    {
        std::cerr << "Failed to load Plugin DLL: " << winvst.lib_error() << std::endl;
        osc_close();
        toolkit_quit();
        return 1;
    }

    toolkit_loop(&winvst, title, false);

    winvst.lib_close();
    osc_send_exiting();
    osc_close();
    toolkit_quit();

    winvst.delete_me();

    return 0;
}
