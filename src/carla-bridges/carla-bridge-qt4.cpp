/* Carla GUI Bridges Code */

#include "carla-bridge.h"
#include "osc.h"

static UiData* ui;
static QDialog* window;
static const char* plugin_name = nullptr;

// Static QApplication
static int argc = 0;
static char* argv[] = { nullptr };
static QApplication* app = nullptr;

class MessageChecker : public QTimer {
public:
    MessageChecker() {}

    void timerEvent(QTimerEvent*)
    {
        ui->run_messages();
    }
};

void toolkit_init()
{
    app = new QApplication(argc, argv, true);
}

void toolkit_loop(UiData* ui_, const char* plugin_name_, bool reparent)
{
    ui = ui_;
    plugin_name = plugin_name_;

    if (reparent)
    {
        window = new QDialog();
        new QVBoxLayout(window);

        QWidget* widget = (QWidget*)ui->get_widget();
        window->layout()->addWidget(widget);
        widget->setParent(window);
        widget->show();
    }
    else
    {
        window = (QDialog*)ui->get_widget();
    }

    MessageChecker checker;
    checker.start(50);
    QObject::connect(window, SIGNAL(finished(int)), app, SLOT(quit()));

    QSettings settings("Cadence", "CarlaBridges");
    window->restoreGeometry(settings.value(plugin_name).toByteArray());

    if (ui->is_resizable() == false)
        window->setFixedSize(window->width(), window->height());

    window->setWindowTitle(plugin_name);

    osc_send_update();

    // Main loop
    app->exec();
}

void toolkit_quit()
{
    QSettings settings("Cadence", "CarlaBridges");
    settings.setValue(plugin_name, window->saveGeometry());
    settings.sync();
    app->quit();
}

void toolkit_window_show()
{
    window->show();
}

void toolkit_window_hide()
{
    window->hide();
}

void toolkit_window_resize(int width, int height)
{
    if (ui->is_resizable())
        window->resize(width, height);
    else
        window->setFixedSize(width, height);
}
