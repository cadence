/* Carla GUI Bridges Code */

#ifndef CARLA_BRIDGE_H
#define CARLA_BRIDGE_H

#include "bridge-includes.h"

class UiData;

void toolkit_init();
void toolkit_loop(UiData* ui, const char* plugin_name, bool reparent);
void toolkit_quit();
void toolkit_window_show();
void toolkit_window_hide();
void toolkit_window_resize(int width, int height);

enum BridgeMessageType {
    BRIDGE_MESSAGE_NULL         = 0,
    BRIDGE_MESSAGE_PARAMETER    = 1, // index, 0, value
    BRIDGE_MESSAGE_PROGRAM      = 2, // index, 0, 0
    BRIDGE_MESSAGE_MIDI_PROGRAM = 3, // bank, program, 0
    BRIDGE_MESSAGE_NOTE_ON      = 4, // note, velocity, 0
    BRIDGE_MESSAGE_NOTE_OFF     = 5, // note, velocity, 0
    BRIDGE_MESSAGE_SHOW_GUI     = 6, // show, 0, 0
    BRIDGE_MESSAGE_RESIZE_GUI   = 7, // width, height, 0
    BRIDGE_MESSAGE_QUIT         = 8
};

const unsigned int MAX_BRIDGE_MESSAGES = 256;

struct QuequeBridgeMessage {
    bool valid;
    BridgeMessageType type;
    int value1;
    int value2;
    double value3;
};

class UiData {
public:
    UiData()
    {
        lib  = nullptr;
        pthread_mutex_init(&lock, nullptr);

        for (unsigned int i=0; i<MAX_BRIDGE_MESSAGES; i++)
        {
            QuequeBridgeMessages[i].valid  = false;
            QuequeBridgeMessages[i].type   = BRIDGE_MESSAGE_NULL;
            QuequeBridgeMessages[i].value1 = 0;
            QuequeBridgeMessages[i].value2 = 0;
            QuequeBridgeMessages[i].value3 = 0.0;
        }
    }

    ~UiData()
    {
        pthread_mutex_destroy(&lock);
    }

    void queque_message(BridgeMessageType type, int value1, int value2, double value3)
    {
        pthread_mutex_lock(&lock);
        for (unsigned int i=0; i<MAX_BRIDGE_MESSAGES; i++)
        {
            if (QuequeBridgeMessages[i].valid == false)
            {
                QuequeBridgeMessages[i].valid  = true;
                QuequeBridgeMessages[i].type   = type;
                QuequeBridgeMessages[i].value1 = value1;
                QuequeBridgeMessages[i].value2 = value2;
                QuequeBridgeMessages[i].value3 = value3;
                break;
            }
        }
        pthread_mutex_unlock(&lock);
    }

    bool run_messages()
    {
        pthread_mutex_lock(&lock);
        for (unsigned int i=0; i<MAX_BRIDGE_MESSAGES; i++)
        {
            if (QuequeBridgeMessages[i].valid)
            {
                QuequeBridgeMessage* m = &QuequeBridgeMessages[i];

                switch (m->type)
                {
                case BRIDGE_MESSAGE_NULL:
                    break;
                case BRIDGE_MESSAGE_PARAMETER:
                    update_parameter(m->value1, m->value3);
                    break;
                case BRIDGE_MESSAGE_PROGRAM:
                    update_program(m->value1);
                    break;
                case BRIDGE_MESSAGE_MIDI_PROGRAM:
                    update_midi_program(m->value1, m->value2);
                    break;
                case BRIDGE_MESSAGE_NOTE_ON:
                    send_note_on(m->value1, m->value2);
                    break;
                case BRIDGE_MESSAGE_NOTE_OFF:
                    send_note_off(m->value1, m->value2);
                    break;
                case BRIDGE_MESSAGE_SHOW_GUI:
                    if (m->value1)
                        toolkit_window_show();
                    else
                        toolkit_window_hide();
                    break;
                case BRIDGE_MESSAGE_RESIZE_GUI:
                    toolkit_window_resize(m->value1, m->value2);
                    break;
                case BRIDGE_MESSAGE_QUIT:
                    toolkit_quit();
                    pthread_mutex_unlock(&lock);
                    return false;
                default:
                    break;
                }
                m->valid = false;
            }
            else
                break;
        }
        pthread_mutex_unlock(&lock);
        return true;
    }

    virtual void update_parameter(int index, double value) = 0;
    virtual void update_program(int index) = 0;
    virtual void update_midi_program(int bank, int program) = 0;
    virtual void send_note_on(int note, int velocity) = 0;
    virtual void send_note_off(int note, int velocity) = 0;

    virtual void* get_widget() = 0;
    virtual bool is_resizable() = 0;

    bool lib_open(const char* filename)
    {
#ifdef __WINDOWS__
        lib = LoadLibraryA(filename);
#else
        lib = dlopen(filename, RTLD_NOW);
#endif
        return bool(lib);
    }

    bool lib_close()
    {
        if (lib)
#ifdef __WINDOWS__
            return FreeLibrary((HMODULE)lib) != 0;
#else
            return dlclose(lib) != 0;
#endif
        else
            return false;
    }

    void* lib_symbol(const char* symbol)
    {
        if (lib)
#ifdef __WINDOWS__
            return (void*)GetProcAddress((HMODULE)lib, symbol);
#else
            return dlsym(lib, symbol);
#endif
        else
            return nullptr;
    }

    const char* lib_error()
    {
#ifdef __WINDOWS__
        return "Unknown error";
#else
        return dlerror();
#endif
    }

private:
    void* lib;
    pthread_mutex_t lock;
    QuequeBridgeMessage QuequeBridgeMessages[MAX_BRIDGE_MESSAGES];
};

#endif // CARLA_BRIDGE_H
