/* Carla GUI Bridges Code */

#include "carla-bridge.h"
#include "osc.h"

static UiData* ui;
static DocumentWindow* window;

class MessageChecker : public Timer
{
public:
    MessageChecker() {}

    void timerCallback()
    {
        ui->run_messages();
    }
};

void toolkit_init()
{
    initialiseJuce_GUI();
}

void toolkit_loop(UiData* ui_, const char* plugin_name, bool resizable)
{
    ui = ui_;
    window = new DocumentWindow(String::empty, Colour(32, 32, 32), DocumentWindow::closeButton, false);

    // Window Events
    MessageChecker checker;
    checker.startTimer(50);

    window->setResizable(resizable, resizable);

    window->setName(plugin_name);
    window->setUsingNativeTitleBar(true);
    window->setContentNonOwned((Component*)ui->get_widget(), true);

    osc_send_update();

    // Main loop
    window->runModalLoop();

    checker.stopTimer();

    delete window;
    window = nullptr;

    shutdownJuce_GUI();
}

void toolkit_quit()
{
    if (window)
      window->exitModalState(1);
}

void toolkit_window_show()
{
    if (! window->isOnDesktop())
        window->addToDesktop();
    window->setVisible(true);
}

void toolkit_window_hide()
{
    window->setVisible(false);
}
