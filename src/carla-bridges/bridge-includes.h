/* Carla GUI Bridges Code */

#ifndef CARLA_BRIDGE_INCLUDES_H
#define CARLA_BRIDGE_INCLUDES_H

#ifndef nullptr
const class {
    public:
    template<class T> operator T*() const
    {
        return 0;
    }

    template<class C, class T> operator T C::*() const
    {
        return 0;
    }

    private:
    void operator&() const;
} nullptr_ = {};
#define nullptr nullptr_
#endif

#ifdef __WINE__
#define __socklen_t_defined
#define __WINE_WINSOCK2__
//#define HRESULT LONG
//#define _WIN32_X11_
#endif

#if defined(__WIN32__) || defined(__WIN64__) || defined(__WINE__)
#include <windows.h>
#ifdef __WINE__
#include <pthread.h>
#else
#include "pthread-win.h" // TODO
#endif
#ifndef __WINDOWS__
#define __WINDOWS__
#endif
#else
#include <dlfcn.h>
#include <pthread.h>
#undef __cdecl
#define __cdecl
#endif

#if BRIDGE_LV2_GTK2
#define WANT_LV2  1
#define WANT_GTK2 1
#elif BRIDGE_LV2_QT4
#define WANT_LV2  1
#define WANT_QT4  1
#elif BRIDGE_LV2_X11
#define WANT_LV2  1
#define WANT_X11  1
#elif BRIDGE_LV2_JUCE
#define WANT_LV2  1
#define WANT_JUCE 1
#elif BRIDGE_VST_QT4
#define WANT_VST  1
#define WANT_QT4  1
#elif BRIDGE_WINVST
#define WANT_VST  1
#define WANT_EXTRA_OSC_SUPPORT 1
#else
#error undefined configuration
#endif

#ifdef WANT_LV2
#include "lv2/lv2.h"
#include "lv2/uri-map.h"
#include "lv2/urid.h"
#include "lv2/ui.h"
#include "lv2/ui-resize.h"
#endif

#ifdef WANT_VST
#define VST_FORCE_DEPRECATED 0
#include <stdint.h>
#include "aeffectx.h"
#endif

#ifdef WANT_GTK2
#include <gtk/gtk.h>
#endif

#ifdef WANT_QT4
#include <QtGui/QApplication>
#include <QtGui/QDialog>
#include <QtGui/QLayout>
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#endif

#ifdef WANT_X11
#include <QtGui/QApplication>
#include <QtGui/QDialog>
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#endif

#ifdef WANT_JUCE
#include <juce/juce.h>
#endif

#endif // CARLA_BRIDGE_INCLUDES_H
