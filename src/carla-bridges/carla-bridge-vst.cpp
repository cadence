/* Carla GUI Bridges Code */

#include "carla-bridge.h"
#include "osc.h"

#ifndef WANT_QT4
#error VST requires QT4
#endif

#include <iostream>

#ifndef kVstVersion
#define kVstVersion 2400
#endif

#define FAKE_SAMPLE_RATE 44100.0
#define FAKE_BUFFER_SIZE 512

typedef AEffect* (*VST_Function)(audioMasterCallback);

static intptr_t VstHostCallback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
{
    UiData* ui;

    switch (opcode)
    {
    case audioMasterAutomate:
        osc_send_control(index, opt);
        return 0;

    case audioMasterVersion:
        return kVstVersion;

    case audioMasterIdle:
        effect->dispatcher(effect, effEditIdle, 0, 0, nullptr, 0.0f);
        return 1;

    case audioMasterNeedIdle:
        effect->dispatcher(effect, 53 /* effIdle */, 0, 0, nullptr, 0.0f);
        return 1;

    case audioMasterSizeWindow:
        ui = (effect && effect->user) ? (UiData*)effect->user : nullptr;
        if (ui)
            ui->queque_message(BRIDGE_MESSAGE_RESIZE_GUI, index, value, 0.0f);
        return 1;

    case audioMasterGetSampleRate:
        return FAKE_SAMPLE_RATE;

    case audioMasterGetBlockSize:
        return FAKE_BUFFER_SIZE;

    case audioMasterGetVendorString:
        strncpy((char*)ptr, "falkTX", 64);
        return 1;

    case audioMasterGetProductString:
        strncpy((char*)ptr, "Carla-Bridge", 64);
        return 1;

    case audioMasterGetVendorVersion:
        return 0x0300; // 0.3.0

    case audioMasterCanDo:
        if (strcmp((char*)ptr, "sendVstEvents") == 0)
            return 1;
        else if (strcmp((char*)ptr, "sendVstMidiEvent") == 0)
            return 1;
        else if (strcmp((char*)ptr, "sendVstTimeInfo") == 0)
            return 1;
        else if (strcmp((char*)ptr, "receiveVstEvents") == 0)
            return 1;
        else if (strcmp((char*)ptr, "receiveVstMidiEvent") == 0)
            return 1;
        else if (strcmp((char*)ptr, "sizeWindow") == 0)
            return 1;
        else if (strcmp((char*)ptr, "acceptIOChanges") == 0)
            return 1;
        else
            return 0;

    case audioMasterGetLanguage:
        return kVstLangEnglish;

    default:
        return 0;
    }
}

class VstUiData : public UiData {
public:
    VstUiData()
    {
        effect = nullptr;
        widget = new QDialog();
    }

    ~VstUiData()
    {
        delete widget;
    }

    bool init()
    {
        VST_Function vstfn = (VST_Function)lib_symbol("VSTPluginMain");

        if (! vstfn)
        {
            vstfn = (VST_Function)lib_symbol("main");
#ifdef TARGET_API_MAC_CARBON
            if (! vstfn)
                vstfn = (VST_Function)lib_symbol("main_macho");
#endif
        }

        if (vstfn)
        {
            effect = vstfn(VstHostCallback);

            if (effect)
            {
                effect->dispatcher(effect, effOpen, 0, 0, nullptr, 0.0f);
                effect->dispatcher(effect, effSetSampleRate, 0, 0, nullptr, FAKE_SAMPLE_RATE);
                effect->dispatcher(effect, effSetBlockSize, 0, FAKE_BUFFER_SIZE, nullptr, 0.0f);
                effect->dispatcher(effect, effEditOpen, 0, 0, (void*)widget->winId(), 0.0f);

                effect->user = this;

#ifndef ERect
                struct ERect {
                    short top;
                    short left;
                    short bottom;
                    short right;
                };
#endif
                ERect* vst_rect;

                if (effect->dispatcher(effect, effEditGetRect, 0, 0, &vst_rect, 0.0f))
                {
                    int width  = vst_rect->right - vst_rect->left;
                    int height = vst_rect->bottom - vst_rect->top;
                    widget->setFixedSize(width, height);
                    return true;
                }
            }
        }

        return false;
    }

    void close()
    {
        effect->dispatcher(effect, effEditClose, 0, 0, nullptr, 0.0f);
        effect->dispatcher(effect, effClose, 0, 0, nullptr, 0.0f);
    }

    void update_parameter(int index, double value)
    {
        if (effect)
            effect->setParameter(effect, index, value);
    }

    void update_program(int index)
    {
        if (effect)
            effect->dispatcher(effect, effSetProgram, 0, index, nullptr, 0.0f);
    }

    void update_midi_program(int, int) {}
    void send_note_on(int, int) {}
    void send_note_off(int note, int velocity) {}

    void* get_widget()
    {
        return widget;
    }

    bool is_resizable()
    {
        return false;
    }

private:
    AEffect* effect;
    QDialog* widget;
};

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
       std::cerr << argv[0] << " :: bad arguments" << std::endl;
       return 1;
    }

    const char* plugin_name = argv[1];
    const char* ui_binary   = argv[2];
    const char* osc_url     = argv[3];

    // Init toolkit
    toolkit_init();

    // Init VST
    VstUiData ui;

    // Init OSC
    osc_init(&ui, plugin_name, osc_url);

    if (ui.lib_open(ui_binary))
    {
        if (!ui.init())
        {
            std::cerr << "Failed to load VST UI" << std::endl;
            ui.lib_close();
            osc_close();
            return 1;
        }
    }
    else
    {
        std::cerr << "Failed to load UI: " << ui.lib_error() << std::endl;
        osc_close();
        return 1;
    }

    toolkit_loop(&ui, plugin_name, false);

    ui.close();
    ui.lib_close();
    osc_send_exiting();
    osc_close();
    toolkit_quit();

    return 0;
}
