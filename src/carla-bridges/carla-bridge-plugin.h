/* Carla GUI Bridges Code */

#ifndef CARLA_BRIDGE_PLUGIN_H
#define CARLA_BRIDGE_PLUGIN_H

#include "carla-bridge.h"
#include "osc.h"

#define CARLA_INCLUDES
#define CARLA_EXPORT
#include "../carla/carla_backend.h"

#include <Qt>
#include <jack/jack.h>
#include <jack/midiport.h>

#define CARLA_PROCESS_CONTINUE_CHECK if (id != plugin_id) { return; }
#define postpone_event(x, y, z) // TODO

enum PluginOscBridgeInfoType {
};

struct PluginAudioData {
    uint32_t count;
    uint32_t* rindexes;
    jack_port_t** ports;
};

struct PluginMidiData {
    uint32_t count;
    jack_port_t** ports;
};

struct PluginParameterData {
    uint32_t count;
    float* buffers;
    ParameterData* data;
    ParameterRanges* ranges;
    jack_port_t* port_in;
    jack_port_t* port_out;
};

struct PluginProgramData {
    uint32_t count;
    int32_t current;
    const char** names;
};

struct ExternalMidiNote {
    bool valid;
    short plugin_id;
    bool onoff;
    uint8_t note;
    uint8_t velo;
};

class AudioPlugin : public UiData
{
public:
    short id;
    int hints;

    bool active;
    bool active_before;

    // Storage Data (Audio, MIDI, Parameters and Programs)
    PluginAudioData ain;
    PluginAudioData aout;
    PluginMidiData min;
    PluginMidiData mout;
    PluginParameterData param;
    PluginProgramData prog;

    GuiData gui;

    // TODO - postponed events

    double x_drywet, x_vol, x_bal_left, x_bal_right;

    jack_client_t* jack_client;

    AudioPlugin()
    {
        id = -1;
        hints = 0;

        active = false;
        active_before = false;

        ain.count = 0;
        ain.ports = nullptr;
        ain.rindexes = nullptr;

        aout.count = 0;
        aout.ports = nullptr;
        aout.rindexes = nullptr;

        min.count = 0;
        min.ports = nullptr;

        mout.count = 0;
        mout.ports = nullptr;

        param.count    = 0;
        param.buffers  = nullptr;
        param.data     = nullptr;
        param.ranges   = nullptr;
        param.port_in  = nullptr;
        param.port_out = nullptr;

        prog.count   = 0;
        prog.current = -1;
        prog.names   = nullptr;

        gui.visible = false;

        x_drywet = 1.0;
        x_vol    = 1.0;
        x_bal_left = -1.0;
        x_bal_right = 1.0;

        jack_client = nullptr;
    }

    ~AudioPlugin()
    {
        qDebug("[Bridge]AudioPlugin::~AudioPlugin()");

        // Unregister jack ports
        remove_from_jack();

        // Delete data
        delete_buffers();

        if (prog.count > 0)
        {
            for (uint32_t i=0; i < prog.count; i++)
                free((void*)prog.names[i]);

            delete[] prog.names;
        }

        if (jack_client)
            jack_client_close(jack_client);
    }

    void remove_from_jack()
    {
        qDebug("AudioPlugin::remove_from_jack()");

        uint32_t i;

        for (i=0; i < ain.count; i++)
            jack_port_unregister(jack_client, ain.ports[i]);

        for (i=0; i < aout.count; i++)
            jack_port_unregister(jack_client, aout.ports[i]);

        for (i=0; i < min.count; i++)
            jack_port_unregister(jack_client, min.ports[i]);

        for (i=0; i < mout.count; i++)
            jack_port_unregister(jack_client, mout.ports[i]);

        if (param.port_in)
            jack_port_unregister(jack_client, param.port_in);

        if (param.port_out)
            jack_port_unregister(jack_client, param.port_out);

        qDebug("AudioPlugin::remove_from_jack() - end");
    }

    void delete_buffers()
    {
        qDebug("AudioPlugin::delete_buffers()");

        if (ain.count > 0)
        {
            delete[] ain.ports;
            delete[] ain.rindexes;
        }

        if (aout.count > 0)
        {
            delete[] aout.ports;
            delete[] aout.rindexes;
        }

        if (min.count > 0)
        {
            delete[] min.ports;
        }

        if (mout.count > 0)
        {
            delete[] mout.ports;
        }

        if (param.count > 0)
        {
            delete[] param.buffers;
            delete[] param.data;
            delete[] param.ranges;
        }

        ain.count = 0;
        ain.ports = nullptr;
        ain.rindexes = nullptr;

        aout.count = 0;
        aout.ports = nullptr;
        aout.rindexes = nullptr;

        min.count = 0;
        min.ports = nullptr;

        mout.count = 0;
        mout.ports = nullptr;

        param.count    = 0;
        param.buffers  = nullptr;
        param.data     = nullptr;
        param.ranges   = nullptr;
        param.port_in  = nullptr;
        param.port_out = nullptr;

        qDebug("AudioPlugin::delete_buffers() - end");
    }

    void fix_parameter_value(double& value, const ParameterRanges& ranges)
    {
        if (value < ranges.min)
            value = ranges.min;
        else if (value > ranges.max)
            value = ranges.max;
    }

    void set_active(bool active_, bool, bool callback_send)
    {
        active = active_;
        double value = active ? 1.0 : 0.0;

        if (callback_send)
            osc_send_control(PARAMETER_ACTIVE, value);
    }

    void set_drywet(double value, bool, bool callback_send)
    {
        if (value < 0.0)
            value = 0.0;
        else if (value > 1.0)
            value = 1.0;

        x_drywet = value;

        if (callback_send)
            osc_send_control(PARAMETER_DRYWET, value);
    }

    void set_vol(double value, bool, bool callback_send)
    {
        if (value < 0.0)
            value = 0.0;
        else if (value > 1.27)
            value = 1.27;

        x_vol = value;

        if (callback_send)
            osc_send_control(PARAMETER_VOLUME, value);
    }

    void set_balance_left(double value, bool, bool callback_send)
    {
        if (value < -1.0)
            value = -1.0;
        else if (value > 1.0)
            value = 1.0;

        x_bal_left = value;

        if (callback_send)
            osc_send_control(PARAMETER_BALANCE_LEFT, value);
    }

    void set_balance_right(double value, bool, bool callback_send)
    {
        if (value < -1.0)
            value = -1.0;
        else if (value > 1.0)
            value = 1.0;

        x_bal_right = value;

        if (callback_send)
            osc_send_control(PARAMETER_BALANCE_RIGHT, value);
    }

    void set_parameter_value(uint32_t parameter_id, double value, bool gui_send, bool, bool callback_send)
    {
        fix_parameter_value(value, param.ranges[parameter_id]);

        param.buffers[parameter_id] = value;

        if (callback_send)
            osc_send_control(parameter_id, value);

        x_set_parameter_value(parameter_id, value, gui_send);
    }

    void set_program(uint32_t program_id, bool gui_send, bool, bool callback_send, bool block)
    {
        x_set_program(program_id, gui_send, block);

        prog.current = program_id;

        if (callback_send)
            osc_send_program(program_id);
    }

    virtual void x_set_parameter_value(uint32_t parameter_id, double value, bool gui_send) = 0;
    virtual void x_set_program(uint32_t program_id, bool gui_send, bool block) = 0;
};

#endif // CARLA_BRIDGE_PLUGIN_H
