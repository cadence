/* Code for OSS related tasks */

#include "carla-bridge.h"

extern ControlQuequeMessage ControlQuequeMessages[MAX_CONTROL_MESSAGES];
extern pthread_mutex_t lock;

lo_server_thread global_osc_server_thread = nullptr;
const char* global_osc_server_path = nullptr;
OscData global_osc_data = { nullptr, nullptr };

const char* plugin_name = nullptr;

void osc_init(const char* plugin_name_, const char* osc_url)
{
    plugin_name = plugin_name_;

    const char* host = lo_url_get_hostname(osc_url);
    const char* port = lo_url_get_port(osc_url);

    global_osc_data.path   = lo_url_get_path(osc_url);
    global_osc_data.target = lo_address_new(host, port);

    free((void*)host);
    free((void*)port);

    global_osc_server_thread = lo_server_thread_new(nullptr, osc_error_handler);

    const char* osc_thread_path = lo_server_thread_get_url(global_osc_server_thread);

    // Replace ' ' by '_' on plugin_name
    char* fixed_name = strdup(plugin_name);
    for (unsigned int i=0; i<strlen(fixed_name); i++)
    {
        if (fixed_name[i] == ' ')
            fixed_name[i] = '_';
    }

    char osc_path_tmp[strlen(osc_thread_path)+strlen(fixed_name)+1];
    sprintf(osc_path_tmp, "%s%s", osc_thread_path, fixed_name);
    global_osc_server_path = strdup(osc_path_tmp);

    free((void*)fixed_name);
    free((void*)osc_thread_path);

    lo_server_thread_add_method(global_osc_server_thread, nullptr, nullptr, osc_message_handler, nullptr);
    lo_server_thread_start(global_osc_server_thread);
}

void osc_close()
{
    osc_clear_data(&global_osc_data);

    lo_server_thread_stop(global_osc_server_thread);
    lo_server_thread_del_method(global_osc_server_thread, nullptr, nullptr);
    lo_server_thread_free(global_osc_server_thread);

    free((void*)global_osc_server_path);
}

void osc_clear_data(OscData* osc_data)
{
    if (osc_data->path)
        free((void*)osc_data->path);

    if (osc_data->target)
        lo_address_free(osc_data->target);

    osc_data->path = nullptr;
    osc_data->target = nullptr;
}

void osc_error_handler(int num, const char* msg, const char* path)
{
    printf("osc_error_handler(%i, %s, %s)\n", num, msg, path);
}

int osc_message_handler(const char* path, const char* /* types */, lo_arg** argv, int /* argc */, void* /* data */, void* /* user_data */)
{
    char method[24];
    memset(method, 0, sizeof(char)*24);

    unsigned int mindex = strlen(plugin_name)+2;

    for (unsigned int i=mindex; i<strlen(path) && i-mindex<24; i++)
        method[i-mindex] = path[i];

    if (strcmp(method, "control") == 0)
        return osc_control_handler(argv);
    else if (strcmp(method, "show") == 0)
        return osc_show_handler();
    else if (strcmp(method, "hide") == 0)
        return osc_hide_handler();
    else if (strcmp(method, "quit") == 0)
        return osc_quit_handler();
    else if (strcmp(method, "note_on") == 0)
        return 1;
    else if (strcmp(method, "note_off") == 0)
        return 1;
    else
        printf("Got unsupported OSC method '%s' on '%s'\n", method, path);

    return 1;
}

int osc_control_handler(lo_arg** argv)
{
    int32_t index = argv[0]->i;
    float value   = argv[1]->f;

    queque_message(index, value);
    return 0;
}

int osc_show_handler()
{
    queque_message(BRIDGE_MESSAGE_SHOW);
    return 0;
}

int osc_hide_handler()
{
    queque_message(BRIDGE_MESSAGE_HIDE);
    return 0;
}

int osc_quit_handler()
{
    queque_message(BRIDGE_MESSAGE_QUIT);
    return 0;
}

void osc_send_update()
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+8];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/update");
        lo_send(global_osc_data.target, target_path, "s", global_osc_server_path);
    }
}

void osc_send_control(int parameter_id, float value)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/control");
        lo_send(global_osc_data.target, target_path, "if", parameter_id, value);
    }
}

void osc_send_program(int program_id)
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/program");
        lo_send(global_osc_data.target, target_path, "i", program_id);
    }
}

void osc_send_exiting()
{
    if (global_osc_data.target)
    {
        char target_path[strlen(global_osc_data.path)+9];
        strcpy(target_path, global_osc_data.path);
        strcat(target_path, "/exiting");
        lo_send(global_osc_data.target, target_path, "");
    }
}
