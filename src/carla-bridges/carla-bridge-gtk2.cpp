/* Carla GUI Bridges Code */

#include "carla-bridge.h"
#include "osc.h"

static UiData* ui;
static GtkWidget* window;

gboolean gtk_ui_recheck(void*)
{
    return ui->run_messages();
}

void gtk_ui_destroy(GtkWidget*, void*)
{
    window = nullptr;
    gtk_main_quit();
}

void toolkit_init()
{
    int argc = 0;
    char** argv = nullptr;
    gtk_init(&argc, &argv);
}

void toolkit_loop(UiData* ui_, const char* plugin_name, bool reparent)
{
    ui = ui_;

    if (reparent)
    {
        window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_container_add(GTK_CONTAINER(window), (GtkWidget*)ui->get_widget());
    }
    else
    {
        window = (GtkWidget*)ui->get_widget();
    }

    g_timeout_add(50, gtk_ui_recheck, nullptr);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_ui_destroy), nullptr);

    gtk_window_set_resizable(GTK_WINDOW(window), ui->is_resizable());
    gtk_window_set_title(GTK_WINDOW(window), plugin_name);

    osc_send_update();

    // Main loop
    gtk_main();
}

void toolkit_quit()
{
    if (window)
    {
        gtk_widget_destroy(window);
        gtk_main_quit();
    }
}

void toolkit_window_show()
{
    gtk_widget_show_all(window);
}

void toolkit_window_hide()
{
    gtk_widget_hide_all(window);
}

void toolkit_window_resize(int width, int height)
{
    gtk_window_resize(GTK_WINDOW(window), width, height);
}
