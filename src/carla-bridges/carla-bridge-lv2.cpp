/* Carla GUI Bridges Code */

#include "carla-bridge.h"
#include "osc.h"

#include <cstring>
#include <iostream>

#define NS_ATOM       "http://lv2plug.in/ns/ext/atom#"
#define LV2_EVENT_URI "http://lv2plug.in/ns/ext/event"

const uint16_t lv2_feature_id_uri_map         = 0;
const uint16_t lv2_feature_id_urid_map        = 1;
const uint16_t lv2_feature_id_urid_unmap      = 2;
const uint16_t lv2_feature_id_ui_resize       = 3;
const uint16_t lv2_feature_id_ui_parent       = 4;
const uint16_t lv2_feature_count              = 5;

const uint16_t CARLA_URI_MAP_ID_EVENT_MIDI    = 1; // 0x1
const uint16_t CARLA_URI_MAP_ID_EVENT_TIME    = 2; // 0x2
const uint16_t CARLA_URI_MAP_ID_ATOM_STRING   = 3;
const uint16_t CARLA_URI_MAP_ID_COUNT         = 4;

// ----------------- URI-Map Feature -------------------------------------------------
static uint32_t carla_lv2_uri_to_id(LV2_URI_Map_Callback_Data, const char* map, const char* uri)
{
    if (map && strcmp(map, LV2_EVENT_URI) == 0)
    {
        // Event types
        if (strcmp(uri, "http://lv2plug.in/ns/ext/midi#MidiEvent") == 0)
            return CARLA_URI_MAP_ID_EVENT_MIDI;
        else if (strcmp(uri, "http://lv2plug.in/ns/ext/time#Position") == 0)
            return CARLA_URI_MAP_ID_EVENT_TIME;
    }
    else if (strcmp(uri, NS_ATOM "String") == 0)
    {
        return CARLA_URI_MAP_ID_ATOM_STRING;
    }

    return 0;
}

// ----------------- URID Feature ----------------------------------------------------
static LV2_URID carla_lv2_urid_map(LV2_URID_Map_Handle, const char* uri)
{
    if (strcmp(uri, "http://lv2plug.in/ns/ext/midi#MidiEvent") == 0)
        return CARLA_URI_MAP_ID_EVENT_MIDI;
    else if (strcmp(uri, "http://lv2plug.in/ns/ext/time#Position") == 0)
        return CARLA_URI_MAP_ID_EVENT_TIME;
    else if (strcmp(uri, NS_ATOM "String") == 0)
        return CARLA_URI_MAP_ID_ATOM_STRING;

    return 0;
}

static const char* carla_lv2_urid_unmap(LV2_URID_Map_Handle, LV2_URID urid)
{
    if (urid == CARLA_URI_MAP_ID_EVENT_MIDI)
        return "http://lv2plug.in/ns/ext/midi#MidiEvent";
    else if (urid == CARLA_URI_MAP_ID_EVENT_TIME)
        return "http://lv2plug.in/ns/ext/time#Position";
    else if (urid == CARLA_URI_MAP_ID_ATOM_STRING)
        return NS_ATOM "String";

    return nullptr;
}

// ----------------- UI Resize Feature -----------------------------------------------
static int carla_lv2_ui_resize(LV2_UI_Resize_Feature_Data data, int width, int height)
{
    if (data)
    {
        UiData* ui = (UiData*)data;
        ui->queque_message(BRIDGE_MESSAGE_RESIZE_GUI, width, height, 0.0);
        return 0;
    }

    return 1;
}

// ----------------- UI Extension ----------------------------------------------------
static void carla_lv2_ui_write_function(LV2UI_Controller controller, uint32_t port_index, uint32_t buffer_size, uint32_t format, const void* buffer)
{
    if (controller)
    {
        //UiData* ui = (UiData*)controller;

        if (format == 0 && buffer_size == sizeof(float))
        {
            float value = *(float*)buffer;
            osc_send_control(port_index, value);
        }
        else if (format == CARLA_URI_MAP_ID_EVENT_MIDI)
        {
        }
        else if (format == CARLA_URI_MAP_ID_EVENT_TIME)
        {
        }
    }
}

class Lv2UiData : public UiData {
public:
    Lv2UiData()
    {
        handle = nullptr;
        widget = nullptr;
        descriptor = nullptr;

#ifdef WANT_X11
        x11_widget = new QDialog();
#endif

        // Initialize features
        LV2_URI_Map_Feature* URI_Map_Feature = new LV2_URI_Map_Feature;
        URI_Map_Feature->callback_data = this;
        URI_Map_Feature->uri_to_id     = carla_lv2_uri_to_id;

        LV2_URID_Map* URID_Map_Feature = new LV2_URID_Map;
        URID_Map_Feature->handle       = this;
        URID_Map_Feature->map          = carla_lv2_urid_map;

        LV2_URID_Unmap* URID_Unmap_Feature = new LV2_URID_Unmap;
        URID_Unmap_Feature->handle     = this;
        URID_Unmap_Feature->unmap      = carla_lv2_urid_unmap;

        LV2_UI_Resize_Feature* UI_Resize_Feature = new LV2_UI_Resize_Feature;
        UI_Resize_Feature->data        = this;
        UI_Resize_Feature->ui_resize   = carla_lv2_ui_resize;

        features[lv2_feature_id_uri_map]          = new LV2_Feature;
        features[lv2_feature_id_uri_map]->URI     = LV2_URI_MAP_URI;
        features[lv2_feature_id_uri_map]->data    = URI_Map_Feature;

        features[lv2_feature_id_urid_map]         = new LV2_Feature;
        features[lv2_feature_id_urid_map]->URI    = LV2_URID_MAP_URI;
        features[lv2_feature_id_urid_map]->data   = URID_Map_Feature;

        features[lv2_feature_id_urid_unmap]       = new LV2_Feature;
        features[lv2_feature_id_urid_unmap]->URI  = LV2_URID_UNMAP_URI;
        features[lv2_feature_id_urid_unmap]->data = URID_Unmap_Feature;

        features[lv2_feature_id_ui_resize]        = new LV2_Feature;
        features[lv2_feature_id_ui_resize]->URI   = LV2_UI_RESIZE_URI "#UIResize";
        features[lv2_feature_id_ui_resize]->data  = UI_Resize_Feature;

#ifdef WANT_X11
        features[lv2_feature_id_ui_parent]        = new LV2_Feature;
        features[lv2_feature_id_ui_parent]->URI   = LV2_UI_URI "#parent";
        features[lv2_feature_id_ui_parent]->data  = (void*)x11_widget->winId();
#else
        features[lv2_feature_id_ui_parent]        = nullptr;
#endif

        features[lv2_feature_count] = nullptr;

        resizable = true;
    }

    ~Lv2UiData()
    {
        delete (LV2_URI_Map_Feature*)features[lv2_feature_id_uri_map]->data;
        delete (LV2_URID_Map*)features[lv2_feature_id_urid_map]->data;
        delete (LV2_URID_Unmap*)features[lv2_feature_id_urid_unmap]->data;
        delete (LV2_UI_Resize_Feature*)features[lv2_feature_id_ui_resize]->data;

        delete features[lv2_feature_id_uri_map];
        delete features[lv2_feature_id_urid_map];
        delete features[lv2_feature_id_urid_unmap];
        delete features[lv2_feature_id_ui_resize];

#ifdef WANT_X11
        delete features[lv2_feature_id_ui_parent];
        //delete x11_widget;
#endif
    }

    bool init(const char* plugin_uri, const char* ui_uri, const char* ui_bundle, bool resizable_)
    {
        LV2UI_DescriptorFunction ui_descfn = (LV2UI_DescriptorFunction)lib_symbol("lv2ui_descriptor");

        if (ui_descfn)
        {
            uint32_t i = 0;
            while ((descriptor = ui_descfn(i++)))
            {
                if (strcmp(descriptor->URI, ui_uri) == 0)
                    break;
            }

            if (descriptor)
            {
                handle = descriptor->instantiate(descriptor,
                                                 plugin_uri,
                                                 ui_bundle,
                                                 carla_lv2_ui_write_function,
                                                 this,
                                                 &widget,
                                                 features);

                resizable = resizable_;

                return bool(handle);
            }
        }

        return false;
    }

    void close()
    {
        descriptor->cleanup(handle);
    }

    void update_parameter(int index, double value)
    {
        if (descriptor->port_event)
        {
            float value_f = value;
            descriptor->port_event(handle, index, sizeof(float), 0, &value_f);
        }
    }

    void update_program(int) {}
    void update_midi_program(int, int) {}
    void send_note_on(int, int) {}
    void send_note_off(int note, int velocity) {}

    void* get_widget()
    {
#ifdef WANT_X11
        return x11_widget;
#else
        return widget;
#endif
    }

    bool is_resizable()
    {
        return resizable;
    }

private:
    LV2UI_Handle handle;
    LV2UI_Widget widget;
    const LV2UI_Descriptor* descriptor;
    LV2_Feature* features[lv2_feature_count+1];
    bool resizable;

#ifdef WANT_X11
    QDialog* x11_widget;
#endif
};

int main(int argc, char* argv[])
{
    if (argc != 8)
    {
       std::cerr << argv[0] << " :: bad arguments" << std::endl;
       return 1;
    }

    const char* plugin_uri  = argv[1];
    const char* plugin_name = argv[2];
    const char* ui_uri      = argv[3];
    const char* ui_binary   = argv[4];
    const char* ui_bundle   = argv[5];
    const char* osc_url     = argv[6];
    const char* resizable   = argv[7];

    // Init toolkit
    toolkit_init();

    // Init LV2
    Lv2UiData ui;

    // Init OSC
    osc_init(&ui, plugin_name, osc_url);

    if (ui.lib_open(ui_binary))
    {
        if (ui.init(plugin_uri, ui_uri, ui_bundle, (strcmp(resizable, "true") == 0)) == false)
        {
            std::cerr << "Failed to load LV2 UI" << std::endl;
            ui.lib_close();
            osc_close();
            return 1;
        }
    }
    else
    {
        std::cerr << "Failed to load UI: " << ui.lib_error() << std::endl;
        osc_close();
        return 1;
    }

    toolkit_loop(&ui, plugin_name,
#ifdef WANT_X11
                 false);
#else
                 true);
#endif

    ui.close();
    ui.lib_close();
    osc_send_exiting();
    osc_close();
    toolkit_quit();

    return 0;
}
