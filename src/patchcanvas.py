#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import pyqtSlot, qDebug, qCritical, qFatal, Qt, QObject, SIGNAL, SLOT
from PyQt4.QtCore import QAbstractAnimation, QLineF, QPointF, QRectF, QSettings, QString, QTimer
from PyQt4.QtGui import QColor, QLinearGradient, QPen, QPolygonF, QPainter, QPainterPath
from PyQt4.QtGui import QCursor, QFont, QFontMetrics, QInputDialog, QLineEdit, QMenu
from PyQt4.QtGui import QGraphicsScene, QGraphicsItem, QGraphicsLineItem, QGraphicsPathItem, QGraphicsColorizeEffect, QGraphicsDropShadowEffect
from PyQt4.QtSvg import QGraphicsSvgItem, QSvgRenderer

# Imports (Custom)
from theme import *

# Properly convert QString to str
def QStringStr(string):
  return str(unicode(string).encode('utf-8'))

PATCHCANVAS_ORGANISATION_NAME = "Cadence"

# ------------------------------------------------------------------------------
# patchcanvas-api.h

# Port Mode
PORT_MODE_NULL   = 0
PORT_MODE_INPUT  = 1
PORT_MODE_OUTPUT = 2

# Port Type
PORT_TYPE_NULL       = 0
PORT_TYPE_AUDIO_JACK = 1
PORT_TYPE_MIDI_JACK  = 2
PORT_TYPE_MIDI_A2J   = 3
PORT_TYPE_MIDI_ALSA  = 4

# Callback Action
ACTION_GROUP_INFO       = 0 # group_id, N, N
ACTION_GROUP_RENAME     = 1 # group_id, N, new_name
ACTION_GROUP_SPLIT      = 2 # group_id, N, N
ACTION_GROUP_JOIN       = 3 # group_id, N, N
ACTION_PORT_INFO        = 4 # port_id, N, N
ACTION_PORT_RENAME      = 5 # port_id, N, new_name
ACTION_PORTS_CONNECT    = 6 # out_id, in_id, N
ACTION_PORTS_DISCONNECT = 7 # conn_id, N, N

# Icon
ICON_HARDWARE    = 0
ICON_APPLICATION = 1
ICON_LADISH_ROOM = 2

# Split Option
SPLIT_UNDEF = 0
SPLIT_NO    = 1
SPLIT_YES   = 2

# Canvas options
class options_t(object):
  __slots__ = [
    'theme_name',
    'bezier_lines',
    'antialiasing',
    'auto_hide_groups',
    'fancy_eyecandy'
  ]

# Canvas features
class features_t(object):
  __slots__ = [
    'group_info',
    'group_rename',
    'port_info',
    'port_rename',
    'handle_group_pos'
  ]

# ------------------------------------------------------------------------------
# patchcanvas.h

# object types
CanvasBoxType           = QGraphicsItem.UserType + 1
CanvasIconType          = QGraphicsItem.UserType + 2
CanvasPortType          = QGraphicsItem.UserType + 3
CanvasLineType          = QGraphicsItem.UserType + 4
CanvasBezierLineType    = QGraphicsItem.UserType + 5
CanvasLineMovType       = QGraphicsItem.UserType + 6
CanvasBezierLineMovType = QGraphicsItem.UserType + 7

# object lists
class group_dict_t(object):
  __slots__ = [
    'group_id',
    'group_name',
    'split',
    'icon',
    'widgets'
  ]

class port_dict_t(object):
  __slots__ = [
    'group_id',
    'port_id',
    'port_name',
    'port_mode',
    'port_type',
    'widget'
  ]

class connection_dict_t(object):
  __slots__ = [
    'connection_id',
    'port_in_id',
    'port_out_id',
    'widget'
  ]

class animation_dict_t(object):
  __slots__ = [
    'animation',
    'item'
  ]

# Main Canvas object
class Canvas(object):
  __slots__ = [
    'scene',
    'callback',
    'debug',
    'last_z_value',
    'last_group_id',
    'last_connection_id',
    'initial_pos',
    'group_list',
    'port_list',
    'connection_list',
    'animation_list',
    'postponed_groups',
    'qobject',
    'settings',
    'theme',
    'size_rect',
    'initiated'
  ]

# ------------------------------------------------------------------------------
# patchcanvas.cpp

class CanvasObject(QObject):
  def __init__(self, parent=None):
    super(CanvasObject, self).__init__(parent)

  @pyqtSlot()
  def CanvasPostponedGroups(self):
    CanvasPostponedGroups()

  @pyqtSlot()
  def PortContextMenuDisconnect(self):
    connection_id_try = self.sender().data().toInt()
    if (connection_id_try[1]):
      CanvasCallback(ACTION_PORTS_DISCONNECT, connection_id_try[0], 0, "")

# Global objects
canvas = Canvas()
canvas.qobject = None
canvas.settings = None
canvas.theme = None
canvas.initiated = False

options = options_t()
options.theme_name        = getThemeName(getDefaultTheme())
options.bezier_lines      = True
options.antialiasing      = Qt.PartiallyChecked
options.auto_hide_groups  = True
options.fancy_eyecandy    = False

features = features_t()
features.group_info       = False
features.group_rename     = True
features.port_info        = False
features.port_rename      = True
features.handle_group_pos = False

# Internal functions
def bool2str(check):
  return "True" if check else "False"

# PatchCanvas API
def set_options(new_options):
  if (canvas.initiated): return
  options.theme_name        = new_options.theme_name
  options.bezier_lines      = new_options.bezier_lines
  options.antialiasing      = new_options.antialiasing
  options.auto_hide_groups  = new_options.auto_hide_groups
  options.fancy_eyecandy    = new_options.fancy_eyecandy

def set_features(new_features):
  if (canvas.initiated): return
  features.group_info       = new_features.group_info
  features.group_rename     = new_features.group_rename
  features.port_info        = new_features.port_info
  features.port_rename      = new_features.port_rename
  features.handle_group_pos = new_features.handle_group_pos

def init(scene, callback, debug=False):
  if (debug):
    qDebug("patchcanvas::init(%s, %s, %s)" % (scene, callback, bool2str(debug)))

  if (canvas.initiated):
    qCritical("patchcanvas::init() - already initiated")
    return

  if (not callback):
    qFatal("patchcanvas::init() - fatal error: callback not set")
    return

  canvas.scene = scene
  canvas.callback = callback
  canvas.debug = debug

  canvas.last_z_value = 0
  canvas.last_group_id = 0
  canvas.last_connection_id = 0
  canvas.initial_pos = QPointF(0, 0)

  canvas.group_list = []
  canvas.port_list = []
  canvas.connection_list = []
  canvas.animation_list = []

  canvas.postponed_groups = []
  if (not canvas.qobject): canvas.qobject = CanvasObject()
  if (not canvas.settings): canvas.settings = QSettings(PATCHCANVAS_ORGANISATION_NAME, "PatchCanvas")

  for i in range(Theme.THEME_MAX):
    this_theme_name = getThemeName(i)
    if (this_theme_name == options.theme_name):
      canvas.theme = Theme(i)
      break
  else:
    canvas.theme = Theme(getDefaultTheme())

  canvas.size_rect = QRectF()

  canvas.scene.rubberbandByTheme()
  canvas.scene.setBackgroundBrush(canvas.theme.canvas_bg)

  canvas.initiated = True

def clear():
  if (canvas.debug):
    qDebug("patchcanvas::clear()")

  tmp_group_list = []
  tmp_port_list = []
  tmp_connection_list = []

  for i in range(len(canvas.group_list)):
    tmp_group_list.append(canvas.group_list[i].group_id)

  for i in range(len(canvas.port_list)):
    tmp_port_list.append(canvas.port_list[i].port_id)

  for i in range(len(canvas.connection_list)):
    tmp_connection_list.append(canvas.connection_list[i].connection_id)

  for i in range(len(canvas.animation_list)):
    if (canvas.animation_list[i].animation.state() == QAbstractAnimation.Running):
      canvas.animation_list[i].animation.stop()
      RemoveItemFX(canvas.animation_list[i].item)
  
  canvas.animation_list = []

  for i in range(len(tmp_connection_list)):
    disconnectPorts(tmp_connection_list[i])

  for i in range(len(tmp_port_list)):
    removePort(tmp_port_list[i])

  for i in range(len(tmp_group_list)):
    removeGroup(tmp_group_list[i])

  canvas.last_z_value = 0
  canvas.last_group_id = 0
  canvas.last_connection_id = 0

  canvas.group_list = []
  canvas.port_list = []
  canvas.connection_list = []
  canvas.postponed_groups = []

  canvas.initiated = False

def setInitialPos(x, y):
  if (canvas.debug):
    qDebug("patchcanvas::setInitialPos(%i, %i)" % (x, y))

  canvas.initial_pos.setX(x)
  canvas.initial_pos.setY(y)

def setCanvasSize(x, y, width, height):
  if (canvas.debug):
    qDebug("patchcanvas::setCanvasSize(%i, %i, %i, %i)" % (x, y, width, height))

  canvas.size_rect.setX(x)
  canvas.size_rect.setY(y)
  canvas.size_rect.setWidth(width)
  canvas.size_rect.setHeight(height)

def addGroup(group_id, group_name, split=SPLIT_UNDEF, icon=ICON_APPLICATION):
  if (type(group_name) != QString):
    group_name = QString(group_name)

  if (canvas.debug):
    qDebug("patchcanvas::addGroup(%i, %s, %i, %i)" % (group_id, QStringStr(group_name), split, icon))

  if (split == SPLIT_UNDEF and features.handle_group_pos):
    split_try = canvas.settings.value(QString("CanvasPositions/%1_s").arg(group_name), split).toInt()
    if (split_try[1]):
      split = split_try[0]

  group_box  = CanvasBox(group_id, group_name, icon)

  group_dict = group_dict_t()
  group_dict.group_id   = group_id
  group_dict.group_name = group_name
  group_dict.split = (split == SPLIT_YES)
  group_dict.icon  = icon
  group_dict.widgets = [group_box, None]

  if (split == SPLIT_YES):
    group_box.setSplit(True, PORT_MODE_OUTPUT)

    if (features.handle_group_pos):
      group_box.setPos(canvas.settings.value(QString("CanvasPositions/%1_o").arg(group_name), CanvasGetNewGroupPos()).toPointF())
    else:
      group_box.setPos(CanvasGetNewGroupPos())

    group_sbox = CanvasBox(group_id, group_name, icon)
    group_sbox.setSplit(True, PORT_MODE_INPUT)

    group_dict.widgets[1] = group_sbox

    if (features.handle_group_pos):
      group_sbox.setPos(canvas.settings.value(QString("CanvasPositions/%1_i").arg(group_name), CanvasGetNewGroupPos(True)).toPointF())
    else:
      group_sbox.setPos(CanvasGetNewGroupPos(True))

    if (not options.auto_hide_groups and options.fancy_eyecandy):
      ItemFX(group_sbox, True)

    canvas.last_z_value += 1
    group_sbox.setZValue(canvas.last_z_value)

  else:
    group_box.setSplit(False)

    if (features.handle_group_pos):
      group_box.setPos(canvas.settings.value(QString("CanvasPositions/%1").arg(group_name), CanvasGetNewGroupPos()).toPointF())
    else:
      # Special ladish fake-split groups
      horizontal = (icon == ICON_HARDWARE or icon == ICON_LADISH_ROOM)
      group_box.setPos(CanvasGetNewGroupPos(horizontal))

  if (not options.auto_hide_groups and options.fancy_eyecandy):
    ItemFX(group_box, True)

  canvas.last_z_value += 1
  group_box.setZValue(canvas.last_z_value)

  canvas.group_list.append(group_dict)

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def removeGroup(group_id):
  if (canvas.debug):
    qDebug("patchcanvas::removeGroup(%i)" % (group_id))

  if (CanvasGetGroupPortCount(group_id) > 0):
    if (canvas.debug):
      qDebug("patchcanvas::removeGroup() - This group still has ports, postpone it's removal")
    canvas.postponed_groups.append(group_id)
    QTimer.singleShot(100, canvas.qobject, SLOT("CanvasPostponedGroups()"))
    return

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      item = canvas.group_list[i].widgets[0]
      group_name = canvas.group_list[i].group_name

      if (canvas.group_list[i].split):
        s_item = canvas.group_list[i].widgets[1]

        if (features.handle_group_pos):
          canvas.settings.setValue(QString("CanvasPositions/%1_o").arg(group_name), item.pos())
          canvas.settings.setValue(QString("CanvasPositions/%1_i").arg(group_name), s_item.pos())
          canvas.settings.setValue(QString("CanvasPositions/%1_s").arg(group_name), SPLIT_YES)

        if (options.fancy_eyecandy and s_item.isVisible()):
          ItemFX(s_item, False)
        else:
          s_item.removeIconFromScene()
          canvas.scene.removeItem(s_item)

      else:
        if (features.handle_group_pos):
          canvas.settings.setValue(QString("CanvasPositions/%1").arg(group_name), item.pos())
          canvas.settings.setValue(QString("CanvasPositions/%1_s").arg(group_name), SPLIT_NO)

      if (options.fancy_eyecandy and item.isVisible()):
        ItemFX(item, False)
      else:
        item.removeIconFromScene()
        canvas.scene.removeItem(item)

      canvas.group_list.pop(i)

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::removeGroup() - Unable to find group to remove")

def renameGroup(group_id, new_group_name):
  if (type(new_group_name) != QString):
    new_group_name = QString(new_group_name)

  if (canvas.debug):
    qDebug("patchcanvas::renameGroup(%i, %s)" % (group_id, QStringStr(new_name)))

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      canvas.group_list[i].widgets[0].setGroupName(new_group_name)
      canvas.group_list[i].group_name = new_group_name

      if (canvas.group_list[i].split):
        canvas.group_list[i].widgets[1].setGroupName(new_group_name)

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::renameGroup() - Unable to find group to rename")

def splitGroup(group_id):
  if (canvas.debug):
    qDebug("patchcanvas::splitGroup(%i)" % (group_id))

  item = None
  group_name = ""
  group_icon = ICON_APPLICATION
  ports_data = []
  conns_data = []

  # Step 1 - Store all Item data
  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      if (canvas.group_list[i].split):
        qCritical("patchcanvas::splitGroup() - group is already splitted")
        return

      item = canvas.group_list[i].widgets[0]
      group_name = canvas.group_list[i].group_name
      group_icon = canvas.group_list[i].icon
      break

  if (not item):
    qCritical("PatchCanvas::splitGroup() - Unable to find group to split")
    return

  port_list_ids = list(item.getPortList())

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id in port_list_ids):
      port_dict = port_dict_t()
      port_dict.group_id  = canvas.port_list[i].group_id
      port_dict.port_id   = canvas.port_list[i].port_id
      port_dict.port_name = canvas.port_list[i].port_name
      port_dict.port_mode = canvas.port_list[i].port_mode
      port_dict.port_type = canvas.port_list[i].port_type
      port_dict.widget    = None
      ports_data.append(port_dict)

  for i in range(len(canvas.connection_list)):
    if (canvas.connection_list[i].port_out_id in port_list_ids or canvas.connection_list[i].port_in_id in port_list_ids):
      conns_data.append(canvas.connection_list[i])

  # Step 2 - Remove Item and Children
  for i in range(len(conns_data)):
    disconnectPorts(conns_data[i].connection_id)

  for i in range(len(port_list_ids)):
    removePort(port_list_ids[i])

  removeGroup(group_id)

  # Step 3 - Re-create Item, now splitted
  addGroup(group_id, group_name, SPLIT_YES, group_icon)

  for i in range(len(ports_data)):
    addPort(group_id, ports_data[i].port_id, ports_data[i].port_name, ports_data[i].port_mode, ports_data[i].port_type)

  for i in range(len(conns_data)):
    connectPorts(conns_data[i].connection_id, conns_data[i].port_out_id, conns_data[i].port_in_id)

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def joinGroup(group_id):
  if (canvas.debug):
    qDebug("patchcanvas::joinGroup(%i)" % (group_id))

  item = None
  s_item = None
  group_name = ""
  group_icon = ICON_APPLICATION
  ports_data = []
  conns_data = []

  # Step 1 - Store all Item data
  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      if (not canvas.group_list[i].split):
        qCritical("patchcanvas::joinGroup() - group is not splitted")
        return

      item   = canvas.group_list[i].widgets[0]
      s_item = canvas.group_list[i].widgets[1]
      group_name = canvas.group_list[i].group_name
      group_icon = canvas.group_list[i].icon
      break
  else:
    qCritical("patchcanvas::joinGroup() - Unable to find groups to join")
    return

  port_list_ids = list(item.getPortList())
  port_list_idss = s_item.getPortList()
  for i in range(len(port_list_idss)):
    if (port_list_idss[i] not in port_list_ids):
      port_list_ids.append(port_list_idss[i])

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id in port_list_ids):
      port_dict = port_dict_t()
      port_dict.group_id  = canvas.port_list[i].group_id
      port_dict.port_id   = canvas.port_list[i].port_id
      port_dict.port_name = canvas.port_list[i].port_name
      port_dict.port_mode = canvas.port_list[i].port_mode
      port_dict.port_type = canvas.port_list[i].port_type
      port_dict.widget    = None
      ports_data.append(port_dict)

  for i in range(len(canvas.connection_list)):
    if (canvas.connection_list[i].port_out_id in port_list_ids or canvas.connection_list[i].port_in_id in port_list_ids):
      conns_data.append(canvas.connection_list[i])

  # Step 2 - Remove Item and Children
  for i in range(len(conns_data)):
    disconnectPorts(conns_data[i].connection_id)

  for i in range(len(port_list_ids)):
    removePort(port_list_ids[i])

  removeGroup(group_id)

  # Step 3 - Re-create Item, now together
  addGroup(group_id, group_name, SPLIT_NO, group_icon)

  for i in range(len(ports_data)):
    addPort(group_id, ports_data[i].port_id, ports_data[i].port_name, ports_data[i].port_mode, ports_data[i].port_type)

  for i in range(len(conns_data)):
    connectPorts(conns_data[i].connection_id, conns_data[i].port_out_id, conns_data[i].port_in_id)

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def getGroupPos(group_id, port_mode=PORT_MODE_OUTPUT):
  if (canvas.debug):
    qDebug("patchcanvas::getGroupPos(%i, %i)" % (group_id, port_mode))

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      if (canvas.group_list[i].split):
        if (port_mode == PORT_MODE_OUTPUT):
          return canvas.group_list[i].widgets[0].pos()
        elif (port_mode == PORT_MODE_INPUT):
          return canvas.group_list[i].widgets[1].pos()
        else:
          return QPointF(0,0)
      else:
        return canvas.group_list[i].widgets[0].pos()

  qCritical("patchcanvas::getGroupPos() - Unable to find group")

def setGroupPos(group_id, group_pos_x, group_pos_y):
  setGroupPos(group_id, group_pos_x, group_pos_y, group_pos_x, group_pos_y)

def setGroupPos(group_id, group_pos_x_o, group_pos_y_o, group_pos_x_i, group_pos_y_i):
  if (canvas.debug):
    qDebug("patchcanvas::setGroupPos(%i, %i, %i, %i, %i)" % (group_id, group_pos_x_o, group_pos_y_o, group_pos_x_i, group_pos_y_i))

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      canvas.group_list[i].widgets[0].setPos(group_pos_x_o, group_pos_y_o)

      if (canvas.group_list[i].split):
        canvas.group_list[i].widgets[1].setPos(group_pos_x_i, group_pos_y_i)

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::setGroupPos() - Unable to find group to reposition")

def setGroupIcon(group_id, icon):
  if (canvas.debug):
    qDebug("patchcanvas::setGroupIcon(%i, %i)" % (group_id, icon))

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      canvas.group_list[i].icon = icon
      canvas.group_list[i].widgets[0].setIcon(icon)

      if (canvas.group_list[i].split):
        canvas.group_list[i].widgets[1].setIcon(icon)

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::setGroupIcon() - Unable to find group to change icon")

def addPort(group_id, port_id, port_name, port_mode, port_type):
  if (type(port_name) != QString):
    port_name = QString(port_name)

  if (canvas.debug):
    qDebug("patchcanvas::addPort(%i, %i, %s, %i, %i)" % (group_id, port_id, QStringStr(port_name), port_mode, port_type))

  box_widget = None
  port_widget = None

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      n_widget = 0
      if (canvas.group_list[i].split and canvas.group_list[i].widgets[0].getSplittedMode() != port_mode):
        n_widget = 1
      box_widget = canvas.group_list[i].widgets[n_widget]
      port_widget = box_widget.addPortFromGroup(port_id, port_mode, port_type, port_name)
      break

  if (not box_widget or not port_widget):
    qCritical("patchcanvas::addPort() - Unable to find parent group")
    return

  if (options.fancy_eyecandy):
    ItemFX(port_widget, True)

  port_dict = port_dict_t()
  port_dict.group_id  = group_id
  port_dict.port_id   = port_id
  port_dict.port_name = port_name
  port_dict.port_mode = port_mode
  port_dict.port_type = port_type
  port_dict.widget    = port_widget
  canvas.port_list.append(port_dict)

  box_widget.updatePositions()

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def removePort(port_id):
  if (canvas.debug):
    qDebug("patchcanvas::removePort(%i)" % (port_id))

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_id):
      item = canvas.port_list[i].widget
      item.parentItem().removePortFromGroup(port_id)
      if (options.fancy_eyecandy):
        ItemFX(item, False, True)
      else:
        canvas.scene.removeItem(item)
      canvas.port_list.pop(i)

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::removePort() - Unable to find port to remove")

def renamePort(port_id, new_port_name):
  if (type(new_port_name) != QString):
    new_port_name = QString(new_port_name)

  if (canvas.debug):
    qDebug("patchcanvas::renamePort(%i, %s)" % (port_id, QStringStr(new_port_name)))

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_id):
      canvas.port_list[i].port_name = new_port_name
      canvas.port_list[i].widget.setPortName(new_port_name)
      canvas.port_list[i].widget.parentItem().updatePositions()

      QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))
      return

  qCritical("patchcanvas::renamePort() - Unable to find port to rename")

def connectPorts(connection_id, port_out_id, port_in_id):
  if (canvas.debug):
    qDebug("patchcanvas::connectPorts(%i, %i, %i)" % (connection_id, port_out_id, port_in_id))

  port_out = None
  port_in  = None
  port_out_parent = None
  port_in_parent  = None

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_out_id):
      port_out = canvas.port_list[i].widget
      port_out_parent = port_out.parentItem()
    elif (canvas.port_list[i].port_id == port_in_id):
      port_in = canvas.port_list[i].widget
      port_in_parent = port_in.parentItem()

  if (not port_out or not port_in):
    qCritical("patchcanvas::connectPorts() - Unable to find ports to connect")
    return

  connection_dict = connection_dict_t()
  connection_dict.connection_id = connection_id
  connection_dict.port_out_id = port_out_id
  connection_dict.port_in_id  = port_in_id

  if (options.bezier_lines):
    connection_dict.widget = CanvasBezierLine(port_out, port_in, None)
  else:
    connection_dict.widget = CanvasLine(port_out, port_in, None)

  port_out_parent.addLineFromGroup(connection_dict.widget, connection_id)
  port_in_parent.addLineFromGroup(connection_dict.widget, connection_id)

  canvas.last_z_value += 1
  port_out_parent.setZValue(canvas.last_z_value)
  port_in_parent.setZValue(canvas.last_z_value)

  canvas.last_z_value += 1
  connection_dict.widget.setZValue(canvas.last_z_value)

  canvas.connection_list.append(connection_dict)

  if (options.fancy_eyecandy):
    ItemFX(connection_dict.widget, True)

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def disconnectPorts(connection_id):
  if (canvas.debug):
    qDebug("patchcanvas::disconnectPorts(%i)" % (connection_id))

  port_1_id = port_2_id = 0
  line  = None
  item1 = None
  item2 = None

  for i in range(len(canvas.connection_list)):
    if (canvas.connection_list[i].connection_id == connection_id):
      port_1_id = canvas.connection_list[i].port_out_id
      port_2_id = canvas.connection_list[i].port_in_id
      line = canvas.connection_list[i].widget
      canvas.connection_list.pop(i)
      break

  if (not line):
    qCritical("patchcanvas::disconnectPorts - Unable to find connection ports")
    return

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_1_id):
      item1 = canvas.port_list[i].widget
      break
  else:
    qCritical("patchcanvas::disconnectPorts - Unable to find output port")
    return

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_2_id):
      item2 = canvas.port_list[i].widget
      break
  else:
    qCritical("patchcanvas::disconnectPorts - Unable to find input port")
    return

  item1.parentItem().removeLineFromGroup(connection_id)
  item2.parentItem().removeLineFromGroup(connection_id)

  if (options.fancy_eyecandy):
    ItemFX(line, False, True)
  else:
    canvas.scene.removeItem(line)

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

def Arrange():
  if (canvas.debug):
    qDebug("patchcanvas::Arrange()")

  # Make graph
  graph = []
  items = canvas.scene.items()
  for i in range(len(items)):
    if (items[i].type() == CanvasBoxType):
      ports = []
      conns = []
      port_flags = 0
      #for j in range(len(items[i].port_list)):
        #port_flags |= items[i].port_list[j].port_mode
        #ports.append(items[i].port_list[j].port_id)
      for k in range(len(items[i].connection_lines)):
        conns.append(items[i].connection_lines[k].connection_id)
      graph.append((items[i], ports, port_flags, conns))

  # Initial X values
  initial_x = canvas.initial_pos.x()-(canvas.initial_pos.x()*1/3)
  min_x_out = initial_x
  min_x_both = initial_x
  min_x_in = initial_x

  # Get width of boxes
  for i in range(len(graph)):
    if (graph[i][2] == PORT_MODE_INPUT+PORT_MODE_OUTPUT):
      item_width = graph[i][0].box_width
      if (item_width > min_x_in-initial_x):
        min_x_in = item_width+initial_x
    elif (graph[i][2] == PORT_MODE_OUTPUT):
      item_width = graph[i][0].box_width
      if (item_width > min_x_both-initial_x):
        min_x_both = item_width+initial_x

  # Make some space between ports
  if (min_x_both > initial_x):
    min_x_both += 100.0

  if (min_x_in > initial_x):
    min_x_in += 100.0

  min_x_in += min_x_both-initial_x

  # Initial Y values
  last_y_out = last_y_both = last_y_in = canvas.initial_pos.y() #-(canvas.initial_pos.y()*1/3)

  # List of groups that have already been repositioned
  items_ready = []

  # Move boxes with connections
  for i in range(len(graph)):
    item = graph[i][0]
    ports = graph[i][1]
    port_flags = graph[i][2]
    conns = graph[i][3]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and conns):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and conns):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and conns):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  # Move boxes without connections
  for i in range(len(graph)):
    item = graph[i][0]
    ports = graph[i][1]
    port_flags = graph[i][2]
    conns = graph[i][3]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  # Move empty boxes
  for i in range(len(graph)):
    item = graph[i][0]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and not item.group_id in items_ready):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and not item.group_id in items_ready):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and not item.group_id in items_ready):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  QTimer.singleShot(0, canvas.scene, SIGNAL("update()"))

# Extra Internal functions

def CanvasGetGroupName(group_id):
  if (canvas.debug):
    qDebug("PatchCanvas::CanvasGetGroupName(%i)", group_id)

  for i in range(len(canvas.group_list)):
    if (canvas.group_list[i].group_id == group_id):
      return canvas.group_list[i].group_name

  qCritical("PatchCanvas::CanvasGetGroupName() - unable to find group")
  return QString("")

def CanvasGetGroupPortCount(group_id):
  if (canvas.debug):
    qDebug("patchcanvas::CanvasGetGroupPortCount(%i)" % (group_id))

  port_count = 0
  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].group_id == group_id):
      port_count += 1

  return port_count

def CanvasGetNewGroupPos(horizontal=False):
  if (canvas.debug):
    qDebug("patchcanvas::CanvasGetNewGroupPos(%s)" % (bool2str(horizontal)))

  new_pos = QPointF(canvas.initial_pos.x(), canvas.initial_pos.y())
  items = canvas.scene.items()

  break_loop = False
  while (not break_loop):
    break_for = False
    for i in range(len(items)):
      if (items[i].type() == CanvasBoxType):
        if (items[i].sceneBoundingRect().contains(new_pos)):
          if (horizontal):
            new_pos += QPointF(items[i].boundingRect().width()+15, 0)
          else:
            new_pos += QPointF(0, items[i].boundingRect().height()+15)
          break_for = True
          break

      if (i >= len(items)-1 and not break_for):
        break_loop = True

  return new_pos

def CanvasGetPortName(port_id):
  if (canvas.debug):
    qDebug("PatchCanvas::CanvasGetPortName(%i)" % (port_id))

  for i in range(len(canvas.port_list)):
    if (canvas.port_list[i].port_id == port_id):
      group_id = canvas.port_list[i].group_id
      for j in range(len(canvas.group_list)):
        if (canvas.group_list[j].group_id == group_id):
          return canvas.group_list[j].group_name + ":" + canvas.port_list[i].port_name
      break

  qCritical("PatchCanvas::CanvasGetPortName() - unable to find port")
  return QString("")

def CanvasGetPortConnectionList(port_id):
  if (canvas.debug):
    qDebug("patchcanvas::CanvasGetPortConnectionList(%i)" % (port_id))

  port_con_list = []

  for i in range(len(canvas.connection_list)):
    if (canvas.connection_list[i].port_out_id == port_id or canvas.connection_list[i].port_in_id == port_id):
      port_con_list.append(canvas.connection_list[i].connection_id)

  return port_con_list

def CanvasGetConnectedPort(connection_id, port_id):
  if (canvas.debug):
    qDebug("PatchCanvas::CanvasGetConnectedPort(%i, %i)" % (connection_id, port_id))

  for i in range(len(canvas.connection_list)):
    if (canvas.connection_list[i].connection_id == connection_id):
      if (canvas.connection_list[i].port_out_id == port_id):
        return canvas.connection_list[i].port_in_id
      else:
        return canvas.connection_list[i].port_out_id

  qCritical("PatchCanvas::CanvasGetConnectedPort() - unable to find connection")
  return 0

def CanvasPostponedGroups():
  if (canvas.debug):
    qDebug("patchcanvas::CanvasPostponedGroups()")

  for i in range(len(canvas.postponed_groups)):
    group_id = canvas.postponed_groups[i]

    for j in range(len(canvas.group_list)):
      if (canvas.group_list[j].group_id == group_id):
        item = canvas.group_list[j].widgets[0]
        s_item = None

        if (canvas.group_list[j].split):
          s_item = canvas.group_list[j].widgets[1]

        if (item.getPortCount() == 0 and (not s_item or s_item.getPortCount() == 0)):
          removeGroup(group_id)
          canvas.postponed_groups.pop(i)

        break

  if (len(canvas.postponed_groups) > 0):
    QTimer.singleShot(100, canvas.qobject, SLOT("CanvasPostponedGroups()"));

def CanvasCallback(action, value1, value2, value_str):
  if (canvas.debug):
    qDebug("PatchCanvas::CanvasCallback(%i, %i, %i, %s)" % (action, value1, value2, QStringStr(value_str)))

  canvas.callback(action, value1, value2, value_str);

def ItemFX(item, show, destroy=True):
  if (canvas.debug):
    qDebug("patchcanvas::ItemFX(%s, %s, %s)" % (item, bool2str(show), bool2str(destroy)))

  # Check if item already has an animationItemFX
  for i in range(len(canvas.animation_list)):
    if (canvas.animation_list[i].item == item):
      canvas.animation_list[i].animation.stop()
      canvas.animation_list.pop(i)
      break

  animation = CanvasFadeAnimation(item, show)
  animation.setDuration(750 if (show) else 500)
  animation.start()

  animation_dict = animation_dict_t()
  animation_dict.animation = animation
  animation_dict.item = item
  canvas.animation_list.append(animation_dict)

  if (not show):
    if (destroy):
      QObject.connect(animation, SIGNAL("finished()"), lambda item_=item: RemoveItemFX(item_))
    else:
      QObject.connect(animation, SIGNAL("finished()"), lambda vis=False: item.setVisible(vis))

def RemoveItemFX(item):
  if (canvas.debug):
    qDebug("patchcanvas::RemoveItemFX(%s)" % (item))

  if (item.type() == CanvasBoxType):
    item.removeIconFromScene()

  canvas.scene.removeItem(item)

# ------------------------------------------------------------------------------
# patchscene.cpp

class PatchScene(QGraphicsScene):
    def __init__(self, parent):
        super(PatchScene, self).__init__(parent)

        self.ctrl_down = False
        self.mouse_down_init  = False
        self.mouse_rubberband = False
        self.fake_selection  = False
        self.fake_rubberband = self.addRect(QRectF(0, 0, 0, 0))
        self.fake_rubberband.setZValue(-1)
        self.fake_rubberband.hide()
        self.orig_point = QPointF(0, 0)

    def rubberbandByTheme(self):
        self.fake_rubberband.setPen(canvas.theme.rubberband_pen)
        self.fake_rubberband.setBrush(canvas.theme.rubberband_brush)

    def fixScaleFactor(self):
        view  = self.views()[0]
        scale = view.transform().m11()
        if (scale > 3.0):
          view.resetTransform()
          view.scale(3.0, 3.0)
        elif (scale < 0.2):
          view.resetTransform()
          view.scale(0.2, 0.2)
        self.emit(SIGNAL("scaleChanged(double)"), view.transform().m11())

    def zoom_fit(self):
      min_x = min_y = max_x = max_y = None
      items_list = self.items()

      if (len(items_list) > 0):
        for i in range(len(items_list)):
          if (items_list[i].isVisible() and items_list[i].type() == CanvasBoxType):
            pos  = items_list[i].scenePos()
            rect = items_list[i].boundingRect()

            if (not min_x):
              min_x = pos.x()
            elif (pos.x() < min_x):
              min_x = pos.x()

            if (not min_y):
              min_y = pos.y()
            elif (pos.y() < min_y):
              min_y = pos.y()

            if (not max_x):
              max_x = pos.x()+rect.width()
            elif (pos.x()+rect.width() > max_x):
              max_x = pos.x()+rect.width()

            if (not max_y):
              max_y = pos.y()+rect.height()
            elif (pos.y()+rect.height() > max_y):
              max_y = pos.y()+rect.height()

        self.views()[0].fitInView(min_x, min_y, abs(max_x-min_x), abs(max_y-min_y), Qt.KeepAspectRatio)
        self.fixScaleFactor()

    def zoom_in(self):
        view = self.views()[0]
        if (view.transform().m11() < 3.0):
          view.scale(1.2, 1.2)
        self.emit(SIGNAL("scaleChanged(double)"), view.transform().m11())

    def zoom_out(self):
        view = self.views()[0]
        if (view.transform().m11() > 0.2):
          view.scale(0.8, 0.8)
        self.emit(SIGNAL("scaleChanged(double)"), view.transform().m11())

    def zoom_reset(self):
        view = self.views()[0]
        view.resetTransform()
        self.emit(SIGNAL("scaleChanged(double)"), 1.0)

    def keyPressEvent(self, event):
        if (event.key() == Qt.Key_Control):
          self.ctrl_down = True

        if (event.key() == Qt.Key_Home):
          self.zoom_fit()
          event.accept()

        elif (self.ctrl_down):
          if (event.key() == Qt.Key_Plus):
            self.zoom_in()
            event.accept()
          elif (event.key() == Qt.Key_Minus):
            self.zoom_out()
            event.accept()
          elif (event.key() == Qt.Key_1):
            self.zoom_reset()
            event.accept()
          else:
            QGraphicsScene.keyPressEvent(self, event)

        else:
          QGraphicsScene.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        if (event.key() == Qt.Key_Control):
          self.ctrl_down = False
        QGraphicsScene.keyReleaseEvent(self, event)

    def wheelEvent(self, event):
        if (self.ctrl_down):
          view   = self.views()[0]
          factor = 1.41 ** (event.delta()/240.0)
          view.scale(factor, factor)

          self.fixScaleFactor()
          event.accept()

        else:
          QGraphicsScene.wheelEvent(self, event)

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton):
          self.mouse_down_init = True
        else:
          self.mouse_down_init = False
        self.mouse_rubberband = False
        QGraphicsScene.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (self.mouse_down_init):
          self.mouse_rubberband = (len(self.selectedItems()) == 0)
          self.mouse_down_init = False

        if (self.mouse_rubberband):
          if (not self.fake_selection):
            self.orig_point = event.scenePos()
            self.fake_rubberband.show()
            self.fake_selection = True

          pos = event.scenePos()

          if (pos.x() > self.orig_point.x()): x = self.orig_point.x()
          else: x = pos.x()

          if (pos.y() > self.orig_point.y()): y = self.orig_point.y()
          else: y = pos.y()

          self.fake_rubberband.setRect(x, y, abs(pos.x()-self.orig_point.x()), abs(pos.y()-self.orig_point.y()))

          event.accept()

        else:
          QGraphicsScene.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if (self.fake_selection):
          items_list = self.items()
          if (len(items_list) > 0):
            for i in range(len(items_list)):
              if (items_list[i].isVisible() and items_list[i].type() == CanvasBoxType):
                item_rect = items_list[i].sceneBoundingRect()
                if ( self.fake_rubberband.contains(QPointF(item_rect.x(), item_rect.y())) and
                     self.fake_rubberband.contains(QPointF(item_rect.x()+item_rect.width(), item_rect.y()+item_rect.height())) ):
                  items_list[i].setSelected(True)

            self.fake_rubberband.hide()
            self.fake_rubberband.setRect(0, 0, 0, 0)
            self.fake_selection = False

        else:
          items_list = self.selectedItems()
          for i in range(len(items_list)):
            if (items_list[i].isVisible() and items_list[i].type() == CanvasBoxType):
              cbox = items_list[i]
              cbox.checkItemPos()
              is_input = (cbox.isSplitted() and cbox.getSplittedMode() == PORT_MODE_INPUT)
              self.emit(SIGNAL("sceneGroupMoved(int, int, QPointF)"), cbox.getGroupId(), cbox.getSplittedMode(), cbox.scenePos())

        self.mouse_down_init = False
        self.mouse_rubberband = False
        QGraphicsScene.mouseReleaseEvent(self, event)

# ------------------------------------------------------------------------------
# canvasfadeanimation.cpp

class CanvasFadeAnimation(QAbstractAnimation):
    def __init__(self, item, show, parent=None):
        super(CanvasFadeAnimation, self).__init__(parent)

        self._item = item
        self._show = show
        self._duration = 0

    def setDuration(self, time):
        if (not self._show and self._item.opacity() == 0):
          self._duration = 0
        else:
          self._item.show()
          self._duration = time

    def duration(self):
        return self._duration

    def totalDuration(self):
        return self._duration

    def updateCurrentTime(self, time):
        if (self._duration == 0):
          return

        if (self._show):
          value = float(time)/self._duration
        else:
          value = 1.0-(float(time)/self._duration)

        self._item.setOpacity(value)

    def updateDirection(self, direction):
        pass

    def updateState(self, oldState, newState):
        pass

# ------------------------------------------------------------------------------
# canvasline.cpp

class CanvasLine(QGraphicsLineItem):
    def __init__(self, item1, item2, parent):
        super(CanvasLine, self).__init__(parent, canvas.scene)

        if (options.fancy_eyecandy):
          self.setOpacity(0)

        self.item1 = item1
        self.item2 = item2

        self.locked = False
        self.line_selected = False

        self.setGraphicsEffect(None)
        self.updateLinePos()

    def isLocked(self):
        return self.locked

    def setLocked(self, yesno):
        self.locked = yesno

    def isLineSelected(self):
        return self.line_selected

    def setLineSelected(self, yesno):
        if (self.locked): return
        if (options.fancy_eyecandy):
          if (yesno):
            self.setGraphicsEffect(CanvasPortGlow(self.item1.getPortType(), self.toGraphicsObject()))
          else:
            self.setGraphicsEffect(None)

        self.updateLineGradient(yesno)
        self.line_selected = True

    def updateLinePos(self):
        if (self.item1.getPortMode() == PORT_MODE_OUTPUT):
          line = QLineF(self.item1.scenePos().x()+self.item1.getPortWidth()+12, self.item1.scenePos().y()+7.5, self.item2.scenePos().x(), self.item2.scenePos().y()+7.5)
          self.setLine(line)

          self.line_selected = False
          self.updateLineGradient(self.line_selected)

    def updateLineGradient(self, selected):
        pos_top = self.boundingRect().top()
        pos_bot = self.boundingRect().bottom()
        if (self.item2.scenePos().y() >= self.item1.scenePos().y()):
          pos1 = 0
          pos2 = 1
        else:
          pos1 = 1
          pos2 = 0

        port_type1 = self.item1.getPortType()
        port_type2 = self.item2.getPortType()
        port_gradient = QLinearGradient(0, pos_top, 0, pos_bot)

        if (port_type1 == PORT_TYPE_AUDIO_JACK):
          port_gradient.setColorAt(pos1, canvas.theme.line_audio_jack_sel if (selected) else canvas.theme.line_audio_jack)
        elif (port_type1 == PORT_TYPE_MIDI_JACK):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_jack_sel if (selected) else canvas.theme.line_midi_jack)
        elif (port_type1 == PORT_TYPE_MIDI_A2J):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_a2j_sel if (selected) else canvas.theme.line_midi_a2j)
        elif (port_type1 == PORT_TYPE_MIDI_ALSA):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_alsa_sel if (selected) else canvas.theme.line_midi_alsa)

        if (port_type2 == PORT_TYPE_AUDIO_JACK):
          port_gradient.setColorAt(pos2, canvas.theme.line_audio_jack_sel if (selected) else canvas.theme.line_audio_jack)
        elif (port_type2 == PORT_TYPE_MIDI_JACK):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_jack_sel if (selected) else canvas.theme.line_midi_jack)
        elif (port_type2 == PORT_TYPE_MIDI_A2J):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_a2j_sel if (selected) else canvas.theme.line_midi_a2j)
        elif (port_type2 == PORT_TYPE_MIDI_ALSA):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_alsa_sel if (selected) else canvas.theme.line_midi_alsa)

        self.setPen(QPen(port_gradient, 2))

    def type(self):
        return CanvasLineType

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options.antialiasing))
        QGraphicsLineItem.paint(self, painter, option, widget)

# ------------------------------------------------------------------------------
# canvasbezierline.cpp

class CanvasBezierLine(QGraphicsPathItem):
    def __init__(self, item1, item2, parent):
        super(CanvasBezierLine, self).__init__(parent, canvas.scene)

        if (options.fancy_eyecandy):
          self.setOpacity(0)

        self.item1 = item1
        self.item2 = item2

        self.locked = False
        self.line_selected = False

        self.setBrush(QColor(0,0,0,0))
        self.setGraphicsEffect(None)
        self.updateLinePos()

    def isLocked(self):
        return self.locked

    def setLocked(self, yesno):
        self.locked = yesno

    def isLineSelected(self):
        return self.line_selected

    def setLineSelected(self, yesno):
        if self.locked: return
        if (options.fancy_eyecandy):
          if (yesno):
            self.setGraphicsEffect(CanvasPortGlow(self.item1.getPortType(), self.toGraphicsObject()))
          else:
            self.setGraphicsEffect(None)

        self.updateLineGradient(yesno)
        self.line_selected = True

    def updateLinePos(self):
        if (self.item1.port_mode == PORT_MODE_OUTPUT):
          item1_x = self.item1.scenePos().x()+self.item1.getPortWidth()+12
          item1_y = self.item1.scenePos().y()+7.5

          item2_x = self.item2.scenePos().x()
          item2_y = self.item2.scenePos().y()+7.5

          item1_mid_x = abs(item1_x-item2_x)/2
          item1_new_x = item1_x+item1_mid_x

          item2_mid_x = abs(item1_x-item2_x)/2
          item2_new_x = item2_x-item2_mid_x

          path = QPainterPath(QPointF(item1_x, item1_y))
          path.cubicTo(item1_new_x, item1_y, item2_new_x, item2_y, item2_x, item2_y)
          self.setPath(path)

          self.line_selected = False
          self.updateLineGradient(self.line_selected)

    def updateLineGradient(self, selected):
        pos_top = self.boundingRect().top()
        pos_bot = self.boundingRect().bottom()
        if (self.item2.scenePos().y() >= self.item1.scenePos().y()):
          pos1 = 0
          pos2 = 1
        else:
          pos1 = 1
          pos2 = 0

        port_type1 = self.item1.getPortType()
        port_type2 = self.item2.getPortType()
        port_gradient = QLinearGradient(0, pos_top, 0, pos_bot)

        if (port_type1 == PORT_TYPE_AUDIO_JACK):
          port_gradient.setColorAt(pos1, canvas.theme.line_audio_jack_sel if (selected) else canvas.theme.line_audio_jack)
        elif (port_type1 == PORT_TYPE_MIDI_JACK):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_jack_sel if (selected) else canvas.theme.line_midi_jack)
        elif (port_type1 == PORT_TYPE_MIDI_A2J):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_a2j_sel if (selected) else canvas.theme.line_midi_a2j)
        elif (port_type1 == PORT_TYPE_MIDI_ALSA):
          port_gradient.setColorAt(pos1, canvas.theme.line_midi_alsa_sel if (selected) else canvas.theme.line_midi_alsa)

        if (port_type2 == PORT_TYPE_AUDIO_JACK):
          port_gradient.setColorAt(pos2, canvas.theme.line_audio_jack_sel if (selected) else canvas.theme.line_audio_jack)
        elif (port_type2 == PORT_TYPE_MIDI_JACK):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_jack_sel if (selected) else canvas.theme.line_midi_jack)
        elif (port_type2 == PORT_TYPE_MIDI_A2J):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_a2j_sel if (selected) else canvas.theme.line_midi_a2j)
        elif (port_type2 == PORT_TYPE_MIDI_ALSA):
          port_gradient.setColorAt(pos2, canvas.theme.line_midi_alsa_sel if (selected) else canvas.theme.line_midi_alsa)

        self.setPen(QPen(port_gradient, 2))

    def type(self):
        return CanvasBezierLineType

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options.antialiasing))
        QGraphicsPathItem.paint(self, painter, option, widget)

# ------------------------------------------------------------------------------
# canvaslivemov.cpp

class CanvasLineMov(QGraphicsLineItem):
    def __init__(self, port_mode, port_type, parent):
        super(CanvasLineMov, self).__init__(parent, canvas.scene)

        self.port_mode = port_mode
        self.port_type = port_type
        self.item = None

        # Port position doesn't change while moving around line
        self.line_x = self.scenePos().x()
        self.line_y = self.scenePos().y()
        self.item_width = self.parentItem().getPortWidth()

        if (self.port_type == PORT_TYPE_AUDIO_JACK):
          pen = QPen(canvas.theme.line_audio_jack, 2)
        elif (self.port_type == PORT_TYPE_MIDI_JACK):
          pen = QPen(canvas.theme.line_midi_jack, 2)
        elif (self.port_type == PORT_TYPE_MIDI_A2J):
          pen = QPen(canvas.theme.line_midi_a2j, 2)
        elif (self.port_type == PORT_TYPE_MIDI_ALSA):
          pen = QPen(canvas.theme.line_midi_alsa, 2)

        self.setPen(pen)
        self.update()

    def updateLinePos(self, scenePos):
        item_pos = [0, 0]
        if (self.port_mode == PORT_MODE_INPUT):
          item_pos[0] = 0
          item_pos[1] = 7.5
        elif (self.port_mode == PORT_MODE_OUTPUT):
          item_pos[0] = self.item_width+12
          item_pos[1] = 7.5
        else:
          return

        line = QLineF(item_pos[0], item_pos[1], scenePos.x()-self.line_x, scenePos.y()-self.line_y)
        self.setLine(line)

    def type(self):
        return CanvasLineMovType

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options.antialiasing))
        QGraphicsLineItem.paint(self, painter, option, widget)

# ------------------------------------------------------------------------------
# canvasbezierlinemov.cpp

class CanvasBezierLineMov(QGraphicsPathItem):
    def __init__(self, port_mode, port_type, parent):
        super(CanvasBezierLineMov, self).__init__(parent, canvas.scene)

        self.port_mode = port_mode
        self.port_type = port_type
        self.item = None

        # Port position doesn't change while moving around line
        self.item_x = self.parentItem().scenePos().x()
        self.item_y = self.parentItem().scenePos().y()
        self.item_width = self.parentItem().getPortWidth()

        if (self.port_type == PORT_TYPE_AUDIO_JACK):
          pen = QPen(canvas.theme.line_audio_jack, 2)
        elif (self.port_type == PORT_TYPE_MIDI_JACK):
          pen = QPen(canvas.theme.line_midi_jack, 2)
        elif (self.port_type == PORT_TYPE_MIDI_A2J):
          pen = QPen(canvas.theme.line_midi_a2j, 2)
        elif (self.port_type == PORT_TYPE_MIDI_ALSA):
          pen = QPen(canvas.theme.line_midi_alsa, 2)

        color = QColor(0,0,0,0)
        self.setBrush(color)
        self.setPen(pen)
        self.update()

    def updateLinePos(self, scenePos):
        if (self.port_mode == PORT_MODE_INPUT):
          old_x = 0
          old_y = 7.5
          mid_x = abs(scenePos.x()-self.item_x)/2
          new_x = old_x-mid_x
        elif (self.port_mode == PORT_MODE_OUTPUT):
          old_x = self.item_width+12
          old_y = 7.5
          mid_x = abs(scenePos.x()-(self.item_x+old_x))/2
          new_x = old_x+mid_x
        else:
          return

        #mid_fx = abs(old_x-scenePos.x())/2
        #new_fx = scenePos.x()-mid_x
        final_x = scenePos.x()-self.item_x
        final_y = scenePos.y()-self.item_y

        path = QPainterPath(QPointF(old_x, old_y))
        path.cubicTo(new_x, old_y, new_x, final_y, final_x, final_y)
        self.setPath(path)

    def type(self):
        return CanvasBezierLineMovType

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options.antialiasing))
        QGraphicsPathItem.paint(self, painter, option, widget)

# ------------------------------------------------------------------------------
# canvasport.cpp

class CanvasPort(QGraphicsItem):
    def __init__(self, port_id, port_name, port_mode, port_type, parent):
        super(CanvasPort, self).__init__(parent, canvas.scene)

        # Save Variables, useful for later
        self.port_id   = port_id
        self.port_mode = port_mode
        self.port_type = port_type
        self.port_name = port_name

        # Base Variables
        self.port_width  = 15
        self.port_height = 15
        self.port_font   = QFont(canvas.theme.port_font_name, canvas.theme.port_font_size, canvas.theme.port_font_state)

        self.mov_line   = None
        self.hover_item = None
        self.last_selected_state = False

        self.mouse_down    = False
        self.moving_cursor = False

        self.setFlags(QGraphicsItem.ItemIsSelectable)

    def getPortId(self):
        return self.port_id

    def getPortMode(self):
        return self.port_mode

    def getPortType(self):
        return self.port_type

    def getPortName(self):
        return self.port_name

    def getFullPortName(self):
        return self.parentItem().getGroupName()+":"+self.port_name

    def getPortWidth(self):
        return self.port_width

    def getPortHeight(self):
        return self.port_height

    def setPortMode(self, port_mode):
        self.port_mode = port_mode
        self.update()

    def setPortType(self, port_type):
        self.port_type = port_type
        self.update()

    def setPortName(self, port_name):
        if (QFontMetrics(self.port_font).width(port_name) < QFontMetrics(self.port_font).width(self.port_name)):
          QTimer.singleShot(0, canvas.scene, SIGNAL("update()"));

        self.port_name = port_name
        self.update()

    def setPortWidth(self, port_width):
        if (port_width < self.port_width):
          QTimer.singleShot(0, canvas.scene, SIGNAL("update()"));

        self.port_width = port_width
        self.update()

    def type(self):
        return CanvasPortType

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton):
          self.mouse_down = True
        else:
          self.mouse_down = False

        self.hover_item = None
        self.moving_cursor = False

        QGraphicsItem.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (self.mouse_down):

          if (not self.moving_cursor):
            self.setCursor(QCursor(Qt.CrossCursor))
            self.moving_cursor = True

            for i in range(len(canvas.connection_list)):
              if (canvas.connection_list[i].port_out_id == self.port_id or canvas.connection_list[i].port_in_id == self.port_id):
                canvas.connection_list[i].widget.setLocked(True)

          if (not self.mov_line):
            if (options.bezier_lines):
              new_mov_line = CanvasBezierLineMov(self.port_mode, self.port_type, self)
              new_mov_line.setZValue(canvas.last_z_value)
              self.mov_line = new_mov_line
            else:
              new_mov_line = CanvasLineMov(self.port_mode, self.port_type, self)
              new_mov_line.setZValue(canvas.last_z_value)
              self.mov_line = new_mov_line

            canvas.last_z_value += 1
            self.parentItem().setZValue(canvas.last_z_value)
            canvas.last_z_value += 1

          item = None
          items = canvas.scene.items(event.scenePos(), Qt.ContainsItemShape, Qt.AscendingOrder)
          for i in range(len(items)):
            if (items[i].type() == CanvasPortType):
              if (items[i] != self):
                if not item:
                  item = items[i]
                elif (items[i].parentItem().zValue() > item.parentItem().zValue()):
                  item = items[i]

          if (self.hover_item and self.hover_item != item):
            self.hover_item.setSelected(False)

          if (item):
            a2j_connection = (item.getPortType() == PORT_TYPE_MIDI_JACK and self.port_type == PORT_TYPE_MIDI_A2J) or (item.getPortType() == PORT_TYPE_MIDI_A2J and self.port_type == PORT_TYPE_MIDI_JACK)
            if (item.getPortMode() != self.port_mode and (item.getPortType() == self.port_type or a2j_connection)):
              item.setSelected(True)
              self.hover_item = item
            else:
              self.hover_item = None
          else:
            self.hover_item = None

          self.mov_line.updateLinePos(event.scenePos())
          
          event.accept()

        else:
          QGraphicsItem.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if (self.mouse_down):

          if (self.mov_line):
            canvas.scene.removeItem(self.mov_line)
            self.mov_line = None

          for i in range(len(canvas.connection_list)):
            if (canvas.connection_list[i].port_out_id == self.port_id or canvas.connection_list[i].port_in_id == self.port_id):
              canvas.connection_list[i].widget.setLocked(False)

          if (self.hover_item):
            check = False
            for i in range(len(canvas.connection_list)):
              if ( (canvas.connection_list[i].port_out_id == self.port_id and canvas.connection_list[i].port_in_id == self.hover_item.getPortId()) or
                  (canvas.connection_list[i].port_out_id == self.hover_item.getPortId() and canvas.connection_list[i].port_in_id == self.port_id) ):
                canvas.callback(ACTION_PORTS_DISCONNECT, canvas.connection_list[i].connection_id, 0, "")
                check = True
                break

            if (not check):
              if (self.port_mode == PORT_MODE_OUTPUT):
                canvas.callback(ACTION_PORTS_CONNECT, self.port_id, self.hover_item.port_id, "")
              else:
                canvas.callback(ACTION_PORTS_CONNECT, self.hover_item.port_id, self.port_id, "")

            canvas.scene.clearSelection()

        if (self.moving_cursor):
          self.setCursor(QCursor(Qt.ArrowCursor))

        self.hover_item = None
        self.mouse_down = False
        self.moving_cursor = False

        QGraphicsItem.mouseReleaseEvent(self, event)

    def contextMenuEvent(self, event):
        canvas.scene.clearSelection()
        self.setSelected(True)

        menu = QMenu()
        discMenu = QMenu("Disconnect", menu)

        port_con_list = CanvasGetPortConnectionList(self.port_id)

        if (len(port_con_list) > 0):
          for i in range(len(port_con_list)):
            port_con_id = CanvasGetConnectedPort(port_con_list[i], self.port_id)
            act_x_disc = discMenu.addAction(CanvasGetPortName(port_con_id))
            act_x_disc.setData(port_con_list[i])
            QObject.connect(act_x_disc, SIGNAL("triggered()"), canvas.qobject, SLOT("PortContextMenuDisconnect()"))
        else:
          act_x_disc = discMenu.addAction("No connections")
          act_x_disc.setEnabled(False)

        menu.addMenu(discMenu)
        act_x_disc_all = menu.addAction("Disconnect &All")
        act_x_sep_1    = menu.addSeparator()
        act_x_info     = menu.addAction("Get &Info")
        act_x_rename   = menu.addAction("&Rename")

        if (not features.port_info):
          act_x_info.setVisible(False)

        if (not features.port_rename):
          act_x_rename.setVisible(False)

        if (not features.port_info and not features.port_rename):
          act_x_sep1.setVisible(False)

        act_selected = menu.exec_(event.screenPos())

        if (act_selected == act_x_disc_all):
          for i in range(len(port_con_list)):
            canvas.callback(ACTION_PORTS_DISCONNECT, port_con_list[i], 0, "")

        elif (act_selected == act_x_info):
          canvas.callback(ACTION_PORT_INFO, self.port_id, 0, "")

        elif (act_selected == act_x_rename):
          new_name_try = QInputDialog.getText(None, "Rename Port", "New name:", QLineEdit.Normal, self.port_name)
          if (new_name_try[1] and not new_name_try[0].isEmpty()):
            canvas.callback(ACTION_PORT_RENAME, self.port_id, 0, QStringStr(new_name_try[0]))

        event.accept()

    def boundingRect(self):
        return QRectF(0, 0, self.port_width+12, self.port_height)

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, (options.antialiasing == Qt.Checked))

        poly_locx = [0, 0, 0, 0, 0]

        if (self.port_mode == PORT_MODE_INPUT):
          text_pos = QPointF(3, 12)

          if (canvas.theme.port_mode == Theme.THEME_PORT_POLYGON):
            poly_locx[0] = 0
            poly_locx[1] = self.port_width+5
            poly_locx[2] = self.port_width+12
            poly_locx[3] = self.port_width+5
            poly_locx[4] = 0
          elif (canvas.theme.port_mode == Theme.THEME_PORT_SQUARE):
            poly_locx[0] = 0
            poly_locx[1] = self.port_width+5
            poly_locx[2] = self.port_width+5
            poly_locx[3] = self.port_width+5
            poly_locx[4] = 0
          else:
            qCritical("CanvasPort::paint() - Invalid Theme Port mode")
            return

        elif (self.port_mode == PORT_MODE_OUTPUT):
          text_pos = QPointF(9, 12)

          if (canvas.theme.port_mode == Theme.THEME_PORT_POLYGON):
            poly_locx[0] = self.port_width+12
            poly_locx[1] = 7
            poly_locx[2] = 0
            poly_locx[3] = 7
            poly_locx[4] = self.port_width+12
          elif (canvas.theme.port_mode == Theme.THEME_PORT_SQUARE):
            poly_locx[0] = self.port_width+12
            poly_locx[1] = 5
            poly_locx[2] = 5
            poly_locx[3] = 5
            poly_locx[4] = self.port_width+12
          else:
            qCritical("CanvasPort::paint() - Invalid Theme Port mode")
            return

        else:
          qCritical("CanvasPort::paint() - Invalid Port Mode")
          return

        if (self.port_type == PORT_TYPE_AUDIO_JACK):
          poly_color = canvas.theme.port_audio_jack_bg_sel if (self.isSelected()) else canvas.theme.port_audio_jack_bg
          poly_pen = canvas.theme.port_audio_jack_pen_sel if (self.isSelected()) else canvas.theme.port_audio_jack_pen
        elif (self.port_type == PORT_TYPE_MIDI_JACK):
          poly_color = canvas.theme.port_midi_jack_bg_sel if (self.isSelected()) else canvas.theme.port_midi_jack_bg
          poly_pen = canvas.theme.port_midi_jack_pen_sel if (self.isSelected()) else canvas.theme.port_midi_jack_pen
        elif (self.port_type == PORT_TYPE_MIDI_A2J):
          poly_color = canvas.theme.port_midi_a2j_bg_sel if (self.isSelected()) else canvas.theme.port_midi_a2j_bg
          poly_pen = canvas.theme.port_midi_a2j_pen_sel if (self.isSelected()) else canvas.theme.port_midi_a2j_pen
        elif (self.port_type == PORT_TYPE_MIDI_ALSA):
          poly_color = canvas.theme.port_midi_alsa_bg_sel if (self.isSelected()) else canvas.theme.port_midi_alsa_bg
          poly_pen = canvas.theme.port_midi_alsa_pen_sel if (self.isSelected()) else canvas.theme.port_midi_alsa_pen
        else:
          qCritical("CanvasPort::paint() - Invalid Port Type")
          return

        polygon = QPolygonF()
        polygon += QPointF(poly_locx[0], 0)
        polygon += QPointF(poly_locx[1], 0)
        polygon += QPointF(poly_locx[2], 7.5)
        polygon += QPointF(poly_locx[3], 15)
        polygon += QPointF(poly_locx[4], 15)

        painter.setBrush(poly_color)
        painter.setPen(poly_pen)
        painter.drawPolygon(polygon)

        painter.setPen(canvas.theme.port_text)
        painter.setFont(self.port_font)
        painter.drawText(text_pos, self.port_name)

        if (self.isSelected() != self.last_selected_state):
          for i in range(len(canvas.connection_list)):
            if (canvas.connection_list[i].port_out_id == self.port_id or canvas.connection_list[i].port_in_id == self.port_id):
              canvas.connection_list[i].widget.setLineSelected(self.isSelected())

        self.last_selected_state = self.isSelected()

# ------------------------------------------------------------------------------
# canvasbox.cpp

class cb_line_t(object):
  __slots__ = [
    'line',
    'connection_id'
  ]

class CanvasBox(QGraphicsItem):
    def __init__(self, group_id, group_name, icon, parent=None):
        super(CanvasBox, self).__init__(parent, canvas.scene)

        # Save Variables, useful for later
        self.group_id   = group_id
        self.group_name = group_name

        # Base Variables
        self.box_width  = 50
        self.box_height = 25
        self.port_list_ids = []
        self.connection_lines = []

        self.last_pos = QPointF()
        self.splitted = False
        self.splitted_mode = PORT_MODE_NULL
        self.forced_split  = False
        self.moving_cursor = False
        self.mouse_down    = False

        # Set Font
        self.font_name = QFont(canvas.theme.box_font_name, canvas.theme.box_font_size, canvas.theme.box_font_state)
        self.font_port = QFont(canvas.theme.port_font_name, canvas.theme.port_font_size, canvas.theme.port_font_state)

        # Icon
        self.icon_svg = CanvasIcon(icon, self.group_name, self)

        # Shadow
        if (options.fancy_eyecandy):
          self.shadow = CanvasBoxShadow(self.toGraphicsObject())
          self.shadow.setFakeParent(self)
          self.setGraphicsEffect(self.shadow)
        else:
          self.shadow = None

        # Final touches
        self.setFlags(QGraphicsItem.ItemIsMovable|QGraphicsItem.ItemIsSelectable)

        # Wait for at least 1 port
        if (options.auto_hide_groups or options.fancy_eyecandy):
          self.setVisible(False)

        self.updatePositions()

    def getGroupId(self):
        return self.group_id

    def getGroupName(self):
        return self.group_name

    def isSplitted(self):
        return self.splitted

    def getSplittedMode(self):
        return self.splitted_mode

    def getPortCount(self):
        return len(self.port_list_ids)

    def getPortList(self):
        return self.port_list_ids

    def setIcon(self, icon):
        self.icon_svg.setIcon(icon, self.group_name)

    def setSplit(self, split, mode=PORT_MODE_NULL):
        self.splitted = split
        self.splitted_mode = mode

    def setGroupName(self, group_name):
        self.group_name = group_name
        self.updatePositions()

    def addPortFromGroup(self, port_id, port_mode, port_type, port_name):
        if (len(self.port_list_ids) == 0):
          if (options.fancy_eyecandy):
            ItemFX(self, True)
          if (options.auto_hide_groups):
            self.setVisible(True)

        new_widget = CanvasPort(port_id, port_name, port_mode, port_type, self)

        port_dict = port_dict_t()
        port_dict.group_id  = self.group_id
        port_dict.port_id   = port_id
        port_dict.port_name = port_name
        port_dict.port_mode = port_mode
        port_dict.port_type = port_type
        port_dict.widget    = new_widget

        self.port_list_ids.append(port_id)

        return new_widget

    def removePortFromGroup(self, port_id):
        for i in range(len(self.port_list_ids)):
          if (port_id == self.port_list_ids[i]):
            self.port_list_ids.pop(i)
            break

        if (port_id in self.port_list_ids):
          qCritical("patchcanvas::CanvasBox.removePort() - Unable to find port to remove")
          return

        if (len(self.port_list_ids) > 0):
          self.updatePositions()
        elif (self.isVisible()):
          if (options.fancy_eyecandy):
            ItemFX(self, False, False)
          elif (options.auto_hide_groups):
            self.setVisible(False)

    def addLineFromGroup(self, line, connection_id):
        new_cbline = cb_line_t()
        new_cbline.line = line
        new_cbline.connection_id = connection_id
        self.connection_lines.append(new_cbline)

    def removeLineFromGroup(self, connection_id):
        for i in range(len(self.connection_lines)):
          if (self.connection_lines[i].connection_id == connection_id):
            self.connection_lines.pop(i)
            return
        qCritical("patchcanvas::CanvasBox.removeLineFromGroup() - Unable to find line to remove")

    def checkItemPos(self):
        if not canvas.size_rect.isNull():
          pos = self.scenePos()
          if (not canvas.size_rect.contains(pos) or not canvas.size_rect.contains(pos+QPointF(self.box_width, self.box_height))):
            if (pos.x() < canvas.size_rect.x()):
              self.setPos(canvas.size_rect.x(), pos.y())
            elif (pos.x()+self.box_width > canvas.size_rect.width()):
              self.setPos(canvas.size_rect.width()-self.box_width, pos.y())
            pos = self.scenePos()
            if (pos.y() < canvas.size_rect.y()):
              self.setPos(pos.x(), canvas.size_rect.y())
            elif (pos.y()+self.box_height > canvas.size_rect.height()):
              self.setPos(pos.x(), canvas.size_rect.height()-self.box_height)

    def removeIconFromScene(self):
        canvas.scene.removeItem(self.icon_svg)

    def updatePositions(self):
        self.prepareGeometryChange()

        max_in_width   = 0
        max_in_height  = 24
        max_out_width  = 0
        max_out_height = 24
        have_audio_jack_in = have_audio_jack_out = have_midi_jack_in = have_midi_jack_out = False
        have_midi_a2j_in = have_midi_a2j_out = have_midi_alsa_in = have_midi_alsa_out = False

        # reset box size
        self.box_width  = 50
        self.box_height = 25

        # Check Text Name size
        app_name_size = QFontMetrics(self.font_name).width(self.group_name)+30
        if (app_name_size > self.box_width):
          self.box_width = app_name_size

        # Get Port List
        port_list = []
        for i in range(len(canvas.port_list)):
          if (canvas.port_list[i].port_id in self.port_list_ids):
            port_list.append(canvas.port_list[i])

        # Get Max Box Width/Height
        for i in range(len(port_list)):
          if (port_list[i].port_mode == PORT_MODE_INPUT):
            max_in_height += 18

            size = QFontMetrics(self.font_port).width(port_list[i].port_name)
            if (size > max_in_width):
              max_in_width = size

            if (port_list[i].port_type == PORT_TYPE_AUDIO_JACK and not have_audio_jack_in):
              have_audio_jack_in = True
              max_in_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_JACK and not have_midi_jack_in):
              have_midi_jack_in = True
              max_in_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_A2J and not have_midi_a2j_in):
              have_midi_a2j_in = True
              max_in_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_ALSA and not have_midi_alsa_in):
              have_midi_alsa_in = True
              max_in_height += 2

          elif (port_list[i].port_mode == PORT_MODE_OUTPUT):
            max_out_height += 18

            size = QFontMetrics(self.font_port).width(port_list[i].port_name)
            if (size > max_out_width):
              max_out_width = size

            if (port_list[i].port_type == PORT_TYPE_AUDIO_JACK and not have_audio_jack_out):
              have_audio_jack_out = True
              max_out_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_JACK and not have_midi_jack_out):
              have_midi_jack_out = True
              max_out_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_A2J and not have_midi_a2j_out):
              have_midi_a2j_out = True
              max_out_height += 2
            elif (port_list[i].port_type == PORT_TYPE_MIDI_ALSA and not have_midi_alsa_out):
              have_midi_alsa_out = True
              max_out_height += 2

        final_width = 30 + max_in_width + max_out_width
        if (final_width > self.box_width):
          self.box_width = final_width

        if (max_in_height > self.box_height):
          self.box_height = max_in_height

        if (max_out_height > self.box_height):
          self.box_height = max_out_height

        # Remove bottom space
        self.box_height -= 2

        last_in_pos   = 24
        last_out_pos  = 24
        last_in_type  = PORT_TYPE_NULL
        last_out_type = PORT_TYPE_NULL

        # Re-position ports, AUDIO_JACK
        for i in range(len(port_list)):
          if (port_list[i].port_mode == PORT_MODE_INPUT and port_list[i].port_type == PORT_TYPE_AUDIO_JACK):

            port_list[i].widget.setPos(QPointF(1, last_in_pos))
            port_list[i].widget.setPortWidth(max_in_width)

            last_in_pos += 18
            last_in_type = port_list[i].port_type

          elif (port_list[i].port_mode == PORT_MODE_OUTPUT and port_list[i].port_type == PORT_TYPE_AUDIO_JACK):

            port_list[i].widget.setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            port_list[i].widget.setPortWidth(max_out_width)

            last_out_pos += 18
            last_out_type = port_list[i].port_type

        # Re-position ports, MIDI_JACK
        for i in range(len(port_list)):
          if (port_list[i].port_mode == PORT_MODE_INPUT and port_list[i].port_type == PORT_TYPE_MIDI_JACK):

            if (last_in_type != PORT_TYPE_NULL and port_list[i].port_type != last_in_type):
              last_in_pos += 2

            port_list[i].widget.setPos(QPointF(1, last_in_pos))
            port_list[i].widget.setPortWidth(max_in_width)

            last_in_pos += 18
            last_in_type = port_list[i].port_type

          elif (port_list[i].port_mode == PORT_MODE_OUTPUT and port_list[i].port_type == PORT_TYPE_MIDI_JACK):

            if (last_out_type != PORT_TYPE_NULL and port_list[i].port_type != last_out_type):
              last_out_pos += 2

            port_list[i].widget.setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            port_list[i].widget.setPortWidth(max_out_width)

            last_out_pos += 18
            last_out_type = port_list[i].port_type

        # Re-position ports, MIDI_A2J
        for i in range(len(port_list)):
          if (port_list[i].port_mode == PORT_MODE_INPUT and port_list[i].port_type == PORT_TYPE_MIDI_A2J):

            if (last_in_type != PORT_TYPE_NULL and port_list[i].port_type != last_in_type):
              last_in_pos += 2

            port_list[i].widget.setPos(QPointF(1, last_in_pos))
            port_list[i].widget.setPortWidth(max_in_width)

            last_in_pos += 18
            last_in_type = port_list[i].port_type

          elif (port_list[i].port_mode == PORT_MODE_OUTPUT and port_list[i].port_type == PORT_TYPE_MIDI_A2J):

            if (last_out_type != PORT_TYPE_NULL and port_list[i].port_type != last_out_type):
              last_out_pos += 2

            port_list[i].widget.setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            port_list[i].widget.setPortWidth(max_out_width)

            last_out_pos += 18
            last_out_type = port_list[i].port_type

        # Re-position ports, MIDI_ALSA
        for i in range(len(port_list)):
          if (port_list[i].port_mode == PORT_MODE_INPUT and port_list[i].port_type == PORT_TYPE_MIDI_ALSA):

            if (last_in_type != PORT_TYPE_NULL and port_list[i].port_type != last_in_type):
              last_in_pos += 2

            port_list[i].widget.setPos(QPointF(1, last_in_pos))
            port_list[i].widget.setPortWidth(max_in_width)

            last_in_pos += 18
            last_in_type = port_list[i].port_type

          elif (port_list[i].port_mode == PORT_MODE_OUTPUT and port_list[i].port_type == PORT_TYPE_MIDI_ALSA):

            if (last_out_type != PORT_TYPE_NULL and port_list[i].port_type != last_out_type):
              last_out_pos += 2

            port_list[i].widget.setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            port_list[i].widget.setPortWidth(max_out_width)

            last_out_pos += 18
            last_out_type = port_list[i].port_type

        self.repaintLines(True)
        self.update()

    def repaintLines(self, forced=False):
        if (self.pos() != self.last_pos or forced):
          for i in range(len(self.connection_lines)):
            self.connection_lines[i].line.updateLinePos()

        self.last_pos = self.pos()

    def resetLinesZValue(self):
        for i in range(len(canvas.connection_list)):
          if (canvas.connection_list[i].port_out_id in self.port_list_ids and canvas.connection_list[i].port_in_id in self.port_list_ids):
            z_value = canvas.last_z_value
          else:
            z_value = canvas.last_z_value-1

          canvas.connection_list[i].widget.setZValue(z_value)

    def type(self):
        return CanvasBoxType

    def contextMenuEvent(self, event):
        menu = QMenu()
        discMenu = QMenu("Disconnect", menu)

        port_con_list     = []
        port_con_list_ids = []

        for i in range(len(self.port_list_ids)):
          tmp_port_con_list = CanvasGetPortConnectionList(self.port_list_ids[i])
          for j in range(len(tmp_port_con_list)):
            if (tmp_port_con_list[j] not in port_con_list):
              port_con_list.append(tmp_port_con_list[j])
              port_con_list_ids.append(self.port_list_ids[i])

        if (len(port_con_list) > 0):
          for i in range(len(port_con_list)):
            port_con_id = CanvasGetConnectedPort(port_con_list[i], port_con_list_ids[i])
            act_x_disc = discMenu.addAction(CanvasGetPortName(port_con_id))
            act_x_disc.setData(port_con_list[i])
            QObject.connect(act_x_disc, SIGNAL("triggered()"), canvas.qobject, SLOT("PortContextMenuDisconnect()"))
        else:
          act_x_disc = discMenu.addAction("No connections")
          act_x_disc.setEnabled(False)

        menu.addMenu(discMenu)
        act_x_disc_all   = menu.addAction("Disconnect &All")
        act_x_sep1       = menu.addSeparator()
        act_x_info       = menu.addAction("&Info")
        act_x_rename     = menu.addAction("&Rename")
        act_x_sep2       = menu.addSeparator()
        act_x_split_join = menu.addAction("Join" if self.splitted else "Split")

        if (not features.group_info):
          act_x_info.setVisible(False)

        if (not features.group_rename):
          act_x_rename.setVisible(False)

        if (not features.group_info and not features.group_rename):
          act_x_sep1.setVisible(False)

        haveIns = haveOuts = False
        for i in range(len(canvas.port_list)):
          if (canvas.port_list[i].port_id in self.port_list_ids):
            if (canvas.port_list[i].port_mode == PORT_MODE_INPUT):
              haveIns = True
            elif (canvas.port_list[i].port_mode == PORT_MODE_OUTPUT):
              haveOuts = True

        if (not self.splitted and not (haveIns and haveOuts)):
          act_x_sep2.setVisible(False)
          act_x_split_join.setVisible(False)

        act_selected = menu.exec_(event.screenPos())

        if (act_selected == act_x_disc_all):
          for i in range(len(port_con_list)):
            canvas.callback(ACTION_PORTS_DISCONNECT, port_con_list[i], 0, "")

        elif (act_selected == act_x_info):
          canvas.callback(ACTION_GROUP_INFO, self.group_id, 0, "")

        elif (act_selected == act_x_rename):
          new_name_try = QInputDialog.getText(None, "Rename Group", "New name:", QLineEdit.Normal, self.group_name)
          if (new_name_try[1] and not new_name_try[0].isEmpty()):
            canvas.callback(ACTION_GROUP_RENAME, self.group_id, 0, new_name_try[0])

        elif (act_selected == act_x_split_join):
          if (self.splitted):
            canvas.callback(ACTION_GROUP_JOIN, self.group_id, 0, "")
          else:
            canvas.callback(ACTION_GROUP_SPLIT, self.group_id, 0, "")

        event.accept()

    def mousePressEvent(self, event):
        canvas.last_z_value += 1
        self.setZValue(canvas.last_z_value)
        self.resetLinesZValue()
        self.moving_cursor = False

        if (event.button() == Qt.RightButton):
          canvas.scene.clearSelection()
          self.setSelected(True)
          self.mouse_down = False
          event.accept()
          return

        elif (event.button() == Qt.LeftButton):
          if (self.sceneBoundingRect().contains(event.scenePos())):
            self.mouse_down = True
          else:
             # Fixes a weird Qt behaviour with right-click mouseMove
            self.mouse_down = False
            event.ignore()
            return

        else:
          self.mouse_down = False

        QGraphicsItem.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (self.mouse_down):
          if (not self.moving_cursor):
            self.setCursor(QCursor(Qt.SizeAllCursor))
            self.moving_cursor = True
          self.repaintLines()
        QGraphicsItem.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if (self.moving_cursor):
          self.setCursor(QCursor(Qt.ArrowCursor))
        self.mouse_down = False
        self.moving_cursor = False
        QGraphicsItem.mouseReleaseEvent(self, event)

    def boundingRect(self):
        return QRectF(0, 0, self.box_width, self.box_height)

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, False)

        if (self.isSelected()):
          painter.setPen(canvas.theme.box_pen_sel)
        else:
          painter.setPen(canvas.theme.box_pen)

        box_gradient = QLinearGradient(0, 0, 0, self.box_height)
        box_gradient.setColorAt(0, canvas.theme.box_bg_1)
        box_gradient.setColorAt(1, canvas.theme.box_bg_2)

        painter.setBrush(box_gradient)
        painter.drawRect(0, 0, self.box_width, self.box_height)

        text_pos = QPointF(25, 16)

        painter.setFont(self.font_name)
        painter.setPen(canvas.theme.box_text)
        painter.drawText(text_pos, self.group_name)

        self.repaintLines()

# ------------------------------------------------------------------------------
# canvasicon.cpp

class CanvasIcon(QGraphicsSvgItem):
    def __init__(self, icon, name, parent):
        super(CanvasIcon, self).__init__(parent)

        self.renderer = None
        self.setIcon(icon, name)

        self.colorFX = QGraphicsColorizeEffect(self)
        self.colorFX.setColor(canvas.theme.box_text.color())
        self.setGraphicsEffect(self.colorFX)

    def setIcon(self, icon, name):
        if (type(name) != QString):
          name = QString(name)

        name = name.toLower()
        icon_path = ""

        if (icon == ICON_APPLICATION):
          self.size = QRectF(3, 2, 19, 18)

          if (name.contains("audacious")):
            self.size = QRectF(5, 4, 16, 16)
            icon_path = ":/svg/pb_audacious.svg"
          elif (name.contains("clementine")):
            self.size = QRectF(5, 4, 16, 16)
            icon_path = ":/svg/pb_clementine.svg"
          elif (name.contains("jamin")):
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/pb_jamin.svg"
          elif (name.contains("mplayer")):
            self.size = QRectF(5, 4, 16, 16)
            icon_path = ":/svg/pb_mplayer.svg"
          elif (name.contains("vlc")):
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/pb_vlc.svg"

          else:
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/pb_generic.svg"

        elif (icon == ICON_HARDWARE):
            self.size = QRectF(5, 2, 16, 16)
            icon_path = ":/svg/pb_hardware.svg"

        elif (icon == ICON_LADISH_ROOM):
            self.size = QRectF(5, 2, 16, 16)
            icon_path = ":/svg/pb_hardware.svg"

        else:
          self.size = QRectF(0, 0, 0, 0)
          qCritical("patchcanvas::CanvasIcon.setIcon() - Unsupported Icon requested")
          return

        self.renderer = QSvgRenderer(icon_path, canvas.scene)
        self.setSharedRenderer(self.renderer)
        self.update()

    def type(self):
        return CanvasIconType

    def boundingRect(self):
        return QRectF(self.size)

    def paint(self, painter, option, widget):
        if (self.renderer):
          painter.setRenderHint(QPainter.Antialiasing, False)
          painter.setRenderHint(QPainter.TextAntialiasing, False)
          self.renderer.render(painter, QRectF(self.size))
        else:
          QGraphicsSvgItem.paint(self, painter, option, widget)

# ------------------------------------------------------------------------------
# canvasportglow.cpp

class CanvasPortGlow(QGraphicsDropShadowEffect):
    def __init__(self, port_type, parent):
        super(CanvasPortGlow, self).__init__(parent)

        self.setBlurRadius(12)
        self.setOffset(0, 0)

        if (port_type == PORT_TYPE_AUDIO_JACK):
          self.setColor(canvas.theme.line_audio_jack_glow)
        elif (port_type == PORT_TYPE_MIDI_JACK):
          self.setColor(canvas.theme.line_midi_jack_glow)
        elif (port_type == PORT_TYPE_MIDI_A2J):
          self.setColor(canvas.theme.line_midi_a2j_glow)
        elif (port_type == PORT_TYPE_MIDI_ALSA):
          self.setColor(canvas.theme.line_midi_alsa_glow)

# ------------------------------------------------------------------------------
# canvasboxshadow.cpp

class CanvasBoxShadow(QGraphicsDropShadowEffect):
    def __init__(self, parent):
        super(CanvasBoxShadow, self).__init__(parent)

        self.setBlurRadius(20)
        self.setColor(canvas.theme.box_shadow)
        self.setOffset(0, 0)
        self.fake_parent = None

    def setFakeParent(self, fake_parent):
        self.fake_parent = fake_parent

    def draw(self, painter):
        if (self.fake_parent):
         self.fake_parent.repaintLines()
        return QGraphicsDropShadowEffect.draw(self, painter)
