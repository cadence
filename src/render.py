#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os
from time import sleep
from PyQt4.QtCore import QProcess, QString, QTime, QTimer, SIGNAL
from PyQt4.QtGui import QDialog, QIcon, QFileDialog, QFontMetrics, QMessageBox

# Imports (Custom Stuff)
import jacklib, icons_rc, ui_render

HOME = os.getenv("HOME")
PATH = os.getenv("PATH")

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# QLineEdit and QPushButtom combo
def getAndSetPath(parent, cur_path, lineEdit):
    new_path = QFileDialog.getExistingDirectory(parent, parent.tr("Set Path"), cur_path, QFileDialog.ShowDirsOnly)
    if not new_path.isEmpty():
      lineEdit.setText(new_path)
    return new_path

# Get Icon from user theme, using our own as backup (Oxygen)
def getIcon(icon, size=16):
    return QIcon.fromTheme(icon, QIcon(":/%ix%i/%s.png" % (size, size, icon)))

global jack_client
jack_client = None

# Render Window
class RenderW(QDialog, ui_render.Ui_RenderW):
    def __init__(self, parent=None):
        super(RenderW, self).__init__(parent)
        self.setupUi(self)

        global jack_client

        if (jack_client):
          self.close_jack_on_exit = False
        else:
          jack_client = jacklib.client_open("Render-Dialog", jacklib.NullOption, 0)
          self.close_jack_on_exit = True

        self.process = QProcess(self)
        self.timer = QTimer()

        self.b_render.setIcon(getIcon("media-record"))
        self.b_stop.setIcon(getIcon("media-playback-stop"))
        self.b_close.setIcon(getIcon("dialog-close"))
        self.b_open.setIcon(getIcon("document-open"))
        self.b_stop.setVisible(False)
        self.le_folder.setText(HOME)

        self.buffer_size = str(jacklib.get_buffer_size(jack_client))
        for i in range(self.cb_buffer_size.count()):
          if (str(self.cb_buffer_size.itemText(i)) == self.buffer_size):
            self.cb_buffer_size.setCurrentIndex(i)

        self.sample_rate = jacklib.get_sample_rate(jack_client)

        # Get List of formats
        self.process.start("jack_capture", ["-pf"])
        self.process.waitForFinished()

        formats = QString(self.process.readAllStandardOutput()).split(" ")
        for i in range(len(formats)-1):
          self.cb_format.addItem(formats[i])
          if (formats[i] == "wav"):
            self.cb_format.setCurrentIndex(i)

        self.cb_depth.setCurrentIndex(4) #Float
        self.rb_stereo.setChecked(True)

        self.last_time = 0
        self.freewheel = False

        self.max_time = 180
        self.te_end.setTime(QTime(0, 3, 0))
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)

        self.connect(self.b_render, SIGNAL("clicked()"), self.render_start)
        self.connect(self.b_stop, SIGNAL("clicked()"), self.render_stop)
        self.connect(self.b_now_start, SIGNAL("clicked()"), self.setStartNow)
        self.connect(self.b_now_end, SIGNAL("clicked()"), self.setEndNow)
        self.connect(self.te_start, SIGNAL("timeChanged(const QTime)"), self.updateStartTime)
        self.connect(self.te_end, SIGNAL("timeChanged(const QTime)"), self.updateEndTime)
        self.connect(self.timer, SIGNAL("timeout()"), self.updateProgressbar)

        self.connect(self.b_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_folder.text(),
                                                       lineEdit=self.le_folder: getAndSetPath(parent, current_path, lineEdit))

    def render_start(self):
        global jack_client
        self.group_render.setEnabled(False)
        self.group_time.setEnabled(False)
        self.group_encoding.setEnabled(False)
        self.b_render.setVisible(False)
        self.b_stop.setVisible(True)
        self.b_close.setEnabled(False)

        self.freewheel = bool(self.cb_render_mode.currentIndex() == 1)
        buffer_size = int(str(self.cb_buffer_size.currentText()))

        time_start = self.te_start.time()
        time_end   = self.te_end.time()
        MinTime = (time_start.hour()*3600)+(time_start.minute()*60)+(time_start.second())
        MaxTime = (time_end.hour()*3600)+(time_end.minute()*60)+(time_end.second())
        self.max_time = MaxTime

        self.progressBar.setMinimum(MinTime)
        self.progressBar.setMaximum(MaxTime)
        self.progressBar.setValue(MinTime)
        self.progressBar.update()

        if (self.freewheel):
          self.timer.setInterval(100)
        else:
          self.timer.setInterval(500)

        arguments = []

        # Bit depth
        arguments.append("-b")
        arguments.append(self.cb_depth.currentText())

        # Channels
        arguments.append("-c")
        if (self.rb_mono.isChecked()):
          arguments.append("1")
        elif (self.rb_stereo.isChecked()):
          arguments.append("2")
        elif (self.rb_outro.isChecked()):
          arguments.append(str(self.sb_channels.value()))
        else:
          arguments.append("2")

        # Format
        arguments.append("-f")
        arguments.append(self.cb_format.currentText())

        # Controlled by transport
        arguments.append("-jt")

        # Silent mode
        arguments.append("-dc")
        arguments.append("-s")

        os.chdir(QStringStr(self.le_folder.text()))

        if (buffer_size != jacklib.get_buffer_size(jack_client)):
          jacklib.set_buffer_size(jack_client, buffer_size)
          print "NOTICE: buffer size changed before render"

        if (jacklib.transport_query(jack_client, None) > jacklib.TransportStopped): #Rolling
          jacklib.transport_stop(jack_client)

        jacklib.transport_locate(jack_client, MinTime*self.sample_rate)
        self.last_time = -1

        self.process.start("jack_capture", arguments)
        self.process.waitForStarted()

        if (self.freewheel):
          sleep(1)

        if (self.freewheel):
          jacklib.set_freewheel(jack_client, 1)
          print "NOTICE: rendering in freewheel mode"

        self.timer.start()
        jacklib.transport_start(jack_client)

    def render_stop(self):
        global jack_client
        jacklib.transport_stop(jack_client)

        if (self.freewheel):
          jacklib.set_freewheel(jack_client, 0)

        sleep(1)

        self.process.close()
        self.timer.stop()

        self.group_render.setEnabled(True)
        self.group_time.setEnabled(True)
        self.group_encoding.setEnabled(True)
        self.b_render.setVisible(True)
        self.b_stop.setVisible(False)
        self.b_close.setEnabled(True)

        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)
        self.progressBar.update()

        # Restore buffer size
        this_buffer_size = str(jacklib.get_buffer_size(jack_client))
        if (this_buffer_size != self.buffer_size):
          jacklib.set_buffer_size(jack_client, int(buffer_size))

    def setStartNow(self):
        global jack_client
        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        secs = time % 60
        mins = (time / 60) % 60
        hrs  = (time / 3600) % 60

        self.te_start.setTime(QTime(hrs, mins, secs))

    def setEndNow(self):
        global jack_client
        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        secs = time % 60
        mins = (time / 60) % 60
        hrs  = (time / 3600) % 60

        self.te_end.setTime(QTime(hrs, mins, secs))

    def updateProgressbar(self):
        global jack_client
        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        self.progressBar.setValue(time)

        if (time > self.max_time or (self.last_time > time and not self.freewheel)):
          self.render_stop()

        self.last_time = time

    def updateStartTime(self, time):
        if (time >= self.te_end.time()):
          self.te_end.setTime(time)
          self.b_render.setEnabled(False)
        else:
          self.b_render.setEnabled(True)

    def updateEndTime(self, time):
        if (time <= self.te_start.time()):
          time = self.te_start.setTime(time)
          self.b_render.setEnabled(False)
        else:
          self.b_render.setEnabled(True)

    def closeEvent(self, event):
        if (self.close_jack_on_exit):
          print "closing jack"
          global jack_client
          jacklib.client_close(jack_client)
          jack_client = None
        QDialog.closeEvent(self, event)

# Allow to use this as a standalone app
if __name__ == '__main__':

    # Additional imports
    import sys
    from PyQt4.QtGui import QApplication

    if not HOME:
      print "HOME variable not set, cannot continue"
      sys.exit(1)

    if not PATH:
      print "PATH variable not set, cannot continue"
      sys.exit(1)

    # App initialization
    app = QApplication(sys.argv)

    for PATHi in PATH.split(":"):
      if os.path.exists(os.path.join(PATHi, "jack_capture")):
        break
    else:
      QMessageBox.critical(None, app.translate("RenderW", "Error"), app.translate("RenderW", "The 'jack_capture' application is not available.\nIs not possible to render without it!"))
      sys.exit(1)

    jack_client = jacklib.client_open("Render", jacklib.NullOption, 0)

    if not jack_client:
      sys.exit(1)

    # Show GUI
    gui = RenderW()
    gui.setWindowIcon(QIcon.fromTheme("media-record"))
    gui.show()

    # App-Loop
    ret = app.exec_()

    if (jack_client):
      jacklib.client_close(jack_client)

    # Exit properly
    sys.exit(ret)
