#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------------
#  C types

# Imports (Global)
from ctypes import *

# Base Types
LADSPA_Data = c_float
LADSPA_Property = c_int
LADSPA_PluginType = c_ulonglong

# Unit Types
LADSPA_UNIT_DB                = 0x01
LADSPA_UNIT_COEF              = 0x02
LADSPA_UNIT_HZ                = 0x04
LADSPA_UNIT_S                 = 0x08
LADSPA_UNIT_MS                = 0x10
LADSPA_UNIT_MIN               = 0x20

LADSPA_UNIT_CLASS_AMPLITUDE   = LADSPA_UNIT_DB|LADSPA_UNIT_COEF
LADSPA_UNIT_CLASS_FREQUENCY   = LADSPA_UNIT_HZ
LADSPA_UNIT_CLASS_TIME        = LADSPA_UNIT_S|LADSPA_UNIT_MS|LADSPA_UNIT_MIN

# Port Types (Official API)
LADSPA_PORT_INPUT             = 0x1
LADSPA_PORT_OUTPUT            = 0x2
LADSPA_PORT_CONTROL           = 0x4
LADSPA_PORT_AUDIO             = 0x8

# Port Hints
LADSPA_PORT_UNIT              = 0x1
LADSPA_PORT_DEFAULT           = 0x2
LADSPA_PORT_LABEL             = 0x4

# Plugin Types
LADSPA_CLASS_UTILITY          = 0x000000001
LADSPA_CLASS_GENERATOR        = 0x000000002
LADSPA_CLASS_SIMULATOR        = 0x000000004
LADSPA_CLASS_OSCILLATOR       = 0x000000008
LADSPA_CLASS_TIME             = 0x000000010
LADSPA_CLASS_DELAY            = 0x000000020
LADSPA_CLASS_PHASER           = 0x000000040
LADSPA_CLASS_FLANGER          = 0x000000080
LADSPA_CLASS_CHORUS           = 0x000000100
LADSPA_CLASS_REVERB           = 0x000000200
LADSPA_CLASS_FREQUENCY        = 0x000000400
LADSPA_CLASS_FREQUENCY_METER  = 0x000000800
LADSPA_CLASS_FILTER           = 0x000001000
LADSPA_CLASS_LOWPASS          = 0x000002000
LADSPA_CLASS_HIGHPASS         = 0x000004000
LADSPA_CLASS_BANDPASS         = 0x000008000
LADSPA_CLASS_COMB             = 0x000010000
LADSPA_CLASS_ALLPASS          = 0x000020000
LADSPA_CLASS_EQ               = 0x000040000
LADSPA_CLASS_PARAEQ           = 0x000080000
LADSPA_CLASS_MULTIEQ          = 0x000100000
LADSPA_CLASS_AMPLITUDE        = 0x000200000
LADSPA_CLASS_PITCH            = 0x000400000
LADSPA_CLASS_AMPLIFIER        = 0x000800000
LADSPA_CLASS_WAVESHAPER       = 0x001000000
LADSPA_CLASS_MODULATOR        = 0x002000000
LADSPA_CLASS_DISTORTION       = 0x004000000
LADSPA_CLASS_DYNAMICS         = 0x008000000
LADSPA_CLASS_COMPRESSOR       = 0x010000000
LADSPA_CLASS_EXPANDER         = 0x020000000
LADSPA_CLASS_LIMITER          = 0x040000000
LADSPA_CLASS_GATE             = 0x080000000
LADSPA_CLASS_SPECTRAL         = 0x100000000
LADSPA_CLASS_NOTCH            = 0x200000000

LADSPA_GROUP_DYNAMICS         = LADSPA_CLASS_DYNAMICS|LADSPA_CLASS_COMPRESSOR|LADSPA_CLASS_EXPANDER|LADSPA_CLASS_LIMITER|LADSPA_CLASS_GATE
LADSPA_GROUP_AMPLITUDE        = LADSPA_CLASS_AMPLITUDE|LADSPA_CLASS_AMPLIFIER|LADSPA_CLASS_WAVESHAPER|LADSPA_CLASS_MODULATOR|LADSPA_CLASS_DISTORTION|LADSPA_GROUP_DYNAMICS
LADSPA_GROUP_EQ               = LADSPA_CLASS_EQ|LADSPA_CLASS_PARAEQ|LADSPA_CLASS_MULTIEQ
LADSPA_GROUP_FILTER           = LADSPA_CLASS_FILTER|LADSPA_CLASS_LOWPASS|LADSPA_CLASS_HIGHPASS|LADSPA_CLASS_BANDPASS|LADSPA_CLASS_COMB|LADSPA_CLASS_ALLPASS|LADSPA_CLASS_NOTCH
LADSPA_GROUP_FREQUENCY        = LADSPA_CLASS_FREQUENCY|LADSPA_CLASS_FREQUENCY_METER|LADSPA_GROUP_FILTER|LADSPA_GROUP_EQ|LADSPA_CLASS_PITCH
LADSPA_GROUP_SIMULATOR        = LADSPA_CLASS_SIMULATOR|LADSPA_CLASS_REVERB
LADSPA_GROUP_TIME             = LADSPA_CLASS_TIME|LADSPA_CLASS_DELAY|LADSPA_CLASS_PHASER|LADSPA_CLASS_FLANGER|LADSPA_CLASS_CHORUS|LADSPA_CLASS_REVERB
LADSPA_GROUP_GENERATOR        = LADSPA_CLASS_GENERATOR|LADSPA_CLASS_OSCILLATOR

# Plugin Hints
LADSPA_PLUGIN_CREATOR         = 0x1
LADSPA_PLUGIN_TITLE           = 0x2
LADSPA_PLUGIN_RIGHTS          = 0x4

# A Scale Point
class LADSPA_RDF_ScalePoint(Structure):
  _fields_ = [
    ("Value", LADSPA_Data),
    ("Label", c_char_p)
  ]

# A Port
class LADSPA_RDF_Port(Structure):
  _fields_ = [
    ("Type", LADSPA_Property),
    ("Hints", LADSPA_Property),
    ("Unit", LADSPA_Property),
    ("Default", LADSPA_Data),
    ("Label", c_char_p),

    ("ScalePointCount", c_ulong),
    ("ScalePoints", POINTER(LADSPA_RDF_ScalePoint))
  ]

# The actual plugin descriptor
class LADSPA_RDF_Descriptor(Structure):
  _fields_ = [
    ("Type", LADSPA_PluginType),
    ("Hints", LADSPA_Property),
    ("UniqueID", c_ulong),
    ("Creator", c_char_p),
    ("Title", c_char_p),
    ("Rights", c_char_p),

    ("PortCount", c_ulong),
    ("Ports", POINTER(LADSPA_RDF_Port))
  ]

# -------------------------------------------------------------------------------
#  Python compatible C types

PyLADSPA_RDF_ScalePoint = {
  'Value': 0.0,
  'Label': ""
}

PyLADSPA_RDF_Port = {
  'Type': 0,
  'Hints': 0,
  'Unit': 0,
  'Default': 0.0,
  'Label': "",

  'ScalePointCount': 0,
  'ScalePoints': [],

  # Only here to help, NOT in the API:
  'index': 0
}

PyLADSPA_RDF_Descriptor = {
  'Type': 0,
  'Hints': 0,
  'UniqueID': 0,
  'Creator': "",
  'Title': "",
  'Rights': "",

  'PortCount': 0,
  'Ports': []
}

# -------------------------------------------------------------------------------
#  RDF data and conversions

# Prefixes (sorted alphabetically and by type)
rdf_prefix = {
  # Base types
  'dc':   "http://purl.org/dc/elements/1.1/",
  'rdf':  "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
  'rdfs': "http://www.w3.org/2000/01/rdf-schema#",

  # Extended types
  'dc:creator': "http://purl.org/dc/elements/1.1/creator",
  'dc:rights':  "http://purl.org/dc/elements/1.1/rights",
  'dc:title':   "http://purl.org/dc/elements/1.1/title",
  'rdf:value':  "http://www.w3.org/1999/02/22-rdf-syntax-ns#value",
  'rdf:type':   "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",

  # LADSPA
  'ladspa':              "http://ladspa.org/ontology#",
  'ladspa:forPort':      "http://ladspa.org/ontology#forPort",
  'ladspa:hasLabel':     "http://ladspa.org/ontology#hasLabel",
  'ladspa:hasPoint':     "http://ladspa.org/ontology#hasPoint",
  'ladspa:hasPort':      "http://ladspa.org/ontology#hasPort",
  'ladspa:hasPortValue': "http://ladspa.org/ontology#hasPortValue",
  'ladspa:hasScale':     "http://ladspa.org/ontology#hasScale",
  'ladspa:hasSetting':   "http://ladspa.org/ontology#hasSetting",
  'ladspa:hasUnit':      "http://ladspa.org/ontology#hasUnit",
  'ladspa:hasUnits':     "http://ladspa.org/ontology#hasUnits",

  # LADSPA "Extensions"
  'ladspa:NotchPlugin':    "http://ladspa.org/ontology#NotchPlugin",
  'ladspa:SpectralPlugin': "http://ladspa.org/ontology#SpectralPlugin"
}

def get_c_plugin_class(value):
  value_str = value.replace(rdf_prefix['ladspa'],"")

  if (value_str == "Plugin"):
    return 0
  elif (value_str == "UtilityPlugin"):
    return LADSPA_CLASS_UTILITY
  elif (value_str == "GeneratorPlugin"):
    return LADSPA_CLASS_GENERATOR
  elif (value_str == "SimulatorPlugin"):
    return LADSPA_CLASS_SIMULATOR
  elif (value_str == "OscillatorPlugin"):
    return LADSPA_CLASS_OSCILLATOR
  elif (value_str == "TimePlugin"):
    return LADSPA_CLASS_TIME
  elif (value_str == "DelayPlugin"):
    return LADSPA_CLASS_DELAY
  elif (value_str == "PhaserPlugin"):
    return LADSPA_CLASS_PHASER
  elif (value_str == "FlangerPlugin"):
    return LADSPA_CLASS_FLANGER
  elif (value_str == "ChorusPlugin"):
    return LADSPA_CLASS_CHORUS
  elif (value_str == "ReverbPlugin"):
    return LADSPA_CLASS_REVERB
  elif (value_str == "FrequencyPlugin"):
    return LADSPA_CLASS_FREQUENCY
  elif (value_str == "FrequencyMeterPlugin"):
    return LADSPA_CLASS_FREQUENCY_METER
  elif (value_str == "FilterPlugin"):
    return LADSPA_CLASS_FILTER
  elif (value_str == "LowpassPlugin"):
    return LADSPA_CLASS_LOWPASS
  elif (value_str == "HighpassPlugin"):
    return LADSPA_CLASS_HIGHPASS
  elif (value_str == "BandpassPlugin"):
    return LADSPA_CLASS_BANDPASS
  elif (value_str == "CombPlugin"):
    return LADSPA_CLASS_COMB
  elif (value_str == "AllpassPlugin"):
    return LADSPA_CLASS_ALLPASS
  elif (value_str in ("EQPlugin", "EqualizerPlugin", "MixerPlugin")):
    return LADSPA_CLASS_EQ
  elif (value_str == "ParaEQPlugin"):
    return LADSPA_CLASS_PARAEQ
  elif (value_str == "MultiEQPlugin"):
    return LADSPA_CLASS_MULTIEQ
  elif (value_str == "AmplitudePlugin"):
    return LADSPA_CLASS_AMPLITUDE
  elif (value_str == "PitchPlugin"):
    return LADSPA_CLASS_PITCH
  elif (value_str == "AmplifierPlugin"):
    return LADSPA_CLASS_AMPLIFIER
  elif (value_str == "WaveshaperPlugin"):
    return LADSPA_CLASS_WAVESHAPER
  elif (value_str in ("ModulatorPlugin", "ModulationPlugin")):
    return LADSPA_CLASS_MODULATOR
  elif (value_str == "DistortionPlugin"):
    return LADSPA_CLASS_DISTORTION
  elif (value_str == "DynamicsPlugin"):
    return LADSPA_CLASS_DYNAMICS
  elif (value_str == "CompressorPlugin"):
    return LADSPA_CLASS_COMPRESSOR
  elif (value_str == "ExpanderPlugin"):
    return LADSPA_CLASS_EXPANDER
  elif (value_str == "LimiterPlugin"):
    return LADSPA_CLASS_LIMITER
  elif (value_str == "GatePlugin"):
    return LADSPA_CLASS_GATE
  elif (value_str == "SpectralPlugin"):
    return LADSPA_CLASS_SPECTRAL
  elif (value_str == "NotchPlugin"):
    return LADSPA_CLASS_NOTCH
  else:
    print "LADSPA_RDF - Got an unknown plugin type", value_str
    return 0

def get_c_port_type(value):
  value_str = value.replace(rdf_prefix['ladspa'],"")

  if (value_str == "Port"):
    return 0
  elif (value_str == "ControlPort"):
    return LADSPA_PORT_CONTROL
  elif (value_str == "AudioPort"):
    return LADSPA_PORT_AUDIO
  elif (value_str == "InputPort"):
    return LADSPA_PORT_INPUT
  elif (value_str == "OutputPort"):
    return LADSPA_PORT_OUTPUT
  elif (value_str in ("ControlInputPort", "InputControlPort")):
    return LADSPA_PORT_CONTROL|LADSPA_PORT_INPUT
  elif (value_str in ("ControlOutputPort", "OutputControlPort")):
    return LADSPA_PORT_CONTROL|LADSPA_PORT_OUTPUT
  elif (value_str in ("AudioInputPort", "InputAudioPort")):
    return LADSPA_PORT_AUDIO|LADSPA_PORT_INPUT
  elif (value_str in ("AudioOutputPort", "OutputAudioPort")):
    return LADSPA_PORT_AUDIO|LADSPA_PORT_OUTPUT
  else:
    print "LADSPA_RDF - Got an unknown port type", value_str
    return 0

def get_c_unit_type(value):
  value_str = value.replace(rdf_prefix['ladspa'],"")

  if (value_str in ("Unit", "Units", "AmplitudeUnits", "FrequencyUnits", "TimeUnits")):
    return 0
  elif (value_str == "dB"):
    return LADSPA_UNIT_DB
  elif (value_str == "coef"):
    return LADSPA_UNIT_COEF
  elif (value_str == "Hz"):
    return LADSPA_UNIT_HZ
  elif (value_str == "seconds"):
    return LADSPA_UNIT_S
  elif (value_str == "milliseconds"):
    return LADSPA_UNIT_MS
  elif (value_str == "minutes"):
    return LADSPA_UNIT_MIN
  else:
    print "LADSPA_RDF - Got an unknown unit type", value_str
    return 0

# -------------------------------------------------------------------------------
#  RDF query for LADSPA

# Imports (Global)
import os
from copy import deepcopy
from rdflib import *

global LADSPA_RDF_PATH, LADSPA_Plugins

LADSPA_Plugins = []
LADSPA_RDF_PATH = ["/usr/share/ladspa/rdf", "/usr/local/share/ladspa/rdf"]

LADSPA_RDF_TYPE_PLUGIN = 1
LADSPA_RDF_TYPE_PORT   = 2

# Set LADSPA_RDF_PATH variable
def set_rdf_path(PATH):
  global LADSPA_RDF_PATH
  LADSPA_RDF_PATH = PATH

def to_float(string):
  return float(str(string).replace("f",""))

# Convert RDF LADSPA Type into a number
def to_plugin_number(rdf_type):
  return str(rdf_type).replace(rdf_prefix['ladspa'],"")

# Convert RDF LADSPA Type into 2 numbers
def to_plugin_numbers(rdf_type):
  numbers = str(rdf_type).replace(rdf_prefix['ladspa'],"").split(".")
  return (numbers[0], numbers[1])

# Convert RDF LADSPA Type into a port number
def to_plugin_port(rdf_type):
  return to_plugin_numbers(rdf_type)[1]

# Check RDF Type
def rdf_is_type(rdf_type, compare):
  if (compare == LADSPA_RDF_TYPE_PLUGIN):
    if (type(rdf_type) == URIRef and rdf_prefix['ladspa'] in rdf_type and to_plugin_number(rdf_type).isdigit()):
      return True
    else:
      return False
  elif (compare == LADSPA_RDF_TYPE_PORT):
    if (type(rdf_type) == URIRef and rdf_prefix['ladspa'] in rdf_type and "." in to_plugin_number(rdf_type)):
      return True
    else:
      return False

def check_and_add_plugin(plugin_id):
  global LADSPA_Plugins
  for i in range(len(LADSPA_Plugins)):
    if (LADSPA_Plugins[i]['UniqueID'] == plugin_id):
      return i
  else:
    plugin = deepcopy(PyLADSPA_RDF_Descriptor)
    plugin['UniqueID'] = plugin_id
    LADSPA_Plugins.append(plugin)
    return len(LADSPA_Plugins)-1

def set_plugin_value(plugin_id, key, value):
  global LADSPA_Plugins
  index = check_and_add_plugin(plugin_id)
  LADSPA_Plugins[index][key] = value

def add_plugin_value(plugin_id, key, value):
  global LADSPA_Plugins
  index = check_and_add_plugin(plugin_id)
  LADSPA_Plugins[index][key] += value

def or_plugin_value(plugin_id, key, value):
  global LADSPA_Plugins
  index = check_and_add_plugin(plugin_id)
  LADSPA_Plugins[index][key] |= value

def append_plugin_value(plugin_id, key, value):
  global LADSPA_Plugins
  index = check_and_add_plugin(plugin_id)
  LADSPA_Plugins[index][key].append(value)

def check_and_add_port(plugin_id, port_id):
  global LADSPA_Plugins
  index = check_and_add_plugin(plugin_id)
  Ports = LADSPA_Plugins[index]['Ports']
  for i in range(len(Ports)):
    if (Ports[i]['index'] == port_id):
      return (index, i)
  else:
    pcount = LADSPA_Plugins[index]['PortCount']
    port = deepcopy(PyLADSPA_RDF_Port)
    port['index'] = port_id
    Ports.append(port)
    LADSPA_Plugins[index]['PortCount'] += 1
    return (index, pcount)

def set_port_value(plugin_id, port_id, key, value):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  LADSPA_Plugins[i]['Ports'][j][key] = value

def add_port_value(plugin_id, port_id, key, value):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  LADSPA_Plugins[i]['Ports'][j][key] += value

def or_port_value(plugin_id, port_id, key, value):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  LADSPA_Plugins[i]['Ports'][j][key] |= value

def append_port_value(plugin_id, port_id, key, value):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  LADSPA_Plugins[i]['Ports'][j][key].append(value)

def add_scalepoint(plugin_id, port_id, value, label):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  Port = LADSPA_Plugins[i]['Ports'][j]
  scalepoint = deepcopy(PyLADSPA_RDF_ScalePoint)
  scalepoint['Value'] = value
  scalepoint['Label'] = label
  Port['ScalePoints'].append(scalepoint)
  Port['ScalePointCount'] += 1

def set_port_default(plugin_id, port_id, value):
  global LADSPA_Plugins
  i, j = check_and_add_port(plugin_id, port_id)
  Port = LADSPA_Plugins[i]['Ports'][j]
  Port['Default'] = value
  Port['Hints'] |= LADSPA_PORT_DEFAULT

def get_node_values(value_nodes, node_index):
  ret_nodes = []
  for node in value_nodes:
    index, uri, value = node
    if (node_index == index):
      ret_nodes.append((uri, value))
  return ret_nodes

# -------------------------------------------------------------------------------
#  RDF sort data methods

# Fully parse rdf file
def parse_rdf_file(filename):
  primer = ConjunctiveGraph()

  try:
    primer.parse(filename, format='xml')
    rdf_list = [(x, y, z) for x, y, z in primer]
  except:
    rdf_list = []

  # For BNodes
  index_nodes = [] # index, URI, Plugin, Port
  value_nodes = [] # index, URI, Value

  for i in range(len(rdf_list)):
    rdf_type  = rdf_list[i][0]
    rdf_uri   = rdf_list[i][1]
    rdf_value = rdf_list[i][2]

    if (rdf_is_type(rdf_type, LADSPA_RDF_TYPE_PLUGIN)):
      plugin_id = long(to_plugin_number(rdf_type))

      if (rdf_uri == URIRef(rdf_prefix['dc:creator'])):
        set_plugin_value(plugin_id, 'Creator', str(rdf_value))
        or_plugin_value(plugin_id, 'Hints', LADSPA_PLUGIN_CREATOR)

      elif (rdf_uri == URIRef(rdf_prefix['dc:rights'])):
        set_plugin_value(plugin_id, 'Rights', str(rdf_value))
        or_plugin_value(plugin_id, 'Hints', LADSPA_PLUGIN_RIGHTS)

      elif (rdf_uri == URIRef(rdf_prefix['dc:title'])):
        set_plugin_value(plugin_id, 'Title', str(rdf_value))
        or_plugin_value(plugin_id, 'Hints', LADSPA_PLUGIN_TITLE)

      elif (rdf_uri == URIRef(rdf_prefix['rdf:type'])):
        c_class = get_c_plugin_class(str(rdf_value))
        or_plugin_value(plugin_id, 'Type', c_class)

      elif (rdf_uri == URIRef(rdf_prefix['ladspa:hasPort'])):
        pass # No useful information here

      elif (rdf_uri == URIRef(rdf_prefix['ladspa:hasSetting'])):
        index_nodes.append((rdf_value, rdf_uri, plugin_id, None))

      else:
        print "LADSPA_RDF - Plugin URI", rdf_uri, "not handled"

    elif (rdf_is_type(rdf_type, LADSPA_RDF_TYPE_PORT)):
      plugin_port = to_plugin_numbers(rdf_type)
      plugin_id = int(plugin_port[0])
      port_id   = int(plugin_port[1])

      if (rdf_uri == URIRef(rdf_prefix['rdf:type'])):
        c_class = get_c_port_type(str(rdf_value))
        or_port_value(plugin_id, port_id, 'Type', c_class)

      elif (rdf_uri == URIRef(rdf_prefix['ladspa:hasLabel'])):
        set_port_value(plugin_id, port_id, 'Label', str(rdf_value))
        or_port_value(plugin_id, port_id, 'Hints', LADSPA_PORT_LABEL)

      elif (rdf_uri == URIRef(rdf_prefix['ladspa:hasScale'])):
        index_nodes.append((rdf_value, rdf_uri, plugin_id, port_id))

      elif (rdf_uri in (URIRef(rdf_prefix['ladspa:hasUnit']), URIRef(rdf_prefix['ladspa:hasUnits']))):
        c_unit = get_c_unit_type(str(rdf_value))
        set_port_value(plugin_id, port_id, 'Unit', c_unit)
        or_port_value(plugin_id, port_id, 'Hints', LADSPA_PORT_UNIT)

      else:
        print "LADSPA_RDF - Port URI", rdf_uri, "not handled"

    elif (rdf_type in (URIRef(rdf_prefix['ladspa:NotchPlugin']), URIRef(rdf_prefix['ladspa:SpectralPlugin']))):
      pass # These "extensions" are already implemented

    elif (type(rdf_type) == BNode):
      value_nodes.append((rdf_type, rdf_uri, rdf_value))

    else:
      print "LADSPA_RDF - Unknown URI Type", rdf_type

  # Parse BNodes, indexes
  bnodes_data_dump = []

  for i in range(len(index_nodes)):
    node_index = index_nodes[i][0]
    node_uri   = index_nodes[i][1]
    plugin_id  = index_nodes[i][2]
    port_id    = index_nodes[i][3]

    node_values = get_node_values(value_nodes, node_index)

    for j in range(len(node_values)):
      subnode_uri   = node_values[j][0]
      subnode_index = node_values[j][1]

      subnode_values = get_node_values(value_nodes, subnode_index)

      for k in range(len(subnode_values)):
        real_uri   = subnode_values[k][0]
        real_value = subnode_values[k][1]

        if (node_uri == URIRef(rdf_prefix['ladspa:hasScale']) and subnode_uri == URIRef(rdf_prefix['ladspa:hasPoint'])):
          bnodes_data_dump.append(("scalepoint", subnode_index, plugin_id, port_id, real_uri, real_value))
        elif (node_uri == URIRef(rdf_prefix['ladspa:hasSetting']) and subnode_uri == URIRef(rdf_prefix['ladspa:hasPortValue'])):
          # FIXME - needs another check for 'default'
          bnodes_data_dump.append(("port_default", subnode_index, plugin_id, port_id, real_uri, real_value))
        else:
          print "LADSPA_RDF - Unknown SubNode Combo", node_uri, subnode_uri

  # Process BNodes, values
  scalepoints = []
  port_defaults = []

  for i in range(len(bnodes_data_dump)):
    node_type   = bnodes_data_dump[i][0]
    node_index  = bnodes_data_dump[i][1]
    node_plugin = bnodes_data_dump[i][2]
    node_port   = bnodes_data_dump[i][3]
    node_vtype  = bnodes_data_dump[i][4]
    node_value  = bnodes_data_dump[i][5]

    if (node_type == "scalepoint"):
      for j in range(len(scalepoints)):
        if (scalepoints[j][0] == node_index):
          index = j
          break
      else:
        scalepoints.append([node_index, node_plugin, node_port, None, None])
        index = len(scalepoints)-1

      if (node_vtype == URIRef(rdf_prefix['rdf:value'])):
        scalepoints[index][3] = to_float(node_value)
      elif (node_vtype == URIRef(rdf_prefix['ladspa:hasLabel'])):
        scalepoints[index][4] = str(node_value)

    elif (node_type == "port_default"):
      for j in range(len(port_defaults)):
        if (port_defaults[j][0] == node_index):
          index = j
          break
      else:
        port_defaults.append([node_index, node_plugin, None, None])
        index = len(port_defaults)-1

      if (node_vtype == URIRef(rdf_prefix['rdf:value'])):
        port_defaults[index][3] = to_float(node_value)
      elif (node_vtype == URIRef(rdf_prefix['ladspa:forPort'])):
        port_defaults[index][2] = int(to_plugin_port(node_value))

  # Now add the last information
  for scalepoint in scalepoints:
    index, plugin_id, port_id, value, label = scalepoint
    add_scalepoint(plugin_id, port_id, value, label)

  for port_default in port_defaults:
    index, plugin_id, port_id, value = port_default
    set_port_default(plugin_id, port_id, value)

def append_and_sort(value, vlist):
  if (len(vlist) == 0):
    vlist.append(value)
  elif (value < vlist[0]):
    vlist.insert(0, value)
  elif (value > vlist[len(vlist)-1]):
    vlist.append(value)
  else:
    for i in range(len(vlist)):
      if (value < vlist[i]):
        vlist.insert(i, value)
        break
    else:
      print "LADSPA_RDF - CRITICAL ERROR #001"

  return vlist

def get_value_index(value, vlist):
  for i in range(len(vlist)):
    if (vlist[i] == value):
      return i
  else:
    print "LADSPA_RDF - CRITICAL ERROR #002"
    return 0

# Sort the plugin's port's ScalePoints by value
def SORT_PyLADSPA_RDF_ScalePoints(old_dict_list):
  new_dict_list = []
  indexes_list = []

  for i in range(len(old_dict_list)):
    new_dict_list.append(deepcopy(PyLADSPA_RDF_ScalePoint))
    append_and_sort(old_dict_list[i]['Value'], indexes_list)

  for i in range(len(old_dict_list)):
    index = get_value_index(old_dict_list[i]['Value'], indexes_list)
    new_dict_list[index]['Value'] = old_dict_list[i]['Value']
    new_dict_list[index]['Label'] = old_dict_list[i]['Label']

  return new_dict_list

# Sort the plugin's port by index
def SORT_PyLADSPA_RDF_Ports(old_dict_list):
  new_dict_list = []
  max_index = -1

  for i in range(len(old_dict_list)):
    if (old_dict_list[i]['index'] > max_index):
      max_index = old_dict_list[i]['index']

  for i in range(max_index+1):
    new_dict_list.append(deepcopy(PyLADSPA_RDF_Port))

  for i in range(len(old_dict_list)):
    index = old_dict_list[i]['index']
    new_dict_list[index]['index']   = old_dict_list[i]['index']
    new_dict_list[index]['Type']    = old_dict_list[i]['Type']
    new_dict_list[index]['Hints']   = old_dict_list[i]['Hints']
    new_dict_list[index]['Unit']    = old_dict_list[i]['Unit']
    new_dict_list[index]['Default'] = old_dict_list[i]['Default']
    new_dict_list[index]['Label']   = old_dict_list[i]['Label']
    new_dict_list[index]['ScalePointCount'] = old_dict_list[i]['ScalePointCount']
    new_dict_list[index]['ScalePoints'] = SORT_PyLADSPA_RDF_ScalePoints(old_dict_list[i]['ScalePoints'])

  return new_dict_list

# Main function - check all rdfs for information about ladspa plugins
def recheck_all_plugins(qobject=None):
  global LADSPA_RDF_PATH, LADSPA_Plugins

  LADSPA_Plugins = []
  rdf_extensions = (".rdf", ".rdF", ".rDF", ".RDF", ".RDf", "Rdf")
  rdf_files = []

  for PATH in LADSPA_RDF_PATH:
    for root, dirs, files in os.walk(PATH):
      for name in [name for name in files if name.endswith(rdf_extensions)]:
        rdf_files.append(os.path.join(root, name))

  for i in range(len(rdf_files)):
    rdf_file = rdf_files[i]

    # Tell GUI we're parsing this bundle
    if (qobject):
      percent = (qobject.percent_value * 0.80) + ( (float(i) / len(rdf_files) ) * qobject.percent_value * 0.20 )
      qobject.pluginLook(percent, rdf_file)

    # Parse RDF
    parse_rdf_file(rdf_file)

  return LADSPA_Plugins

# Convert [PyQt] LADSPA_Plugins into ctype structs
def get_c_ladspa_rdfs(PyPluginList):
  C_LADSPA_Plugins = []

  for plugin in PyPluginList:
    # Sort the ports by index
    ladspa_ports = SORT_PyLADSPA_RDF_Ports(plugin['Ports'])

    # Initial data
    desc = LADSPA_RDF_Descriptor()
    desc.Type      = plugin['Type']
    desc.Hints     = plugin['Hints']
    desc.UniqueID  = plugin['UniqueID']

    try:
      desc.Creator = plugin['Creator']
    except:
      desc.Creator = "(unicode error)"

    try:
      desc.Title   = plugin['Title']
    except:
      desc.Title   = "(unicode error)"

    try:
      desc.Rights  = plugin['Rights']
    except:
      desc.Rights  = "(unicode error)"

    desc.PortCount = plugin['PortCount']

    # Ports
    _PortType  = LADSPA_RDF_Port*desc.PortCount
    desc.Ports = _PortType()

    for i in range(desc.PortCount):
      port    = LADSPA_RDF_Port()
      py_port = ladspa_ports[i]

      port.Type    = py_port['Type']
      port.Hints   = py_port['Hints']
      port.Unit    = py_port['Unit']
      port.Default = py_port['Default']
      port.Label   = py_port['Label']

      # ScalePoints
      port.ScalePointCount = py_port['ScalePointCount']

      _ScalePointType  = LADSPA_RDF_ScalePoint*port.ScalePointCount
      port.ScalePoints = _ScalePointType()

      for j in range(port.ScalePointCount):
        scalepoint    = LADSPA_RDF_ScalePoint()
        py_scalepoint = py_port['ScalePoints'][j]

        scalepoint.Label = py_scalepoint['Label']
        scalepoint.Value = py_scalepoint['Value']

        port.ScalePoints[j] = scalepoint

      desc.Ports[i] = port

    C_LADSPA_Plugins.append(desc)

  return C_LADSPA_Plugins


# Implementation test
#if __name__ == '__main__':
    #plugins = recheck_all_plugins()

    ## Save to file
    #import json
    #f = open('ladspa_rdf.dump.txt', 'w')
    #json.dump(plugins, f)
    #f.close()

    ## Read back from file
    #fr = open('ladspa_rdf.dump.txt', 'r')
    #plugins_json = json.load(fr)

    ## Check if saved stuff is valid (print to std-out)
    #for plugin in plugins_json:
      #print "----------------------"
      #print "Type:     ", plugin["Type"]
      #print "Hints:    ", plugin["Hints"]
      #print "UniqueID: ", plugin["UniqueID"]
      #print "Creator:  ", plugin["Creator"]
      #print "Title:    ", plugin["Title"]
      #print "Rights:   ", plugin["Rights"]
      #print "Ports:    (%i)" % (plugin["PortCount"])
      #for i in range(plugin["PortCount"]):
        #port = plugin["Ports"][i]
        #print "  Type:        ", port["Type"]
        #print "  Hints:       ", port["Hints"]
        #print "  Unit:        ", port["Unit"]
        #print "  Default:     ", port["Default"]
        #print "  Label:       ", port["Label"]
        #print "  ScalePoints: (%i)" % (port["ScalePointCount"])
        #for j in range(port["ScalePointCount"]):
          #scalepoint = port["ScalePoints"][j]
          #print "    Value: ", scalepoint["Value"]
          #print "    Label: ", scalepoint["Label"]
