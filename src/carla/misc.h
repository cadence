/* JACK Backend code for Carla */

#ifndef CARLA_MISC_H
#define CARLA_MISC_H

#include <QtCore/QThread>

class AudioPlugin;
class QProcess;

// --------------------------------------------------------------------------------------------------------
// Helper functions

const char* bool2str(bool yesno);

// --------------------------------------------------------------------------------------------------------
// PluginOscThread

class PluginOscThread : public QThread
{
public:
    PluginOscThread(QObject* parent=0);
    ~PluginOscThread();

    void set_plugin(AudioPlugin* plugin);
    void run();

private:
    AudioPlugin* m_plugin;
    QProcess* m_process;
};

// --------------------------------------------------------------------------------------------------------
// CarlaCheckThread

class CarlaCheckThread : public QThread
{
public:
    CarlaCheckThread(QObject* parent = 0);
    void run();
};

#endif // CARLA_MISC_H
