/* JACK Backend code for Carla */

#include "misc.h"
#include "audio_plugin.h"

#include <QtCore/QProcess>

// Global variables
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Global OSC stuff
extern OscData global_osc_data;

// --------------------------------------------------------------------------------------------------------
// Helper functions

const char* bool2str(bool yesno)
{
    if (yesno)
        return "true";
    else
        return "false";
}

// --------------------------------------------------------------------------------------------------------
// PluginOscThread

PluginOscThread::PluginOscThread(QObject *parent) :
    QThread(parent)
{
    qDebug("PluginOscThread::PluginOscThread(%p)", parent);
    m_process = new QProcess(parent);
}

PluginOscThread::~PluginOscThread()
{
    // TODO - kill process
    delete m_process;
}

void PluginOscThread::set_plugin(AudioPlugin* plugin)
{
    m_plugin = plugin;
}

void PluginOscThread::run()
{
}

// --------------------------------------------------------------------------------------------------------
// CarlaCheckThread

CarlaCheckThread::CarlaCheckThread(QObject *parent) :
    QThread(parent)
{
    qDebug("CarlaCheckThread::CarlaCheckThread(%p)", parent);
}

void CarlaCheckThread::run()
{
    qDebug("CarlaCheckThread::run()");

    uint32_t j;
    PluginPostEvent post_events[MAX_POSTEVENTS];

    while (carla_is_engine_running())
    {
        for (unsigned short i=0; i<MAX_PLUGINS; i++)
        {
            AudioPlugin* plugin = AudioPlugins[i];
            if (plugin && plugin->id >= 0)
            {
                // --------------------------------------------------------------------------------------------------------
                // Process postponed events

                // Make a safe copy of events, and clear them
                plugin->post_events_lock();
                memcpy(&post_events, &plugin->post_events.events, sizeof(PluginPostEvent)*MAX_POSTEVENTS);

                for (j=0; j < MAX_POSTEVENTS; j++)
                    plugin->post_events.events[j].valid = false;

                plugin->post_events_unlock();

                // Process events now
                for (j=0; j < MAX_POSTEVENTS; j++)
                {
                    if (post_events[j].valid)
                    {
                        switch (post_events[j].type)
                        {
                        case PostEventDebug:
                            callback_action(CALLBACK_DEBUG, plugin->id, post_events[j].index, 0, post_events[j].value);
                            break;

                        case PostEventParameter:
                            osc_send_set_parameter_value(&global_osc_data, plugin->id, post_events[j].index, post_events[j].value);
                            callback_action(CALLBACK_PARAMETER_CHANGED, plugin->id, post_events[j].index, 0, post_events[j].value);

                            if (plugin->hints & PLUGIN_IS_BRIDGE)
                                osc_send_control(&plugin->osc.data, post_events[j].index, post_events[j].value);
                            break;

                        case PostEventProgram:
                            osc_send_set_program(&global_osc_data, plugin->id, post_events[j].index);
                            callback_action(CALLBACK_PROGRAM_CHANGED, plugin->id, post_events[j].index, 0, 0.0f);

                            if (plugin->hints & PLUGIN_IS_BRIDGE)
                                osc_send_program(&plugin->osc.data, post_events[j].index);

                            for (uint32_t k=0; k < plugin->param.count; k++)
                                osc_send_set_default_value(&global_osc_data, plugin->id, k, plugin->param.ranges[k].def);
                            break;

                        case PostEventMidiProgram:
                            osc_send_set_midi_program(&global_osc_data, plugin->id, post_events[j].index);                            
                            callback_action(CALLBACK_MIDI_PROGRAM_CHANGED, plugin->id, post_events[j].index, 0, 0.0f);

                            if (plugin->type == PLUGIN_DSSI)
                                osc_send_program_as_midi(&plugin->osc.data, plugin->midiprog.data[post_events[j].index].bank, plugin->midiprog.data[post_events[j].index].program);

                            if (plugin->hints & PLUGIN_IS_BRIDGE)
                                osc_send_midi_program(&plugin->osc.data, plugin->midiprog.data[post_events[j].index].bank, plugin->midiprog.data[post_events[j].index].program);

                            for (uint32_t k=0; k < plugin->param.count; k++)
                                osc_send_set_default_value(&global_osc_data, plugin->id, k, plugin->param.ranges[k].def);
                            break;

                        case PostEventNoteOn:
                            osc_send_note_on(&global_osc_data, plugin->id, post_events[j].index, post_events[j].value);
                            callback_action(CALLBACK_NOTE_ON, plugin->id, post_events[j].index, post_events[j].value, 0.0f);

                            if (plugin->hints & PLUGIN_IS_BRIDGE)
                                osc_send_note_on(&plugin->osc.data, plugin->id, post_events[j].index, post_events[j].value);
                            break;

                        case PostEventNoteOff:
                            osc_send_note_off(&global_osc_data, plugin->id, post_events[j].index, post_events[j].value);
                            callback_action(CALLBACK_NOTE_OFF, plugin->id, post_events[j].index, post_events[j].value, 0.0f);

                            if (plugin->hints & PLUGIN_IS_BRIDGE)
                                osc_send_note_off(&plugin->osc.data, plugin->id, post_events[j].index, post_events[j].value);
                            break;

                        default:
                            break;
                        }
                    }
                }

                // --------------------------------------------------------------------------------------------------------
                // Idle plugin
                if (plugin->gui.visible && plugin->gui.type != GUI_EXTERNAL_OSC)
                    plugin->idle_gui();

                // --------------------------------------------------------------------------------------------------------
                // Update ports

                // Check if it needs update
                bool update_ports_gui = (plugin->gui.visible && plugin->gui.type == GUI_EXTERNAL_OSC && plugin->osc.data.target);

                if (!global_osc_data.target && !update_ports_gui)
                    continue;

                // Update
                for (j=0; j < plugin->param.count; j++)
                {
                    if (plugin->param.data[j].type == PARAMETER_OUTPUT && (plugin->param.data[j].hints & PARAMETER_IS_AUTOMABLE) > 0)
                    {
                        if (update_ports_gui)
                            osc_send_control(&plugin->osc.data, plugin->param.data[j].rindex, plugin->param.buffers[j]);

                        osc_send_set_parameter_value(&global_osc_data, plugin->id, j, plugin->param.buffers[j]);
                    }
                }

                // --------------------------------------------------------------------------------------------------------
                // Send peak values (OSC)
                if (global_osc_data.target)
                {
                    if (plugin->ain.count > 0)
                    {
                        osc_send_set_input_peak_value(&global_osc_data, plugin->id, 1, ains_peak[(plugin->id*2)+0]);
                        osc_send_set_input_peak_value(&global_osc_data, plugin->id, 2, ains_peak[(plugin->id*2)+1]);
                    }
                    if (plugin->aout.count > 0)
                    {
                        osc_send_set_output_peak_value(&global_osc_data, plugin->id, 1, aouts_peak[(plugin->id*2)+0]);
                        osc_send_set_output_peak_value(&global_osc_data, plugin->id, 2, aouts_peak[(plugin->id*2)+1]);
                    }
                }
            }
        }
        usleep(50000); // 50 ms
    }
}
