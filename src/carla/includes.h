/* JACK Backend code for Carla */

#ifndef CARLA_INCLUDES_H
#define CARLA_INCLUDES_H

#include <stdint.h>

// we need this for qDebug/Warning/Critical sections
#if __WORDSIZE == 64
#define P_INTPTR "%li"
#define P_SIZE   "%lu"
#else
#define P_INTPTR "%i"
#define P_SIZE   "%u"
#endif

#ifndef nullptr
const class {
public:
    template<class T> operator T*() const
    {
        return 0;
    }

    template<class C, class T> operator T C::*() const
    {
        return 0;
    }

private:
    void operator&() const;
} nullptr_ = {};
#define nullptr nullptr_
#endif

#if defined(__WIN32__) || defined(__WIN64__)
#include <windows.h>
#ifndef __WINDOWS__
#define __WINDOWS__
#endif
#else
#include <dlfcn.h>
#undef __cdecl
#define __cdecl
#endif

#ifdef __WINDOWS__
#define CARLA_EXPORT extern "C" __declspec (dllexport)
#else
#define CARLA_EXPORT extern "C" __attribute__ ((visibility("default")))
#endif

#endif // CARLA_INCLUDES_H
