/* Code for OSS related tasks */

#include "osc.h"
#include "audio_plugin.h"

#include <stdio.h>

// Global variables
extern const char* carla_client_name;
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Global OSC stuff
extern lo_server_thread global_osc_server_thread;
extern const char* global_osc_server_path;
extern OscData global_osc_data;

void osc_init()
{
    qDebug("osc_init()");

    global_osc_server_thread = lo_server_thread_new(nullptr, osc_error_handler);
    const char* osc_thread_path = lo_server_thread_get_url(global_osc_server_thread);

    char osc_path_tmp[strlen(osc_thread_path)+strlen(carla_client_name)+1];
    sprintf(osc_path_tmp, "%s%s", osc_thread_path, carla_client_name);
    global_osc_server_path = strdup(osc_path_tmp);

    free((void*)osc_thread_path);

    lo_server_thread_add_method(global_osc_server_thread, nullptr, nullptr, osc_message_handler, nullptr);
    lo_server_thread_start(global_osc_server_thread);

    printf("Carla OSC -> %s\n", global_osc_server_path);
}

void osc_close()
{
    qDebug("osc_close()");

    osc_clear_data(&global_osc_data);

    lo_server_thread_stop(global_osc_server_thread);
    lo_server_thread_del_method(global_osc_server_thread, nullptr, nullptr);
    lo_server_thread_free(global_osc_server_thread);

    free((void*)global_osc_server_path);
}

void osc_clear_data(OscData* osc_data)
{
    qDebug("osc_clear_data(%p)", osc_data);

    if (osc_data->path)
        free((void*)osc_data->path);

    if (osc_data->source)
        lo_address_free(osc_data->source);

    if (osc_data->target)
        lo_address_free(osc_data->target);

    osc_data->path = nullptr;
    osc_data->source = nullptr;
    osc_data->target = nullptr;
}

void osc_error_handler(int num, const char* msg, const char* path)
{
    qCritical("osc_error_handler(%i, %s, %s)", num, msg, path);
}

int osc_message_handler(const char* path, const char* types, lo_arg** argv, int argc, void* data, void* user_data)
{
    //qDebug("osc_message_handler(%s, %s, %p, %i, %p, %p)", path, types, argv, argc, data, user_data);

    size_t client_len = strlen(carla_client_name);

    // Initial path check
    if (strcmp(path, "register") == 0)
    {
        lo_message message = lo_message(data);
        lo_address source  = lo_message_get_source(message);
        return osc_register_handler(argv, source);
    }
    else if (strcmp(path, "unregister") == 0)
    {
        return osc_unregister_handler();
    }
    else
    {
        // Check if message is for us
        for (size_t i=0; i<client_len; i++)
        {
            // ex: /Carla/0/message
            if (path[i+1] != carla_client_name[i])
            {
                qWarning("osc_message_handler() - message not for us -> %s != /%s", path, carla_client_name);
                return 1;
            }
        }
    }

    // Get id from message
    int plugin_id = 0;
    if (std::isdigit(path[client_len+2]))
        plugin_id += path[client_len+2]-'0';
    if (std::isdigit(path[client_len+3]))
        plugin_id += (path[client_len+3]-'0')*10;

    if (plugin_id < 0 || plugin_id > MAX_PLUGINS)
    {
        qCritical("osc_message_handler() - failed to get plugin_id -> %i", plugin_id);
        return 1;
    }

    AudioPlugin* plugin = AudioPlugins[plugin_id];

    if (!plugin || plugin->id != plugin_id) {
        qWarning("osc_message_handler() - invalid plugin '%i', probably has been removed", plugin_id);
        return 1;
    }

    // Get method from path (/Carla/i/method)
    unsigned int mindex = client_len + 3;
    mindex += (plugin_id > 10) ? 2 : 1;
    char method[24] = { 0 };

    for (size_t i=mindex; i<strlen(path) && i<mindex+24; i++)
        method[i-mindex] = path[i];

    // Internal OSC Stuff
    if (strcmp(method, "set_active") == 0)
        return osc_set_active_handler(plugin, argv);
    else if (strcmp(method, "set_drywet") == 0)
        return osc_set_drywet_handler(plugin, argv);
    else if (strcmp(method, "set_vol") == 0)
        return osc_set_vol_handler(plugin, argv);
    else if (strcmp(method, "set_balance_left") == 0)
        return osc_set_balance_left_handler(plugin, argv);
    else if (strcmp(method, "set_balance_right") == 0)
        return osc_set_balance_right_handler(plugin, argv);
    else if (strcmp(method, "set_parameter") == 0)
        return osc_set_parameter_handler(plugin, argv);
    else if (strcmp(method, "set_program") == 0)
        return osc_set_program_handler(plugin, argv);
    else if (strcmp(method, "note_on") == 0)
        return osc_note_on_handler(plugin, argv);
    else if (strcmp(method, "note_off") == 0)
        return osc_note_off_handler(plugin, argv);

    // Plugin Bridges
    else if (strcmp(method, "bridge_audio_count") == 0)
        return plugin->set_osc_bridge_info(OscBridgeAudioCountInfo, argv);
    else if (strcmp(method, "bridge_midi_count") == 0)
        return plugin->set_osc_bridge_info(OscBridgeMidiCountInfo, argv);
    else if (strcmp(method, "bridge_param_count") == 0)
        return plugin->set_osc_bridge_info(OscBridgeParameterCountInfo, argv);
    else if (strcmp(method, "bridge_program_count") == 0)
        return plugin->set_osc_bridge_info(OscBridgeProgramCountInfo, argv);
    else if (strcmp(method, "bridge_midi_program_count") == 0)
        return plugin->set_osc_bridge_info(OscBridgeMidiProgramCountInfo, argv);
    else if (strcmp(method, "bridge_plugin_info") == 0)
        return plugin->set_osc_bridge_info(OscBridgePluginInfo, argv);
    else if (strcmp(method, "bridge_param_info") == 0)
        return plugin->set_osc_bridge_info(OscBridgeParameterInfo, argv);
    else if (strcmp(method, "bridge_param_data") == 0)
        return plugin->set_osc_bridge_info(OscBridgeParameterDataInfo, argv);
    else if (strcmp(method, "bridge_param_ranges") == 0)
        return plugin->set_osc_bridge_info(OscBridgeParameterRangesInfo, argv);
    else if (strcmp(method, "bridge_program_name") == 0)
        return plugin->set_osc_bridge_info(OscBridgeProgramName, argv);
    else if (strcmp(method, "bridge_ains_peak") == 0)
        return osc_bridge_ains_peak_handler(plugin, argv);
    else if (strcmp(method, "bridge_aouts_peak") == 0)
        return osc_bridge_aouts_peak_handler(plugin, argv);
    else if (strcmp(method, "bridge_update") == 0)
        return plugin->set_osc_bridge_info(OscBridgeUpdateNow, argv);

    // Misc Stuff
    else
    {
        if (strcmp(method, "update") == 0)
        {
            lo_message message = lo_message(data);
            lo_address source  = lo_message_get_source(message);
            return osc_update_handler(plugin, argv, source);
        }
        else if (strcmp(method, "configure") == 0)
            return osc_configure_handler(plugin, argv);
        else if (strcmp(method, "control") == 0)
            return osc_control_handler(plugin, argv);
        else if (strcmp(method, "program") == 0)
            return (plugin->type == PLUGIN_DSSI) ? osc_midi_program_handler(plugin, argv) : osc_program_handler(plugin, argv);
        else if (strcmp(method, "midi") == 0)
            return osc_midi_handler(plugin, argv);
        else if (strcmp(method, "exiting") == 0)
            return osc_exiting_handler(plugin);
        else
            qWarning("osc_message_handler() - unsupported OSC method '%s'", method);
    }

    return 1;

    Q_UNUSED(types);
    Q_UNUSED(argc);
    Q_UNUSED(user_data);
}

int osc_register_handler(lo_arg** argv, lo_address source)
{
    qDebug("osc_register_handler()");

    if (!global_osc_data.path)
    {
        const char* url = (const char*)&argv[0]->s;
        const char* host;
        const char* port;

        printf("osc_register_handler() - OSC backend registered to %s\n", url);

        host = lo_address_get_hostname(source);
        port = lo_address_get_port(source);
        global_osc_data.source = lo_address_new(host, port);

        host = lo_url_get_hostname(url);
        port = lo_url_get_port(url);
        global_osc_data.target = lo_address_new(host, port);

        global_osc_data.path = lo_url_get_path(url);

        free((void*)host);
        free((void*)port);

        for (unsigned short i=0; i<MAX_PLUGINS; i++)
        {
            AudioPlugin* plugin = AudioPlugins[i];
            if (plugin && plugin->id >= 0)
                osc_new_plugin(plugin);
        }

        return 0;
    }
    else
        qCritical("osc_register_handler() - OSC backend already registered to %s", global_osc_data.path);

    return 1;
}

int osc_unregister_handler()
{
    qDebug("osc_unregister_handler()");

    if (global_osc_data.path)
    {
        osc_clear_data(&global_osc_data);
        return 0;
    }
    else
        qCritical("osc_unregister_handler() - OSC backend is not registered yet");

    return 1;
}

int osc_set_active_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_active_handler()");

    bool value = (bool)argv[0]->i;
    plugin->set_active(value, false, true);

    return 0;
}

int osc_set_drywet_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_drywet_handler()");

    double value = argv[0]->f;
    plugin->set_drywet(value, false, true);

    return 0;
}

int osc_set_vol_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_vol_handler()");

    double value = argv[0]->f;
    plugin->set_vol(value, false, true);

    return 0;
}

int osc_set_balance_left_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_balance_left_handler()");

    double value = argv[0]->f;
    plugin->set_balance_left(value, false, true);

    return 0;
}

int osc_set_balance_right_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_balance_right_handler()");

    double value = argv[0]->f;
    plugin->set_balance_right(value, false, true);

    return 0;
}

int osc_set_parameter_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_parameter_handler()");

    uint32_t parameter_id = argv[0]->i;
    double value = argv[1]->f;
    plugin->set_parameter_value(parameter_id, value, true, false, true);

    return 0;
}

int osc_set_program_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_set_program_handler()");

    uint32_t program_id = argv[0]->i;
    plugin->set_program(program_id, true, false, true, true);

    return 0;
}

int osc_note_on_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_note_on_handler()");

    int note = argv[0]->i;
    int velo = argv[1]->i;
    send_plugin_midi_note(plugin->id, true, note, velo, true, false, true);

    return 0;
}

int osc_note_off_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_note_off_handler()");

    int note = argv[0]->i;
    int velo = argv[1]->i;
    send_plugin_midi_note(plugin->id, false, note, velo, true, false, true);

    return 0;
}

int osc_bridge_ains_peak_handler(AudioPlugin* plugin, lo_arg** argv)
{
    int index    = argv[0]->i;
    double value = argv[1]->f;

    ains_peak[(plugin->id*2)+index-1] = value;
    return 0;
}

int osc_bridge_aouts_peak_handler(AudioPlugin* plugin, lo_arg** argv)
{
    int index    = argv[0]->i;
    double value = argv[1]->f;

    aouts_peak[(plugin->id*2)+index-1] = value;
    return 0;
}

int osc_update_handler(AudioPlugin* plugin, lo_arg** argv, lo_address source)
{
    qDebug("osc_update_handler()");

    const char* url = (const char*)&argv[0]->s;
    const char* host;
    const char* port;

    osc_clear_data(&plugin->osc.data);

    host = lo_address_get_hostname(source);
    port = lo_address_get_port(source);
    plugin->osc.data.source = lo_address_new(host, port);

    host = lo_url_get_hostname(url);
    port = lo_url_get_port(url);

    plugin->osc.data.path = lo_url_get_path(url);
    plugin->osc.data.target = lo_address_new(host, port);

    free((void*)host);
    free((void*)port);

    for (int i=0; i < plugin->custom.count(); i++)
    {
        if (plugin->custom[i].type == CUSTOM_DATA_STRING)
            osc_send_configure(&plugin->osc.data, plugin->custom[i].key, plugin->custom[i].value);
    }

    if (plugin->prog.current >= 0)
        osc_send_program(&plugin->osc.data, plugin->prog.current);

    if (plugin->midiprog.current >= 0)
    {
        int32_t midi_id = plugin->midiprog.current;
        osc_send_midi_program(&plugin->osc.data, plugin->midiprog.data[midi_id].bank, plugin->midiprog.data[midi_id].program);

        if (plugin->type == PLUGIN_DSSI)
            osc_send_program_as_midi(&plugin->osc.data, plugin->midiprog.data[midi_id].bank, plugin->midiprog.data[midi_id].program);
    }

    for (uint32_t i=0; i < plugin->param.count; i++)
        osc_send_control(&plugin->osc.data, plugin->param.data[i].rindex, plugin->param.buffers[i]);

    if (plugin->hints & PLUGIN_IS_BRIDGE)
    {
        osc_send_control(&plugin->osc.data, PARAMETER_ACTIVE, plugin->active ? 1.0 : 0.0);
        osc_send_control(&plugin->osc.data, PARAMETER_DRYWET, plugin->x_drywet);
        osc_send_control(&plugin->osc.data, PARAMETER_VOLUME, plugin->x_vol);
        osc_send_control(&plugin->osc.data, PARAMETER_BALANCE_LEFT, plugin->x_bal_left);
        osc_send_control(&plugin->osc.data, PARAMETER_BALANCE_RIGHT, plugin->x_bal_right);
    }

    return 0;
}

int osc_configure_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_configure_handler()");

    const char* key   = (const char*)&argv[0]->s;
    const char* value = (const char*)&argv[1]->s;

    plugin->set_custom_data(CUSTOM_DATA_STRING, key, value, false);

    return 0;
}

int osc_control_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_control_handler()");

    int32_t rindex = argv[0]->i;
    double value = argv[1]->f;

    int32_t parameter_id = -1;

    for (uint32_t i=0; i < plugin->param.count; i++)
    {
        if (plugin->param.data[i].rindex == rindex)
        {
            parameter_id = i;
            break;
        }
    }

    if (parameter_id >= 0)
    {
        plugin->set_parameter_value(parameter_id, value, false, true, true);
        return 0;
    }
    else
        return 1;
}

int osc_program_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_program_handler()");

    uint32_t program_id = argv[0]->i;

    if (program_id < plugin->prog.count)
    {
        plugin->set_program(program_id, false, true, true, true);
        return 0;
    }
    else
        qCritical("osc_program_handler() - program_id '%i' out of bounds", program_id);

    return 1;
}

int osc_midi_program_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_midi_program_handler()");

    uint32_t bank_id    = argv[0]->i;
    uint32_t program_id = argv[1]->i;

    for (uint32_t i=0; i < plugin->midiprog.count; i++)
    {
        if (plugin->midiprog.data[i].bank == bank_id && plugin->midiprog.data[i].program == program_id)
        {
            plugin->set_midi_program(i, false, true, true, false);
            return 0;
        }
    }

    qCritical("osc_midi_program_handler() - failed to find respective bank/program");
    return 1;
}

int osc_midi_handler(AudioPlugin* plugin, lo_arg** argv)
{
    qDebug("osc_midi_handler()");

    if (plugin->min.count > 0)
    {
        uint8_t* data = argv[0]->m;
        uint8_t mode = data[1] & 0xff;
        uint8_t note = data[2] & 0x7f;
        uint8_t velo = data[3] & 0x7f;

        // only receive notes
        if (mode < 0x80 || mode > 0x9F)
            return 1;

        // fix bad note off
        if (mode >= 0x90 && velo == 0)
        {
            mode -= 0x10;
            velo = 0x64;
        }

        send_plugin_midi_note(plugin->id, (mode >= 0x90), note, velo, false, true, true);
    }

    return 0;
}

int osc_exiting_handler(AudioPlugin* plugin)
{
    qDebug("osc_exiting_handler()");

    // TODO - check for non-UIs (dssi-vst) and set to -1 instead
    plugin->gui.visible = false;
    callback_action(CALLBACK_SHOW_GUI, plugin->id, 0, 0, 0.0f);
    osc_clear_data(&plugin->osc.data);

    return 0;
}

void osc_new_plugin(AudioPlugin* plugin)
{
    qDebug("osc_new_plugin()");

    if (global_osc_data.target)
    {
        osc_send_add_plugin(&global_osc_data, plugin->id, plugin->name);

        PluginInfo* plinfo = get_plugin_info(plugin->id);
        PortCountInfo* pocinfo = get_parameter_count_info(plugin->id);

        osc_send_set_plugin_data(&global_osc_data, plugin->id, plinfo->type, plinfo->category, plinfo->hints, get_real_plugin_name(plugin->id),
                                 plinfo->label, plinfo->maker, plinfo->copyright, plinfo->unique_id);

        osc_send_set_plugin_ports(&global_osc_data, plugin->id,
                                  plugin->ain.count, plugin->aout.count,
                                  plugin->min.count, plugin->mout.count,
                                  pocinfo->ins, pocinfo->outs, pocinfo->total);

        osc_send_set_parameter_value(&global_osc_data, plugin->id, PARAMETER_ACTIVE, plugin->active ? 1.0f : 0.0f);
        osc_send_set_parameter_value(&global_osc_data, plugin->id, PARAMETER_DRYWET, plugin->x_drywet);
        osc_send_set_parameter_value(&global_osc_data, plugin->id, PARAMETER_VOLUME, plugin->x_vol);
        osc_send_set_parameter_value(&global_osc_data, plugin->id, PARAMETER_BALANCE_LEFT, plugin->x_bal_left);
        osc_send_set_parameter_value(&global_osc_data, plugin->id, PARAMETER_BALANCE_RIGHT, plugin->x_bal_right);

        uint32_t i;

        if (plugin->param.count > 0 && plugin->param.count < 200)
        {
            for (i=0; i < plugin->param.count; i++)
            {
                ParameterInfo* info     = get_parameter_info(plugin->id, i);
                ParameterData* data     = &plugin->param.data[i];
                ParameterRanges* ranges = &plugin->param.ranges[i];

                osc_send_set_parameter_data(&global_osc_data, plugin->id, i, data->type, data->hints, info->name, info->label, plugin->param.buffers[i],
                                            ranges->min, ranges->max, ranges->def,
                                            ranges->step, ranges->step_small, ranges->step_large);
            }
        }

        osc_send_set_program_count(&global_osc_data, plugin->id, plugin->prog.count);

        for (i=0; i < plugin->prog.count; i++)
            osc_send_set_program_name(&global_osc_data, plugin->id, i, plugin->prog.names[i]);

        osc_send_set_program(&global_osc_data, plugin->id, plugin->prog.current);

        osc_send_set_midi_program_count(&global_osc_data, plugin->id, plugin->midiprog.count);

        for (i=0; i < plugin->midiprog.count; i++)
            osc_send_set_program_name(&global_osc_data, plugin->id, i, plugin->midiprog.names[i]);

        osc_send_set_midi_program(&global_osc_data, plugin->id, plugin->midiprog.current);
    }
}

void osc_send_add_plugin(OscData* osc_data, int plugin_id, const char* plugin_name)
{
    qDebug("osc_send_add_plugin()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+12];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/add_plugin");
        lo_send(osc_data->target, target_path, "is", plugin_id, plugin_name);
    }
}

void osc_send_remove_plugin(OscData* osc_data, int plugin_id)
{
    qDebug("osc_send_remove_plugin()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+15];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/remove_plugin");
        lo_send(osc_data->target, target_path, "i", plugin_id);
    }
}

void osc_send_set_plugin_data(OscData* osc_data, int plugin_id, int type, int category, int hints, const char* name, const char* label, const char* maker, const char* copyright, long unique_id)
{
    qDebug("osc_send_set_plugin_data()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+17];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_plugin_data");
        lo_send(osc_data->target, target_path, "iiiissssi", plugin_id, type, category, hints, name, label, maker, copyright, unique_id);
    }
}

void osc_send_set_plugin_ports(OscData* osc_data, int plugin_id, int ains, int aouts, int mins, int mouts, int cins, int couts, int ctotals)
{
    qDebug("osc_send_set_plugin_ports()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+18];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_plugin_ports");
        lo_send(osc_data->target, target_path, "iiiiiiii", plugin_id, ains, aouts, mins, mouts, cins, couts, ctotals);
    }
}

void osc_send_set_parameter_value(OscData* osc_data, int plugin_id, int param_id, double value)
{
    //qDebug("osc_send_set_parameter_value()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+21];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_parameter_value");
        lo_send(osc_data->target, target_path, "iif", plugin_id, param_id, value);
    }
}

void osc_send_set_parameter_data(OscData* osc_data, int plugin_id, int param_id, int ptype, int hints, const char* name, const char* label, double current, double x_min, double x_max, double x_def, double x_step, double x_step_small, double x_step_large)
{
    qDebug("osc_send_set_parameter_data()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+20];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_parameter_data");
        lo_send(osc_data->target, target_path, "iiiissfffffff", plugin_id, param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large);
    }
}

void osc_send_set_parameter_midi_channel(OscData* osc_data, int plugin_id, int parameter_id, int midi_channel)
{
    qDebug("osc_send_set_parameter_midi_channel()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+28];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_parameter_midi_channel");
        lo_send(osc_data->target, target_path, "iii", plugin_id, parameter_id, midi_channel);
    }
}

void osc_send_set_parameter_midi_cc(OscData* osc_data, int plugin_id, int parameter_id, int midi_cc)
{
    qDebug("osc_send_set_parameter_midi_cc()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+23];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_parameter_midi_cc");
        lo_send(osc_data->target, target_path, "iii", plugin_id, parameter_id, midi_cc);
    }
}

void osc_send_set_default_value(OscData* osc_data, int plugin_id, int param_id, double value)
{
    qDebug("osc_send_set_default_value()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+19];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_default_value");
        lo_send(osc_data->target, target_path, "iif", plugin_id, param_id, value);
    }
}

void osc_send_set_input_peak_value(OscData* osc_data, int plugin_id, int port_id, double value)
{
    //qDebug("osc_send_set_input_peak_value()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+22];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_input_peak_value");
        lo_send(osc_data->target, target_path, "iif", plugin_id, port_id, value);
    }
}

void osc_send_set_output_peak_value(OscData* osc_data, int plugin_id, int port_id, double value)
{
    //qDebug("osc_send_set_output_peak_value()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+23];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_output_peak_value");
        lo_send(osc_data->target, target_path, "iif", plugin_id, port_id, value);
    }
}

void osc_send_set_program(OscData* osc_data, int plugin_id, int program_id)
{
    qDebug("osc_send_set_program()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+13];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_program");
        lo_send(osc_data->target, target_path, "ii", plugin_id, program_id);
    }
}

void osc_send_set_program_count(OscData* osc_data, int plugin_id, int program_count)
{
    qDebug("osc_send_set_program_count()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+19];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_program_count");
        lo_send(osc_data->target, target_path, "ii", plugin_id, program_count);
    }
}

void osc_send_set_program_name(OscData* osc_data, int plugin_id, int program_id, const char* program_name)
{
    qDebug("osc_send_set_program_name()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+18];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_program_name");
        lo_send(osc_data->target, target_path, "iis", plugin_id, program_id, program_name);
    }
}

void osc_send_set_midi_program(OscData* osc_data, int plugin_id, int midi_program_id)
{
    qDebug("osc_send_set_midi_program()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+18];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_midi_program");
        lo_send(osc_data->target, target_path, "ii", plugin_id, midi_program_id);
    }
}

void osc_send_set_midi_program_count(OscData* osc_data, int plugin_id, int midi_program_count)
{
    qDebug("osc_send_set_midi_program_count()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+24];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_midi_program_count");
        lo_send(osc_data->target, target_path, "ii", plugin_id, midi_program_count);
    }
}

void osc_send_set_midi_program_data(OscData* osc_data, int plugin_id, int midi_program_id, int bank_id, int program_id, const char* midi_program_name)
{
    qDebug("osc_send_set_midi_program_data()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+23];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/set_midi_program_data");
        lo_send(osc_data->target, target_path, "iiiiis", plugin_id, midi_program_id, bank_id, program_id, midi_program_name);
    }
}

void osc_send_note_on(OscData* osc_data, int plugin_id, int note, int velo)
{
    qDebug("osc_send_note_on()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+9];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/note_on");
        lo_send(osc_data->target, target_path, "iii", plugin_id, note, velo);
    }
}

void osc_send_note_off(OscData* osc_data, int plugin_id, int note, int velo)
{
    qDebug("osc_send_note_off()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+10];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/note_off");
        lo_send(osc_data->target, target_path, "iii", plugin_id, note, velo);
    }
}

void osc_send_exit(OscData* osc_data)
{
    qDebug("osc_send_exit()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+6];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/exit");
        lo_send(osc_data->target, target_path, "");
    }
}

void osc_send_configure(OscData* osc_data, const char* key, const char* value)
{
    qDebug("osc_send_configure()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+11];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/configure");
        lo_send(osc_data->target, target_path, "ss", key, value);
    }
}

void osc_send_control(OscData* osc_data, int param, double value)
{
    //qDebug("osc_send_control()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+9];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/control");
        lo_send(osc_data->target, target_path, "if", param, value);
    }
}

void osc_send_program(OscData* osc_data, int program_id)
{
    qDebug("osc_send_program()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+9];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/program");
        lo_send(osc_data->target, target_path, "i", program_id);
    }
}

void osc_send_program_as_midi(OscData* osc_data, int bank, int program)
{
    qDebug("osc_send_program_as_midi()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+9];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/program");
        lo_send(osc_data->target, target_path, "ii", bank, program);
    }
}

void osc_send_midi_program(OscData* osc_data, int bank, int program)
{
    qDebug("osc_send_midi_program()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+9];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/midi_program");
        lo_send(osc_data->target, target_path, "ii", bank, program);
    }
}

void osc_send_show(OscData* osc_data)
{
    qDebug("osc_send_show()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+6];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/show");
        lo_send(osc_data->target, target_path, "");
    }
}

void osc_send_hide(OscData* osc_data)
{
    qDebug("osc_send_hide()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+6];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/hide");
        lo_send(osc_data->target, target_path, "");
    }
}

void osc_send_quit(OscData* osc_data)
{
    qDebug("osc_send_quit()");
    if (osc_data->target)
    {
        char target_path[strlen(osc_data->path)+6];
        strcpy(target_path, osc_data->path);
        strcat(target_path, "/quit");
        lo_send(osc_data->target, target_path, "");
    }
}
