/* JACK Backend code for Carla */

#include "carla_backend.h"
#include "audio_plugin.h"

// Global variables (shared)
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

// Global JACK client
extern jack_client_t* carla_jack_client;
extern jack_nframes_t carla_buffer_size;
extern jack_nframes_t carla_sample_rate;

// Global options
extern carla_options_t carla_options;

int carla_jack_process_callback(jack_nframes_t nframes, void* arg)
{
    if (carla_options.global_jack_client)
    {
        for (unsigned short i=0; i<MAX_PLUGINS; i++)
        {
            AudioPlugin* plugin = AudioPlugins[i];
            if (plugin && plugin->id >= 0)
            {
                carla_proc_lock();
                plugin->process(nframes);
                carla_proc_unlock();
            }
        }
    }
    else if (arg)
    {
        AudioPlugin* plugin = (AudioPlugin*)arg;
        if (plugin->id >= 0)
        {
            carla_proc_lock();
            plugin->process(nframes);
            carla_proc_unlock();
        }
    }

    return 0;
}

int carla_jack_bufsize_callback(jack_nframes_t new_buffer_size, void*)
{
    carla_buffer_size = new_buffer_size;

    for (unsigned short i=0; i<MAX_PLUGINS; i++)
    {
        AudioPlugin* plugin = AudioPlugins[i];
        if (plugin && plugin->id >= 0)
            plugin->buffer_size_changed(new_buffer_size);
    }

    return 0;
}

int carla_jack_srate_callback(jack_nframes_t new_sample_rate, void*)
{
    carla_sample_rate = new_sample_rate;
    return 0;
}

void carla_jack_shutdown_callback(void*)
{
    for (unsigned short i=0; i<MAX_PLUGINS; i++)
    {
        AudioPlugin* plugin = AudioPlugins[i];
        if (plugin && plugin->id >= 0)
            plugin->jack_client = nullptr;
    }
    carla_jack_client = nullptr;
    callback_action(CALLBACK_QUIT, 0, 0, 0, 0.0f);
}

void carla_jack_register_plugin(AudioPlugin* plugin)
{
    plugin->jack_client = jack_client_open(plugin->name, JackNullOption, 0);

    if (plugin->jack_client)
    {
        jack_set_process_callback(plugin->jack_client, carla_jack_process_callback, plugin);
        //jack_set_buffer_size_callback(plugin->jack_client, carla_jack_bufsize_callback, plugin);
        //jack_set_sample_rate_callback(plugin->jack_client, carla_jack_srate_callback, plugin);
    }
}
