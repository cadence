# QtCreator project file

CONFIG = warn_on qt release shared dll plugin

TEMPLATE = lib
VERSION = 0.3.0

SOURCES = carla_backend.cpp \
    jack.cpp osc.cpp misc.cpp \
    ladspa.cpp dssi.cpp lv2.cpp vst.cpp winvst.cpp sf2.cpp

HEADERS = carla_backend.h \
    includes.h audio_plugin.h osc.h misc.h \
    ladspa_rdf.h lv2_rdf.h vst_shared.h

TARGET = carla_backend

INCLUDEPATH = vestige

LIBS += -ljack -llo -lpthread
