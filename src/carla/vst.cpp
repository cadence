/* Code for VST plugins */

#include "carla_backend.h"
#include "vst_shared.h"

// Global variables
extern const char* unique_names[MAX_PLUGINS];
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

// Global options
extern carla_options_t carla_options;

// Global JACK client
extern jack_client_t* carla_jack_client;

// jack.cpp
void carla_jack_register_plugin(AudioPlugin* plugin);

#if 0
class VstOscGuiThread : public QThread
{
public:
    VstOscGuiThread(QObject* parent=0);
    void set_plugin_id(short plugin_id);
    void run();

private:
    short plugin_id;
};
#endif

short add_plugin_vst(const char* filename, const char* label)
{
    qDebug("add_plugin_vst(%s, %s)", filename, label);

    short id = get_new_plugin_id();

    if (id >= 0)
    {
        VstAudioPlugin* plugin = new VstAudioPlugin;

        if ((plugin->lib_open(filename)))
        {
            VST_Function vstfn = (VST_Function)plugin->lib_symbol("VSTPluginMain");
            if (!vstfn)
            {
                vstfn = (VST_Function)plugin->lib_symbol("main");
#ifdef TARGET_API_MAC_CARBON
                if (!vstfn)
                    vstfn = (VST_Function)plugin->lib_symbol("main_macho");
#endif
            }

            if (vstfn)
            {
                plugin->effect = vstfn(VstHostCallback);

                if (plugin->effect)
                {
                    plugin->filename = strdup(filename);

                    char buf_str[STR_MAX] = { 0 };
                    plugin->effect->dispatcher(plugin->effect, effGetEffectName, 0, 0, buf_str, 0.0f);

                    if (buf_str[0] != 0)
                        plugin->name = get_unique_name(buf_str);
                    else
                        plugin->name = get_unique_name(label);

                    // Init plugin
                    plugin->effect->dispatcher(plugin->effect, effOpen, 0, 0, nullptr, 0.0f);
                    plugin->effect->dispatcher(plugin->effect, effSetSampleRate, 0, 0, nullptr, get_sample_rate());
                    plugin->effect->dispatcher(plugin->effect, effSetBlockSize, 0, get_buffer_size(), nullptr, 0.0f);
                    plugin->effect->dispatcher(plugin->effect, 77 /* effSetProcessPrecision */, 0, 0 /* float 32bit */, nullptr, 0.0f);
                    plugin->effect->user = plugin;

                    intptr_t version = plugin->effect->dispatcher(plugin->effect, effGetVstVersion, 0, 0, nullptr, 0.0f);
                    if (version != 0 && version <= 2300)
                        plugin->is_oldsdk = true;

                    if (carla_options.global_jack_client)
                        plugin->jack_client = carla_jack_client;
                    else
                        carla_jack_register_plugin(plugin);

                    if (plugin->jack_client)
                    {
                        plugin->reload();

                        plugin->id = id;
                        unique_names[id] = plugin->name;
                        AudioPlugins[id] = plugin;

                        // GUI Stuff (on Linux, only show gui for new vst plugins)
#ifdef __linux__
                        if (plugin->is_oldsdk == false)
#endif
                        {
                            if (plugin->effect->flags & effFlagsHasEditor)
                            {
                                plugin->gui.type = GUI_INTERNAL_QT4;
                                plugin->hints |= PLUGIN_HAS_GUI;
                            }
                        }

                        osc_new_plugin(plugin);
                    }
                    else
                    {
                        set_last_error("Failed to register plugin in JACK");
                        delete plugin;
                        id = -1;
                    }
                }
                else
                {
                    set_last_error("Plugin failed to initialize");
                    delete plugin;
                    id = -1;
                }
            }
            else
            {
                set_last_error("Could not find the VST main entry point in the plugin library");
                delete plugin;
                id = -1;
            }
        }
        else
        {
            set_last_error(plugin->lib_error());
            delete plugin;
            id = -1;
        }
    }
    else
        set_last_error("Maximum number of plugins reached");

    return id;
}
