/* Code for VST plugins */

#ifdef BRIDGE_WINVST
#include "carla-bridge-plugin.h"
#else
#include "audio_plugin.h"
#include <QtGui/QDialog>
#endif

#include "misc.h"

#include <math.h>
#include <stdio.h>
#include <QtCore/QByteArray>

#define VST_FORCE_DEPRECATED 0
#include "aeffectx.h"

#ifndef kVstVersion
#define kVstVersion 2400
#endif

// Global variables
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];
extern ExternalMidiNote ExternalMidiNotes[MAX_MIDI_EVENTS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Shared native/wine VST AudioPlugin class
class VstAudioPlugin : public AudioPlugin
{
public:
    AEffect* effect;
    struct {
        int32_t numEvents;
        intptr_t reserved;
        VstEvent* events[MAX_MIDI_EVENTS];
    } events;
    VstMidiEvent midi_events[MAX_MIDI_EVENTS];
    bool is_oldsdk;

    VstAudioPlugin()
    {
        qDebug("VstAudioPlugin::VstAudioPlugin()");
#ifndef BRIDGE_WINVST
        type = PLUGIN_VST;
#endif

        effect = nullptr;
        events.numEvents = 0;
        events.reserved  = 0;
        is_oldsdk = false;

        for (unsigned short i=0; i<MAX_MIDI_EVENTS; i++)
        {
            events.events[i] = nullptr;
            memset(&midi_events[i], 0, sizeof(VstMidiEvent));
        }
    }

    ~VstAudioPlugin()
    {
        qDebug("VstAudioPlugin::~VstAudioPlugin()");

        if (effect)
        {
            // close UI
            if (hints & PLUGIN_HAS_GUI)
            {
#ifndef BRIDGE_WINVST
                if (gui.type == GUI_EXTERNAL_OSC)
                {
                    if (gui.visible)
                        osc_send_hide(&osc.data);

                    osc_send_quit(&osc.data);

                    if (osc.thread)
                    {
                        osc.thread->quit();

                        if (!osc.thread->wait(3000)) // 3 sec
                            qWarning("Failed to properly stop LV2 OSC GUI thread");

                        delete osc.thread;
                    }

                    osc_clear_data(&osc.data);

                }
                else
#endif
                    effect->dispatcher(effect, effEditClose, 0, 0, nullptr, 0.0f);
            }

            if (active_before)
            {
                effect->dispatcher(effect, 72 /* effStopProcess */, 0, 0, nullptr, 0.0f);
                effect->dispatcher(effect, effMainsChanged, 0, 0, nullptr, 0.0f);
            }

            effect->dispatcher(effect, effClose, 0, 0, nullptr, 0.0f);
        }
    }

    void delete_me()
    {
        qDebug("VstAudioPlugin::delete_me()");
        delete this;
    }

    int set_osc_bridge_info(PluginOscBridgeInfoType, lo_arg**)
    {
        return 1;
    }

    PluginCategory get_category()
    {
#ifndef BRIDGE_WINVST
        intptr_t Category = effect->dispatcher(effect, 35 /* effGetPlugCategory */, 0, 0, nullptr, 0.0f);

        switch(Category)
        {
        case 1 /* kPlugCategEffect */:
            return PLUGIN_CATEGORY_OUTRO;
        case 2 /* kPlugCategSynth */:
            return PLUGIN_CATEGORY_SYNTH;
        case 3 /* kPlugCategAnalysis */:
            return PLUGIN_CATEGORY_UTILITY;
        case 4 /* kPlugCategMastering */:
            return PLUGIN_CATEGORY_DYNAMICS;
        case 5 /* kPlugCategSpacializer */:
            return PLUGIN_CATEGORY_OUTRO;
        case 6 /* kPlugCategRoomFx */:
            return PLUGIN_CATEGORY_DELAY;
        case 7 /* kPlugSurroundFx */:
            return PLUGIN_CATEGORY_OUTRO;
        case 8 /* kPlugCategRestoration */:
            return PLUGIN_CATEGORY_UTILITY;
        case 11 /* kPlugCategGenerator */:
            return PLUGIN_CATEGORY_SYNTH;
        }

        if (effect->flags & effFlagsIsSynth)
            return PLUGIN_CATEGORY_SYNTH;
#endif
        return PLUGIN_CATEGORY_NONE;
    }

    void get_label(char* buf_str)
    {
        effect->dispatcher(effect, effGetProductString, 0, 0, buf_str, 0.0f);
    }

    void get_maker(char* buf_str)
    {
        effect->dispatcher(effect, effGetVendorString, 0, 0, buf_str, 0.0f);
    }

    void get_copyright(char* buf_str)
    {
        effect->dispatcher(effect, effGetVendorString, 0, 0, buf_str, 0.0f);
    }

    void get_real_name(char* buf_str)
    {
        effect->dispatcher(effect, effGetEffectName, 0, 0, buf_str, 0.0f);
    }

    long get_unique_id()
    {
        return effect->uniqueID;
    }

    void get_parameter_name(uint32_t rindex, char* buf_str)
    {
        effect->dispatcher(effect, effGetParamName, rindex, 0, buf_str, 0.0f);
    }

    void get_parameter_symbol(uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    void get_parameter_label(uint32_t rindex, char* buf_str)
    {
        effect->dispatcher(effect, 6 /* effGetParamLabel */, rindex, 0, buf_str, 0.0f);
    }

    uint32_t get_scalepoint_count(uint32_t)
    {
        return 0;
    }

    double get_scalepoint_value(uint32_t, uint32_t)
    {
        return 0.0;
    }

    void get_scalepoint_label(uint32_t, uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    int32_t get_chunk_data(void** data_ptr)
    {
        return effect->dispatcher(effect, 23 /* effGetChunk */, 1 /* Preset */, 0, data_ptr, 0.0f);
    }

    void x_set_parameter_value(uint32_t parameter_id, double value, bool)
    {
        effect->setParameter(effect, param.data[parameter_id].rindex, value);
    }

    void x_set_program(uint32_t program_id, bool, bool block)
    {
        if (block) carla_proc_lock();
        effect->dispatcher(effect, effSetProgram, 0, program_id, nullptr, 0.0f);
        if (block) carla_proc_unlock();

        // Change default value (VST version)
        for (uint32_t i=0; i < param.count; i++)
            param.buffers[i] = param.ranges[i].def = effect->getParameter(effect, param.data[i].rindex);
    }

    void x_set_midi_program(uint32_t, bool, bool)
    {
    }

    void x_set_custom_data(CustomDataType, const char*, const char*, bool)
    {
    }

    void set_chunk_data(const char* string_data)
    {
        QByteArray chunk = QByteArray::fromBase64(string_data);
        effect->dispatcher(effect, 24 /* effSetChunk */, 1 /* Preset */, chunk.size(), chunk.data(), 0.0f);
    }

    void set_gui_data(int data, void* ptr)
    {
#ifndef BRIDGE_WINVST
        if (effect->dispatcher(effect, effEditOpen, 0, data, (void*)((QDialog*)ptr)->winId(), 0.0f) == 1)
        {
#ifndef ERect
            struct ERect {
                short top;
                short left;
                short bottom;
                short right;
            };
#endif
            ERect* vst_rect;

            if (effect->dispatcher(effect, effEditGetRect, 0, 0, &vst_rect, 0.0f))
            {
                int width  = vst_rect->right  - vst_rect->left;
                int height = vst_rect->bottom - vst_rect->top;

                if (width <= 0 || height <= 0)
                {
                    qCritical("Failed to get proper Plugin Window size");
                    return;
                }

                gui.width  = width;
                gui.height = height;
            }
            else
                qCritical("Failed to get Plugin Window size");
        }
        else
        {
            // failed to open UI
            hints -= PLUGIN_HAS_GUI;
            callback_action(CALLBACK_SHOW_GUI, id, -1, 0, 0.0);

            effect->dispatcher(effect, effEditClose, 0, 0, nullptr, 0.0f);
        }
#endif
    }

    void show_gui(bool yesno)
    {
        gui.visible = yesno;

#ifndef BRIDGE_WINVST
        if (gui.visible && gui.width > 0 && gui.height > 0)
            callback_action(CALLBACK_RESIZE_GUI, id, gui.width, gui.height, 0.0);
#endif
    }

    void idle_gui()
    {
        effect->dispatcher(effect, 53 /* effIdle */, 0, 0, nullptr, 0.0f);

        if (gui.visible)
            effect->dispatcher(effect, effEditIdle, 0, 0, nullptr, 0.0f);
    }

    void reload()
    {
        qDebug("VstAudioPlugin::reload()");

        short _id = id;

        // Safely disable plugin for reload
        carla_proc_lock();
        id = -1;

#ifndef BRIDGE_WINVST
        if (carla_options.global_jack_client == false)
#endif
            jack_deactivate(jack_client);

        carla_proc_unlock();

        // Unregister jack ports
        remove_from_jack();

        // Delete old data
        delete_buffers();

        uint32_t ains, aouts, mins, mouts, params, j;
        ains = aouts = mins = mouts = params = 0;

        ains   = effect->numInputs;
        aouts  = effect->numOutputs;
        params = effect->numParams;

        if (effect->flags & effFlagsIsSynth || effect->dispatcher(effect, effCanDo, 0, 0, (void*)"receiveVstMidiEvent", 0.0f) == 1)
            mins = 1;

        if (effect->dispatcher(effect, effCanDo, 0, 0, (void*)"sendVstMidiEvent", 0.0f) == 1)
            mouts = 1;

        if (ains > 0)
        {
            ain.rindexes = new uint32_t[ains];
            ain.ports    = new jack_port_t*[ains];
        }

        if (aouts > 0)
        {
            aout.rindexes = new uint32_t[aouts];
            aout.ports    = new jack_port_t*[aouts];
        }

        if (mins > 0)
        {
            min.ports = new jack_port_t*[mins];
        }

        if (mouts > 0)
        {
            mout.ports = new jack_port_t*[mouts];
        }

        if (params > 0)
        {
            param.buffers = new float[params];
            param.data    = new ParameterData[params];
            param.ranges  = new ParameterRanges[params];
        }

        for (j=0; j<MAX_MIDI_EVENTS; j++)
            events.events[j] = (VstEvent*)&midi_events[j];

        const int port_name_size = jack_port_name_size();
        char port_name[port_name_size];
        bool needs_cin = false;

        for (j=0; j<ains; j++)
        {
            ain.rindexes[j] = j;

#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
                snprintf(port_name, port_name_size, "%s:input_%02i", name, j+1);
            else
#endif
                snprintf(port_name, port_name_size, "input_%02i", j+1);

            ain.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        }

        for (j=0; j<aouts; j++)
        {
            aout.rindexes[j] = j;
            needs_cin = true;

#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
                snprintf(port_name, port_name_size, "%s:output_%02i", name, j+1);
            else
#endif
                snprintf(port_name, port_name_size, "output_%02i", j+1);

            aout.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        }

        for (j=0; j<params; j++)
        {
            param.data[j].index  = j;
            param.data[j].rindex = j;
            param.data[j].hints  = 0;
            param.data[j].midi_channel = 0;
            param.data[j].midi_cc = -1;

            double min, max, def, step, step_small, step_large;

            VstParameterProperties prop;
            prop.flags = 0;

            if (effect->dispatcher(effect, 56 /* effGetParameterProperties */, j, 0, &prop, 0))
            {
                if (prop.flags & kVstParameterUsesIntegerMinMax)
                {
                    min = prop.minInteger;
                    max = prop.maxInteger;
                }
                else
                {
                    min = 0.0;
                    max = 1.0;
                }

                if (prop.flags & kVstParameterUsesIntStep)
                {
                    step = prop.stepInteger;
                    step_small = prop.stepInteger;
                    step_large = prop.largeStepInteger;
                }
                else if (prop.flags & kVstParameterUsesFloatStep)
                {
                    step = prop.stepFloat;
                    step_small = prop.smallStepFloat;
                    step_large = prop.largeStepFloat;
                }
                else if (prop.flags & kVstParameterIsSwitch)
                {
                    step = max - min;
                    step_small = step;
                    step_large = step;
                }
                else
                {
                    double range = max - min;
                    step = range/100.0;
                    step_small = range/1000.0;
                    step_large = range/10.0;
                }
            }
            else
            {
                min = 0.0;
                max = 1.0;
                step = 0.001;
                step_small = 0.0001;
                step_large = 0.1;
            }

            if (min > max)
                max = min;
            else if (max < min)
                min = max;

            // no such thing as VST default parameters
            def = effect->getParameter(effect, j);

            if (def < min)
                def = min;
            else if (def > max)
                def = max;

            if (max - min == 0.0)
            {
                qWarning("Broken plugin parameter -> max - min == 0");
                max = min + 0.1;
            }

            param.data[j].type = PARAMETER_INPUT;
            param.ranges[j].min = min;
            param.ranges[j].max = max;
            param.ranges[j].def = def;
            param.ranges[j].step = step;
            param.ranges[j].step_small = step_small;
            param.ranges[j].step_large = step_large;

            // Start parameters in their default values
            param.buffers[j] = def;

            // hints
            param.data[j].hints |= PARAMETER_IS_ENABLED;

            if (is_oldsdk || effect->dispatcher(effect, 26 /* effCanBeAutomated */, j, 0, nullptr, 0.0f) == 1)
                param.data[j].hints |= PARAMETER_IS_AUTOMABLE;

            needs_cin = true;
        }

        if (needs_cin)
        {
#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-in");
            }
            else
#endif
                strcpy(port_name, "control-in");

            param.port_in = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        if (mins == 1)
        {
#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":midi-in");
            }
            else
#endif
                strcpy(port_name, "midi-in");

            min.ports[0] = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        if (mouts == 1)
        {
#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":midi-out");
            }
            else
#endif
                strcpy(port_name, "midi-out");

            mout.ports[0] = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
        }

        ain.count   = ains;
        aout.count  = aouts;
        min.count   = mins;
        mout.count  = mouts;
        param.count = params;

        reload_programs(true);

        // Check if plugin is synth
        intptr_t Category = effect->dispatcher(effect, 35 /* effGetPlugCategory */, 0, 0, nullptr, 0.0f);

        if (Category == 2 /* kPlugCategSynth */ || Category == 11 /* kPlugCategGenerator */)
            hints |= PLUGIN_IS_SYNTH;

        // Check if plugin uses chunks
        if (effect->flags & 0x20 /* effFlagsProgramChunks */) // FIXME - 0x20???
            hints |= PLUGIN_USES_CHUNKS;

        // Other plugin checks
        if (aouts > 0 && (ains == aouts || ains == 1))
            hints |= PLUGIN_CAN_DRYWET;

        if (aouts > 0)
            hints |= PLUGIN_CAN_VOL;

        if (aouts >= 2 && aouts%2 == 0)
            hints |= PLUGIN_CAN_BALANCE;

#ifdef BRIDGE_WINVST
        // Update host data
        osc_send_bridge_plugin_info(hints);

        osc_send_bridge_audio_count(ain.count, aout.count, ain.count+aout.count);
        osc_send_bridge_midi_count(min.count, mout.count, min.count+mout.count);

        if (param.count < MAX_PARAMETERS)
        {
            osc_send_bridge_param_count(param.count, 0, param.count);

            char p_name[STR_MAX] = { 0 };
            char p_label[STR_MAX] = { 0 };

            for (uint32_t i=0; i < param.count; i++)
            {
                effect->dispatcher(effect, effGetParamName, i, 0, p_name, 0.0f);
                effect->dispatcher(effect, 6 /* effGetParamLabel */, i, 0, p_label, 0.0f);
                osc_send_bridge_param_info(i, p_name, p_label);
                osc_send_bridge_param_data(i, param.data[i].type, param.data[i].hints, param.data[i].index, param.data[i].rindex, param.data[i].midi_channel, param.data[i].midi_cc);
                osc_send_bridge_param_ranges(i, param.ranges[i].def, param.ranges[i].min, param.ranges[i].max, param.ranges[i].step, param.ranges[i].step_small, param.ranges[i].step_large);
                osc_send_control(i, param.buffers[i]);

                p_name[0] = 0;
                p_label[0] = 0;
            }
        }

        osc_send_bridge_program_count(prog.count);

        for (uint32_t i=0; i < prog.count; i++)
            osc_send_bridge_program_name(i, prog.names[i]);

        osc_send_bridge_update();
#endif

        carla_proc_lock();
        id = _id;
        carla_proc_unlock();

#ifndef BRIDGE_WINVST
        if (carla_options.global_jack_client == false)
#endif
            jack_activate(jack_client);
    }

    void reload_programs(bool init)
    {
        qDebug("VstAudioPlugin::reload_programs(%s)", bool2str(init));
        uint32_t i, old_count = prog.count;

        // Delete old programs
        if (prog.count > 0)
        {
            for (uint32_t i=0; i < prog.count; i++)
                free((void*)prog.names[i]);

            delete[] prog.names;
        }
        prog.count = 0;

        // Query new programs
        prog.count = effect->numPrograms;

        if (prog.count > 0)
            prog.names = new const char* [prog.count];

        // Update names
        for (i=0; i < prog.count; i++)
        {
            char buf_str[STR_MAX] = { 0 };
            if (effect->dispatcher(effect, 29 /* effGetProgramNameIndexed */, i, 0, buf_str, 0.0f) != 1)
            {
                // program will be [re-]changed later
                effect->dispatcher(effect, effSetProgram, 0, i, nullptr, 0.0f);
                effect->dispatcher(effect, effGetProgramName, 0, 0, buf_str, 0.0f);
            }
            prog.names[i] = strdup(buf_str);
        }

#ifndef BRIDGE_WINVST
        // Update OSC Names
        osc_send_set_program_count(&osc.data, id, prog.count);

        if (prog.count > 0)
        {
            for (i=0; i < prog.count; i++)
                osc_send_set_program_name(&osc.data, id, i, prog.names[i]);
        }

        callback_action(CALLBACK_RELOAD_PROGRAMS, id, 0, 0, 0.0);
#endif

        if (init)
        {
            if (prog.count > 0)
                set_program(0, false, false, false, true);
        }
        else
        {
#ifndef BRIDGE_WINVST
            callback_action(CALLBACK_UPDATE, id, 0, 0, 0.0);
#endif

            // Check if current program is invalid
            bool program_changed = false;

            if (prog.count == old_count+1)
            {
                // one program added, probably created by user
                prog.current = old_count;
                program_changed = true;
            }
            else if (prog.current >= (int32_t)prog.count)
            {
                // current program > count
                prog.current = 0;
                program_changed = true;
            }
            else if (prog.current < 0 && prog.count > 0)
            {
                // programs exist now, but not before
                prog.current = 0;
                program_changed = true;
            }
            else if (prog.current >= 0 && prog.count == 0)
            {
                // programs existed before, but not anymore
                prog.current = -1;
                program_changed = true;
            }

            if (program_changed)
                set_program(prog.current, true, true, true, true);
            else
            {
                // Program was changed during update, re-set it
                if (prog.current >= 0)
                    effect->dispatcher(effect, effSetProgram, 0, prog.current, nullptr, 0.0f);
            }
        }
    }

    void prepare_for_save()
    {
    }

    void process(jack_nframes_t nframes)
    {
        uint32_t i, k;
        unsigned short plugin_id = id;
        uint32_t midi_event_count = 0;

        double ains_peak_tmp[2]  = { 0.0 };
        double aouts_peak_tmp[2] = { 0.0 };

        jack_default_audio_sample_t* ains_buffer[ain.count];
        jack_default_audio_sample_t* aouts_buffer[aout.count];
        jack_default_audio_sample_t* mins_buffer[min.count];
        jack_default_audio_sample_t* mouts_buffer[mout.count];

        for (i=0; i < ain.count; i++)
            ains_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(ain.ports[i], nframes);

        for (i=0; i < aout.count; i++)
            aouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(aout.ports[i], nframes);

        for (i=0; i < min.count; i++)
            mins_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(min.ports[i], nframes);

        for (i=0; i < mout.count; i++)
            mouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(mout.ports[i], nframes);

        // --------------------------------------------------------------------------------------------------------
        // Input VU

        if (ain.count > 0)
        {
            short j2 = (ain.count == 1) ? 0 : 1;

            for (k=0; k<nframes; k++)
            {
                if (ains_buffer[0][k] > ains_peak_tmp[0])
                    ains_peak_tmp[0] = ains_buffer[0][k];
                if (ains_buffer[j2][k] > ains_peak_tmp[1])
                    ains_peak_tmp[1] = ains_buffer[j2][k];
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Parameters Input [Automation]

        if (param.port_in)
        {
            jack_default_audio_sample_t* pin_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_in, nframes);

            jack_midi_event_t pin_event;
            uint32_t n_pin_events = jack_midi_get_event_count(pin_buffer);

            for (i=0; i<n_pin_events; i++)
            {
                if (jack_midi_event_get(&pin_event, pin_buffer, i) != 0)
                    break;

                unsigned char channel = pin_event.buffer[0] & 0x0F;
                unsigned char mode    = pin_event.buffer[0] & 0xF0;

                // Status change
                if (mode == 0xB0)
                {
                    unsigned char status  = pin_event.buffer[1] & 0x7F;
                    unsigned char velo    = pin_event.buffer[2] & 0x7F;
                    double value, velo_per = double(velo)/127;

                    // Control GUI stuff (channel 0 only)
                    if (channel == 0)
                    {
                        if (status == 0x78)
                        {
                            // All Sound Off
                            set_active(false, false, false);
                            postpone_event(PostEventParameter, PARAMETER_ACTIVE, 0.0);
                            break;
                        }
                        else if (status == 0x09 && hints & PLUGIN_CAN_DRYWET)
                        {
                            // Dry/Wet (using '0x09', undefined)
                            set_drywet(velo_per, false, false);
                            postpone_event(PostEventParameter, PARAMETER_DRYWET, velo_per);
                        }
                        else if (status == 0x07 && hints & PLUGIN_CAN_VOL)
                        {
                            // Volume
                            value = double(velo)/100;
                            set_vol(value, false, false);
                            postpone_event(PostEventParameter, PARAMETER_VOLUME, value);
                        }
                        else if (status == 0x08 && hints & PLUGIN_CAN_BALANCE)
                        {
                            // Balance
                            double left, right;
                            value = (double(velo)-63.5)/63.5;

                            if (value < 0)
                            {
                                left  = -1.0;
                                right = (value*2)+1.0;
                            }
                            else if (value > 0)
                            {
                                left  = (value*2)-1.0;
                                right = 1.0;
                            }
                            else
                            {
                                left  = -1.0;
                                right = 1.0;
                            }

                            set_balance_left(left, false, false);
                            set_balance_right(right, false, false);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_LEFT, left);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_RIGHT, right);
                        }
                    }

                    // Control plugin parameters
                    for (k=0; k < param.count; k++)
                    {
                        if (param.data[k].type == PARAMETER_INPUT && (param.data[k].hints & PARAMETER_IS_AUTOMABLE) > 0 &&
                                param.data[k].midi_channel == channel && param.data[k].midi_cc == status)
                        {
                            value = (velo_per * (param.ranges[k].max - param.ranges[k].min)) + param.ranges[k].min;
                            set_parameter_value(k, value, false, false, false);
                            postpone_event(PostEventParameter, k, value);
                        }
                    }
                }
                // Program change
                else if (mode == 0xC0)
                {
                    unsigned char program = pin_event.buffer[1] & 0x7F;

                    if (program < prog.count)
                    {
                        set_program(program, false, false, false, false);
                        postpone_event(PostEventProgram, program, 0.0);
                    }
                }
            }
        } // End of Parameters Input

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (External)

        if (min.count > 0)
        {
            carla_midi_lock();

            for (i=0; i<MAX_MIDI_EVENTS && midi_event_count < MAX_MIDI_EVENTS; i++)
            {
                if (ExternalMidiNotes[i].valid)
                {
                    if (ExternalMidiNotes[i].plugin_id == plugin_id)
                    {
                        ExternalMidiNote* enote = &ExternalMidiNotes[i];
                        enote->valid = false;

                        VstMidiEvent* midi_event = &midi_events[midi_event_count];
                        memset(midi_event, 0, sizeof(VstMidiEvent));

                        midi_event->type = kVstMidiType;
                        midi_event->byteSize = sizeof(VstMidiEvent);
                        midi_event->midiData[0] = enote->onoff ? 0x90 : 0x80;
                        midi_event->midiData[1] = enote->note;
                        midi_event->midiData[2] = enote->velo;

                        midi_event_count += 1;
                    }
                }
                else
                    break;
            }

            carla_midi_unlock();

        } // End of MIDI Input (External)

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (JACK)

        if (min.count > 0)
        {
            jack_midi_event_t min_event;
            uint32_t n_min_events = jack_midi_get_event_count(mins_buffer[0]);

            for (k=0; k<n_min_events && midi_event_count < MAX_MIDI_EVENTS; k++)
            {
                if (jack_midi_event_get(&min_event, mins_buffer[0], k) != 0)
                    break;

                unsigned char channel = min_event.buffer[0] & 0x0F;
                unsigned char mode = min_event.buffer[0] & 0xF0;
                unsigned char note = min_event.buffer[1] & 0x7F;
                unsigned char velo = min_event.buffer[2] & 0x7F;

                // fix bad note off
                if (mode == 0x90 && velo == 0)
                {
                    mode = 0x80;
                    velo = 64;
                }

                if (mode == 0x80)
                    postpone_event(PostEventNoteOff, note, velo);
                else if (mode == 0x90)
                    postpone_event(PostEventNoteOn, note, velo);

                VstMidiEvent* midi_event = &midi_events[midi_event_count];
                memset(midi_event, 0, sizeof(VstMidiEvent));

                midi_event->type = kVstMidiType;
                midi_event->byteSize = sizeof(VstMidiEvent);
                midi_event->deltaFrames = min_event.time;
                midi_event->midiData[0] = mode+channel;
                midi_event->midiData[1] = note;
                midi_event->midiData[2] = velo;

                midi_event_count += 1;
            }
        } // End of MIDI Input (JACK)

        // VST Events
        if (midi_event_count > 0)
        {
            events.numEvents = midi_event_count;
            events.reserved = 0;
            effect->dispatcher(effect, effProcessEvents, 0, 0, &events, 0.0f);
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Plugin processing

        if (active)
        {
            if (!active_before)
            {
                effect->dispatcher(effect, effMainsChanged, 0, 1, nullptr, 0.0f);
                effect->dispatcher(effect, 71 /* effStartProcess */, 0, 0, nullptr, 0.0f);
            }

            if (effect->flags & effFlagsCanReplacing)
            {
                effect->processReplacing(effect, ains_buffer, aouts_buffer, nframes);
            }
            else
            {
                for (i=0; i < aout.count; i++)
                    memset(aouts_buffer[i], 0, sizeof(jack_default_audio_sample_t)*nframes);

                //effect->process(effect, ains_buffer, aouts_buffer, nframes);
            }
        }
        else
        {
            if (active_before)
            {
                effect->dispatcher(effect, 72 /* effStopProcess */, 0, 0, nullptr, 0.0f);
                effect->dispatcher(effect, effMainsChanged, 0, 0, nullptr, 0.0f);
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Post-processing (dry/wet, volume and balance)

        if (active)
        {
            double bal_rangeL, bal_rangeR;
            jack_default_audio_sample_t old_bal_left[nframes];

            for (i=0; i < aout.count; i++)
            {
                // Dry/Wet and Volume
                for (k=0; k<nframes; k++)
                {
                    if (hints & PLUGIN_CAN_DRYWET && x_drywet != 1.0)
                    {
                        if (aout.count == 1)
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[0][k]*(1.0-x_drywet));
                        else
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[i][k]*(1.0-x_drywet));
                    }

                    if (hints & PLUGIN_CAN_VOL)
                        aouts_buffer[i][k] *= x_vol;
                }

                // Balance
                if (hints & PLUGIN_CAN_BALANCE)
                {
                    if (i%2 == 0)
                        memcpy(&old_bal_left, aouts_buffer[i], sizeof(jack_default_audio_sample_t)*nframes);

                    bal_rangeL = (x_bal_left+1.0)/2;
                    bal_rangeR = (x_bal_right+1.0)/2;

                    for (k=0; k<nframes; k++)
                    {
                        if (i%2 == 0)
                        {
                            // left output
                            aouts_buffer[i][k]  = old_bal_left[k]*(1.0-bal_rangeL);
                            aouts_buffer[i][k] += aouts_buffer[i+1][k]*(1.0-bal_rangeR);
                        }
                        else
                        {
                            // right
                            aouts_buffer[i][k]  = aouts_buffer[i][k]*bal_rangeR;
                            aouts_buffer[i][k] += old_bal_left[k]*bal_rangeL;
                        }
                    }
                }

                // Output VU
                if (i < 2)
                {
                    for (k=0; k<nframes; k++)
                    {
                        if (aouts_buffer[i][k] > aouts_peak_tmp[i])
                            aouts_peak_tmp[i] = aouts_buffer[i][k];
                    }
                }
            }
        }
        else
        {
            // disable any output sound if not active
            for (i=0; i < aout.count; i++)
                memset(aouts_buffer[i], 0.0f, sizeof(jack_default_audio_sample_t)*nframes);

            aouts_peak_tmp[0] = 0.0;
            aouts_peak_tmp[1] = 0.0;

        } // End of Post-processing

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Output
        if (mout.count > 0)
        {
            jack_midi_clear_buffer(mouts_buffer[0]);

            // TODO - read jack ringuffer events
            //jack_midi_event_write(mout_buffer, midi_event->deltaFrames, (unsigned char*)midi_event->midiData, midi_event->byteSize);

        } // End of MIDI Output

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Peak Values

        ains_peak[(plugin_id*2)+0]  = ains_peak_tmp[0];
        ains_peak[(plugin_id*2)+1]  = ains_peak_tmp[1];
        aouts_peak[(plugin_id*2)+0] = aouts_peak_tmp[0];
        aouts_peak[(plugin_id*2)+1] = aouts_peak_tmp[1];

        active_before = active;
    }

    void buffer_size_changed(jack_nframes_t new_buffer_size)
    {
        if (active)
        {
            effect->dispatcher(effect, 72 /* effStopProcess */, 0, 0, nullptr, 0.0f);
            effect->dispatcher(effect, effMainsChanged, 0, 0, nullptr, 0.0f);
        }

        effect->dispatcher(effect, effSetBlockSize, 0, new_buffer_size, nullptr, 0.0f);

        if (active)
        {
            effect->dispatcher(effect, effMainsChanged, 0, 1, nullptr, 0.0f);
            effect->dispatcher(effect, 71 /* effStartProcess */, 0, 0, nullptr, 0.0f);
        }
    }
};


typedef AEffect* (*VST_Function)(audioMasterCallback);

#if 0
static void ppqToBarBeatTick(const double ppq, const int numerator, const int denominator, const double ticksPerBar, int& bar, int& beat, double& tick)
{
    bar = 0;
    beat = 0;
    tick = 0;

    if (numerator == 0 || denominator == 0)
       return;

    const int ppqPerBar = (numerator * 4 / denominator); // aka beats per bar
    //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator;
    const double beats  = (ppq / ppqPerBar) * numerator; // total beats, and fraction

    bar      = ((int) ppq) / ppqPerBar + 1;
    beat     = ((int) beats) + 1;
    //tick     = ((int) (fmod (beats * ppqPerBar, 1.0) * 960.0));
    tick     = ((int) ((beats / ppqPerBar) * ticksPerBar));
}
#endif

static intptr_t VstHostCallback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
{
    //qDebug("VstHostCallback() - code: %02i, index: %02i, value: " P_INTPTR ", opt: %03f", opcode, index, value, opt);

    uint32_t i;
    jack_position_t jack_pos;
    char buf_str[STR_MAX] = { 0 };

    VstAudioPlugin* plugin = (effect && effect->user) ? (VstAudioPlugin*)effect->user : nullptr;

    // protect against invalid pointer
    if (!plugin || plugin->id < 0 || plugin->id > MAX_PLUGINS || AudioPlugins[plugin->id]->id != plugin->id)
    {
        qCritical("VST Callback called with invalid user pointer");
        plugin = nullptr;
    }

    switch (opcode)
    {
    case audioMasterAutomate:
        if (plugin)
            plugin->set_parameter_value(index, opt, false, true, true);
        return 1; // FIXME?

    case audioMasterVersion:
        return kVstVersion;

    //case audioMasterCurrentId:
        // TODO
        //return 0;

    case audioMasterIdle:
        if (effect)
            effect->dispatcher(effect, effEditIdle, 0, 0, nullptr, 0.0f);
        return 1; // FIXME?

    //case audioMasterPinConnected:
        // Deprecated in VST SDK 2.4r2
        //return 0;

    case audioMasterWantMidi:
        // Deprecated in VST SDK 2.4
#if 0
        if (plugin && plugin->jack_client && plugin->min.count == 0)
        {
            i = plugin->id;
            bool unlock = carla_proc_trylock();
            plugin->id = -1;
            if (unlock) carla_proc_unlock();

            const int port_name_size = jack_port_name_size();
            char port_name[port_name_size];

#ifndef BRIDGE_WINVST
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, plugin->name, (port_name_size/2)-2);
                strcat(port_name, ":midi-in");
            }
            else
#endif
                strcpy(port_name, "midi-in");

            plugin->min.count    = 1;
            plugin->min.ports    = new jack_port_t*[1];
            plugin->min.ports[0] = jack_port_register(plugin->jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);

            plugin->id = i;
#ifndef BRIDGE_WINVST
            callback_action(CALLBACK_RELOAD_INFO, plugin->id, 0, 0, 0.0f);
#endif
        }
#endif
        return 1;

    case audioMasterGetTime:
        if (plugin && plugin->jack_client)
        {
            static VstTimeInfo timeInfo;
            memset(&timeInfo, 0, sizeof(VstTimeInfo));

            static jack_transport_state_t jack_state;
            jack_state = jack_transport_query(plugin->jack_client, &jack_pos);

            if (jack_state == JackTransportRolling)
                timeInfo.flags |= kVstTransportChanged|kVstTransportPlaying;
            else
                timeInfo.flags |= kVstTransportChanged;

            if (jack_pos.unique_1 == jack_pos.unique_2)
            {
                timeInfo.samplePos  = jack_pos.frame;
                timeInfo.sampleRate = jack_pos.frame_rate;

                if (jack_pos.valid & JackPositionBBT)
                {
                    // Tempo
                    timeInfo.tempo = jack_pos.beats_per_minute;
                    timeInfo.timeSigNumerator = jack_pos.beats_per_bar;
                    timeInfo.timeSigDenominator = jack_pos.beat_type;
                    timeInfo.flags |= kVstTempoValid|kVstTimeSigValid;

                    // Position
                    double dPos = timeInfo.samplePos / timeInfo.sampleRate;
                    timeInfo.nanoSeconds = dPos * 1000.0;
                    timeInfo.flags |= kVstNanosValid;

                    // Position
                    timeInfo.barStartPos = 0;
                    timeInfo.ppqPos = dPos * timeInfo.tempo / 60.0;
                    timeInfo.flags |= kVstBarsValid|kVstPpqPosValid;
                }
            }

            return (intptr_t)&timeInfo;
        }
        return 0;

    case audioMasterProcessEvents:
#if 0
        if (plugin && plugin->mout.count > 0 && ptr)
        {
            VstEvents* events = (VstEvents*)ptr;

            jack_default_audio_sample_t* mout_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(plugin->mout.ports[0], get_buffer_size());
            jack_midi_clear_buffer(mout_buffer);

            for (int32_t i=0; i < events->numEvents; i++)
            {
                VstMidiEvent* midi_event = (VstMidiEvent*)events->events[i];

                if (midi_event && midi_event->type == kVstMidiType)
                {
                    // TODO - send event over jack rinbuffer
                }
            }
            return 1;
        }
        else
            qDebug("Some MIDI Out events were ignored");
#endif

        return 0;

    //case audioMasterSetTime:
        // Deprecated in VST SDK 2.4
        //int bar, beat, tick;
        //VstTimeInfo* timeInfo = (VstTimeInfo*)ptr;
        //ppqToBarBeatTick(timeInfo->ppqPos, timeInfo->timeSigNumerator, timeInfo->timeSigDenominator, timeInfo->tempo, &bar, &beat, &tick);
        //return 0;

    case audioMasterTempoAt:
        // Deprecated in VST SDK 2.4
        if (plugin && plugin->jack_client)
        {
            jack_transport_query(plugin->jack_client, &jack_pos);

            if (jack_pos.valid & JackPositionBBT)
                return jack_pos.beats_per_minute;
        }
        return 120.0;

    case audioMasterGetNumAutomatableParameters:
        // Deprecated in VST SDK 2.4
        return MAX_PARAMETERS; // FIXME - what exacly is this for?

    //case audioMasterGetParameterQuantization:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    case audioMasterIOChanged:
        if (plugin && plugin->jack_client)
        {
            i = plugin->id;
            bool unlock = carla_proc_trylock();
            plugin->id = -1;
            if (unlock) carla_proc_unlock();

            if (plugin->active)
            {
                plugin->effect->dispatcher(plugin->effect, 72 /* effStopProcess */, 0, 0, nullptr, 0.0f);
                plugin->effect->dispatcher(plugin->effect, effMainsChanged, 0, 0, nullptr, 0.0f);
            }

            plugin->reload();

            if (plugin->active)
            {
                plugin->effect->dispatcher(plugin->effect, effMainsChanged, 0, 1, nullptr, 0.0f);
                plugin->effect->dispatcher(plugin->effect, 71 /* effStartProcess */, 0, 0, nullptr, 0.0f);
            }

            plugin->id = i;
#ifndef BRIDGE_WINVST
            callback_action(CALLBACK_RELOAD_ALL, plugin->id, 0, 0, 0.0);
#endif
        }
        return 1;

    case audioMasterNeedIdle:
        // Deprecated in VST SDK 2.4
        effect->dispatcher(effect, 53 /* effIdle */, 0, 0, nullptr, 0.0f);
        return 1;

    case audioMasterSizeWindow:
        if (plugin)
        {
#ifdef BRIDGE_WINVST
            plugin->queque_message(BRIDGE_MESSAGE_RESIZE_GUI, index, value, 0.0);
#else
            plugin->gui.width  = index;
            plugin->gui.height = value;
            callback_action(CALLBACK_RESIZE_GUI, plugin->id, index, value, 0.0);
#endif
        }
        return 1;

    case audioMasterGetSampleRate:
#ifdef BRIDGE_WINVST
        if (plugin && plugin->jack_client)
            return jack_get_sample_rate(plugin->jack_client);
        else
            return 44100;
#else
        return get_sample_rate();
#endif

    case audioMasterGetBlockSize:
#ifdef BRIDGE_WINVST
        if (plugin && plugin->jack_client)
            return jack_get_buffer_size(plugin->jack_client);
        else
            return 512;
#else
        return get_buffer_size();
#endif

    //case audioMasterGetInputLatency:
        //return 0;

    //case audioMasterGetOutputLatency:
        //return 0;

    //case audioMasterGetPreviousPlug:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    //case audioMasterGetNextPlug:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    case audioMasterWillReplaceOrAccumulate:
        // Deprecated in VST SDK 2.4
        return 1; // replace, FIXME

    case audioMasterGetCurrentProcessLevel:
#if 0
        if (pthread_self() == user_thread) {
            return 1 /* kVstProcessLevelUser */;
        } else if (pthread_self() == proc_thread) {
            return 2 /* kVstProcessLevelRealtime */;
        } else {
            // TODO - return 4 if offline
            printf("VstHostCallback() - audioMasterGetCurrentProcessLevel :: Don't know which level we are...\n");
            return 0 /* kVstProcessLevelUnknown */;
        }
#else
        return 0 /* kVstProcessLevelUnknown */;
#endif

    case audioMasterGetAutomationState:
        return 2 /* kVstAutomationRead */; // FIXME

        //audioMasterOfflineStart
        //audioMasterOfflineRead
        //audioMasterOfflineWrite
        //audioMasterOfflineGetCurrentPass
        //audioMasterOfflineGetCurrentMetaPass

    case audioMasterSetOutputSampleRate:
        // Deprecated in VST SDK 2.4
        // Not possible in JACK
        return 0;

    //case 69 /* audioMasterGet[Output]SpeakerArrangement */:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    case audioMasterGetVendorString:
        if (ptr) strcpy((char*)ptr, "falkTX");
        return 1;

    case audioMasterGetProductString:
        if (ptr) strcpy((char*)ptr, "Carla");
        return 1;

    case audioMasterGetVendorVersion:
        return 0x0300; // 0.3.0

    case audioMasterVendorSpecific:
        // ignored
        return 0;

    //case audioMasterSetIcon:
        // Deprecated in VST SDK 2.4
        // Err, never used in VST
        //return 0;

    case audioMasterCanDo:
        if (strcmp((char*)ptr, "sendVstEvents") == 0)
            return 1;
        else if (strcmp((char*)ptr, "sendVstMidiEvent") == 0)
            return 1;
        else if (strcmp((char*)ptr, "sendVstTimeInfo") == 0)
            return 1;
        else if (strcmp((char*)ptr, "receiveVstEvents") == 0)
            return 1;
        else if (strcmp((char*)ptr, "receiveVstMidiEvent") == 0)
            return 1;
        else if (strcmp((char*)ptr, "receiveVstTimeInfo") == 0)
            // Deprecated in VST SDK 2.4
            return -1;
        else if (strcmp((char*)ptr, "sizeWindow") == 0)
            return 1;
        else if (strcmp((char*)ptr, "acceptIOChanges") == 0)
            return 1;
        else
        {
            // "reportConnectionChanges"
            // "offline"
            // "openFileSelector"
            // "closeFileSelector"
            // "startStopProcess"
            // "shellCategory"
            // "sendVstMidiEventFlagIsRealtime"
            qDebug("VstHostCallback() - audioMasterCanDo '%s'\n", (char*)ptr);
            return 0;
        }

    case audioMasterGetLanguage:
        return kVstLangEnglish;

    //case audioMasterOpenWindow:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    //case audioMasterCloseWindow:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

        //audioMasterGetDirectory

    case audioMasterUpdateDisplay:
        if (plugin)
        {
            // Update current program name
            if (plugin->prog.current >= 0 && plugin->prog.count > 0)
            {
                if (plugin->effect->dispatcher(plugin->effect, 29 /* effGetProgramNameIndexed */, plugin->prog.current, 0, buf_str, 0.0f) != 1)
                    plugin->effect->dispatcher(plugin->effect, effGetProgramName, 0, 0, buf_str, 0.0f);

                if (plugin->prog.names[plugin->prog.current])
                    free((void*)plugin->prog.names[plugin->prog.current]);

                plugin->prog.names[plugin->prog.current] = strdup(buf_str);
            }
#ifndef BRIDGE_WINVST
            callback_action(CALLBACK_UPDATE, plugin->id, 0, 0, 0.0);
#endif
        }
        return 1;

        //audioMasterBeginEdit
        //audioMasterEndEdit
        //audioMasterOpenFileSelector
        //audioMasterCloseFileSelector

    //case audioMasterEditFile:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    //case audioMasterGetChunkFile:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    //case audioMasterGetInputSpeakerArrangement:
        // Deprecated in VST SDK 2.4
        // TODO
        //return 0;

    default:
        qDebug("VstHostCallback() - code: %02i, index: %02i, value: " P_INTPTR ", opt: %03f", opcode, index, value, opt);
        return 0;
    }

    return 0;
}
