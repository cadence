/* JACK Backend code for Carla */

#ifndef CARLA_AUDIO_PLUGIN
#define CARLA_AUDIO_PLUGIN

#include "carla_backend.h"
#include "osc.h"

#include <stdlib.h>

#include <jack/jack.h>
#include <jack/midiport.h>

#include <QtCore/QList>
#include <QtCore/QMutex>
#include <QtCore/QThread>

#define CARLA_PROCESS_CONTINUE_CHECK if (id != plugin_id) { callback_action(CALLBACK_DEBUG, plugin_id, id, 0, 0.0f); return; }

// Global OSC stuff
extern OscData global_osc_data;

// Global options
extern carla_options_t carla_options;

const unsigned short MAX_POSTEVENTS = 128;

enum PluginPostEventType {
    PostEventDebug,
    PostEventParameter,
    PostEventProgram,
    PostEventMidiProgram,
    PostEventNoteOn,
    PostEventNoteOff
};

enum PluginOscBridgeInfoType {
    OscBridgeAudioCountInfo = 1,
    OscBridgeMidiCountInfo,
    OscBridgeParameterCountInfo,
    OscBridgeProgramCountInfo,
    OscBridgeMidiProgramCountInfo,
    OscBridgePluginInfo,
    OscBridgeParameterInfo,
    OscBridgeParameterDataInfo,
    OscBridgeParameterRangesInfo,
    OscBridgeProgramName,
    OscBridgeUpdateNow
};

struct midi_program_t {
    uint32_t bank;
    uint32_t program;
};

struct PluginAudioData {
    uint32_t count;
    uint32_t* rindexes;
    jack_port_t** ports;
};

struct PluginMidiData {
    uint32_t count;
    jack_port_t** ports;
};

struct PluginParameterData {
    uint32_t count;
    float* buffers;
    ParameterData* data;
    ParameterRanges* ranges;
    jack_port_t* port_in;
    jack_port_t* port_out;
};

struct PluginProgramData {
    uint32_t count;
    int32_t current;
    const char** names;
};

struct PluginMidiProgramData {
    uint32_t count;
    int32_t current;
    midi_program_t* data;
    const char** names;
};

struct PluginPostEvent {
    bool valid;
    PluginPostEventType type;
    int32_t index;
    double value;
};

struct ExternalMidiNote {
    bool valid;
    short plugin_id;
    bool onoff;
    uint8_t note;
    uint8_t velo;
};

class AudioPlugin
{
public:
    PluginType type;
    short id;
    int32_t hints;

    bool active;
    bool active_before;

    void* lib;
    const char* name;
    const char* filename;

    // Storage Data (Audio, MIDI, Parameters and Programs)
    PluginAudioData ain;
    PluginAudioData aout;
    PluginMidiData min;
    PluginMidiData mout;
    PluginParameterData param;
    PluginProgramData prog;
    PluginMidiProgramData midiprog;

    QList<CustomData> custom;
    GuiData gui;

    struct {
        OscData data;
        QThread* thread;
    } osc;

    struct {
        QMutex lock;
        PluginPostEvent events[MAX_POSTEVENTS];
    } post_events;

    double x_drywet, x_vol, x_bal_left, x_bal_right;

    jack_client_t* jack_client;

    AudioPlugin()
    {
        qDebug("AudioPlugin::AudioPlugin()");

        type = PLUGIN_NONE;
        id = -1;
        hints = 0;

        active = false;
        active_before = false;

        lib = nullptr;
        name = nullptr;
        filename = nullptr;

        ain.count = 0;
        ain.ports = nullptr;
        ain.rindexes = nullptr;

        aout.count = 0;
        aout.ports = nullptr;
        aout.rindexes = nullptr;

        min.count = 0;
        min.ports = nullptr;

        mout.count = 0;
        mout.ports = nullptr;

        param.count    = 0;
        param.buffers  = nullptr;
        param.data     = nullptr;
        param.ranges   = nullptr;
        param.port_in  = nullptr;
        param.port_out = nullptr;

        prog.count   = 0;
        prog.current = -1;
        prog.names   = nullptr;

        midiprog.count   = 0;
        midiprog.current = -1;
        midiprog.data    = nullptr;
        midiprog.names   = nullptr;

        custom.clear();

        gui.type = GUI_NONE;
        gui.visible = false;
        gui.resizable = false;
        gui.width = 0;
        gui.height = 0;
        gui.name = nullptr;
        gui.show_now = false;

        osc.data.path = nullptr;
        osc.data.source = nullptr;
        osc.data.target = nullptr;
        osc.thread = nullptr;

        for (unsigned short i=0; i < MAX_POSTEVENTS; i++)
            post_events.events[i].valid = false;

        x_drywet = 1.0;
        x_vol    = 1.0;
        x_bal_left = -1.0;
        x_bal_right = 1.0;

        jack_client = nullptr;
    }

    ~AudioPlugin()
    {
        qDebug("AudioPlugin::~AudioPlugin()");

        // Unregister jack ports
        remove_from_jack();

        // Delete data
        delete_buffers();

        lib_close();

        if (name)
            free((void*)name);

        if (filename)
            free((void*)filename);

        if (gui.name)
            free((void*)gui.name);

        if (prog.count > 0)
        {
            for (uint32_t i=0; i < prog.count; i++)
                free((void*)prog.names[i]);

            delete[] prog.names;
        }

        if (midiprog.count > 0)
        {
            for (uint32_t i=0; i < prog.count; i++)
                free((void*)midiprog.names[i]);

            delete[] midiprog.data;
            delete[] midiprog.names;
        }

        if (custom.count() > 0)
        {
            for (int i=0; i < custom.count(); i++)
            {
                if (custom[i].key)
                    free((void*)custom[i].key);

                if (custom[i].value)
                    free((void*)custom[i].value);
            }

            custom.clear();
        }

        if (carla_options.global_jack_client == false && jack_client)
            jack_client_close(jack_client);
    }

    void remove_from_jack()
    {
        qDebug("AudioPlugin::remove_from_jack()");

        if (!jack_client)
            return;

        uint32_t i;

        for (i=0; i < ain.count; i++)
            jack_port_unregister(jack_client, ain.ports[i]);

        for (i=0; i < aout.count; i++)
            jack_port_unregister(jack_client, aout.ports[i]);

        for (i=0; i < min.count; i++)
            jack_port_unregister(jack_client, min.ports[i]);

        for (i=0; i < mout.count; i++)
            jack_port_unregister(jack_client, mout.ports[i]);

        if (param.port_in)
            jack_port_unregister(jack_client, param.port_in);

        if (param.port_out)
            jack_port_unregister(jack_client, param.port_out);

        qDebug("AudioPlugin::remove_from_jack() - end");
    }

    void delete_buffers()
    {
        qDebug("AudioPlugin::delete_buffers()");

        if (ain.count > 0)
        {
            delete[] ain.ports;
            delete[] ain.rindexes;
        }

        if (aout.count > 0)
        {
            delete[] aout.ports;
            delete[] aout.rindexes;
        }

        if (min.count > 0)
        {
            delete[] min.ports;
        }

        if (mout.count > 0)
        {
            delete[] mout.ports;
        }

        if (param.count > 0)
        {
            delete[] param.buffers;
            delete[] param.data;
            delete[] param.ranges;
        }

        ain.count = 0;
        ain.ports = nullptr;
        ain.rindexes = nullptr;

        aout.count = 0;
        aout.ports = nullptr;
        aout.rindexes = nullptr;

        min.count = 0;
        min.ports = nullptr;

        mout.count = 0;
        mout.ports = nullptr;

        param.count    = 0;
        param.buffers  = nullptr;
        param.data     = nullptr;
        param.ranges   = nullptr;
        param.port_in  = nullptr;
        param.port_out = nullptr;

        qDebug("AudioPlugin::delete_buffers() - end");
    }

    void fix_parameter_value(double& value, const ParameterRanges& ranges)
    {
        if (value < ranges.min)
            value = ranges.min;
        else if (value > ranges.max)
            value = ranges.max;
    }

    void post_events_lock()
    {
        post_events.lock.lock();
    }

    void post_events_unlock()
    {
        post_events.lock.unlock();
    }

    void postpone_event(PluginPostEventType etype, int32_t index, double value)
    {
        post_events_lock();
        for (unsigned short i=0; i<MAX_POSTEVENTS; i++)
        {
            if (!post_events.events[i].valid)
            {
                post_events.events[i].valid = true;
                post_events.events[i].type  = etype;
                post_events.events[i].index = index;
                post_events.events[i].value = value;
                break;
            }
        }
        post_events_unlock();
    }

    void set_active(bool active_, bool osc_send, bool callback_send)
    {
        active = active_;
        double value = active ? 1.0 : 0.0;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, PARAMETER_ACTIVE, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, PARAMETER_ACTIVE, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, PARAMETER_ACTIVE, 0, value);
    }

    void set_drywet(double value, bool osc_send, bool callback_send)
    {
        if (value < 0.0)
            value = 0.0;
        else if (value > 1.0)
            value = 1.0;

        x_drywet = value;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, PARAMETER_DRYWET, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, PARAMETER_DRYWET, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, PARAMETER_DRYWET, 0, value);
    }

    void set_vol(double value, bool osc_send, bool callback_send)
    {
        if (value < 0.0)
            value = 0.0;
        else if (value > 1.27)
            value = 1.27;

        x_vol = value;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, PARAMETER_VOLUME, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, PARAMETER_VOLUME, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, PARAMETER_VOLUME, 0, value);
    }

    void set_balance_left(double value, bool osc_send, bool callback_send)
    {
        if (value < -1.0)
            value = -1.0;
        else if (value > 1.0)
            value = 1.0;

        x_bal_left = value;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, PARAMETER_BALANCE_LEFT, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, PARAMETER_BALANCE_LEFT, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, PARAMETER_BALANCE_LEFT, 0, value);
    }

    void set_balance_right(double value, bool osc_send, bool callback_send)
    {
        if (value < -1.0)
            value = -1.0;
        else if (value > 1.0)
            value = 1.0;

        x_bal_right = value;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, PARAMETER_BALANCE_RIGHT, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, PARAMETER_BALANCE_RIGHT, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, PARAMETER_BALANCE_RIGHT, 0, value);
    }

    void set_parameter_value(uint32_t parameter_id, double value, bool gui_send, bool osc_send, bool callback_send)
    {
        fix_parameter_value(value, param.ranges[parameter_id]);

        param.buffers[parameter_id] = value;

        if (osc_send)
        {
            osc_send_set_parameter_value(&global_osc_data, id, parameter_id, value);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_control(&osc.data, parameter_id, value);
        }

        if (callback_send)
            callback_action(CALLBACK_PARAMETER_CHANGED, id, parameter_id, 0, value);

        x_set_parameter_value(parameter_id, value, gui_send);
    }

    void set_program(uint32_t program_id, bool gui_send, bool osc_send, bool callback_send, bool block)
    {
        x_set_program(program_id, gui_send, block);

        prog.current = program_id;

        // Change default value
        for (uint32_t i=0; i < param.count; i++)
        {
            param.ranges[i].def = param.buffers[i];

            if (osc_send)
                osc_send_set_default_value(&global_osc_data, id, i, param.ranges[i].def);
        }

        if (osc_send)
        {
            osc_send_set_program(&global_osc_data, id, program_id);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_program(&osc.data, program_id);
        }

        if (callback_send)
            callback_action(CALLBACK_PROGRAM_CHANGED, id, program_id, 0, 0.0f);
    }

    void set_midi_program(uint32_t midi_program_id, bool gui_send, bool osc_send, bool callback_send, bool block=true)
    {
        x_set_midi_program(midi_program_id, gui_send, block);

        midiprog.current = midi_program_id;

        // Change default value
        for (uint32_t i=0; i < param.count; i++)
        {
            param.ranges[i].def = param.buffers[i];

            if (osc_send)
                osc_send_set_default_value(&global_osc_data, id, i, param.ranges[i].def);
        }

        if (osc_send)
        {
            osc_send_set_midi_program(&global_osc_data, id, midi_program_id);

            if (hints & PLUGIN_IS_BRIDGE)
                osc_send_program(&osc.data, midi_program_id);
        }

        if (callback_send)
            callback_action(CALLBACK_MIDI_PROGRAM_CHANGED, id, midi_program_id, 0, 0.0f);
    }

    void set_custom_data(CustomDataType dtype, const char* key, const char* value, bool gui_send)
    {
        bool save_data = true;
        bool already_have = false;

        switch (dtype)
        {
        case CUSTOM_DATA_INVALID:
            save_data = false;
            break;
        case CUSTOM_DATA_STRING:
            // ignore some calf keys, sent ~10x sec
            if (QString(key).startsWith("OSC:", Qt::CaseSensitive))
                save_data = false;
            break;
        default:
            break;
        }

        if (save_data)
        {
            // Check if we already have this key
            for (int i=0; i < custom.count(); i++)
            {
                if (strcmp(custom[i].key, key) == 0)
                {
                    free((void*)custom[i].value);
                    custom[i].value = strdup(value);
                    already_have = true;
                    break;
                }
            }

            if (!already_have)
            {
                CustomData new_data;
                new_data.type  = dtype;
                new_data.key   = strdup(key);
                new_data.value = strdup(value);
                custom.append(new_data);
            }
        }

        x_set_custom_data(dtype, key, value, gui_send);
    }

    virtual void delete_me() = 0;
    virtual int set_osc_bridge_info(PluginOscBridgeInfoType, lo_arg**) = 0;

    virtual PluginCategory get_category() = 0;
    virtual void get_label(char* buf_str) = 0;
    virtual void get_maker(char* buf_str) = 0;
    virtual void get_copyright(char* buf_str) = 0;
    virtual void get_real_name(char* buf_str) = 0;
    virtual long get_unique_id() = 0;

    virtual void get_parameter_name(uint32_t rindex, char* buf_str) = 0;
    virtual void get_parameter_symbol(uint32_t rindex, char* buf_str) = 0;
    virtual void get_parameter_label(uint32_t rindex, char* buf_str) = 0;

    virtual uint32_t get_scalepoint_count(uint32_t rindex) = 0;
    virtual double get_scalepoint_value(uint32_t rindex, uint32_t scalepoint_id) = 0;
    virtual void get_scalepoint_label(uint32_t rindex, uint32_t scalepoint_id, char* buf_str) = 0;

    virtual int32_t get_chunk_data(void** data_ptr) = 0;

    virtual void x_set_parameter_value(uint32_t parameter_id, double value, bool gui_send) = 0;
    virtual void x_set_program(uint32_t program_id, bool gui_send, bool block) = 0;
    virtual void x_set_midi_program(uint32_t midi_program_id, bool gui_send, bool block) = 0;
    virtual void x_set_custom_data(CustomDataType dtype, const char* key, const char* value, bool gui_send) = 0;

    virtual void set_chunk_data(const char* string_data) = 0;
    virtual void set_gui_data(int data, void* ptr) = 0;

    virtual void show_gui(bool yesno) = 0;
    virtual void idle_gui() = 0;

    virtual void reload() = 0;
    virtual void reload_programs(bool init) = 0;
    virtual void prepare_for_save() = 0;

    virtual void process(jack_nframes_t nframes) = 0;
    virtual void buffer_size_changed(jack_nframes_t new_buffer_size) = 0;

    bool lib_open(const char* filename)
    {
#ifdef __WINDOWS__
        lib = LoadLibraryA(filename);
#else
        lib = dlopen(filename, RTLD_NOW);
#endif
        return bool(lib);
    }

    bool lib_close()
    {
        if (lib)
#ifdef __WINDOWS__
            return FreeLibrary((HMODULE)lib) != 0;
#else
            return dlclose(lib) != 0;
#endif
        else
            return false;
    }

    void* lib_symbol(const char* symbol)
    {
        if (lib)
#ifdef __WINDOWS__
            return (void*)GetProcAddress((HMODULE)lib, symbol);
#else
            return dlsym(lib, symbol);
#endif
        else
            return nullptr;
    }

    const char* lib_error()
    {
#ifdef __WINDOWS__
        return "Unknown error";
#else
        return dlerror();
#endif
    }
};

#endif // CARLA_AUDIO_PLUGIN
