/* Code for LADSPA plugins */

#include "carla_backend.h"
#include "audio_plugin.h"
#include "misc.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "ladspa/ladspa.h"
#include "ladspa_rdf.h"

// Global variables
extern const char* unique_names[MAX_PLUGINS];
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Global JACK client
extern jack_client_t* carla_jack_client;

// Global options
extern carla_options_t carla_options;

// jack.cpp
void carla_jack_register_plugin(AudioPlugin* plugin);

class LadspaAudioPlugin : public AudioPlugin
{
public:
    LADSPA_Handle handle;
    const LADSPA_Descriptor* descriptor;
    const LADSPA_RDF_Descriptor* rdf_descriptor;

    LadspaAudioPlugin()
    {
        qDebug("LadspaAudioPlugin::LadspaAudioPlugin()");
        type = PLUGIN_LADSPA;

        handle = nullptr;
        descriptor = nullptr;
        rdf_descriptor = nullptr;
    }

    ~LadspaAudioPlugin()
    {
        qDebug("LadspaAudioPlugin::~LadspaAudioPlugin()");

        if (handle && descriptor->deactivate && active_before)
            descriptor->deactivate(handle);

        if (handle && descriptor->cleanup)
            descriptor->cleanup(handle);

        if (rdf_descriptor)
            ladspa_rdf_free(rdf_descriptor);
    }

    void delete_me()
    {
        qDebug("LadspaAudioPlugin::delete_me()");
        delete this;
    }

    int set_osc_bridge_info(PluginOscBridgeInfoType, lo_arg**)
    {
        return 1;
    }

    PluginCategory get_category()
    {
        if (rdf_descriptor)
        {
            LADSPA_Properties Category = rdf_descriptor->Type;

            // Specific Types
            if (Category & (LADSPA_CLASS_DELAY|LADSPA_CLASS_REVERB))
                return PLUGIN_CATEGORY_DELAY;
            else if (Category & (LADSPA_CLASS_PHASER|LADSPA_CLASS_FLANGER|LADSPA_CLASS_CHORUS))
                return PLUGIN_CATEGORY_MODULATOR;
            else if (Category & (LADSPA_CLASS_AMPLIFIER))
                return PLUGIN_CATEGORY_DYNAMICS;
            else if (Category & (LADSPA_CLASS_UTILITY|LADSPA_CLASS_SPECTRAL|LADSPA_CLASS_FREQUENCY_METER))
                return PLUGIN_CATEGORY_UTILITY;

            // Pre-set LADSPA Types
            else if (LADSPA_IS_PLUGIN_DYNAMICS(Category))
                return PLUGIN_CATEGORY_DYNAMICS;
            else if (LADSPA_IS_PLUGIN_AMPLITUDE(Category))
                return PLUGIN_CATEGORY_MODULATOR;
            else if (LADSPA_IS_PLUGIN_EQ(Category))
                return PLUGIN_CATEGORY_EQ;
            else if (LADSPA_IS_PLUGIN_FILTER(Category))
                return PLUGIN_CATEGORY_FILTER;
            else if (LADSPA_IS_PLUGIN_FREQUENCY(Category))
                return PLUGIN_CATEGORY_UTILITY;
            else if (LADSPA_IS_PLUGIN_SIMULATOR(Category))
                return PLUGIN_CATEGORY_OUTRO;
            else if (LADSPA_IS_PLUGIN_TIME(Category))
                return PLUGIN_CATEGORY_DELAY;
            else if (LADSPA_IS_PLUGIN_GENERATOR(Category))
                return PLUGIN_CATEGORY_SYNTH;
        }

        return PLUGIN_CATEGORY_NONE;
    }

    void get_label(char* buf_str)
    {
        strncpy(buf_str, descriptor->Label, STR_MAX);
    }

    void get_maker(char* buf_str)
    {
        strncpy(buf_str, descriptor->Maker, STR_MAX);
    }

    void get_copyright(char* buf_str)
    {
        strncpy(buf_str, descriptor->Copyright, STR_MAX);
    }

    void get_real_name(char* buf_str)
    {
        if (rdf_descriptor && LADSPA_PLUGIN_HAS_TITLE(rdf_descriptor->Hints))
            strncpy(buf_str, rdf_descriptor->Title, STR_MAX);
        else
            strncpy(buf_str, descriptor->Name, STR_MAX);
    }

    long get_unique_id()
    {
        return descriptor->UniqueID;
    }

    void get_parameter_name(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, descriptor->PortNames[rindex], STR_MAX);
    }

    void get_parameter_symbol(uint32_t rindex, char* buf_str)
    {
        bool HasPortRDF = (rdf_descriptor && rindex < rdf_descriptor->PortCount);
        if (HasPortRDF)
        {
            LADSPA_RDF_Port Port = rdf_descriptor->Ports[rindex];
            if (LADSPA_PORT_HAS_LABEL(Port.Hints))
            {
                strncpy(buf_str, Port.Label, STR_MAX);
                return;
            }
        }
        *buf_str = 0;
    }

    void get_parameter_label(uint32_t rindex, char* buf_str)
    {
        bool HasPortRDF = (rdf_descriptor && rindex < rdf_descriptor->PortCount);
        if (HasPortRDF)
        {
            LADSPA_RDF_Port Port = rdf_descriptor->Ports[rindex];
            if (LADSPA_PORT_HAS_UNIT(Port.Hints))
            {
                switch (Port.Unit) {
                case LADSPA_UNIT_DB:
                    strncpy(buf_str, "dB", STR_MAX);
                    return;
                case LADSPA_UNIT_COEF:
                    strncpy(buf_str, "(coef)", STR_MAX);
                    return;
                case LADSPA_UNIT_HZ:
                    strncpy(buf_str, "Hz", STR_MAX);
                    return;
                case LADSPA_UNIT_S:
                    strncpy(buf_str, "s", STR_MAX);
                    return;
                case LADSPA_UNIT_MS:
                    strncpy(buf_str, "ms", STR_MAX);
                    return;
                case LADSPA_UNIT_MIN:
                    strncpy(buf_str, "min", STR_MAX);
                    return;
                }
            }
        }
        *buf_str = 0;
    }

    uint32_t get_scalepoint_count(uint32_t rindex)
    {
        bool HasPortRDF = (rdf_descriptor && rindex < rdf_descriptor->PortCount);
        if (HasPortRDF)
            return rdf_descriptor->Ports[rindex].ScalePointCount;
        else
            return 0;
    }

    double get_scalepoint_value(uint32_t rindex, uint32_t scalepoint_id)
    {
        bool HasPortRDF = (rdf_descriptor && rindex < rdf_descriptor->PortCount);
        if (HasPortRDF)
            return rdf_descriptor->Ports[rindex].ScalePoints[scalepoint_id].Value;
        else
            return 0.0;
    }

    void get_scalepoint_label(uint32_t rindex, uint32_t scalepoint_id, char* buf_str)
    {
        bool HasPortRDF = (rdf_descriptor && rindex < rdf_descriptor->PortCount);
        if (HasPortRDF)
            strncpy(buf_str, rdf_descriptor->Ports[rindex].ScalePoints[scalepoint_id].Label, STR_MAX);
        else
            *buf_str = 0;
    }

    int32_t get_chunk_data(void**)
    {
        return 0;
    }

    void x_set_parameter_value(uint32_t, double, bool)
    {
    }

    void x_set_program(uint32_t, bool, bool)
    {
    }

    void x_set_midi_program(uint32_t, bool, bool)
    {
    }

    void x_set_custom_data(CustomDataType, const char*, const char*, bool)
    {
    }

    void set_chunk_data(const char*)
    {
    }

    void set_gui_data(int, void*)
    {
    }

    void show_gui(bool)
    {
    }

    void idle_gui()
    {
    }

    void reload()
    {
        qDebug("LadspaAudioPlugin::reload()");
        short _id = id;

        // Safely disable plugin for reload
        carla_proc_lock();

        id = -1;
        if (carla_options.global_jack_client == false)
            jack_deactivate(jack_client);

        carla_proc_unlock();

        // Unregister jack ports
        remove_from_jack();

        // Delete old data
        delete_buffers();

        uint32_t ains, aouts, params, j;
        ains = aouts = params = 0;

        const unsigned long PortCount = descriptor->PortCount;

        for (unsigned long i=0; i<PortCount; i++)
        {
            LADSPA_PortDescriptor PortType = descriptor->PortDescriptors[i];
            if (LADSPA_IS_PORT_AUDIO(PortType))
            {
                if (LADSPA_IS_PORT_INPUT(PortType))
                    ains += 1;
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                    aouts += 1;
            }
            else if (LADSPA_IS_PORT_CONTROL(PortType))
                params += 1;
        }

        if (ains > 0)
        {
            ain.rindexes = new uint32_t[ains];
            ain.ports    = new jack_port_t*[ains];
        }

        if (aouts > 0)
        {
            aout.rindexes = new uint32_t[aouts];
            aout.ports    = new jack_port_t*[aouts];
        }

        if (params > 0)
        {
            param.buffers = new float[params];
            param.data    = new ParameterData[params];
            param.ranges  = new ParameterRanges[params];
        }

        const int port_name_size = jack_port_name_size();
        char port_name[port_name_size];
        bool needs_cin  = false;
        bool needs_cout = false;

        for (unsigned long i=0; i<PortCount; i++)
        {
            LADSPA_PortDescriptor PortType = descriptor->PortDescriptors[i];
            LADSPA_PortRangeHint PortHint  = descriptor->PortRangeHints[i];
            bool HasPortRDF = (rdf_descriptor && i < rdf_descriptor->PortCount);

            if (LADSPA_IS_PORT_AUDIO(PortType))
            {
                if (carla_options.global_jack_client)
                {
                    strncpy(port_name, name, (port_name_size/2)-2);
                    strcat(port_name, ":");
                    strncat(port_name, descriptor->PortNames[i], port_name_size/2);
                }
                else
                    strncpy(port_name, descriptor->PortNames[i], port_name_size);

                if (LADSPA_IS_PORT_INPUT(PortType))
                {
                    j = ain.count++;
                    ain.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
                    ain.rindexes[j] = i;
                }
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                {
                    j = aout.count++;
                    aout.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
                    aout.rindexes[j] = i;
                    needs_cin = true;
                }
                else
                    qWarning("WARNING - Got a broken Port (Audio, but not input or output)");
            }
            else if (LADSPA_IS_PORT_CONTROL(PortType))
            {
                j = param.count++;
                param.data[j].index  = j;
                param.data[j].rindex = i;
                param.data[j].hints  = 0;
                param.data[j].midi_channel = 0;
                param.data[j].midi_cc = -1;

                double min, max, def, step, step_small, step_large;

                // min value
                if (LADSPA_IS_HINT_BOUNDED_BELOW(PortHint.HintDescriptor))
                    min = PortHint.LowerBound;
                else
                    min = 0.0;

                // max value
                if (LADSPA_IS_HINT_BOUNDED_ABOVE(PortHint.HintDescriptor))
                    max = PortHint.UpperBound;
                else
                    max = 1.0;

                if (min > max)
                    max = min;
                else if (max < min)
                    min = max;

                // default value
                if (HasPortRDF && LADSPA_PORT_HAS_DEFAULT(rdf_descriptor->Ports[j].Hints))
                    def = rdf_descriptor->Ports[j].Default;

                else if (LADSPA_IS_HINT_HAS_DEFAULT(PortHint.HintDescriptor))
                {
                    switch (PortHint.HintDescriptor & LADSPA_HINT_DEFAULT_MASK)
                    {
                    case LADSPA_HINT_DEFAULT_MINIMUM:
                        def = min;
                        break;
                    case LADSPA_HINT_DEFAULT_MAXIMUM:
                        def = max;
                        break;
                    case LADSPA_HINT_DEFAULT_0:
                        def = 0.0;
                        break;
                    case LADSPA_HINT_DEFAULT_1:
                        def = 1.0;
                        break;
                    case LADSPA_HINT_DEFAULT_100:
                        def = 100.0;
                        break;
                    case LADSPA_HINT_DEFAULT_440:
                        def = 440.0;
                        break;
                    case LADSPA_HINT_DEFAULT_LOW:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = exp((log(min)*0.75) + (log(max)*0.25));
                        else
                            def = (min*0.75) + (max*0.25);
                        break;
                    case LADSPA_HINT_DEFAULT_MIDDLE:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = sqrt(min*max);
                        else
                            def = (min+max)/2;
                        break;
                    case LADSPA_HINT_DEFAULT_HIGH:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = exp((log(min)*0.25) + (log(max)*0.75));
                        else
                            def = (min*0.25) + (max*0.75);
                        break;
                    default:
                        if (min < 0.0 && max > 0.0)
                            def = 0.0;
                        else
                            def = min;
                        break;
                    }
                }
                else
                {
                    // no default value
                    if (min < 0.0 && max > 0.0)
                        def = 0.0;
                    else
                        def = min;
                }

                if (def < min)
                    def = min;
                else if (def > max)
                    def = max;

                if (max - min == 0.0)
                {
                    qWarning("Broken plugin parameter -> max - min == 0");
                    max = min + 0.1;
                }

                if (LADSPA_IS_HINT_SAMPLE_RATE(PortHint.HintDescriptor))
                {
                    double sample_rate = get_sample_rate();
                    min *= sample_rate;
                    max *= sample_rate;
                    def *= sample_rate;
                    param.data[j].hints |= PARAMETER_USES_SAMPLERATE;
                }

                if (LADSPA_IS_HINT_INTEGER(PortHint.HintDescriptor))
                {
                    step = 1.0;
                    step_small = 1.0;
                    step_large = 10.0;
                }
                else if (LADSPA_IS_HINT_TOGGLED(PortHint.HintDescriptor))
                {
                    step = max - min;
                    step_small = step;
                    step_large = step;
                }
                else
                {
                    double range = max - min;
                    step = range/100.0;
                    step_small = range/1000.0;
                    step_large = range/10.0;
                }

                if (LADSPA_IS_PORT_INPUT(PortType))
                {
                    param.data[j].type = PARAMETER_INPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;
                    param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                    needs_cin = true;
                }
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                {
                    param.data[j].type = PARAMETER_OUTPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;

                    if (strcmp(descriptor->PortNames[i], "latency") != 0 && strcmp(descriptor->PortNames[i], "_latency") != 0)
                    {
                        param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                        needs_cout = true;
                    }
                    else
                    {
                        // latency parameter
                        min = 0;
                        max = get_sample_rate();
                        def = 0;
                        step = 1;
                        step_small = 1;
                        step_large = 1;
                    }
                }
                else
                {
                    param.data[j].type = PARAMETER_UNKNOWN;
                    qWarning("WARNING - Got a broken Port (Control, but not input or output)");
                }

                // check for scalepoints, require at least 2 to make it useful
                if (HasPortRDF && rdf_descriptor->Ports[i].ScalePointCount > 1)
                    param.data[j].hints |= PARAMETER_USES_SCALEPOINTS;

                param.ranges[j].min = min;
                param.ranges[j].max = max;
                param.ranges[j].def = def;
                param.ranges[j].step = step;
                param.ranges[j].step_small = step_small;
                param.ranges[j].step_large = step_large;

                // Start parameters in their default values
                param.buffers[j] = def;

                descriptor->connect_port(handle, i, &param.buffers[j]);
            }
            else
            {
                // Not Audio or Control
                qCritical("ERROR - Got a broken Port (neither Audio or Control)");
                descriptor->connect_port(handle, i, nullptr);
            }
        }

        if (needs_cin)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-in");
            }
            else
                strcpy(port_name, "control-in");

            param.port_in = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        if (needs_cout)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-out");
            }
            else
                strcpy(port_name, "control-out");

            param.port_out = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
        }

        ain.count   = ains;
        aout.count  = aouts;
        param.count = params;

        // Other plugin checks
        if (aouts > 0 && (ains == aouts || ains == 1))
            hints |= PLUGIN_CAN_DRYWET;

        if (aouts > 0)
            hints |= PLUGIN_CAN_VOL;

        if (aouts >= 2 && aouts%2 == 0)
            hints |= PLUGIN_CAN_BALANCE;

        carla_proc_lock();
        id = _id;
        carla_proc_unlock();

        if (carla_options.global_jack_client == false)
            jack_activate(jack_client);
    }

    void reload_programs(bool)
    {
    }

    void prepare_for_save()
    {
    }

    void process(jack_nframes_t nframes)
    {
        uint32_t i, k;
        unsigned short plugin_id = id;

        double ains_peak_tmp[2]  = { 0.0 };
        double aouts_peak_tmp[2] = { 0.0 };

        jack_default_audio_sample_t* ains_buffer[ain.count];
        jack_default_audio_sample_t* aouts_buffer[aout.count];

        for (i=0; i < ain.count; i++)
            ains_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(ain.ports[i], nframes);

        for (i=0; i < aout.count; i++)
            aouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(aout.ports[i], nframes);

        // --------------------------------------------------------------------------------------------------------
        // Input VU

        if (ain.count > 0)
        {
            short j2 = (ain.count == 1) ? 0 : 1;

            for (k=0; k<nframes; k++)
            {
                if (ains_buffer[0][k] > ains_peak_tmp[0])
                    ains_peak_tmp[0] = ains_buffer[0][k];
                if (ains_buffer[j2][k] > ains_peak_tmp[1])
                    ains_peak_tmp[1] = ains_buffer[j2][k];
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Parameters Input [Automation]

        if (param.port_in)
        {
            jack_default_audio_sample_t* pin_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_in, nframes);

            jack_midi_event_t pin_event;
            uint32_t n_pin_events = jack_midi_get_event_count(pin_buffer);

            for (i=0; i<n_pin_events; i++)
            {
                if (jack_midi_event_get(&pin_event, pin_buffer, i) != 0)
                    break;

                unsigned char channel = pin_event.buffer[0] & 0x0F;
                unsigned char mode    = pin_event.buffer[0] & 0xF0;

                // Status change
                if (mode == 0xB0)
                {
                    unsigned char status  = pin_event.buffer[1] & 0x7F;
                    unsigned char velo    = pin_event.buffer[2] & 0x7F;
                    double value, velo_per = double(velo)/127;

                    // Control GUI stuff (channel 0 only)
                    if (channel == 0)
                    {
                        if (status == 0x78)
                        {
                            // All Sound Off
                            set_active(false, false, false);
                            postpone_event(PostEventParameter, PARAMETER_ACTIVE, 0.0);
                            break;
                        }
                        else if (status == 0x09 && hints & PLUGIN_CAN_DRYWET)
                        {
                            // Dry/Wet (using '0x09', undefined)
                            set_drywet(velo_per, false, false);
                            postpone_event(PostEventParameter, PARAMETER_DRYWET, velo_per);
                        }
                        else if (status == 0x07 && hints & PLUGIN_CAN_VOL)
                        {
                            // Volume
                            value = double(velo)/100;
                            set_vol(value, false, false);
                            postpone_event(PostEventParameter, PARAMETER_VOLUME, value);
                        }
                        else if (status == 0x08 && hints & PLUGIN_CAN_BALANCE)
                        {
                            // Balance
                            double left, right;
                            value = (double(velo)-63.5)/63.5;

                            if (value < 0)
                            {
                                left  = -1.0;
                                right = (value*2)+1.0;
                            }
                            else if (value > 0)
                            {
                                left  = (value*2)-1.0;
                                right = 1.0;
                            }
                            else
                            {
                                left  = -1.0;
                                right = 1.0;
                            }

                            set_balance_left(left, false, false);
                            set_balance_right(right, false, false);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_LEFT, left);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_RIGHT, right);
                        }
                    }

                    // Control plugin parameters
                    for (k=0; k < param.count; k++)
                    {
                        if (param.data[k].type == PARAMETER_INPUT && (param.data[k].hints & PARAMETER_IS_AUTOMABLE) > 0 &&
                                param.data[k].midi_channel == channel && param.data[k].midi_cc == status)
                        {
                            value = (velo_per * (param.ranges[k].max - param.ranges[k].min)) + param.ranges[k].min;
                            set_parameter_value(k, value, false, false, false);
                            postpone_event(PostEventParameter, k, value);
                        }
                    }
                }
            }
        } // End of Parameters Input

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Plugin processing

        if (active)
        {
            if (!active_before)
            {
                if (descriptor->activate)
                    descriptor->activate(handle);
            }

            for (i=0; i < ain.count; i++)
                descriptor->connect_port(handle, ain.rindexes[i], ains_buffer[i]);

            for (i=0; i < aout.count; i++)
                descriptor->connect_port(handle, aout.rindexes[i], aouts_buffer[i]);

            if (descriptor->run)
                descriptor->run(handle, nframes);
        }
        else
        {
            if (active_before)
            {
                if (descriptor->deactivate)
                    descriptor->deactivate(handle);
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Post-processing (dry/wet, volume and balance)

        if (active)
        {
            double bal_rangeL, bal_rangeR;
            jack_default_audio_sample_t old_bal_left[nframes];

            for (i=0; i < aout.count; i++)
            {
                // Dry/Wet and Volume
                for (k=0; k<nframes; k++)
                {
                    if (hints & PLUGIN_CAN_DRYWET && x_drywet != 1.0)
                    {
                        if (aout.count == 1)
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[0][k]*(1.0-x_drywet));
                        else
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[i][k]*(1.0-x_drywet));
                    }

                    if (hints & PLUGIN_CAN_VOL)
                        aouts_buffer[i][k] *= x_vol;
                }

                // Balance
                if (hints & PLUGIN_CAN_BALANCE)
                {
                    if (i%2 == 0)
                        memcpy(&old_bal_left, aouts_buffer[i], sizeof(jack_default_audio_sample_t)*nframes);

                    bal_rangeL = (x_bal_left+1.0)/2;
                    bal_rangeR = (x_bal_right+1.0)/2;

                    for (k=0; k<nframes; k++)
                    {
                        if (i%2 == 0)
                        {
                            // left output
                            aouts_buffer[i][k]  = old_bal_left[k]*(1.0-bal_rangeL);
                            aouts_buffer[i][k] += aouts_buffer[i+1][k]*(1.0-bal_rangeR);
                        }
                        else
                        {
                            // right
                            aouts_buffer[i][k]  = aouts_buffer[i][k]*bal_rangeR;
                            aouts_buffer[i][k] += old_bal_left[k]*bal_rangeL;
                        }
                    }
                }

                // Output VU
                if (i < 2)
                {
                    for (k=0; k<nframes; k++)
                    {
                        if (aouts_buffer[i][k] > aouts_peak_tmp[i])
                            aouts_peak_tmp[i] = aouts_buffer[i][k];
                    }
                }
            }
        }
        else
        {
            // disable any output sound if not active
            for (i=0; i < aout.count; i++)
                memset(aouts_buffer[i], 0.0f, sizeof(jack_default_audio_sample_t)*nframes);

            aouts_peak_tmp[0] = 0.0;
            aouts_peak_tmp[1] = 0.0;

        } // End of Post-processing

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Control Output

        if (param.port_out)
        {
            jack_default_audio_sample_t* cout_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_out, nframes);
            jack_midi_clear_buffer(cout_buffer);

            double value_per;

            for (k=0; k < param.count; k++)
            {
                if (param.data[k].type == PARAMETER_OUTPUT && param.data[k].midi_cc >= 0)
                {
                    value_per = (param.buffers[k] - param.ranges[k].min)/(param.ranges[k].max - param.ranges[k].min);

                    jack_midi_data_t* event_buffer = jack_midi_event_reserve(cout_buffer, 0, 3);
                    event_buffer[0] = 0xB0 + param.data[k].midi_channel;
                    event_buffer[1] = param.data[k].midi_cc;
                    event_buffer[2] = 127*value_per;
                }
            }
        } // End of Control Output

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Peak Values

        ains_peak[(plugin_id*2)+0]  = ains_peak_tmp[0];
        ains_peak[(plugin_id*2)+1]  = ains_peak_tmp[1];
        aouts_peak[(plugin_id*2)+0] = aouts_peak_tmp[0];
        aouts_peak[(plugin_id*2)+1] = aouts_peak_tmp[1];

        active_before = active;
    }

    void buffer_size_changed(jack_nframes_t)
    {
    }
};

bool is_port_good(int Type1, int Type2)
{
    if (LADSPA_IS_PORT_INPUT(Type1) && !LADSPA_IS_PORT_INPUT(Type2))
        return false;
    else if (LADSPA_IS_PORT_OUTPUT(Type1) && !LADSPA_IS_PORT_OUTPUT(Type2))
        return false;
    else if (LADSPA_IS_PORT_CONTROL(Type1) && !LADSPA_IS_PORT_CONTROL(Type2))
        return false;
    else if (LADSPA_IS_PORT_AUDIO(Type1) && !LADSPA_IS_PORT_AUDIO(Type2))
        return false;
    else
        return true;
}

bool is_ladspa_rdf_descriptor_valid(LADSPA_RDF_Descriptor* rdf_descriptor, const LADSPA_Descriptor* descriptor)
{
    if (rdf_descriptor)
    {
        if (rdf_descriptor->PortCount <= descriptor->PortCount)
        {
            for (unsigned long i=0; i < rdf_descriptor->PortCount; i++)
            {
                if (!is_port_good(rdf_descriptor->Ports[i].Type, descriptor->PortDescriptors[i]))
                {
                    qWarning("WARNING - Plugin has RDF data, but invalid PortTypes - %i != %i", rdf_descriptor->Ports[i].Type, descriptor->PortDescriptors[i]);
                    return false;
                }
            }
            return true;
        }
        else
        {
            qWarning("WARNING - Plugin has RDF data, but invalid PortCount -> %li > %li :: %s", rdf_descriptor->PortCount, descriptor->PortCount, rdf_descriptor->Title);
            return false;
        }
    }
    else
        // No RDF Descriptor
        return false;
}

short add_plugin_ladspa(const char* filename, const char* label, void* extra_stuff)
{
    qDebug("add_plugin_ladspa(%s, %s, %p)", filename, label, extra_stuff);

    short id = get_new_plugin_id();

    if (id >= 0)
    {
        LadspaAudioPlugin* plugin = new LadspaAudioPlugin;

        if ((plugin->lib_open(filename)))
        {
            LADSPA_Descriptor_Function descfn = (LADSPA_Descriptor_Function)plugin->lib_symbol("ladspa_descriptor");

            if (descfn)
            {
                unsigned long i = 0;
                while ((plugin->descriptor = descfn(i++)))
                {
                    if (strcmp(plugin->descriptor->Label, label) == 0)
                        break;
                }

                if (plugin->descriptor)
                {
                    plugin->handle = plugin->descriptor->instantiate(plugin->descriptor, get_sample_rate());

                    if (plugin->handle)
                    {
                        plugin->filename = strdup(filename);

                        LADSPA_RDF_Descriptor* rdf_descriptor = (LADSPA_RDF_Descriptor*)extra_stuff;

                        if (is_ladspa_rdf_descriptor_valid(rdf_descriptor, plugin->descriptor))
                            plugin->rdf_descriptor = ladspa_rdf_dup(rdf_descriptor);
                        else
                            plugin->rdf_descriptor = nullptr;

                        if (plugin->rdf_descriptor && LADSPA_PLUGIN_HAS_TITLE(plugin->rdf_descriptor->Hints))
                            plugin->name = get_unique_name(plugin->rdf_descriptor->Title);
                        else
                            plugin->name = get_unique_name(plugin->descriptor->Name);

                        if (carla_options.global_jack_client)
                            plugin->jack_client = carla_jack_client;
                        else
                            carla_jack_register_plugin(plugin);

                        if (plugin->jack_client)
                        {
                            plugin->reload();

                            plugin->id = id;
                            unique_names[id] = plugin->name;
                            AudioPlugins[id] = plugin;

                            osc_new_plugin(plugin);
                        }
                        else
                        {
                            set_last_error("Failed to register plugin in JACK");
                            delete plugin;
                            id = -1;
                        }
                    }
                    else
                    {
                        set_last_error("Plugin failed to initialize");
                        delete plugin;
                        id = -1;
                    }
                }
                else
                {
                    set_last_error("Could not find the requested plugin Label in the plugin library");
                    delete plugin;
                    id = -1;
                }
            }
            else
            {
                set_last_error("Could not find the LASDPA Descriptor in the plugin library");
                delete plugin;
                id = -1;
            }
        }
        else
        {
            set_last_error(plugin->lib_error());
            delete plugin;
            id = -1;
        }
    }
    else
        set_last_error("Maximum number of plugins reached");

    return id;
}
