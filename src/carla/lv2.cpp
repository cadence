/* Code for LV2 plugins */

#include "carla_backend.h"
#include "audio_plugin.h"
#include "misc.h"

#include <stdio.h>
#include <unistd.h>

#include <QtGui/QDialog>
#include <QtGui/QLayout>

#include "lv2/lv2.h"
#include "lv2/data-access.h"
#include "lv2/dynparam.h"
#include "lv2/event.h"
#include "lv2/event-helpers.h"
#include "lv2/instance-access.h"
#include "lv2/state.h"
#include "lv2/time.h"
#include "lv2/uri-map.h"
#include "lv2/urid.h"
#include "lv2/ui.h"
#include "lv2/ui-resize.h"
#include "lv2/lv2_external_ui.h"
#include "lv2/lv2_rtmempool.h"
#include "lv2_rdf.h"

extern "C" {
#include "lv2-rtmempool/rtmempool.h"
}

#define NS_ATOM  "http://lv2plug.in/ns/ext/atom#"
#define NS_LV2UI "http://lv2plug.in/ns/extensions/ui#"

// Global variables
extern const char* unique_names[MAX_PLUGINS];
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];
extern ExternalMidiNote ExternalMidiNotes[MAX_MIDI_EVENTS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Global JACK client
extern jack_client_t* carla_jack_client;

// Global options
extern carla_options_t carla_options;

// jack.cpp
void carla_jack_register_plugin(AudioPlugin* plugin);

// static max values
const unsigned int MAX_EVENT_BUFFER = 0x7FFF; // 32767

// feature ids
const uint16_t lv2_feature_id_uri_map         = 0;
const uint16_t lv2_feature_id_urid_map        = 1;
const uint16_t lv2_feature_id_urid_unmap      = 2;
const uint16_t lv2_feature_id_event           = 3;
const uint16_t lv2_feature_id_rtmempool       = 4;
const uint16_t lv2_feature_id_data_access     = 5;
const uint16_t lv2_feature_id_instance_access = 6;
const uint16_t lv2_feature_id_ui_resize       = 7;
const uint16_t lv2_feature_id_ui_parent       = 8;
const uint16_t lv2_feature_id_external_ui     = 9;
const uint16_t lv2_feature_id_external_ui_old = 10;
const uint16_t lv2_feature_count              = 11;

// uri[d] map ids
const uint16_t CARLA_URI_MAP_ID_EVENT_MIDI    = 1; // 0x1
const uint16_t CARLA_URI_MAP_ID_EVENT_TIME    = 2; // 0x2
const uint16_t CARLA_URI_MAP_ID_ATOM_STRING   = 3;
const uint16_t CARLA_URI_MAP_ID_COUNT         = 4;

// extra plugin hints
const unsigned int PLUGIN_HAS_EXTENSION_STATE    = 0x100;
const unsigned int PLUGIN_HAS_EXTENSION_DYNPARAM = 0x200;

static LV2_Time_Position carla_lv2_time_pos = { 0, 0, LV2_TIME_STOPPED, 0, 0, 0, 0, 0, 0, 0.0 };

struct CarlaLv2Event {
    uint16_t type;
    jack_port_t* port;
    LV2_Event_Buffer* buffer;
};

struct CarlaLv2EventData {
    uint32_t count;
    CarlaLv2Event* events;
};

class Lv2OscGuiThread : public QThread
{
public:
    Lv2OscGuiThread(QObject* parent=0);
    void set_plugin_id(short plugin_id);
    void set_ui_type(LV2_Property ui_type);
    void run();

private:
    short plugin_id;
    LV2_Property ui_type;
};

class Lv2AudioPlugin : public AudioPlugin
{
public:
    CarlaLv2EventData evin;
    CarlaLv2EventData evout;

    LV2_Handle handle;
    const LV2_Descriptor* descriptor;
    const LV2_RDF_Descriptor* rdf_descriptor;
    LV2_Feature* features[lv2_feature_count+1];

    struct {
        void* lib;
        LV2UI_Handle handle;
        LV2UI_Widget widget;
        const LV2UI_Descriptor* descriptor;
        const LV2_RDF_UI* rdf_descriptor;
    } ui;

    bool wants_time_pos;
    QList<const char*> custom_uri_ids;

    lv2_rtsafe_memory_pool_provider rtmempool_allocator;
    
    // testing
    lv2dynparam_host_callbacks dynparam_host;
    lv2dynparam_plugin_callbacks* dynparam_plugin;

    Lv2AudioPlugin()
    {
        qDebug("Lv2AudioPlugin::Lv2AudioPlugin()");
        type = PLUGIN_LV2;

        evin.count  = 0;
        evin.events = nullptr;

        evout.count  = 0;
        evout.events = nullptr;

        handle = nullptr;
        descriptor = nullptr;
        rdf_descriptor = nullptr;

        ui.lib = nullptr;
        ui.handle = nullptr;
        ui.descriptor = nullptr;
        ui.rdf_descriptor = nullptr;

        wants_time_pos = false;

        for (uint16_t i=0; i<lv2_feature_count+1; i++)
            features[i] = nullptr;

        custom_uri_ids.clear();

        // Fill pre-set URI keys
        for (uint16_t i=0; i < CARLA_URI_MAP_ID_COUNT; i++)
            custom_uri_ids.append(nullptr);
        
        dynparam_host.group_appear        = carla_lv2_dynparam_group_appear;
        dynparam_host.group_disappear     = carla_lv2_dynparam_group_disappear;
        dynparam_host.parameter_appear    = carla_lv2_dynparam_parameter_appear;
        dynparam_host.parameter_disappear = carla_lv2_dynparam_parameter_disappear;
        dynparam_host.parameter_change    = carla_lv2_dynparam_parameter_change;
        dynparam_host.command_appear      = carla_lv2_dynparam_command_appear;
        dynparam_host.command_disappear   = carla_lv2_dynparam_command_disappear;
        dynparam_plugin = 0;
    }

    ~Lv2AudioPlugin()
    {
        qDebug("Lv2AudioPlugin::~Lv2AudioPlugin()");

        // close UI
        if (hints & PLUGIN_HAS_GUI)
        {
            switch(gui.type)
            {
            case GUI_INTERNAL_QT4:
            case GUI_INTERNAL_X11:
                break;

            case GUI_EXTERNAL_OSC:
                if (gui.visible)
                    osc_send_hide(&osc.data);

                osc_send_quit(&osc.data);

                if (osc.thread)
                {
                    osc.thread->quit();

                    if (!osc.thread->wait(3000)) // 3 sec
                        qWarning("Failed to properly stop LV2 OSC GUI thread");

                    delete osc.thread;
                }

                osc_clear_data(&osc.data);

                break;

            case GUI_EXTERNAL_LV2:
                if (gui.visible)
                    LV2_EXTERNAL_UI_HIDE((lv2_external_ui*)ui.widget);

                break;

            default:
                break;
            }

            if (ui.handle && ui.descriptor && ui.descriptor->cleanup)
                ui.descriptor->cleanup(ui.handle);

            if (features[lv2_feature_id_data_access] && features[lv2_feature_id_data_access]->data)
                delete (LV2_Extension_Data_Feature*)features[lv2_feature_id_data_access]->data;

            if (features[lv2_feature_id_ui_resize] && features[lv2_feature_id_ui_resize]->data)
                delete (LV2_UI_Resize_Feature*)features[lv2_feature_id_ui_resize]->data;

            if (features[lv2_feature_id_external_ui] && features[lv2_feature_id_external_ui]->data)
                delete (lv2_external_ui_host*)features[lv2_feature_id_external_ui]->data;

            ui_lib_close();
        }

        for (int i=0; i < custom_uri_ids.count(); i++)
        {
            if (custom_uri_ids[i])
                free((void*)custom_uri_ids[i]);
        }

        custom_uri_ids.clear();

        if (handle && descriptor->deactivate && active_before)
            descriptor->deactivate(handle);

        if (handle && descriptor->cleanup)
            descriptor->cleanup(handle);

        if (features[lv2_feature_id_uri_map] && features[lv2_feature_id_uri_map]->data)
            delete (LV2_URI_Map_Feature*)features[lv2_feature_id_uri_map]->data;

        if (features[lv2_feature_id_urid_map] && features[lv2_feature_id_urid_map]->data)
            delete (LV2_URID_Map*)features[lv2_feature_id_urid_map]->data;

        if (features[lv2_feature_id_urid_unmap] && features[lv2_feature_id_urid_unmap]->data)
            delete (LV2_URID_Unmap*)features[lv2_feature_id_urid_unmap]->data;

        if (features[lv2_feature_id_event] && features[lv2_feature_id_event]->data)
            delete (LV2_Event_Feature*)features[lv2_feature_id_event]->data;

        for (uint16_t j=0; j<lv2_feature_count && features[j]; j++)
            delete features[j];

        if (rdf_descriptor)
            lv2_rdf_free(rdf_descriptor);

        lv2_remove_from_jack();

        lv2_delete_buffers();
    }

    void lv2_remove_from_jack()
    {
        qDebug("Lv2AudioPlugin::lv2_remove_from_jack()");

        uint32_t i;
        for (i=0; i < evin.count; i++)
            jack_port_unregister(jack_client, evin.events[i].port);

        for (i=0; i < evout.count; i++)
            jack_port_unregister(jack_client, evout.events[i].port);

        // We set fake min/mout values for easy port-count-info
        min.count  = 0;
        mout.count = 0;

        qDebug("Lv2AudioPlugin::lv2_remove_from_jack() - end");
    }

    void lv2_delete_buffers()
    {
        qDebug("Lv2AudioPlugin::lv2_delete_buffers()");

        if (evin.count > 0)
        {
            for (uint32_t i=0; i < evin.count; i++)
                free(evin.events[i].buffer);

            delete[] evin.events;
        }

        if (evout.count > 0)
        {
            for (uint32_t i=0; i < evout.count; i++)
                free(evout.events[i].buffer);

            delete[] evout.events;
        }

        evin.count  = 0;
        evin.events = nullptr;

        evout.count  = 0;
        evout.events = nullptr;

        qDebug("Lv2AudioPlugin::lv2_delete_buffers() - end");
    }

    // Used for preset ports
    int32_t get_parameter_id_from_symbol(const char* symbol)
    {
        int32_t param_id = -1;
        for (uint32_t i=0; i < rdf_descriptor->PortCount; i++)
        {
            if (LV2_IS_PORT_CONTROL(rdf_descriptor->Ports[i].Type))
            {
                param_id += 1;

                if (strcmp(rdf_descriptor->Ports[i].Symbol, symbol) == 0)
                    return param_id;
            }
        }
        return -1;
    }

    bool is_ui_resizable()
    {
        if (ui.rdf_descriptor)
        {
            for (uint32_t i=0; i < ui.rdf_descriptor->FeatureCount; i++)
            {
                if (strcmp(ui.rdf_descriptor->Features[i].URI, NS_LV2UI "fixedSize") == 0 || strcmp(ui.rdf_descriptor->Features[i].URI, NS_LV2UI "noUserResize") == 0)
                    return false;
            }
            return true;
        }
        return false;
    }

    bool is_ui_bridgeable(uint32_t ui_id)
    {
        LV2_RDF_UI* rdf_ui = &rdf_descriptor->UIs[ui_id];

        for (uint32_t i=0; i < rdf_ui->FeatureCount; i++)
        {
            if (strcmp(rdf_ui->Features[i].URI, LV2_INSTANCE_ACCESS_URI) == 0 || strcmp(rdf_ui->Features[i].URI, LV2_DATA_ACCESS_URI) == 0)
                return false;
        }

        return true;
    }

    void reinit_external_ui()
    {
        qDebug("Lv2AudioPlugin::reinit_external_ui()");

        ui.widget = nullptr;
        ui.handle = ui.descriptor->instantiate(ui.descriptor,
                                               descriptor->URI,
                                               ui.rdf_descriptor->Bundle,
                                               carla_lv2_ui_write_function,
                                               this,
                                               &ui.widget,
                                               features);

        if (ui.handle && ui.widget)
            update_ui_ports();
        else
        {
            qDebug("Lv2AudioPlugin::reinit_external_ui() - Failed to re-initiate external UI");

            ui.handle = nullptr;
            ui.widget = nullptr;
            callback_action(CALLBACK_SHOW_GUI, id, -1, 0, 0.0);
        }
    }

    void update_ui_ports()
    {
        qDebug("Lv2AudioPlugin::lv2_ui_update_ports()");

        if (ui.descriptor->port_event)
        {
            for (uint32_t i=0; i < param.count; i++)
                ui.descriptor->port_event(ui.handle, param.data[i].rindex, sizeof(float), 0, &param.buffers[i]);
        }
    }

    uint32_t get_custom_uri_id(const char* uri)
    {
        qDebug("Lv2AudioPlugin::get_custom_uri_id(%s)", uri);

        for (int i=0; i < custom_uri_ids.count(); i++)
        {
            if (custom_uri_ids[i] && strcmp(custom_uri_ids[i], uri) == 0)
                return i;
        }

        custom_uri_ids.append(strdup(uri));
        return custom_uri_ids.count()-1;
    }

    const char* get_custom_uri_string(int uri_id)
    {
        qDebug("Lv2AudioPlugin::get_custom_uri_string(%i)", uri_id);

        if (uri_id < custom_uri_ids.count())
            return custom_uri_ids.at(uri_id);
        else
            return nullptr;
    }

    void delete_me()
    {
        qDebug("Lv2AudioPlugin::delete_me()");
        delete this;
    }

    int set_osc_bridge_info(PluginOscBridgeInfoType, lo_arg**)
    {
        return 1;
    }

    PluginCategory get_category()
    {
        LV2_Property Category = rdf_descriptor->Type;

        // Specific Types
        if (Category & LV2_CLASS_REVERB)
            return PLUGIN_CATEGORY_DELAY;

        // Pre-set LV2 Types
        else if (LV2_IS_GENERATOR(Category))
            return PLUGIN_CATEGORY_SYNTH;
        else if (LV2_IS_UTILITY(Category))
            return PLUGIN_CATEGORY_UTILITY;
        else if (LV2_IS_SIMULATOR(Category))
            return PLUGIN_CATEGORY_OUTRO;
        else if (LV2_IS_DELAY(Category))
            return PLUGIN_CATEGORY_DELAY;
        else if (LV2_IS_MODULATOR(Category))
            return PLUGIN_CATEGORY_MODULATOR;
        else if (LV2_IS_FILTER(Category))
            return PLUGIN_CATEGORY_FILTER;
        else if (LV2_IS_EQUALISER(Category))
            return PLUGIN_CATEGORY_EQ;
        else if (LV2_IS_SPECTRAL(Category))
            return PLUGIN_CATEGORY_UTILITY;
        else if (LV2_IS_DISTORTION(Category))
            return PLUGIN_CATEGORY_OUTRO;
        else if (LV2_IS_DYNAMICS(Category))
            return PLUGIN_CATEGORY_DYNAMICS;
        else
            return PLUGIN_CATEGORY_NONE;
    }

    void get_label(char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->URI, STR_MAX);
    }

    void get_maker(char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->Author, STR_MAX);
    }

    void get_copyright(char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->License, STR_MAX);
    }

    void get_real_name(char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->Name, STR_MAX);
    }

    long get_unique_id()
    {
        return rdf_descriptor->UniqueID;
    }

    void get_parameter_name(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->Ports[rindex].Name, STR_MAX);
    }

    void get_parameter_symbol(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->Ports[rindex].Symbol, STR_MAX);
    }

    void get_parameter_label(uint32_t rindex, char* buf_str)
    {
        LV2_RDF_Port* Port = &rdf_descriptor->Ports[rindex];

        if (LV2_HAVE_UNIT_SYMBOL(Port->Unit.Hints))
            strncpy(buf_str, Port->Unit.Symbol, STR_MAX);

        else if (LV2_HAVE_UNIT(Port->Unit.Hints))
        {
            switch (Port->Unit.Type)
            {
            case LV2_UNIT_BAR:
                strncpy(buf_str, "bars", STR_MAX);
                return;
            case LV2_UNIT_BEAT:
                strncpy(buf_str, "beats", STR_MAX);
                return;
            case LV2_UNIT_BPM:
                strncpy(buf_str, "BPM", STR_MAX);
                return;
            case LV2_UNIT_CENT:
                strncpy(buf_str, "ct", STR_MAX);
                return;
            case LV2_UNIT_CM:
                strncpy(buf_str, "cm", STR_MAX);
                return;
            case LV2_UNIT_COEF:
                strncpy(buf_str, "(coef)", STR_MAX);
                return;
            case LV2_UNIT_DB:
                strncpy(buf_str, "dB", STR_MAX);
                return;
            case LV2_UNIT_DEGREE:
                strncpy(buf_str, "deg", STR_MAX);
                return;
            case LV2_UNIT_HZ:
                strncpy(buf_str, "Hz", STR_MAX);
                return;
            case LV2_UNIT_INCH:
                strncpy(buf_str, "in", STR_MAX);
                return;
            case LV2_UNIT_KHZ:
                strncpy(buf_str, "kHz", STR_MAX);
                return;
            case LV2_UNIT_KM:
                strncpy(buf_str, "km", STR_MAX);
                return;
            case LV2_UNIT_M:
                strncpy(buf_str, "m", STR_MAX);
                return;
            case LV2_UNIT_MHZ:
                strncpy(buf_str, "MHz", STR_MAX);
                return;
            case LV2_UNIT_MIDINOTE:
                strncpy(buf_str, "note", STR_MAX);
                return;
            case LV2_UNIT_MILE:
                strncpy(buf_str, "mi", STR_MAX);
                return;
            case LV2_UNIT_MIN:
                strncpy(buf_str, "min", STR_MAX);
                return;
            case LV2_UNIT_MM:
                strncpy(buf_str, "mm", STR_MAX);
                return;
            case LV2_UNIT_MS:
                strncpy(buf_str, "ms", STR_MAX);
                return;
            case LV2_UNIT_OCT:
                strncpy(buf_str, "oct", STR_MAX);
                return;
            case LV2_UNIT_PC:
                strncpy(buf_str, "%", STR_MAX);
                return;
            case LV2_UNIT_S:
                strncpy(buf_str, "s", STR_MAX);
                return;
            case LV2_UNIT_SEMITONE:
                strncpy(buf_str, "semi", STR_MAX);
                return;
            }
        }
        *buf_str = 0;
    }

    uint32_t get_scalepoint_count(uint32_t rindex)
    {
        return rdf_descriptor->Ports[rindex].ScalePointCount;
    }

    double get_scalepoint_value(uint32_t rindex, uint32_t scalepoint_id)
    {
        return rdf_descriptor->Ports[rindex].ScalePoints[scalepoint_id].Value;
    }

    void get_scalepoint_label(uint32_t rindex, uint32_t scalepoint_id, char* buf_str)
    {
        strncpy(buf_str, rdf_descriptor->Ports[rindex].ScalePoints[scalepoint_id].Label, STR_MAX);
    }

    int32_t get_chunk_data(void**)
    {
        return 0;
    }

    void x_set_parameter_value(uint32_t parameter_id, double value, bool gui_send)
    {
        if (gui_send && gui.visible)
        {
            switch(gui.type)
            {
            case GUI_INTERNAL_QT4:
            case GUI_INTERNAL_X11:
            case GUI_EXTERNAL_LV2:
                if (ui.descriptor->port_event)
                {
                    float fvalue = value;
                    ui.descriptor->port_event(ui.handle, param.data[parameter_id].rindex, sizeof(float), 0, &fvalue);
                }
                break;

            case GUI_EXTERNAL_OSC:
                osc_send_control(&osc.data, param.data[parameter_id].rindex, value);
                break;

            default:
                break;
            }
        }
    }

    void x_set_program(uint32_t program_id, bool, bool)
    {
        // FIXME - state or ports first? should we block audio thread while restoring state?
        LV2_RDF_Preset* Preset = &rdf_descriptor->Presets[program_id];

        // set state
        for (uint32_t i=0; i < Preset->StateCount; i++)
            set_custom_data(CUSTOM_DATA_STRING, Preset->States[i].Key, Preset->States[i].Value, true);

        // set parameters
        int32_t param_id;

        for (uint32_t i=0; i < Preset->PortCount; i++)
        {
            param_id = get_parameter_id_from_symbol(Preset->Ports[i].Symbol);

            if (param_id >= 0)
                set_parameter_value(param_id, Preset->Ports[i].Value, true, false, false);
            else
                qWarning("Lv2Plugin::x_set_program(%i, ...) - Failed to find parameter '%s'", program_id, Preset->Ports[i].Symbol);
        }
    }

    void x_set_midi_program(uint32_t, bool, bool)
    {
    }

    void x_set_custom_data(CustomDataType, const char*, const char*, bool)
    {
        if ((hints & PLUGIN_HAS_EXTENSION_STATE) > 0 && descriptor->extension_data)
        {
            LV2_State_Interface* state = (LV2_State_Interface*)descriptor->extension_data(LV2_STATE_INTERFACE_URI);

            if (state)
                state->restore(handle, carla_lv2_state_retrieve, this, 0, features);
        }
    }

    void set_chunk_data(const char*)
    {
    }

    void set_gui_data(int, void* ptr)
    {
        switch(gui.type)
        {
        case GUI_INTERNAL_QT4:
            if (ui.widget)
            {
                QDialog* qtPtr  = (QDialog*)ptr;
                QWidget* widget = (QWidget*)ui.widget;

                qtPtr->layout()->addWidget(widget);
                widget->setParent(qtPtr);
                widget->show();
            }
            break;

        case GUI_INTERNAL_X11:
            if (ui.descriptor)
            {
                QDialog* qtPtr  = (QDialog*)ptr;
                features[lv2_feature_id_ui_parent]->data = (void*)qtPtr->winId();

                ui.handle = ui.descriptor->instantiate(ui.descriptor,
                                                       descriptor->URI,
                                                       ui.rdf_descriptor->Bundle,
                                                       carla_lv2_ui_write_function,
                                                       this,
                                                       &ui.widget,
                                                       features);

                if (ui.handle && ui.descriptor->port_event)
                    update_ui_ports();
            }
            break;

        default:
            break;
        }
    }

    void show_gui(bool yesno)
    {
        switch(gui.type)
        {
        case GUI_INTERNAL_QT4:
            gui.visible = yesno;
            break;

        case GUI_INTERNAL_X11:
            gui.visible = yesno;

            if (gui.visible && gui.width > 0 && gui.height > 0)
                callback_action(CALLBACK_RESIZE_GUI, id, gui.width, gui.height, 0.0);

            break;

        case GUI_EXTERNAL_OSC:
            if (yesno)
            {
                gui.show_now = true;

                // 40 re-tries, 4 secs
                for (int j=1; j<40 && gui.show_now; j++)
                {
                    if (osc.data.target)
                    {
                        osc_send_show(&osc.data);
                        gui.visible = true;
                        return;
                    }
                    else
                        // 100 ms
                        usleep(100000);
                }

                qDebug("Lv2AudioPlugin::show_gui() - GUI timeout");
                callback_action(CALLBACK_SHOW_GUI, id, 0, 0, 0.0);
            }
            else
            {
                gui.visible = false;
                osc_send_hide(&osc.data);
                osc_send_quit(&osc.data);
                osc_clear_data(&osc.data);
            }
            break;

        case GUI_EXTERNAL_LV2:
            if (!ui.handle)
                reinit_external_ui();

            if (ui.handle && ui.widget)
            {
                if (yesno)
                {
                    LV2_EXTERNAL_UI_SHOW((lv2_external_ui*)ui.widget);
                    gui.visible = true;
                }
                else
                {
                    LV2_EXTERNAL_UI_HIDE((lv2_external_ui*)ui.widget);
                    gui.visible = false;

                    if (ui.descriptor->cleanup)
                        ui.descriptor->cleanup(ui.handle);

                    ui.handle = nullptr;
                }
            }
            else
            {
                // failed to init UI
                gui.visible = false;
                callback_action(CALLBACK_SHOW_GUI, id, -1, 0, 0.0);
            }
            break;

        default:
            break;
        }
    }

    void idle_gui()
    {
        switch(gui.type)
        {
        case GUI_INTERNAL_QT4:
        case GUI_INTERNAL_X11:
        case GUI_EXTERNAL_LV2:
            if (ui.descriptor->port_event)
            {
                for (uint32_t i=0; i < param.count; i++)
                {
                    if (param.data[i].type == PARAMETER_OUTPUT && (param.data[i].hints & PARAMETER_IS_AUTOMABLE) > 0)
                        ui.descriptor->port_event(ui.handle, param.data[i].rindex, sizeof(float), 0, &param.buffers[i]);
                }
            }

            if (gui.type == GUI_EXTERNAL_LV2)
                LV2_EXTERNAL_UI_RUN((lv2_external_ui*)ui.widget);

            break;

        default:
            break;
        }
    }

    void reload()
    {
        qDebug("Lv2AudioPlugin::reload()");
        short _id = id;

        // Safely disable plugin for reload
        carla_proc_lock();

        id = -1;
        if (carla_options.global_jack_client == false)
            jack_deactivate(jack_client);

        carla_proc_unlock();

        // Unregister jack ports
        lv2_remove_from_jack();
        remove_from_jack();

        // Delete old data
        lv2_delete_buffers();
        delete_buffers();

        // All invada plugins have broken output ports
        bool force_strict_bounds = false;
        if (QString(rdf_descriptor->URI).startsWith("http://invadarecords.com/plugins/lv2/"))
            force_strict_bounds = true;

        uint32_t ains, aouts, cv_ins, cv_outs, ev_ins, ev_outs, params, j;
        ains = aouts = cv_ins = cv_outs = ev_ins = ev_outs = params = 0;

        const uint32_t PortCount = rdf_descriptor->PortCount;

        for (uint32_t i=0; i<PortCount; i++)
        {
            LV2_Property PortType = rdf_descriptor->Ports[i].Type;
            if (LV2_IS_PORT_AUDIO(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                    ains += 1;
                else if (LV2_IS_PORT_OUTPUT(PortType))
                    aouts += 1;
            }
            else if (LV2_IS_PORT_CONTROL(PortType))
            {
                params += 1;
            }
            else if (LV2_IS_PORT_CV(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                    cv_ins += 1;
                else if (LV2_IS_PORT_OUTPUT(PortType))
                    cv_outs += 1;
            }
            else if (LV2_IS_PORT_EVENT(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                    ev_ins += 1;
                else if (LV2_IS_PORT_OUTPUT(PortType))
                    ev_outs += 1;
            }
        }

        if (params == 0 && (hints & PLUGIN_HAS_EXTENSION_DYNPARAM) > 0)
        {
            dynparam_plugin = (lv2dynparam_plugin_callbacks*)descriptor->extension_data(LV2DYNPARAM_URI);
            if (dynparam_plugin)
                dynparam_plugin->host_attach(handle, &dynparam_host, this);
        }

        if (ains > 0)
        {
            ain.rindexes = new uint32_t[ains];
            ain.ports    = new jack_port_t*[ains];
        }

        if (aouts > 0)
        {
            aout.rindexes = new uint32_t[aouts];
            aout.ports    = new jack_port_t*[aouts];
        }

        if (ev_ins > 0)
        {
            evin.events = new CarlaLv2Event[ev_ins];

            for (j=0; j < ev_ins; j++)
            {
                evin.events[j].type   = 0;
                evin.events[j].port   = nullptr;
                evin.events[j].buffer = lv2_event_buffer_new(MAX_EVENT_BUFFER, LV2_EVENT_AUDIO_STAMP);
            }
        }

        if (ev_outs > 0)
        {
            evout.events = new CarlaLv2Event[ev_outs];

            for (j=0; j<ev_outs; j++)
            {
                evout.events[j].type   = 0;
                evout.events[j].port   = nullptr;
                evout.events[j].buffer = lv2_event_buffer_new(MAX_EVENT_BUFFER, LV2_EVENT_AUDIO_STAMP);
            }
        }

        if (params > 0)
        {
            param.buffers = new float[params];
            param.data    = new ParameterData[params];
            param.ranges  = new ParameterRanges[params];
        }

        const int port_name_size = jack_port_name_size();
        char port_name[port_name_size];
        bool needs_cin  = false;
        bool needs_cout = false;

        for (uint32_t i=0; i<PortCount; i++)
        {
            LV2_Property PortType = rdf_descriptor->Ports[i].Type;
            LV2_Property PortProps = rdf_descriptor->Ports[i].Properties;
            LV2_RDF_PortPoints PortPoints = rdf_descriptor->Ports[i].Points;

            if (LV2_IS_PORT_AUDIO(PortType) || LV2_IS_PORT_EVENT_MIDI(PortType))
            {
                if (carla_options.global_jack_client)
                {
                    strncpy(port_name, name, (port_name_size/2)-2);
                    strcat(port_name, ":");
                    strncat(port_name, rdf_descriptor->Ports[i].Name, port_name_size/2);
                }
                else
                    strncpy(port_name, rdf_descriptor->Ports[i].Name, port_name_size);
            }

            if (LV2_IS_PORT_AUDIO(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                {
                    j = ain.count++;
                    ain.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
                    ain.rindexes[j] = i;
                }
                else if (LV2_IS_PORT_OUTPUT(PortType))
                {
                    j = aout.count++;
                    aout.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
                    aout.rindexes[j] = i;
                    needs_cin = true;
                }
                else
                    qWarning("WARNING - Got a broken Port (Audio, but not input or output)");
            }
            else if (LV2_IS_PORT_CONTROL(PortType))
            {
                j = param.count++;
                param.data[j].index  = j;
                param.data[j].rindex = i;
                param.data[j].hints  = 0;
                param.data[j].midi_channel = 0;
                param.data[j].midi_cc = -1;

                double min, max, def, step, step_small, step_large;

                // min value
                if (LV2_HAVE_MINIMUM_PORT_POINT(PortPoints.Hints))
                    min = PortPoints.Minimum;
                else
                    min = 0.0;

                // max value
                if (LV2_HAVE_MAXIMUM_PORT_POINT(PortPoints.Hints))
                    max = PortPoints.Maximum;
                else
                    max = 1.0;

                if (min > max)
                    max = min;
                else if (max < min)
                    min = max;

                // default value
                if (LV2_HAVE_DEFAULT_PORT_POINT(PortPoints.Hints))
                    def = PortPoints.Default;
                else
                {
                    // no default value
                    if (min < 0.0 && max > 0.0)
                        def = 0.0;
                    else
                        def = min;
                }

                if (def < min)
                    def = min;
                else if (def > max)
                    def = max;

                if (max - min == 0.0)
                {
                    qWarning("Broken plugin parameter -> max - min == 0");
                    max = min + 0.1;
                }

                if (LV2_IS_PORT_SAMPLE_RATE(PortProps))
                {
                    double sample_rate = get_sample_rate();
                    min *= sample_rate;
                    max *= sample_rate;
                    def *= sample_rate;
                    param.data[j].hints |= PARAMETER_USES_SAMPLERATE;
                }

                if (LV2_IS_PORT_INTEGER(PortProps))
                {
                    step = 1.0;
                    step_small = 1.0;
                    step_large = 10.0;
                }
                else if (LV2_IS_PORT_TOGGLED(PortProps))
                {
                    step = max - min;
                    step_small = step;
                    step_large = step;
                }
                else
                {
                    double range = max - min;
                    step = range/100.0;
                    step_small = range/1000.0;
                    step_large = range/10.0;
                }

                if (LV2_IS_PORT_INPUT(PortType))
                {
                    param.data[j].type = PARAMETER_INPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;
                    param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                    needs_cin = true;

                    // MIDI CC value
                    LV2_RDF_PortMidiMap* PortMidiMap = &rdf_descriptor->Ports[i].MidiMap;
                    if (LV2_IS_PORT_MIDI_MAP_CC(PortMidiMap->Type))
                        param.data[j].midi_cc = PortMidiMap->Number;
                }
                else if (LV2_IS_PORT_OUTPUT(PortType))
                {
                    param.data[j].type = PARAMETER_OUTPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;

                    if (LV2_IS_PORT_LATENCY(PortProps) == false)
                    {
                        param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                        needs_cout = true;
                    }
                    else
                    {
                        // latency parameter
                        min = 0;
                        max = get_sample_rate();
                        def = 0;
                        step = 1;
                        step_small = 1;
                        step_large = 1;
                    }
                }
                else
                {
                    param.data[j].type = PARAMETER_UNKNOWN;
                    qWarning("WARNING - Got a broken Port (Control, but not input or output)");
                }

                // extra plugin hints
                if (LV2_IS_PORT_ENUMERATION(PortProps))
                    param.data[j].hints |= PARAMETER_USES_SCALEPOINTS;

                if (LV2_IS_PORT_HAS_STRICT_BOUNDS(PortProps) || (force_strict_bounds && LV2_IS_PORT_OUTPUT(PortType)))
                    param.data[j].hints |= PARAMETER_HAS_STRICT_BOUNDS;

                // check if parameter is not automable
                if (LV2_IS_PORT_HAS_STRICT_BOUNDS(PortProps))
                {
                    //param.data[j].hints ~= ~PARAMETER_IS_AUTOMABLE;
                }

                param.ranges[j].min = min;
                param.ranges[j].max = max;
                param.ranges[j].def = def;
                param.ranges[j].step = step;
                param.ranges[j].step_small = step_small;
                param.ranges[j].step_large = step_large;

                // Start parameters in their default values
                param.buffers[j] = def;

                descriptor->connect_port(handle, i, &param.buffers[j]);
            }
            else if (LV2_IS_PORT_CV(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                {
                    qWarning("WARNING - CV Ports are not supported yet");
                }
                else if (LV2_IS_PORT_OUTPUT(PortType))
                {
                    qWarning("WARNING - CV Ports are not supported yet");
                }
                else
                    qWarning("WARNING - Got a broken Port (CV, but not input or output)");

                descriptor->connect_port(handle, i, nullptr);
            }
            else if (LV2_IS_PORT_EVENT(PortType))
            {
                if (LV2_IS_PORT_INPUT(PortType))
                {
                    j = evin.count++;
                    descriptor->connect_port(handle, i, evin.events[j].buffer);

                    if (LV2_IS_PORT_EVENT_MIDI(PortType))
                    {
                        min.count++;
                        evin.events[j].type |= CARLA_URI_MAP_ID_EVENT_MIDI;
                        evin.events[j].port  = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
                    }
                    if (LV2_IS_PORT_EVENT_TIME(PortType))
                    {
                        evin.events[j].type |= CARLA_URI_MAP_ID_EVENT_TIME;
                        wants_time_pos = true;
                    }
                }
                else if (LV2_IS_PORT_OUTPUT(PortType))
                {
                    j = evout.count++;
                    descriptor->connect_port(handle, i, evout.events[j].buffer);

                    if (LV2_IS_PORT_EVENT_MIDI(PortType))
                    {
                        mout.count++;
                        evout.events[j].type |= CARLA_URI_MAP_ID_EVENT_MIDI;
                        evout.events[j].port = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
                    }
                    if (LV2_IS_PORT_EVENT_TIME(PortType))
                    {
                        evout.events[j].type |= CARLA_URI_MAP_ID_EVENT_TIME;
                    }
                }
                else
                    qWarning("WARNING - Got a broken Port (Event, but not input or output)");
            }
            else
                // Port Type not supported, but it's optional anyway
                descriptor->connect_port(handle, i, nullptr);
        }

        if (needs_cin)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-in");
            }
            else
                strcpy(port_name, "control-in");

            param.port_in = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        if (needs_cout)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-out");
            }
            else
                strcpy(port_name, "control-out");

            param.port_out = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
        }

        ain.count   = ains;
        aout.count  = aouts;
        evin.count  = ev_ins;
        evout.count = ev_outs;
        param.count = params;

        reload_programs(true);

        // Check if plugin is synth
        if (LV2_IS_GENERATOR(rdf_descriptor->Type))
            hints |= PLUGIN_IS_SYNTH;

        // Other plugin checks
        if (aouts > 0 && (ains == aouts || ains == 1))
            hints |= PLUGIN_CAN_DRYWET;

        if (aouts > 0)
            hints |= PLUGIN_CAN_VOL;

        if (aouts >= 2 && aouts%2 == 0)
            hints |= PLUGIN_CAN_BALANCE;

        carla_proc_lock();
        id = _id;
        carla_proc_unlock();

        if (carla_options.global_jack_client == false)
            jack_activate(jack_client);
    }

    void reload_programs(bool init)
    {
        qDebug("Lv2AudioPlugin::reload_programs(%s)", bool2str(init));
        uint32_t i;

        // Delete old programs
        if (prog.count > 0)
        {
            for (uint32_t i=0; i < prog.count; i++)
                free((void*)prog.names[i]);

            delete[] prog.names;
        }
        prog.count = 0;

        // Query new programs
        prog.count = rdf_descriptor->PresetCount;

        if (prog.count > 0)
            prog.names = new const char* [prog.count];

        // Update names
        for (i=0; i < prog.count; i++)
            prog.names[i] = strdup(rdf_descriptor->Presets[i].Label);

        // Update OSC Names
        osc_send_set_program_count(&osc.data, id, prog.count);

        if (prog.count > 0)
        {
            for (i=0; i < prog.count; i++)
                osc_send_set_program_name(&osc.data, id, i, prog.names[i]);
        }

        callback_action(CALLBACK_RELOAD_PROGRAMS, id, 0, 0, 0.0);

        if (init)
        {
            if (prog.count > 0)
                set_program(0, false, false, false, true);
        }

        // (LV2 programs don't change during runtime)
    }

    void prepare_for_save()
    {
        if ((hints & PLUGIN_HAS_EXTENSION_STATE) > 0 && descriptor->extension_data)
        {
            LV2_State_Interface* state = (LV2_State_Interface*)descriptor->extension_data(LV2_STATE_INTERFACE_URI);

            if (state)
                state->save(handle, carla_lv2_state_store, this, 0, features);
        }
    }

    void process(jack_nframes_t nframes)
    {
        uint32_t i, k;
        unsigned short plugin_id = id;
        uint32_t midi_event_count = 0;
        LV2_Event_Iterator evin_iters[evin.count];

        double ains_peak_tmp[2]  = { 0.0 };
        double aouts_peak_tmp[2] = { 0.0 };

        jack_default_audio_sample_t* ains_buffer[ain.count];
        jack_default_audio_sample_t* aouts_buffer[aout.count];
        jack_default_audio_sample_t* evins_buffer[evin.count];
        jack_default_audio_sample_t* evouts_buffer[evout.count];

        for (i=0; i < ain.count; i++)
            ains_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(ain.ports[i], nframes);

        for (i=0; i < aout.count; i++)
            aouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(aout.ports[i], nframes);

        for (i=0; i < evin.count; i++)
        {
            lv2_event_buffer_reset(evin.events[i].buffer, LV2_EVENT_AUDIO_STAMP, (uint8_t*)(evin.events[i].buffer + 1));
            lv2_event_begin(&evin_iters[i], evin.events[i].buffer);

            if (evin.events[i].port)
                evins_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(evin.events[i].port, nframes);
            else
                evins_buffer[i] = nullptr;
        }

        for (i=0; i < evout.count; i++)
        {
            lv2_event_buffer_reset(evout.events[i].buffer, LV2_EVENT_AUDIO_STAMP, (uint8_t*)(evout.events[i].buffer + 1));

            if (evout.events[i].port)
                evouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(evout.events[i].port, nframes);
            else
                evouts_buffer[i] = nullptr;
        }

        // --------------------------------------------------------------------------------------------------------
        // Input VU

        if (ain.count > 0)
        {
            short j2 = (ain.count == 1) ? 0 : 1;

            for (k=0; k<nframes; k++)
            {
                if (ains_buffer[0][k] > ains_peak_tmp[0])
                    ains_peak_tmp[0] = ains_buffer[0][k];
                if (ains_buffer[j2][k] > ains_peak_tmp[1])
                    ains_peak_tmp[1] = ains_buffer[j2][k];
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Parameters Input [Automation]

        if (param.port_in)
        {
            jack_default_audio_sample_t* pin_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_in, nframes);

            jack_midi_event_t pin_event;
            uint32_t n_pin_events = jack_midi_get_event_count(pin_buffer);

            for (i=0; i<n_pin_events; i++)
            {
                if (jack_midi_event_get(&pin_event, pin_buffer, i) != 0)
                    break;

                unsigned char channel = pin_event.buffer[0] & 0x0F;
                unsigned char mode    = pin_event.buffer[0] & 0xF0;

                // Status change
                if (mode == 0xB0)
                {
                    unsigned char status  = pin_event.buffer[1] & 0x7F;
                    unsigned char velo    = pin_event.buffer[2] & 0x7F;
                    double value, velo_per = double(velo)/127;

                    // Control GUI stuff (channel 0 only)
                    if (channel == 0)
                    {
                        if (status == 0x78)
                        {
                            // All Sound Off
                            set_active(false, false, false);
                            postpone_event(PostEventParameter, PARAMETER_ACTIVE, 0.0);
                            break;
                        }
                        else if (status == 0x09 && hints & PLUGIN_CAN_DRYWET)
                        {
                            // Dry/Wet (using '0x09', undefined)
                            set_drywet(velo_per, false, false);
                            postpone_event(PostEventParameter, PARAMETER_DRYWET, velo_per);
                        }
                        else if (status == 0x07 && hints & PLUGIN_CAN_VOL)
                        {
                            // Volume
                            value = double(velo)/100;
                            set_vol(value, false, false);
                            postpone_event(PostEventParameter, PARAMETER_VOLUME, value);
                        }
                        else if (status == 0x08 && hints & PLUGIN_CAN_BALANCE)
                        {
                            // Balance
                            double left, right;
                            value = (double(velo)-63.5)/63.5;

                            if (value < 0)
                            {
                                left  = -1.0;
                                right = (value*2)+1.0;
                            }
                            else if (value > 0)
                            {
                                left  = (value*2)-1.0;
                                right = 1.0;
                            }
                            else
                            {
                                left  = -1.0;
                                right = 1.0;
                            }

                            set_balance_left(left, false, false);
                            set_balance_right(right, false, false);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_LEFT, left);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_RIGHT, right);
                        }
                    }

                    // Control plugin parameters
                    for (k=0; k < param.count; k++)
                    {
                        if (param.data[k].type == PARAMETER_INPUT && (param.data[k].hints & PARAMETER_IS_AUTOMABLE) > 0 &&
                                param.data[k].midi_channel == channel && param.data[k].midi_cc == status)
                        {
                            value = (velo_per * (param.ranges[k].max - param.ranges[k].min)) + param.ranges[k].min;
                            set_parameter_value(k, value, false, false, false);
                            postpone_event(PostEventParameter, k, value);
                        }
                    }
                }
                // Program change
                else if (mode == 0xC0)
                {
                    unsigned char program = pin_event.buffer[1] & 0x7F;

                    if (program < prog.count)
                    {
                        set_program(program, false, false, false, false);
                        postpone_event(PostEventProgram, program, 0.0);
                    }
                }
            }
        } // End of Parameters Input

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (External)

        if (min.count > 0)
        {
            carla_midi_lock();

            for (i=0; i<MAX_MIDI_EVENTS && midi_event_count < MAX_MIDI_EVENTS; i++)
            {
                if (ExternalMidiNotes[i].valid)
                {
                    if (ExternalMidiNotes[i].plugin_id == plugin_id)
                    {
                        ExternalMidiNote* enote = &ExternalMidiNotes[i];
                        enote->valid = false;

                        for (i=0; i < evin.count; i++)
                        {
                            if (evin.events[i].type & CARLA_URI_MAP_ID_EVENT_MIDI)
                            {
                                uint8_t* midi_event = lv2_event_reserve(&evin_iters[i], 0, 0, CARLA_URI_MAP_ID_EVENT_MIDI, 3);

                                if (midi_event)
                                {
                                    midi_event[0] = enote->onoff ? 0x90 : 0x80;
                                    midi_event[1] = enote->note;
                                    midi_event[2] = enote->velo;
                                }
                            }
                        }

                        midi_event_count += 1;
                    }
                }
                else
                    break;
            }
            carla_midi_unlock();

        } // End of MIDI Input (External)

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (JACK)

        for (i=0; i < evin.count; i++)
        {
            if (!evins_buffer[i])
                continue;

            uint32_t n_min_events = jack_midi_get_event_count(evins_buffer[i]);
            jack_midi_event_t min_event;

            for (k=0; k<n_min_events && midi_event_count < MAX_MIDI_EVENTS; k++)
            {
                if (jack_midi_event_get(&min_event, evins_buffer[i], k) != 0)
                    break;

                //unsigned char channel = min_event.buffer[0] & 0x0F;
                unsigned char mode = min_event.buffer[0] & 0xF0;
                unsigned char note = min_event.buffer[1] & 0x7F;
                unsigned char velo = min_event.buffer[2] & 0x7F;

                // fix bad note off (GUI only)
                if (mode == 0x90 && velo == 0)
                {
                    mode = 0x80;
                    velo = 64;
                }

                if (mode == 0x80)
                    postpone_event(PostEventNoteOff, note, velo);
                else if (mode == 0x90)
                    postpone_event(PostEventNoteOn, note, velo);

                lv2_event_write(&evin_iters[i], min_event.time, 0, CARLA_URI_MAP_ID_EVENT_MIDI, min_event.size, min_event.buffer);

                midi_event_count += 1;
            }
        } // End of MIDI Input (JACK)

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // LV2 Transport

        if (wants_time_pos)
        {
            jack_position_t pos;
            jack_transport_state_t state = jack_transport_query(jack_client, &pos);

            carla_lv2_time_pos.frame = 0;
            carla_lv2_time_pos.flags = 0;
            carla_lv2_time_pos.bar   = 0;
            carla_lv2_time_pos.beat  = 0;
            carla_lv2_time_pos.tick  = 0;
            carla_lv2_time_pos.beats_per_bar    = 0;
            carla_lv2_time_pos.beat_type        = 0;
            carla_lv2_time_pos.ticks_per_beat   = 0;
            carla_lv2_time_pos.beats_per_minute = 0.0;

            if (state == JackTransportRolling)
                carla_lv2_time_pos.state = LV2_TIME_ROLLING;
            else
                carla_lv2_time_pos.state = LV2_TIME_STOPPED;

            if (pos.unique_1 == pos.unique_2)
            {
                carla_lv2_time_pos.frame = pos.frame;

                if (pos.valid & JackPositionBBT)
                {
                    carla_lv2_time_pos.bar  = pos.bar;
                    carla_lv2_time_pos.beat = pos.beat;
                    carla_lv2_time_pos.tick = pos.tick;
                    carla_lv2_time_pos.beats_per_bar    = pos.beats_per_bar;
                    carla_lv2_time_pos.beat_type        = pos.beat_type;
                    carla_lv2_time_pos.ticks_per_beat   = pos.ticks_per_beat;
                    carla_lv2_time_pos.beats_per_minute = pos.beats_per_minute;

                    carla_lv2_time_pos.flags |= LV2_TIME_HAS_BBT;
                }
            }

            for (i=0; i < evin.count; i++)
            {
                if (evin.events[i].type & CARLA_URI_MAP_ID_EVENT_TIME)
                    lv2_event_write(&evin_iters[i], 0, 0, CARLA_URI_MAP_ID_EVENT_TIME, sizeof(LV2_Time_Position), (uint8_t*)&carla_lv2_time_pos);
            }
        }

        // End of LV2 Transport

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Plugin processing

        if (active)
        {
            if (!active_before)
            {
                if (descriptor->activate)
                    descriptor->activate(handle);
            }

            for (i=0; i < ain.count; i++)
                descriptor->connect_port(handle, ain.rindexes[i], ains_buffer[i]);

            for (i=0; i < aout.count; i++)
                descriptor->connect_port(handle, aout.rindexes[i], aouts_buffer[i]);

            if (descriptor->run)
                descriptor->run(handle, nframes);
        }
        else
        {
            if (active_before)
            {
                if (descriptor->deactivate)
                    descriptor->deactivate(handle);
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Post-processing (dry/wet, volume and balance)

        if (active)
        {
            double bal_rangeL, bal_rangeR;
            jack_default_audio_sample_t old_bal_left[nframes];

            for (i=0; i < aout.count; i++)
            {
                // Dry/Wet and Volume
                for (k=0; k<nframes; k++)
                {
                    if (hints & PLUGIN_CAN_DRYWET && x_drywet != 1.0)
                    {
                        if (aout.count == 1)
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[0][k]*(1.0-x_drywet));
                        else
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[i][k]*(1.0-x_drywet));
                    }

                    if (hints & PLUGIN_CAN_VOL)
                        aouts_buffer[i][k] *= x_vol;
                }

                // Balance
                if (hints & PLUGIN_CAN_BALANCE)
                {
                    if (i%2 == 0)
                        memcpy(&old_bal_left, aouts_buffer[i], sizeof(jack_default_audio_sample_t)*nframes);

                    bal_rangeL = (x_bal_left+1.0)/2;
                    bal_rangeR = (x_bal_right+1.0)/2;

                    for (k=0; k<nframes; k++)
                    {
                        if (i%2 == 0)
                        {
                            // left output
                            aouts_buffer[i][k]  = old_bal_left[k]*(1.0-bal_rangeL);
                            aouts_buffer[i][k] += aouts_buffer[i+1][k]*(1.0-bal_rangeR);
                        }
                        else
                        {
                            // right
                            aouts_buffer[i][k]  = aouts_buffer[i][k]*bal_rangeR;
                            aouts_buffer[i][k] += old_bal_left[k]*bal_rangeL;
                        }
                    }
                }

                // Output VU
                if (i < 2)
                {
                    for (k=0; k<nframes; k++)
                    {
                        if (aouts_buffer[i][k] > aouts_peak_tmp[i])
                            aouts_peak_tmp[i] = aouts_buffer[i][k];
                    }
                }
            }
        }
        else
        {
            // disable any output sound if not active
            for (i=0; i < aout.count; i++)
                memset(aouts_buffer[i], 0.0f, sizeof(jack_default_audio_sample_t)*nframes);

            aouts_peak_tmp[0] = 0.0;
            aouts_peak_tmp[1] = 0.0;

        } // End of Post-processing

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Control Output

        if (param.port_out)
        {
            jack_default_audio_sample_t* cout_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_out, nframes);
            jack_midi_clear_buffer(cout_buffer);

            double value_per;

            for (k=0; k < param.count; k++)
            {
                if (param.data[k].type == PARAMETER_OUTPUT && param.data[k].midi_cc >= 0)
                {
                    value_per = (param.buffers[k] - param.ranges[k].min)/(param.ranges[k].max - param.ranges[k].min);

                    jack_midi_data_t* event_buffer = jack_midi_event_reserve(cout_buffer, 0, 3);
                    event_buffer[0] = 0xB0 + param.data[k].midi_channel;
                    event_buffer[1] = param.data[k].midi_cc;
                    event_buffer[2] = 127*value_per;
                }
            }
        } // End of Control Output

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Output

        for (i=0; i < evout.count; i++)
        {
            if (!evouts_buffer[i])
                continue;

            jack_midi_clear_buffer(evouts_buffer[i]);

            LV2_Event* ev;
            uint8_t* data;

            LV2_Event_Iterator iter;
            lv2_event_begin(&iter, evout.events[i].buffer);

            for (k=0; k < iter.buf->event_count; k++)
            {
                ev = lv2_event_get(&iter, &data);
                if (ev && data)
                    jack_midi_event_write(evouts_buffer[i], ev->frames, data, ev->size);

                lv2_event_increment(&iter);
            }
        } // End of MIDI Output

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Peak Values

        ains_peak[(plugin_id*2)+0]  = ains_peak_tmp[0];
        ains_peak[(plugin_id*2)+1]  = ains_peak_tmp[1];
        aouts_peak[(plugin_id*2)+0] = aouts_peak_tmp[0];
        aouts_peak[(plugin_id*2)+1] = aouts_peak_tmp[1];

        active_before = active;
    }

    void buffer_size_changed(jack_nframes_t)
    {
    }

    // ----------------- URI-Map Feature -------------------------------------------------
    static uint32_t carla_lv2_uri_to_id(LV2_URI_Map_Callback_Data data, const char* map, const char* uri)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_uri_to_id(%p, %s, %s)", data, map, uri);

        if (map && strcmp(map, LV2_EVENT_URI) == 0)
        {
            // Event types
            if (strcmp(uri, "http://lv2plug.in/ns/ext/midi#MidiEvent") == 0)
                return CARLA_URI_MAP_ID_EVENT_MIDI;
            else if (strcmp(uri, "http://lv2plug.in/ns/ext/time#Position") == 0)
                return CARLA_URI_MAP_ID_EVENT_TIME;
        }
        else if (strcmp(uri, NS_ATOM "String") == 0)
        {
            return CARLA_URI_MAP_ID_ATOM_STRING;
        }

        // Custom types
        if (data)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)data;
            return plugin->get_custom_uri_id(uri);
        }

        return 0;
    }

    // ----------------- URID Feature ----------------------------------------------------
    static LV2_URID carla_lv2_urid_map(LV2_URID_Map_Handle handle, const char* uri)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_urid_map(%p, %s)", handle, uri);

        if (strcmp(uri, "http://lv2plug.in/ns/ext/midi#MidiEvent") == 0)
            return CARLA_URI_MAP_ID_EVENT_MIDI;
        else if (strcmp(uri, "http://lv2plug.in/ns/ext/time#Position") == 0)
            return CARLA_URI_MAP_ID_EVENT_TIME;
        else if (strcmp(uri, NS_ATOM "String") == 0)
            return CARLA_URI_MAP_ID_ATOM_STRING;

        // Custom types
        if (handle)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)handle;
            return plugin->get_custom_uri_id(uri);
        }

        return 0;
    }

    static const char* carla_lv2_urid_unmap(LV2_URID_Map_Handle handle, LV2_URID urid)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_urid_unmap(%p, %i)", handle, urid);

        if (urid == CARLA_URI_MAP_ID_EVENT_MIDI)
            return "http://lv2plug.in/ns/ext/midi#MidiEvent";
        else if (urid == CARLA_URI_MAP_ID_EVENT_TIME)
            return "http://lv2plug.in/ns/ext/time#Position";
        else if (urid == CARLA_URI_MAP_ID_ATOM_STRING)
            return NS_ATOM "String";

        // Custom types
        if (handle)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)handle;
            return plugin->get_custom_uri_string(urid);
        }

        return nullptr;
    }
    
    // ----------------- DynParam Feature ------------------------------------------------
    static unsigned char carla_lv2_dynparam_group_appear(void* instance_host_context,
                                                         void* parent_group_host_context,
                                                         lv2dynparam_group_handle group,
                                                         const lv2dynparam_hints* hints_ptr,
                                                         void** group_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_group_appear(%p, %p, %p, %p, %p) - %i",
                 instance_host_context, parent_group_host_context, group, hints_ptr, group_host_context, hints_ptr->count);
        
        for (unsigned char i=0; i < hints_ptr->count; i++)
            qWarning("Lv2AudioPlugin::carla_lv2_dynparam_group_appear -> %s | %s", hints_ptr->names[i], hints_ptr->values[i]);

        return 1;
    }

    static unsigned char carla_lv2_dynparam_group_disappear(void* instance_host_context, void* group_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_group_disappear(%p, %p)", instance_host_context, group_host_context);
        return 1;
    }

    static unsigned char carla_lv2_dynparam_parameter_appear(void* instance_host_context,
                                                             void* group_host_context,
                                                             lv2dynparam_parameter_handle parameter,
                                                             const lv2dynparam_hints* hints_ptr,
                                                             void** parameter_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_parameter_appear(%p, %p, %p, %p, %p) - %i",
                 instance_host_context, group_host_context, parameter, hints_ptr, parameter_host_context, hints_ptr->count);

        for (unsigned char i=0; i < hints_ptr->count; i++)
            qWarning("Lv2AudioPlugin::carla_lv2_dynparam_parameter_appear -> %s | %s", hints_ptr->names[i], hints_ptr->values[i]);

        return 1;
    }

    static unsigned char carla_lv2_dynparam_parameter_disappear(void* instance_host_context, void* parameter_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_parameter_disappear(%p, %p)", instance_host_context, parameter_host_context);
        return 1;
    }

    static unsigned char carla_lv2_dynparam_parameter_change(void* instance_host_context, void* parameter_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_parameter_change(%p, %p)", instance_host_context, parameter_host_context);
        return 1;
    }

    static unsigned char carla_lv2_dynparam_command_appear(void* instance_host_context,
                                                           void* group_host_context,
                                                           lv2dynparam_command_handle command,
                                                           const lv2dynparam_hints* hints_ptr,
                                                           void** command_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_command_appear(%p, %p, %p, %p, %p)",
                 instance_host_context, group_host_context, command, hints_ptr, command_host_context);
        return 1;
    }
    
    static unsigned char carla_lv2_dynparam_command_disappear(void* instance_host_context, void* command_host_context)
    {
        qWarning("Lv2AudioPlugin::carla_lv2_dynparam_(%p, %p)", instance_host_context, command_host_context);
        return 1;
    }
    
    // ----------------- Event Feature ---------------------------------------------------
    static uint32_t carla_lv2_event_ref(LV2_Event_Callback_Data data, LV2_Event* event)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_event_ref(%p, %p)", data, event);

        // TODO
        return 0;
    }

    static uint32_t carla_lv2_event_unref(LV2_Event_Callback_Data data, LV2_Event* event)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_event_unref(%p, %p)", data, event);

        // TODO
        return 0;
    }

    // ----------------- State Feature ---------------------------------------------------
    static int carla_lv2_state_store(LV2_State_Handle handle, uint32_t key, const void* value, size_t size, uint32_t type, uint32_t flags)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_state_store(%p, %i, %p, " P_SIZE ", %i, %i)", handle, key, value, size, type, flags);

        if (handle)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)handle;
            const char* uri_key = plugin->get_custom_uri_string(key);

            if (uri_key > 0 && (flags & LV2_STATE_IS_POD) > 0)
            {
                qDebug("Lv2AudioPlugin::carla_lv2_state_store(%p, %i, %p, " P_SIZE ", %i, %i) - Got uri_key and flags", handle, key, value, size, type, flags);

                CustomDataType dtype;

                if (type == CARLA_URI_MAP_ID_ATOM_STRING)
                    dtype = CUSTOM_DATA_STRING;
                else if (type >= CARLA_URI_MAP_ID_COUNT)
                    dtype = CUSTOM_DATA_BINARY;
                else
                    dtype = CUSTOM_DATA_INVALID;

                if (value && dtype != CUSTOM_DATA_INVALID)
                {
                    // Check if we already have this key
                    for (int i=0; i < plugin->custom.count(); i++)
                    {
                        if (strcmp(plugin->custom[i].key, uri_key) == 0)
                        {
                            free((void*)plugin->custom[i].value);

                            if (dtype == CUSTOM_DATA_STRING)
                                plugin->custom[i].value = strdup((const char*)value);
                            else
                            {
                                QByteArray chunk((const char*)value, size);
                                plugin->custom[i].value = strdup(chunk.toBase64().data());
                            }

                            return 0;
                        }
                    }

                    // Add a new one then
                    CustomData new_data;
                    new_data.type = dtype;
                    new_data.key  = strdup(uri_key);

                    if (dtype == CUSTOM_DATA_STRING)
                        new_data.value = strdup((const char*)value);
                    else
                    {
                        QByteArray chunk((const char*)value, size);
                        new_data.value = strdup(chunk.toBase64().data());
                    }

                    plugin->custom.append(new_data);

                    return 0;
                }
                else
                    qCritical("Lv2AudioPlugin::carla_lv2_state_store(%p, %i, %p, " P_SIZE ", %i, %i) - Invalid data", handle, key, value, size, type, flags);
            }
            else
                qWarning("Lv2AudioPlugin::carla_lv2_state_store(%p, %i, %p, " P_SIZE ", %i, %i) - Invalid attributes", handle, key, value, size, type, flags);
        }
        else
            qCritical("Lv2AudioPlugin::carla_lv2_state_store(%p, %i, %p, " P_SIZE ", %i, %i) - Invalid handle", handle, key, value, size, type, flags);

        return 1;
    }

    static const void* carla_lv2_state_retrieve(LV2_State_Handle handle, uint32_t key, size_t* size, uint32_t* type, uint32_t* flags)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_state_retrieve(%p, %i, %p, %p, %p)", handle, key, size, type, flags);

        if (handle)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)handle;
            const char* uri_key = plugin->get_custom_uri_string(key);

            if (uri_key)
            {
                const char* string_data = nullptr;
                CustomDataType dtype = CUSTOM_DATA_INVALID;

                for (int i=0; i < plugin->custom.count(); i++)
                {
                    if (strcmp(plugin->custom[i].key, uri_key) == 0)
                    {
                        dtype = plugin->custom[i].type;
                        string_data = plugin->custom[i].value;
                        break;
                    }
                }

                if (string_data)
                {
                    *size  = 0;
                    *type  = 0;
                    *flags = 0;

                    if (dtype == CUSTOM_DATA_STRING)
                    {
                        *type = CARLA_URI_MAP_ID_ATOM_STRING;
                        return string_data;
                    }
                    else if (dtype == CUSTOM_DATA_BINARY)
                    {
                        static QByteArray chunk;
                        chunk = QByteArray::fromBase64(string_data);

                        *size = chunk.size();
                        *type = key;
                        return chunk.data();
                    }
                    else
                        qCritical("Lv2AudioPlugin::carla_lv2_state_retrieve(%p, %i, %p, %p, %p) - Invalid key type", handle, key, size, type, flags);
                }
                else
                    qCritical("Lv2AudioPlugin::carla_lv2_state_retrieve(%p, %i, %p, %p, %p) - Invalid key", handle, key, size, type, flags);
            }
            else
                qCritical("Lv2AudioPlugin::carla_lv2_state_retrieve(%p, %i, %p, %p, %p) - Failed to find key", handle, key, size, type, flags);
        }
        else
            qCritical("Lv2AudioPlugin::carla_lv2_state_retrieve(%p, %i, %p, %p, %p) - Invalid handle", handle, key, size, type, flags);

        return nullptr;
    }

#if 0
    static char* carla_lv2_state_get_abstract_path(LV2_State_Map_Path_Handle handle, const char* absolute_path)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_state_get_abstract_path(%p, %s)", handle, absolute_path);
        return strdup("");
    }

    static char* carla_lv2_state_get_absolute_path(LV2_State_Map_Path_Handle handle, const char* abstract_path)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_state_get_absolute_path(%p, %s)", handle, abstract_path);
        return strdup("");
    }

    static char* carla_lv2_state_make_path(LV2_State_Make_Path_Handle handle, const char* path)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_state_make_path(%p, %s)", handle, path);
        return strdup("");
    }
#endif

    // ----------------- External UI Feature ---------------------------------------------
    static void carla_lv2_external_ui_closed(LV2UI_Controller controller)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_external_ui_closed(%p)", controller);

        if (controller)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)controller;
            plugin->gui.visible = false;

            if (plugin->ui.descriptor->cleanup)
                plugin->ui.descriptor->cleanup(plugin->ui.handle);

            plugin->ui.handle = nullptr;
            callback_action(CALLBACK_SHOW_GUI, plugin->id, 0, 0, 0.0);
        }
    }

    // ----------------- UI Resize Feature -----------------------------------------------
    static int carla_lv2_ui_resize(LV2_UI_Resize_Feature_Data data, int width, int height)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_ui_resized(%p, %i, %i)", data, width, height);

        if (data)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)data;
            plugin->gui.width  = width;
            plugin->gui.height = height;
            callback_action(CALLBACK_RESIZE_GUI, plugin->id, width, height, 0.0);
            return 0;
        }

        return 1;
    }

    // ----------------- UI Extension ----------------------------------------------------
    static void carla_lv2_ui_write_function(LV2UI_Controller controller, uint32_t port_index, uint32_t buffer_size, uint32_t format, const void* buffer)
    {
        qDebug("Lv2AudioPlugin::carla_lv2_ui_write_function(%p, %i, %i, %i, %p)", controller, port_index, buffer_size, format, buffer);

        if (controller)
        {
            Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)controller;

            if (format == 0 && buffer_size == sizeof(float))
            {
                int32_t param_id = -1;
                float value = *(float*)buffer;

                for (uint32_t i=0; i < plugin->param.count; i++)
                {
                    if (plugin->param.data[i].rindex == (int32_t)port_index)
                    {
                        param_id = i; //plugin->param.data[i].index;
                        break;
                    }
                }

                if (param_id > -1) // && plugin->ctrl.data[param_id].type == PARAMETER_INPUT
                    plugin->set_parameter_value(param_id, value, false, true, true);
            }
        }
    }

    bool ui_lib_open(const char* filename)
    {
#ifdef __WINDOWS__
        ui.lib = LoadLibraryA(filename);
#else
        ui.lib = dlopen(filename, RTLD_NOW);
#endif
        return bool(ui.lib);
    }

    bool ui_lib_close()
    {
        if (ui.lib)
    #ifdef __WINDOWS__
            return FreeLibrary((HMODULE)ui.lib) != 0;
    #else
            return dlclose(ui.lib) != 0;
    #endif
        else
            return false;
    }

    void* ui_lib_symbol(const char* symbol)
    {
        if (ui.lib)
    #ifdef __WINDOWS__
            return (void*)GetProcAddress((HMODULE)ui.lib, symbol);
    #else
            return dlsym(ui.lib, symbol);
    #endif
        else
            return nullptr;
    }

    const char* ui_lib_error()
    {
    #ifdef __WINDOWS__
        return "Unknown error";
    #else
        return dlerror();
    #endif
    }
};

Lv2OscGuiThread::Lv2OscGuiThread(QObject* parent) :
    QThread(parent)
{
    plugin_id = -1;
    ui_type   = 0x0;
}

void Lv2OscGuiThread::set_plugin_id(short plugin_id_)
{
    plugin_id = plugin_id_;
}

void Lv2OscGuiThread::set_ui_type(LV2_Property ui_type_)
{
    ui_type = ui_type_;
}

void Lv2OscGuiThread::run()
{
    if (plugin_id < 0 || plugin_id > MAX_PLUGINS)
    {
        qCritical("Lv2OscGuiThread::run() - invalid plugin id '%i'", plugin_id);
        return;
    }

    Lv2AudioPlugin* plugin = (Lv2AudioPlugin*)AudioPlugins[plugin_id];

    if (!plugin)
    {
        qCritical("Lv2OscGuiThread::run() - invalid plugin");
        return;
    }

    if (plugin->id != plugin_id)
    {
        qCritical("Lv2OscGuiThread::run() - plugin mismatch '%i'' != '%i'", plugin->id, plugin_id);
        return;
    }

    const char* bridge_bin = nullptr;

    switch(ui_type)
    {
    case LV2_UI_X11:
        bridge_bin = carla_options.bridge_path_lv2_x11;
        break;
    case LV2_UI_GTK2:
        bridge_bin = carla_options.bridge_path_lv2_gtk2;
        break;
    case LV2_UI_QT4:
        bridge_bin = carla_options.bridge_path_lv2_qt4;
        break;
    default:
        qCritical("Lv2OscGuiThread::run() - invalid UI Type");
        return;
    }

    QString cmd = QString("%1 '%2' '%3 (GUI)' '%4' '%5' '%6' '%7/%8' %9").arg(bridge_bin).arg(plugin->descriptor->URI).arg(plugin->name).arg(plugin->ui.descriptor->URI).arg(
                plugin->ui.rdf_descriptor->Binary).arg(plugin->ui.rdf_descriptor->Bundle).arg(get_host_osc_url()).arg(plugin_id).arg(plugin->is_ui_resizable() ? "true" : "false");

    qDebug("Lv2OscGuiThread::run() - command: %s", cmd.toStdString().data());

    int ret;

    while (plugin && plugin->id == plugin_id && carla_is_engine_running())
    {
        if (plugin->gui.show_now)
        {
            ret = system(cmd.toStdString().data());
            plugin->gui.visible  = false;
            plugin->gui.show_now = false;

            if (ret == 0)
            {
                // Hide
                callback_action(CALLBACK_SHOW_GUI, plugin_id, 0, 0, 0.0);
            }
            else
            {
                // Kill
                callback_action(CALLBACK_SHOW_GUI, plugin_id, -1, 0, 0.0);
                qWarning("Lv2OscGuiThread::run() - LV2 OSC GUI crashed");
                break;
            }
        }
        else
            sleep(1);
    }

    if (plugin->gui.name)
        free((void*)plugin->gui.name);

    plugin->gui.name = nullptr;
    plugin->gui.visible = false;
}

short add_plugin_lv2(const char* filename, const char* label, void* extra_stuff)
{
    qDebug("add_plugin_lv2(%s, %s, %p)", filename, label, extra_stuff);

    int id = get_new_plugin_id();

    if (id >= 0)
    {
        LV2_RDF_Descriptor* rdf_descriptor = (LV2_RDF_Descriptor*)extra_stuff;

        if (rdf_descriptor)
        {
            Lv2AudioPlugin* plugin = new Lv2AudioPlugin;

            if ((plugin->lib_open(rdf_descriptor->Binary)))
            {
                LV2_Descriptor_Function descfn = (LV2_Descriptor_Function)plugin->lib_symbol("lv2_descriptor");

                if (descfn)
                {
                    uint32_t i = 0;
                    while ((plugin->descriptor = descfn(i++)))
                    {
                        if (strcmp(plugin->descriptor->URI, label) == 0)
                            break;
                    }

                    if (plugin->descriptor)
                    {
                        bool can_continue = true;

                        // Check supported ports
                        for (i=0; i < rdf_descriptor->PortCount; i++)
                        {
                            LV2_Property PortType = rdf_descriptor->Ports[i].Type;
                            if (!LV2_IS_PORT_AUDIO(PortType) && !LV2_IS_PORT_CONTROL(PortType) && !LV2_IS_PORT_CV(PortType) && !LV2_IS_PORT_EVENT(PortType))
                            {
                                if (!LV2_IS_PORT_OPTIONAL(rdf_descriptor->Ports[i].Properties))
                                {
                                    set_last_error("Plugin requires a port that is not currently supported");
                                    can_continue = false;
                                    break;
                                }
                            }
                        }

                        // Check supported features
                        for (i=0; i < rdf_descriptor->FeatureCount; i++)
                        {
                            if (is_lv2_feature_supported(rdf_descriptor->Features[i].URI) == false)
                            {
                                if (LV2_IS_FEATURE_REQUIRED(rdf_descriptor->Features[i].Type))
                                {
                                    QString msg = QString("Plugin requires a feature that is not supported:\n%1").arg(rdf_descriptor->Features[i].URI);
                                    set_last_error(msg.toStdString().data());
                                    can_continue = false;
                                    break;
                                }
                                else
                                    qDebug("Plugin wants a feature that is not supported:\n%s", rdf_descriptor->Features[i].URI);
                            }
                        }

                        // Check extensions
                        for (i=0; i < rdf_descriptor->ExtensionCount; i++)
                        {
                            if (strcmp(rdf_descriptor->Extensions[i], LV2_STATE_INTERFACE_URI) == 0)
                                plugin->hints |= PLUGIN_HAS_EXTENSION_STATE;
                            else if (strcmp(rdf_descriptor->Extensions[i], LV2DYNPARAM_URI) == 0)
                                plugin->hints |= PLUGIN_HAS_EXTENSION_DYNPARAM;
                            else
                                qDebug("Plugin has non-supported extension: '%s'", rdf_descriptor->Extensions[i]);
                        }

                        if (can_continue)
                        {
                            // Initialize features
                            LV2_URI_Map_Feature* URI_Map_Feature = new LV2_URI_Map_Feature;
                            URI_Map_Feature->callback_data       = plugin;
                            URI_Map_Feature->uri_to_id           = plugin->carla_lv2_uri_to_id;

                            LV2_URID_Map* URID_Map_Feature       = new LV2_URID_Map;
                            URID_Map_Feature->handle             = plugin;
                            URID_Map_Feature->map                = plugin->carla_lv2_urid_map;

                            LV2_URID_Unmap* URID_Unmap_Feature   = new LV2_URID_Unmap;
                            URID_Unmap_Feature->handle           = plugin;
                            URID_Unmap_Feature->unmap            = plugin->carla_lv2_urid_unmap;

                            LV2_Event_Feature* Event_Feature     = new LV2_Event_Feature;
                            Event_Feature->callback_data         = plugin;
                            Event_Feature->lv2_event_ref         = plugin->carla_lv2_event_ref;
                            Event_Feature->lv2_event_unref       = plugin->carla_lv2_event_unref;

#if 0
                            LV2_State_Map_Path* State_Map_Path_Feature = new LV2_State_Map_Path;
                            State_Map_Path_Feature->handle        = plugin;
                            State_Map_Path_Feature->abstract_path = plugin->carla_lv2_state_get_abstract_path;
                            State_Map_Path_Feature->absolute_path = plugin->carla_lv2_state_get_absolute_path;

                            LV2_State_Make_Path* State_Make_Path_Feature = new LV2_State_Make_Path;
                            State_Make_Path_Feature->handle       = plugin;
                            State_Make_Path_Feature->path         = plugin->carla_lv2_state_make_path;
#endif

                            plugin->features[lv2_feature_id_uri_map]          = new LV2_Feature;
                            plugin->features[lv2_feature_id_uri_map]->URI     = LV2_URI_MAP_URI;
                            plugin->features[lv2_feature_id_uri_map]->data    = URI_Map_Feature;

                            plugin->features[lv2_feature_id_urid_map]         = new LV2_Feature;
                            plugin->features[lv2_feature_id_urid_map]->URI    = LV2_URID_MAP_URI;
                            plugin->features[lv2_feature_id_urid_map]->data   = URID_Map_Feature;

                            plugin->features[lv2_feature_id_urid_unmap]       = new LV2_Feature;
                            plugin->features[lv2_feature_id_urid_unmap]->URI  = LV2_URID_UNMAP_URI;
                            plugin->features[lv2_feature_id_urid_unmap]->data = URID_Unmap_Feature;

                            plugin->features[lv2_feature_id_event]            = new LV2_Feature;
                            plugin->features[lv2_feature_id_event]->URI       = LV2_EVENT_URI;
                            plugin->features[lv2_feature_id_event]->data      = Event_Feature;

                            /* initialize rtsafe mempool host feature */
                            rtmempool_allocator_init(&plugin->rtmempool_allocator);

                            plugin->features[lv2_feature_id_rtmempool]        = new LV2_Feature;
                            plugin->features[lv2_feature_id_rtmempool]->URI   = LV2_RTSAFE_MEMORY_POOL_URI;
                            plugin->features[lv2_feature_id_rtmempool]->data  = &plugin->rtmempool_allocator;

#if 0
                            plugin->features[lv2_feature_id_state_map_path]        = new LV2_Feature;
                            plugin->features[lv2_feature_id_state_map_path]->URI   = LV2_STATE_MAP_PATH_URI;
                            plugin->features[lv2_feature_id_state_map_path]->data  = State_Map_Path_Feature;

                            plugin->features[lv2_feature_id_state_make_path]       = new LV2_Feature;
                            plugin->features[lv2_feature_id_state_make_path]->URI  = LV2_STATE_MAKE_PATH_URI;
                            plugin->features[lv2_feature_id_state_make_path]->data = State_Make_Path_Feature;
#endif

                            plugin->handle = plugin->descriptor->instantiate(plugin->descriptor, get_sample_rate(), rdf_descriptor->Bundle, plugin->features);

                            if (plugin->handle)
                            {
                                plugin->name = get_unique_name(rdf_descriptor->Name);
                                plugin->filename = strdup(filename);

                                plugin->rdf_descriptor = lv2_rdf_dup(rdf_descriptor);

                                if (carla_options.global_jack_client)
                                    plugin->jack_client = carla_jack_client;
                                else
                                    carla_jack_register_plugin(plugin);

                                if (plugin->jack_client)
                                {
                                    plugin->reload();

                                    plugin->id = id;
                                    unique_names[id] = plugin->name;
                                    AudioPlugins[id] = plugin;

                                    // ----------------- GUI Stuff -------------------------------------------------------

                                    uint32_t UICount = plugin->rdf_descriptor->UICount;

                                    if (UICount > 0)
                                    {
                                        // Find more appropriate UI (Qt4 -> X11 -> Gtk2 -> External, use bridges whenever possible)
                                        int eQt4, eX11, eGtk2, iX11, iQt4, iExt, iFinal;
                                        eQt4 = eX11 = eGtk2 = iQt4 = iX11 = iExt = iFinal = -1;

                                        for (i=0; i < UICount; i++)
                                        {
                                            switch(plugin->rdf_descriptor->UIs[i].Type)
                                            {
                                            case LV2_UI_QT4:
                                                if (plugin->is_ui_bridgeable(i))
                                                    eQt4 = i;
                                                else
                                                    iQt4 = i;
                                                break;

                                            case LV2_UI_X11:
                                                if (plugin->is_ui_bridgeable(i))
                                                    eX11 = i;
                                                else
                                                    iX11 = i;
                                                break;

                                            case LV2_UI_GTK2:
                                                eGtk2 = i;
                                                break;

                                            case LV2_UI_EXTERNAL:
                                            case LV2_UI_OLD_EXTERNAL:
                                                iExt = i;
                                                break;

                                            default:
                                                break;
                                            }
                                        }

                                        if (eQt4 >= 0)
                                            iFinal = eQt4;
                                        else if (eX11 >= 0)
                                            iFinal = eX11;
                                        else if (eGtk2 >= 0)
                                            iFinal = eGtk2;
                                        else if (iQt4 >= 0)
                                            iFinal = iQt4;
                                        else if (iX11 >= 0)
                                            iFinal = iX11;
                                        else if (iExt >= 0)
                                            iFinal = iExt;

                                        bool is_bridged = (iFinal == eQt4 || iFinal == eX11 || iFinal == eGtk2);

                                        // Use proper UI now
                                        if (iFinal >= 0)
                                        {
                                            plugin->ui.rdf_descriptor = &plugin->rdf_descriptor->UIs[iFinal];

                                            // Check supported UI features
                                            can_continue = true;

                                            for (i=0; i < plugin->ui.rdf_descriptor->FeatureCount; i++)
                                            {
                                                if (is_lv2_ui_feature_supported(plugin->ui.rdf_descriptor->Features[i].URI) == false)
                                                {
                                                    if (LV2_IS_FEATURE_REQUIRED(plugin->ui.rdf_descriptor->Features[i].Type))
                                                    {
                                                        qCritical("Plugin requires a feature that is not supported:\n%s", plugin->ui.rdf_descriptor->Features[i].URI);
                                                        can_continue = false;
                                                        break;
                                                    }
                                                    else
                                                        qDebug("UI wants a feature that is not supported:\n%s", plugin->ui.rdf_descriptor->Features[i].URI);
                                                }
                                            }

                                            if (can_continue)
                                            {
                                                if ((plugin->ui_lib_open(plugin->ui.rdf_descriptor->Binary)))
                                                {
                                                    LV2UI_DescriptorFunction ui_descfn = (LV2UI_DescriptorFunction)plugin->ui_lib_symbol("lv2ui_descriptor");

                                                    if (ui_descfn)
                                                    {
                                                        i = 0;
                                                        while ((plugin->ui.descriptor = ui_descfn(i++)))
                                                        {
                                                            if (strcmp(plugin->ui.descriptor->URI, plugin->ui.rdf_descriptor->URI) == 0)
                                                                break;
                                                        }

                                                        if (plugin->ui.descriptor)
                                                        {
                                                            // UI Window Title
                                                            char gui_name[strlen(plugin->name)+7];
                                                            sprintf(gui_name, "%s (GUI)", plugin->name);
                                                            plugin->gui.name = strdup(gui_name);

                                                            LV2_Property UiType = plugin->ui.rdf_descriptor->Type;

                                                            if (is_bridged)
                                                            {
                                                                Lv2OscGuiThread* lv2_thread = new Lv2OscGuiThread();
                                                                lv2_thread->set_plugin_id(plugin->id);
                                                                lv2_thread->set_ui_type(UiType);
                                                                lv2_thread->start();

                                                                plugin->gui.type   = GUI_EXTERNAL_OSC;
                                                                plugin->osc.thread = lv2_thread;

                                                                switch (UiType)
                                                                {
                                                                case LV2_UI_QT4:
                                                                    qDebug("Will use LV2 Qt4 UI, bridged");
                                                                    break;

                                                                case LV2_UI_X11:
                                                                    qDebug("Will use LV2 X11 UI, bridged");
                                                                    break;

                                                                case LV2_UI_GTK2:
                                                                    qDebug("Will use LV2 Gtk2 UI, bridged");
                                                                    break;

                                                                default:
                                                                    qDebug("Will use LV2 Unknown UI, bridged");
                                                                    break;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // Initialize UI features
                                                                LV2_Extension_Data_Feature* UI_Data_Feature = new LV2_Extension_Data_Feature;
                                                                UI_Data_Feature->data_access                = plugin->descriptor->extension_data;

                                                                LV2_UI_Resize_Feature* UI_Resize_Feature    = new LV2_UI_Resize_Feature;
                                                                UI_Resize_Feature->data                     = plugin;
                                                                UI_Resize_Feature->ui_resize                = plugin->carla_lv2_ui_resize;

                                                                lv2_external_ui_host* External_UI_Feature   = new lv2_external_ui_host;
                                                                External_UI_Feature->ui_closed              = plugin->carla_lv2_external_ui_closed;
                                                                External_UI_Feature->plugin_human_id        = plugin->gui.name;

                                                                plugin->features[lv2_feature_id_data_access]           = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_data_access]->URI      = LV2_DATA_ACCESS_URI;
                                                                plugin->features[lv2_feature_id_data_access]->data     = UI_Data_Feature;

                                                                plugin->features[lv2_feature_id_instance_access]       = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_instance_access]->URI  = LV2_INSTANCE_ACCESS_URI;
                                                                plugin->features[lv2_feature_id_instance_access]->data = plugin->handle;

                                                                plugin->features[lv2_feature_id_ui_resize]             = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_ui_resize]->URI        = LV2_UI_RESIZE_URI "#UIResize";
                                                                plugin->features[lv2_feature_id_ui_resize]->data       = UI_Resize_Feature;

                                                                plugin->features[lv2_feature_id_ui_parent]             = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_ui_parent]->URI        = LV2_UI_URI "#parent";
                                                                plugin->features[lv2_feature_id_ui_parent]->data       = nullptr;

                                                                plugin->features[lv2_feature_id_external_ui]           = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_external_ui]->URI      = LV2_EXTERNAL_UI_URI;
                                                                plugin->features[lv2_feature_id_external_ui]->data     = External_UI_Feature;

                                                                plugin->features[lv2_feature_id_external_ui_old]       = new LV2_Feature;
                                                                plugin->features[lv2_feature_id_external_ui_old]->URI  = LV2_EXTERNAL_UI_DEPRECATED_URI;
                                                                plugin->features[lv2_feature_id_external_ui_old]->data = External_UI_Feature;

                                                                switch (UiType)
                                                                {
                                                                case LV2_UI_QT4:
                                                                    qDebug("Will use LV2 Qt4 UI");
                                                                    plugin->gui.type      = GUI_INTERNAL_QT4;
                                                                    plugin->gui.resizable = plugin->is_ui_resizable();

                                                                    plugin->ui.handle = plugin->ui.descriptor->instantiate(plugin->ui.descriptor,
                                                                                                                           plugin->descriptor->URI,
                                                                                                                           plugin->ui.rdf_descriptor->Bundle,
                                                                                                                           plugin->carla_lv2_ui_write_function,
                                                                                                                           plugin,
                                                                                                                           &plugin->ui.widget,
                                                                                                                           plugin->features);

                                                                    if (plugin->ui.handle && plugin->ui.descriptor->port_event)
                                                                        plugin->update_ui_ports();

                                                                    break;

                                                                case LV2_UI_X11:
                                                                    qDebug("Will use LV2 X11 UI");
                                                                    plugin->gui.type      = GUI_INTERNAL_X11;
                                                                    plugin->gui.resizable = plugin->is_ui_resizable();
                                                                    break;

                                                                case LV2_UI_GTK2:
                                                                    qDebug("Will use LV2 Gtk2 UI, NOT!");
                                                                    break;

                                                                case LV2_UI_EXTERNAL:
                                                                case LV2_UI_OLD_EXTERNAL:
                                                                    qDebug("Will use LV2 External UI");
                                                                    plugin->gui.type = GUI_EXTERNAL_LV2;
                                                                    break;

                                                                default:
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            qCritical("Could not find the requested GUI in the plugin UI library");
                                                            plugin->ui_lib_close();
                                                            plugin->ui.lib = nullptr;
                                                            plugin->ui.descriptor = nullptr;
                                                            plugin->ui.rdf_descriptor = nullptr;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        qCritical("Could not find the LV2UI Descriptor in the UI library");
                                                        plugin->ui_lib_close();
                                                        plugin->ui.lib = nullptr;
                                                        plugin->ui.rdf_descriptor = nullptr;
                                                    }
                                                }
                                                else
                                                {
                                                    qCritical("Could not load UI library, error was: \n%s", plugin->ui_lib_error());
                                                    plugin->ui.lib = nullptr;
                                                }
                                            }
                                            else
                                            {
                                                // Some UI Feature not supported
                                                plugin->ui.lib = nullptr;
                                                plugin->ui.rdf_descriptor = nullptr;
                                            }
                                        }
                                        else
                                            qWarning("Failed to find an appropriate LV2 UI for this plugin");
                                    }

                                    // ----------------- End of GUI Stuff ------------------------------------------------

                                    if (plugin->gui.type != GUI_NONE)
                                        plugin->hints |= PLUGIN_HAS_GUI;

                                    osc_new_plugin(plugin);
                                }
                                else
                                {
                                    set_last_error("Failed to register plugin in JACK");
                                    delete plugin;
                                    id = -1;
                                }
                            }
                            else
                            {
                                set_last_error("Plugin failed to initialize");
                                delete plugin;
                                id = -1;
                            }
                        }
                        else
                        {
                            delete plugin;
                            id = -1;
                        }
                    }
                    else
                    {
                        set_last_error("Could not find the requested plugin URI in the plugin library");
                        delete plugin;
                        id = -1;
                    }
                }
                else
                {
                    set_last_error("Could not find the LV2 Descriptor in the plugin library");
                    delete plugin;
                    id = -1;
                }
            }
            else
            {
                set_last_error(plugin->lib_error());
                delete plugin;
                id = -1;
            }
        }
        else
        {
            set_last_error("Failed to find the requested plugin in the LV2 Bundle");
            id = -1;
        }
    }
    else
        set_last_error("Maximum number of plugins reached");

    return id;
}
