/* Code for Windows VST plugins */

#include "carla_backend.h"
#include "audio_plugin.h"
#include "misc.h"

// Global variables
extern const char* unique_names[MAX_PLUGINS];
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];

// Global options
extern carla_options_t carla_options;

struct WinVstParamInfo {
    QString name;
    QString label;
};

struct WinVstPluginInfo {
    PluginCategory category;
    QString name;
    QString label;
    QString maker;
    long unique_id;
    QList<WinVstParamInfo> params;
};

class WinVstAudioPlugin;

class WinVstOscThread : public QThread
{
public:
    WinVstOscThread(QObject* parent=0);
    void set_plugin(WinVstAudioPlugin* plugin);
    void run();

private:
    WinVstAudioPlugin* plugin;
};

class WinVstAudioPlugin : public AudioPlugin
{
public:
    WinVstPluginInfo info;

    WinVstAudioPlugin()
    {
        qDebug("WinVstAudioPlugin::WinVstAudioPlugin()");
        type = PLUGIN_WINVST;

        info.category = PLUGIN_CATEGORY_NONE;
        info.name  = "";
        info.label = "";
        info.maker = "";
        info.unique_id = 0;
        info.params.clear();

        hints = PLUGIN_IS_BRIDGE;
    }

    ~WinVstAudioPlugin()
    {
        qDebug("WinVstAudioPlugin::~WinVstAudioPlugin()");

        if (gui.visible)
            osc_send_hide(&osc.data);

        osc_send_quit(&osc.data);

        if (osc.thread)
        {
            osc.thread->quit();

            if (!osc.thread->wait(3000)) // 3 sec
                qWarning("Failed to properly stop Windows VST OSC thread");

            delete osc.thread;
        }

        osc_clear_data(&osc.data);

        ain.count   = 0;
        aout.count  = 0;
        min.count   = 0;
        mout.count  = 0;
        param.count = 0;
        param.port_in  = nullptr;
        param.port_out = nullptr;
    }

    void set_base_info(WinVstBaseInfo* base_info, const char* label)
    {
        info.category  = base_info->category;
        info.name      = base_info->name;
        info.label     = label;
        info.maker     = base_info->maker;
        info.unique_id = base_info->unique_id;

        hints |= base_info->hints;

        ain.count  = base_info->ains;
        aout.count = base_info->aouts;

        if (hints & PLUGIN_HAS_GUI)
            gui.type = GUI_EXTERNAL_OSC;
    }

    int set_osc_bridge_info(PluginOscBridgeInfoType itype, lo_arg** argv)
    {
        int index;
        const char* str1;
        const char* str2;

        switch(itype)
        {
        case OscBridgeAudioCountInfo:
            ain.count  = argv[0]->i;
            aout.count = argv[1]->i;
            break;

        case OscBridgeMidiCountInfo:
            min.count  = argv[0]->i;
            mout.count = argv[1]->i;
            break;

        case OscBridgeParameterCountInfo:
            // delete old data
            if (param.count > 0)
            {
                delete[] param.buffers;
                delete[] param.data;
                delete[] param.ranges;
            }
            info.params.clear();

            // create new if needed
            param.count = argv[2]->i;

            if (param.count > 0 && param.count < MAX_PARAMETERS)
            {
                param.buffers = new float[param.count];
                param.data    = new ParameterData[param.count];
                param.ranges  = new ParameterRanges[param.count];
            }
            else
                param.count = 0;

            // initialize
            for (uint32_t i=0; i < param.count; i++)
            {
                param.buffers[i] = 0.0f;

                param.data[i].index  = -1;
                param.data[i].rindex = -1;
                param.data[i].hints  = 0;
                param.data[i].midi_channel = 0;
                param.data[i].midi_cc = -1;

                param.ranges[i].def  = 0.0;
                param.ranges[i].min  = 0.0;
                param.ranges[i].max  = 1.0;
                param.ranges[i].step = 0.01;
                param.ranges[i].step_small = 0.0001;
                param.ranges[i].step_large = 0.1;

                WinVstParamInfo infot = { QString(), QString() };
                info.params.append(infot);
            }
            break;

        case OscBridgeProgramCountInfo:
            // delete old data
            if (prog.count > 0)
                delete[] prog.names;

            // create new if needed
            prog.count = argv[0]->i;

            if (prog.count > 0)
            {
                prog.names = new const char* [prog.count];

                // initialize
                for (uint32_t i=0; i < prog.count; i++)
                    prog.names[i] = nullptr;
            }
            break;

        case OscBridgePluginInfo:
            hints |= argv[0]->i;
            break;

        case OscBridgeParameterInfo:
            index = argv[0]->i;
            if (index >= 0 && index < info.params.count())
            {
                str1 = &argv[1]->s;
                str2 = &argv[2]->s;
                info.params[index].name  = QString(str1);
                info.params[index].label = QString(str2);
            }
            break;

        case OscBridgeParameterDataInfo:
            index = argv[0]->i;
            if (index >= 0 && index < (int32_t)param.count)
            {
                param.data[index].type    = static_cast<ParameterType>(argv[1]->i);
                param.data[index].hints   = argv[2]->i;
                param.data[index].index   = argv[3]->i;
                param.data[index].rindex  = argv[4]->i;
                param.data[index].midi_channel = argv[5]->i;
                param.data[index].midi_cc = argv[6]->i;
            }
            break;

        case OscBridgeParameterRangesInfo:
            index = argv[0]->i;
            if (index >= 0 && index < (int32_t)param.count)
            {
                param.ranges[index].def  = argv[1]->f;
                param.ranges[index].min  = argv[2]->f;
                param.ranges[index].max  = argv[3]->f;
                param.ranges[index].step = argv[4]->f;
                param.ranges[index].step_small = argv[5]->f;
                param.ranges[index].step_large = argv[6]->f;
            }
            break;

        case OscBridgeProgramName:
            index = argv[0]->i;
            if (index >= 0 && index < (int32_t)prog.count)
            {
                if (prog.names[index])
                    free((void*)prog.names[index]);
                str1 = &argv[1]->s;
                prog.names[index] = strdup(str1);
            }
            break;

        case OscBridgeUpdateNow:
            callback_action(CALLBACK_RELOAD_ALL, id, 0, 0, 0.0);
            break;

        default:
            break;
        }

        return 0;
    }

    void delete_me()
    {
        qDebug("WinVstAudioPlugin::delete_me()");
        ain.count  = 0;
        aout.count = 0;
        delete this;
    }

    PluginCategory get_category()
    {
        return info.category;
    }

    void get_label(char* buf_str)
    {
        strncpy(buf_str, info.label.toStdString().data(), STR_MAX);
    }

    void get_maker(char* buf_str)
    {
        strncpy(buf_str, info.maker.toStdString().data(), STR_MAX);
    }

    void get_copyright(char* buf_str)
    {
        strncpy(buf_str, info.maker.toStdString().data(), STR_MAX);
    }

    void get_real_name(char* buf_str)
    {
        strncpy(buf_str, info.name.toStdString().data(), STR_MAX);
    }

    long get_unique_id()
    {
        return info.unique_id;
    }

    void get_parameter_name(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, info.params.at(rindex).name.toStdString().data(), STR_MAX);
    }

    void get_parameter_symbol(uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    void get_parameter_label(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, info.params.at(rindex).label.toStdString().data(), STR_MAX);
    }

    uint32_t get_scalepoint_count(uint32_t)
    {
        return 0;
    }

    double get_scalepoint_value(uint32_t, uint32_t)
    {
        return 0.0;
    }

    void get_scalepoint_label(uint32_t, uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    int32_t get_chunk_data(void** data_ptr)
    {
        // TODO
        Q_UNUSED(data_ptr);
        return 0;
    }

    void x_set_parameter_value(uint32_t parameter_id, double value, bool)
    {
        postpone_event(PostEventParameter, parameter_id, value);
    }

    void x_set_program(uint32_t program_id, bool, bool)
    {
        postpone_event(PostEventProgram, program_id, 0.0);
    }

    void x_set_midi_program(uint32_t midi_program_id, bool, bool)
    {
        postpone_event(PostEventMidiProgram, midi_program_id, 0.0);
    }

    void x_set_custom_data(CustomDataType, const char*, const char*, bool)
    {
    }

    void set_chunk_data(const char* string_data)
    {
        // TODO
        Q_UNUSED(string_data);
    }

    void set_gui_data(int, void*)
    {
    }

    void show_gui(bool yesno)
    {
        if (yesno)
        {
            osc_send_show(&osc.data);
            gui.visible = true;
        }
        else
        {
            gui.visible = false;
            osc_send_hide(&osc.data);
        }
    }

    void idle_gui()
    {
    }

    void reload()
    {
        qDebug("WinVstAudioPlugin::reload()");
        // TODO
    }

    void reload_programs(bool init)
    {
        qDebug("WinVstAudioPlugin::reload_programs(%s)", bool2str(init));
        // TODO
    }

    void prepare_for_save()
    {
        // TODO
    }

    void process(jack_nframes_t)
    {
    }

    void buffer_size_changed(jack_nframes_t)
    {
    }
};

WinVstOscThread::WinVstOscThread(QObject* parent) :
    QThread(parent)
{
    plugin = nullptr;
}

void WinVstOscThread::set_plugin(WinVstAudioPlugin* plugin_)
{
    plugin = plugin_;
}

void WinVstOscThread::run()
{
    if (! plugin)
    {
        qCritical("WinVstOscThread::run() - invalid plugin");
        return;
    }

    if (plugin->id < 0 || plugin->id > MAX_PLUGINS)
    {
        qCritical("WinVstOscThread::run() - invalid plugin id '%i'", plugin->id);
        return;
    }

    QString cmd = QString("%1 '%2' '%3' '%3 (GUI)' '%4/%5'").arg(carla_options.bridge_path_winvst).arg(plugin->filename).arg(plugin->name).arg(get_host_osc_url()).arg(plugin->id);

    qDebug("WinVstOscThread::run() - command: %s", cmd.toStdString().data());

    if (plugin && carla_is_engine_running())
    {
        if (system(cmd.toStdString().data()) != 0)
        {
            callback_action(CALLBACK_SHOW_GUI, plugin->id, -1, 0, 0.0f);
            qWarning("WinVstOscThread::run() - Windows VST crashed");
        }
    }
}

short add_plugin_winvst(const char* filename, const char* label, void* extra_stuff)
{
    qDebug("add_plugin_winvst(%s, %s)", filename, label);

    short id = get_new_plugin_id();

    if (id >= 0)
    {
        WinVstAudioPlugin* plugin = new WinVstAudioPlugin;

        if (extra_stuff)
            plugin->set_base_info((WinVstBaseInfo*)extra_stuff, label);

        plugin->filename = strdup(filename);
        plugin->name = get_unique_name(label);

        WinVstOscThread* winvst_thread = new WinVstOscThread();
        winvst_thread->set_plugin(plugin);
        winvst_thread->start();
        plugin->osc.thread = winvst_thread;

        plugin->id = id;
        unique_names[id] = plugin->name;
        AudioPlugins[id] = plugin;

        // Don't register plugin in OSC just yet, wait until we get all required information
    }
    else
        set_last_error("Maximum number of plugins reached");

    return id;
}
