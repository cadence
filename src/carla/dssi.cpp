/* Code for DSSI plugins */

#include "carla_backend.h"
#include "audio_plugin.h"
#include "misc.h"

#include <math.h>
#include <unistd.h>

#include "dssi/dssi+chunks.h"

// Global variables
extern const char* unique_names[MAX_PLUGINS];
extern AudioPlugin* AudioPlugins[MAX_PLUGINS];
extern ExternalMidiNote ExternalMidiNotes[MAX_MIDI_EVENTS];

extern volatile double ains_peak[MAX_PLUGINS*2];
extern volatile double aouts_peak[MAX_PLUGINS*2];

// Global JACK client
extern jack_client_t* carla_jack_client;

// Global options
extern carla_options_t carla_options;

// jack.cpp
void carla_jack_register_plugin(AudioPlugin* plugin);

class DssiOscGuiThread : public QThread
{
public:
    DssiOscGuiThread(QObject* parent=0);
    void set_plugin_id(short plugin_id);
    void run();

private:
    short plugin_id;
};

class DssiAudioPlugin : public AudioPlugin
{
public:
    LADSPA_Handle handle;
    const LADSPA_Descriptor* ldescriptor;
    const DSSI_Descriptor* descriptor;
    snd_seq_event_t midi_events[MAX_MIDI_EVENTS];

    DssiAudioPlugin()
    {
        qDebug("DssiAudioPlugin::DssiAudioPlugin()");
        type = PLUGIN_DSSI;

        handle = nullptr;
        descriptor = nullptr;
        ldescriptor = nullptr;

        for (unsigned int i=0; i<MAX_MIDI_EVENTS; i++)
            memset(&midi_events[i], 0, sizeof(snd_seq_event_t));
    }

    ~DssiAudioPlugin()
    {
        qDebug("DssiAudioPlugin::~DssiAudioPlugin()");

        // close UI
        if (hints & PLUGIN_HAS_GUI)
        {
            if (gui.visible)
                osc_send_hide(&osc.data);

            osc_send_quit(&osc.data);

            if (osc.thread)
            {
                osc.thread->quit();

                if (!osc.thread->wait(3000)) // 3 sec
                    qWarning("Failed to properly stop DSSI GUI thread");

                delete osc.thread;
            }

            osc_clear_data(&osc.data);
        }

        if (handle && ldescriptor->deactivate && active_before)
            ldescriptor->deactivate(handle);

        if (handle && ldescriptor->cleanup)
            ldescriptor->cleanup(handle);
    }

    void delete_me()
    {
        qDebug("DssiAudioPlugin::delete_me()");
        delete this;
    }

    int set_osc_bridge_info(PluginOscBridgeInfoType, lo_arg**)
    {
        return 1;
    }

    PluginCategory get_category()
    {
        if (min.count > 0 && aout.count > 0)
            return PLUGIN_CATEGORY_SYNTH;
        else
            return PLUGIN_CATEGORY_NONE;
    }

    void get_label(char* buf_str)
    {
        strncpy(buf_str, ldescriptor->Label, STR_MAX);
    }

    void get_maker(char* buf_str)
    {
        strncpy(buf_str, ldescriptor->Maker, STR_MAX);
    }

    void get_copyright(char* buf_str)
    {
        strncpy(buf_str, ldescriptor->Copyright, STR_MAX);
    }

    void get_real_name(char* buf_str)
    {
        strncpy(buf_str, ldescriptor->Name, STR_MAX);
    }

    long get_unique_id()
    {
        return ldescriptor->UniqueID;
    }

    void get_parameter_name(uint32_t rindex, char* buf_str)
    {
        strncpy(buf_str, ldescriptor->PortNames[rindex], STR_MAX);
    }

    void get_parameter_symbol(uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    void get_parameter_label(uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    uint32_t get_scalepoint_count(uint32_t)
    {
        return 0;
    }

    double get_scalepoint_value(uint32_t, uint32_t)
    {
        return 0.0;
    }

    void get_scalepoint_label(uint32_t, uint32_t, char* buf_str)
    {
        *buf_str = 0;
    }

    int32_t get_chunk_data(void** data_ptr)
    {
        unsigned long long_data_size = 0;
        if (descriptor->getCustomData(handle, data_ptr, &long_data_size))
            return long_data_size;
        else
            return 0;
    }

    void x_set_parameter_value(uint32_t parameter_id, double value, bool gui_send)
    {
        if (gui_send && gui.visible)
            osc_send_control(&osc.data, param.data[parameter_id].rindex, value);
    }

    void x_set_program(uint32_t, bool, bool)
    {
    }

    void x_set_midi_program(uint32_t midi_program_id, bool gui_send, bool block)
    {
        if (!descriptor->get_program || !descriptor->select_program)
            return;

        if (block) carla_proc_lock();
        descriptor->select_program(handle, midiprog.data[midi_program_id].bank, midiprog.data[midi_program_id].program);
        if (block) carla_proc_unlock();

        if (gui_send && gui.visible)
            osc_send_program_as_midi(&osc.data, midiprog.data[midi_program_id].bank, midiprog.data[midi_program_id].program);
    }

    void x_set_custom_data(CustomDataType, const char* key, const char* value, bool gui_send)
    {
        descriptor->configure(handle, key, value);

        if (gui_send && gui.visible)
            osc_send_configure(&osc.data, key, value);

        if (strcmp(key, "reloadprograms") == 0 || strcmp(key, "load") == 0 || strncmp(key, "patches", 7) == 0)
        {
            reload_programs(false);
        }
        else if (strcmp(key, "names") == 0) // Not in the API!
        {
            if (prog.count > 0)
            {
                osc_send_set_program_count(&osc.data, id, prog.count);

                // Parse names
                int j, k, last_str_n = 0;
                int str_len = strlen(value);
                char name[256];

                for (uint32_t i=0; i < prog.count; i++)
                {
                    for (j=0, k=last_str_n; j<256 && k+j<str_len; j++)
                    {
                        name[j] = value[k+j];
                        if (value[k+j] == ',')
                        {
                            name[j] = 0;
                            last_str_n = k+j+1;
                            free((void*)prog.names[i]);
                            prog.names[i] = strdup(name);
                            break;
                        }
                    }

                    osc_send_set_program_name(&osc.data, id, i, prog.names[i]);
                }

                callback_action(CALLBACK_RELOAD_PROGRAMS, id, 0, 0, 0.0f);
            }
        }
    }

    void set_chunk_data(const char* string_data)
    {
        QByteArray chunk = QByteArray::fromBase64(string_data);
        descriptor->setCustomData(handle, chunk.data(), chunk.size());
    }

    void set_gui_data(int, void*)
    {
    }

    void show_gui(bool yesno)
    {
        if (yesno)
        {
            gui.show_now = true;

            // 40 re-tries, 4 secs
            for (int j=1; j<40 && gui.show_now; j++)
            {
                if (osc.data.target)
                {
                    osc_send_show(&osc.data);
                    gui.visible = true;
                    return;
                }
                else
                    // 100 ms
                    usleep(100000);
            }

            qDebug("DssiAudioPlugin::show_gui() - GUI timeout");
            callback_action(CALLBACK_SHOW_GUI, id, 0, 0, 0.0);
        }
        else
        {
            gui.visible = false;
            osc_send_hide(&osc.data);
            osc_send_quit(&osc.data);
            osc_clear_data(&osc.data);
        }
    }

    void idle_gui()
    {
    }

    void reload()
    {
        qDebug("DssiAudioPlugin::reload()");
        short _id = id;

        // Safely disable plugin for reload
        carla_proc_lock();

        id = -1;
        if (carla_options.global_jack_client == false)
            jack_deactivate(jack_client);

        carla_proc_unlock();

        // Unregister jack ports
        remove_from_jack();

        // Delete old data
        delete_buffers();

        uint32_t ains, aouts, mins, params, j;
        ains = aouts = mins = params = 0;

        const unsigned long PortCount = ldescriptor->PortCount;
        for (unsigned long i=0; i<PortCount; i++)
        {
            LADSPA_PortDescriptor PortType = ldescriptor->PortDescriptors[i];
            if (LADSPA_IS_PORT_AUDIO(PortType))
            {
                if (LADSPA_IS_PORT_INPUT(PortType))
                    ains += 1;
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                    aouts += 1;
            }
            else if (LADSPA_IS_PORT_CONTROL(PortType))
                params += 1;
        }

        if (descriptor->run_synth || descriptor->run_multiple_synths)
            mins = 1;

        if (ains > 0)
        {
            ain.rindexes = new uint32_t[ains];
            ain.ports    = new jack_port_t*[ains];
        }

        if (aouts > 0)
        {
            aout.rindexes = new uint32_t[aouts];
            aout.ports    = new jack_port_t*[aouts];
        }

        if (mins > 0)
        {
            min.ports = new jack_port_t*[mins];
        }

        if (params > 0)
        {
            param.buffers = new float[params];
            param.data    = new ParameterData[params];
            param.ranges  = new ParameterRanges[params];
        }

        const int port_name_size = jack_port_name_size();
        char port_name[port_name_size];
        bool needs_cin  = false;
        bool needs_cout = false;

        for (unsigned long i=0; i<PortCount; i++)
        {
            LADSPA_PortDescriptor PortType = ldescriptor->PortDescriptors[i];
            LADSPA_PortRangeHint PortHint  = ldescriptor->PortRangeHints[i];

            if (LADSPA_IS_PORT_AUDIO(PortType))
            {
                if (carla_options.global_jack_client)
                {
                    strncpy(port_name, name, (port_name_size/2)-2);
                    strcat(port_name, ":");
                    strncat(port_name, ldescriptor->PortNames[i], port_name_size/2);
                }
                else
                    strncpy(port_name, ldescriptor->PortNames[i], port_name_size);

                if (LADSPA_IS_PORT_INPUT(PortType))
                {
                    j = ain.count++;
                    ain.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
                    ain.rindexes[j] = i;
                }
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                {
                    j = aout.count++;
                    aout.ports[j] = jack_port_register(jack_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
                    aout.rindexes[j] = i;
                    needs_cin = true;
                }
                else
                    qWarning("WARNING - Got a broken Port (Audio, but not input or output)");
            }
            else if (LADSPA_IS_PORT_CONTROL(PortType))
            {
                j = param.count++;
                param.data[j].index  = j;
                param.data[j].rindex = i;
                param.data[j].hints  = 0;
                param.data[j].midi_channel = 0;
                param.data[j].midi_cc = -1;

                double min, max, def, step, step_small, step_large;

                // min value
                if (LADSPA_IS_HINT_BOUNDED_BELOW(PortHint.HintDescriptor))
                    min = PortHint.LowerBound;
                else
                    min = 0.0;

                // max value
                if (LADSPA_IS_HINT_BOUNDED_ABOVE(PortHint.HintDescriptor))
                    max = PortHint.UpperBound;
                else
                    max = 1.0;

                if (min > max)
                    max = min;
                else if (max < min)
                    min = max;

                // default value
                if (LADSPA_IS_HINT_HAS_DEFAULT(PortHint.HintDescriptor))
                {
                    switch (PortHint.HintDescriptor & LADSPA_HINT_DEFAULT_MASK)
                    {
                    case LADSPA_HINT_DEFAULT_MINIMUM:
                        def = min;
                        break;
                    case LADSPA_HINT_DEFAULT_MAXIMUM:
                        def = max;
                        break;
                    case LADSPA_HINT_DEFAULT_0:
                        def = 0.0;
                        break;
                    case LADSPA_HINT_DEFAULT_1:
                        def = 1.0;
                        break;
                    case LADSPA_HINT_DEFAULT_100:
                        def = 100.0;
                        break;
                    case LADSPA_HINT_DEFAULT_440:
                        def = 440.0;
                        break;
                    case LADSPA_HINT_DEFAULT_LOW:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = exp((log(min)*0.75) + (log(max)*0.25));
                        else
                            def = (min*0.75) + (max*0.25);
                        break;
                    case LADSPA_HINT_DEFAULT_MIDDLE:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = sqrt(min*max);
                        else
                            def = (min+max)/2;
                        break;
                    case LADSPA_HINT_DEFAULT_HIGH:
                        if (LADSPA_IS_HINT_LOGARITHMIC(PortHint.HintDescriptor))
                            def = exp((log(min)*0.25) + (log(max)*0.75));
                        else
                            def = (min*0.25) + (max*0.75);
                        break;
                    default:
                        if (min < 0.0 && max > 0.0)
                            def = 0.0;
                        else
                            def = min;
                        break;
                    }
                }
                else
                {
                    // no default value
                    if (min < 0.0 && max > 0.0)
                        def = 0.0;
                    else
                        def = min;
                }

                if (def < min)
                    def = min;
                else if (def > max)
                    def = max;

                if (max - min == 0.0)
                {
                    qWarning("Broken plugin parameter -> max - min == 0");
                    max = min + 0.1;
                }

                if (LADSPA_IS_HINT_SAMPLE_RATE(PortHint.HintDescriptor))
                {
                    double sample_rate = get_sample_rate();
                    min *= sample_rate;
                    max *= sample_rate;
                    def *= sample_rate;
                    param.data[j].hints |= PARAMETER_USES_SAMPLERATE;
                }

                if (LADSPA_IS_HINT_INTEGER(PortHint.HintDescriptor))
                {
                    step = 1.0;
                    step_small = 1.0;
                    step_large = 10.0;
                }
                else if (LADSPA_IS_HINT_TOGGLED(PortHint.HintDescriptor))
                {
                    step = max - min;
                    step_small = step;
                    step_large = step;
                }
                else
                {
                    double range = max - min;
                    step = range/100.0;
                    step_small = range/1000.0;
                    step_large = range/10.0;
                }

                if (LADSPA_IS_PORT_INPUT(PortType))
                {
                    param.data[j].type = PARAMETER_INPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;
                    param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                    needs_cin = true;

                    // MIDI CC value
                    if (descriptor->get_midi_controller_for_port)
                    {
                        int cc = descriptor->get_midi_controller_for_port(handle, i);
                        if (DSSI_CONTROLLER_IS_SET(cc) && DSSI_IS_CC(cc))
                            param.data[j].midi_cc = DSSI_CC_NUMBER(cc);
                    }
                }
                else if (LADSPA_IS_PORT_OUTPUT(PortType))
                {
                    param.data[j].type = PARAMETER_OUTPUT;
                    param.data[j].hints |= PARAMETER_IS_ENABLED;

                    if (strcmp(ldescriptor->PortNames[i], "latency") != 0 && strcmp(ldescriptor->PortNames[i], "_latency") != 0)
                    {
                        param.data[j].hints |= PARAMETER_IS_AUTOMABLE;
                        needs_cout = true;
                    }
                    else
                    {
                        // latency parameter
                        min = 0;
                        max = get_sample_rate();
                        def = 0;
                        step = 1;
                        step_small = 1;
                        step_large = 1;
                    }
                }
                else
                {
                    param.data[j].type = PARAMETER_UNKNOWN;
                    qWarning("WARNING - Got a broken Port (Control, but not input or output)");
                }

                param.ranges[j].min = min;
                param.ranges[j].max = max;
                param.ranges[j].def = def;
                param.ranges[j].step = step;
                param.ranges[j].step_small = step_small;
                param.ranges[j].step_large = step_large;

                // Start parameters in their default values
                param.buffers[j] = def;

                ldescriptor->connect_port(handle, i, &param.buffers[j]);
            }
            else
            {
                // Not Audio or Control
                qCritical("ERROR - Got a broken Port (neither Audio or Control)");
                ldescriptor->connect_port(handle, i, nullptr);
            }
        }

        if (needs_cin)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-in");
            }
            else
                strcpy(port_name, "control-in");

            param.port_in = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        if (needs_cout)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":control-out");
            }
            else
                strcpy(port_name, "control-out");

            param.port_out = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
        }

        if (mins == 1)
        {
            if (carla_options.global_jack_client)
            {
                strncpy(port_name, name, (port_name_size/2)-2);
                strcat(port_name, ":midi-in");
            }
            else
                strcpy(port_name, "midi-in");

            min.ports[0] = jack_port_register(jack_client, port_name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
        }

        ain.count   = ains;
        aout.count  = aouts;
        min.count   = mins;
        param.count = params;

        reload_programs(true);

        // Check if plugin is synth
        if (min.count > 0 && aout.count > 0)
            hints |= PLUGIN_IS_SYNTH;

        // Check if plugin uses chunks -> hack by name check ["some-name VST"]
        if (QString(ldescriptor->Name).endsWith(" VST", Qt::CaseSensitive))
        {
            if (descriptor->getCustomData && descriptor->setCustomData)
                hints |= PLUGIN_USES_CHUNKS;
        }

        // Other plugin checks
        if (aouts > 0 && (ains == aouts || ains == 1))
            hints |= PLUGIN_CAN_DRYWET;

        if (aouts > 0)
            hints |= PLUGIN_CAN_VOL;

        if (aouts >= 2 && aouts%2 == 0)
            hints |= PLUGIN_CAN_BALANCE;

        carla_proc_lock();
        id = _id;
        carla_proc_unlock();

        if (carla_options.global_jack_client == false)
            jack_activate(jack_client);
    }

    void reload_programs(bool init)
    {
        qDebug("DssiAudioPlugin::reload_programs(%s)", bool2str(init));
        uint32_t i, old_count = midiprog.count;

        // Delete old programs
        if (midiprog.count > 0)
        {
            for (uint32_t i=0; i < midiprog.count; i++)
                free((void*)midiprog.names[i]);

            delete[] midiprog.data;
            delete[] midiprog.names;
        }
        midiprog.count = 0;

        // Query new programs
        if (descriptor->get_program && descriptor->select_program)
        {
            while (descriptor->get_program(handle, midiprog.count))
                midiprog.count += 1;
        }

        if (midiprog.count > 0)
        {
            midiprog.data  = new midi_program_t [midiprog.count];
            midiprog.names = new const char* [midiprog.count];
        }

        // Update data
        for (i=0; i < midiprog.count; i++)
        {
            const DSSI_Program_Descriptor* pdesc = descriptor->get_program(handle, i);
            if (pdesc)
            {
                midiprog.data[i].bank    = pdesc->Bank;
                midiprog.data[i].program = pdesc->Program;
                midiprog.names[i] = strdup(pdesc->Name);
            }
            else
            {
                midiprog.data[i].bank    = 0;
                midiprog.data[i].program = 0;
                midiprog.names[i] = strdup("(error)");
            }
        }

        // Update OSC Names
        osc_send_set_midi_program_count(&global_osc_data, id, midiprog.count);

        if (midiprog.count > 0)
        {
            for (i=0; i < midiprog.count; i++)
                osc_send_set_midi_program_data(&global_osc_data, id, i, midiprog.data[i].bank, midiprog.data[i].program, midiprog.names[i]);
        }

        callback_action(CALLBACK_RELOAD_PROGRAMS, id, 0, 0, 0.0);

        if (init)
        {
            if (midiprog.count > 0)
                set_midi_program(0, false, false, false, true);
        }
        else
        {
            callback_action(CALLBACK_UPDATE, id, 0, 0, 0.0);

            // Check if current program is invalid
            bool program_changed = false;

            if (midiprog.count == old_count+1)
            {
                // one midi program added, probably created by user
                midiprog.current = old_count;
                program_changed  = true;
            }
            else if (midiprog.current >= (int32_t)midiprog.count)
            {
                // current midi program > count
                midiprog.current = 0;
                program_changed  = true;
            }
            else if (midiprog.current < 0 && midiprog.count > 0)
            {
                // programs exist now, but not before
                midiprog.current = 0;
                program_changed  = true;
            }
            else if (midiprog.current >= 0 && midiprog.count == 0)
            {
                // programs existed before, but not anymore
                midiprog.current = -1;
                program_changed  = true;
            }

            if (program_changed)
                set_midi_program(midiprog.current, true, true, true, true);
        }
    }

    void prepare_for_save()
    {
    }

    void process(jack_nframes_t nframes)
    {
        uint32_t i, k;
        unsigned short plugin_id = id;
        unsigned long midi_event_count = 0;

        double ains_peak_tmp[2]  = { 0.0 };
        double aouts_peak_tmp[2] = { 0.0 };

        jack_default_audio_sample_t* ains_buffer[ain.count];
        jack_default_audio_sample_t* aouts_buffer[aout.count];
        jack_default_audio_sample_t* mins_buffer[min.count];

        for (i=0; i < ain.count; i++)
            ains_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(ain.ports[i], nframes);

        for (i=0; i < aout.count; i++)
            aouts_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(aout.ports[i], nframes);

        for (i=0; i < min.count; i++)
            mins_buffer[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(min.ports[i], nframes);

        // --------------------------------------------------------------------------------------------------------
        // Input VU

        if (ain.count > 0)
        {
            short j2 = (ain.count == 1) ? 0 : 1;

            for (k=0; k<nframes; k++)
            {
                if (ains_buffer[0][k] > ains_peak_tmp[0])
                    ains_peak_tmp[0] = ains_buffer[0][k];
                if (ains_buffer[j2][k] > ains_peak_tmp[1])
                    ains_peak_tmp[1] = ains_buffer[j2][k];
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Parameters Input [Automation]

        if (param.port_in)
        {
            jack_default_audio_sample_t* pin_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_in, nframes);

            jack_midi_event_t pin_event;
            uint32_t n_pin_events = jack_midi_get_event_count(pin_buffer);

            for (i=0; i<n_pin_events; i++)
            {
                if (jack_midi_event_get(&pin_event, pin_buffer, i) != 0)
                    break;

                unsigned char channel = pin_event.buffer[0] & 0x0F;
                unsigned char mode    = pin_event.buffer[0] & 0xF0;

                // Status change
                if (mode == 0xB0)
                {
                    unsigned char status  = pin_event.buffer[1] & 0x7F;
                    unsigned char velo    = pin_event.buffer[2] & 0x7F;
                    double value, velo_per = double(velo)/127;

                    // Control GUI stuff (channel 0 only)
                    if (channel == 0)
                    {
                        if (status == 0x78)
                        {
                            // All Sound Off
                            set_active(false, false, false);
                            postpone_event(PostEventParameter, PARAMETER_ACTIVE, 0.0);
                            break;
                        }
                        else if (status == 0x09 && hints & PLUGIN_CAN_DRYWET)
                        {
                            // Dry/Wet (using '0x09', undefined)
                            set_drywet(velo_per, false, false);
                            postpone_event(PostEventParameter, PARAMETER_DRYWET, velo_per);
                        }
                        else if (status == 0x07 && hints & PLUGIN_CAN_VOL)
                        {
                            // Volume
                            value = double(velo)/100;
                            set_vol(value, false, false);
                            postpone_event(PostEventParameter, PARAMETER_VOLUME, value);
                        }
                        else if (status == 0x08 && hints & PLUGIN_CAN_BALANCE)
                        {
                            // Balance
                            double left, right;
                            value = (double(velo)-63.5)/63.5;

                            if (value < 0)
                            {
                                left  = -1.0;
                                right = (value*2)+1.0;
                            }
                            else if (value > 0)
                            {
                                left  = (value*2)-1.0;
                                right = 1.0;
                            }
                            else
                            {
                                left  = -1.0;
                                right = 1.0;
                            }

                            set_balance_left(left, false, false);
                            set_balance_right(right, false, false);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_LEFT, left);
                            postpone_event(PostEventParameter, PARAMETER_BALANCE_RIGHT, right);
                        }
                    }

                    // Control plugin parameters
                    for (k=0; k < param.count; k++)
                    {
                        if (param.data[k].type == PARAMETER_INPUT && (param.data[k].hints & PARAMETER_IS_AUTOMABLE) > 0 &&
                                param.data[k].midi_channel == channel && param.data[k].midi_cc == status)
                        {
                            value = (velo_per * (param.ranges[k].max - param.ranges[k].min)) + param.ranges[k].min;
                            set_parameter_value(k, value, false, false, false);
                            postpone_event(PostEventParameter, k, value);
                        }
                    }
                }
                // Program change
                else if (mode == 0xC0)
                {
                    unsigned char midi_program = pin_event.buffer[1] & 0x7F;

                    if (midi_program < midiprog.count)
                    {
                        set_midi_program(midi_program, false, false, false, false);
                        postpone_event(PostEventMidiProgram, midi_program, 0.0);
                    }
                }
            }
        } // End of Parameters Input

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (External)

        if (min.count > 0)
        {
            carla_midi_lock();

            for (i=0; i<MAX_MIDI_EVENTS && midi_event_count < MAX_MIDI_EVENTS; i++)
            {
                if (ExternalMidiNotes[i].valid)
                {
                    if (ExternalMidiNotes[i].plugin_id == plugin_id)
                    {
                        ExternalMidiNote* enote = &ExternalMidiNotes[i];
                        enote->valid = false;

                        snd_seq_event_t* midi_event = &midi_events[midi_event_count];
                        memset(midi_event, 0, sizeof(snd_seq_event_t));

                        midi_event->type = enote->onoff ? SND_SEQ_EVENT_NOTEON : SND_SEQ_EVENT_NOTEOFF;
                        midi_event->data.note.channel  = 0;
                        midi_event->data.note.note     = enote->note;
                        midi_event->data.note.velocity = enote->velo;

                        midi_event_count += 1;
                    }
                }
                else
                    break;
            }

            carla_midi_unlock();

        } // End of MIDI Input (External)

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // MIDI Input (JACK)

        if (min.count > 0)
        {
            jack_midi_event_t min_event;
            uint32_t n_min_events = jack_midi_get_event_count(mins_buffer[0]);

            for (k=0; k<n_min_events && midi_event_count < MAX_MIDI_EVENTS; k++)
            {
                if (jack_midi_event_get(&min_event, mins_buffer[0], k) != 0)
                    break;

                unsigned char channel = min_event.buffer[0] & 0x0F;
                unsigned char mode = min_event.buffer[0] & 0xF0;
                unsigned char note = min_event.buffer[1] & 0x7F;
                unsigned char velo = min_event.buffer[2] & 0x7F;

                // fix bad note off
                if (mode == 0x90 && velo == 0)
                {
                    mode = 0x80;
                    velo = 64;
                }

                snd_seq_event_t* midi_event = &midi_events[midi_event_count];
                memset(midi_event, 0, sizeof(snd_seq_event_t));

                if (mode == 0x80)
                {
                    midi_event->type = SND_SEQ_EVENT_NOTEOFF;
                    midi_event->data.note.channel = channel;
                    midi_event->data.note.note = note;
                    midi_event->data.note.velocity = velo;
                    midi_event->time.tick = min_event.time;
                    postpone_event(PostEventNoteOff, note, velo);
                }
                else if (mode == 0x90)
                {
                    midi_event->type = SND_SEQ_EVENT_NOTEON;
                    midi_event->data.note.channel = channel;
                    midi_event->data.note.note = note;
                    midi_event->data.note.velocity = velo;
                    midi_event->time.tick = min_event.time;
                    postpone_event(PostEventNoteOn, note, velo);
                }
                else if (mode == 0xB0)
                {
                    midi_event->type = SND_SEQ_EVENT_CONTROLLER;
                    midi_event->data.control.channel = channel;
                    midi_event->data.control.param = note;
                    midi_event->data.control.value = velo;
                    midi_event->time.tick = min_event.time;
                }
                else if (mode == 0xE0)
                {
                    // TODO
                    //midi_event->type = SND_SEQ_EVENT_PITCHBEND;
                    //midi_event->data.control.channel = channel;
                    //midi_event->data.control.param = note;
                    //midi_event->data.control.value = (min_event.buffer[2] << 7) | min_event.buffer[1];
                    //midi_event->time.tick = min_event.time;
                }

                midi_event_count += 1;
            }
        } // End of MIDI Input (JACK)

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Plugin processing

        if (active)
        {
            if (!active_before)
            {
                if (ldescriptor->activate)
                    ldescriptor->activate(handle);
            }

            for (i=0; i < ain.count; i++)
                ldescriptor->connect_port(handle, ain.rindexes[i], ains_buffer[i]);

            for (i=0; i < aout.count; i++)
                ldescriptor->connect_port(handle, aout.rindexes[i], aouts_buffer[i]);

            if (descriptor->run_synth)
            {
                descriptor->run_synth(handle, nframes, midi_events, midi_event_count);
            }
            else if (descriptor->run_multiple_synths)
            {
                snd_seq_event_t* dssi_events_ptr[] = { midi_events, nullptr };
                descriptor->run_multiple_synths(1, &handle, nframes, dssi_events_ptr, &midi_event_count);
            }
            else if (ldescriptor->run)
                ldescriptor->run(handle, nframes);
        }
        else
        {
            if (active_before)
            {
                if (ldescriptor->deactivate)
                    ldescriptor->deactivate(handle);
            }
        }

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Post-processing (dry/wet, volume and balance)

        if (active)
        {
            double bal_rangeL, bal_rangeR;
            jack_default_audio_sample_t old_bal_left[nframes];

            for (i=0; i < aout.count; i++)
            {
                // Dry/Wet and Volume
                for (k=0; k<nframes; k++)
                {
                    if (hints & PLUGIN_CAN_DRYWET && x_drywet != 1.0)
                    {
                        if (aout.count == 1)
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[0][k]*(1.0-x_drywet));
                        else
                            aouts_buffer[i][k] = (aouts_buffer[i][k]*x_drywet)+(ains_buffer[i][k]*(1.0-x_drywet));
                    }

                    if (hints & PLUGIN_CAN_VOL)
                        aouts_buffer[i][k] *= x_vol;
                }

                // Balance
                if (hints & PLUGIN_CAN_BALANCE)
                {
                    if (i%2 == 0)
                        memcpy(&old_bal_left, aouts_buffer[i], sizeof(jack_default_audio_sample_t)*nframes);

                    bal_rangeL = (x_bal_left+1.0)/2;
                    bal_rangeR = (x_bal_right+1.0)/2;

                    for (k=0; k<nframes; k++)
                    {
                        if (i%2 == 0)
                        {
                            // left output
                            aouts_buffer[i][k]  = old_bal_left[k]*(1.0-bal_rangeL);
                            aouts_buffer[i][k] += aouts_buffer[i+1][k]*(1.0-bal_rangeR);
                        }
                        else
                        {
                            // right
                            aouts_buffer[i][k]  = aouts_buffer[i][k]*bal_rangeR;
                            aouts_buffer[i][k] += old_bal_left[k]*bal_rangeL;
                        }
                    }
                }

                // Output VU
                if (i < 2)
                {
                    for (k=0; k<nframes; k++)
                    {
                        if (aouts_buffer[i][k] > aouts_peak_tmp[i])
                            aouts_peak_tmp[i] = aouts_buffer[i][k];
                    }
                }
            }
        }
        else
        {
            // disable any output sound if not active
            for (i=0; i < aout.count; i++)
                memset(aouts_buffer[i], 0.0f, sizeof(jack_default_audio_sample_t)*nframes);

            aouts_peak_tmp[0] = 0.0;
            aouts_peak_tmp[1] = 0.0;

        } // End of Post-processing

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Control Output

        if (param.port_out)
        {
            jack_default_audio_sample_t* cout_buffer = (jack_default_audio_sample_t*)jack_port_get_buffer(param.port_out, nframes);
            jack_midi_clear_buffer(cout_buffer);

            double value_per;

            for (k=0; k < param.count; k++)
            {
                if (param.data[k].type == PARAMETER_OUTPUT && param.data[k].midi_cc >= 0)
                {
                    value_per = (param.buffers[k] - param.ranges[k].min)/(param.ranges[k].max - param.ranges[k].min);

                    jack_midi_data_t* event_buffer = jack_midi_event_reserve(cout_buffer, 0, 3);
                    event_buffer[0] = 0xB0 + param.data[k].midi_channel;
                    event_buffer[1] = param.data[k].midi_cc;
                    event_buffer[2] = 127*value_per;
                }
            }
        } // End of Control Output

        CARLA_PROCESS_CONTINUE_CHECK;

        // --------------------------------------------------------------------------------------------------------
        // Peak Values

        ains_peak[(plugin_id*2)+0]  = ains_peak_tmp[0];
        ains_peak[(plugin_id*2)+1]  = ains_peak_tmp[1];
        aouts_peak[(plugin_id*2)+0] = aouts_peak_tmp[0];
        aouts_peak[(plugin_id*2)+1] = aouts_peak_tmp[1];

        active_before = active;
    }

    void buffer_size_changed(jack_nframes_t)
    {
    }
};

DssiOscGuiThread::DssiOscGuiThread(QObject* parent) :
    QThread(parent)
{
    plugin_id = -1;
}

void DssiOscGuiThread::set_plugin_id(short plugin_id_)
{
    plugin_id = plugin_id_;
}

void DssiOscGuiThread::run()
{
    if (plugin_id < 0 || plugin_id > MAX_PLUGINS)
    {
        qCritical("DssiOscGuiThread::run() - invalid plugin id '%i'", plugin_id);
        return;
    }

    DssiAudioPlugin* plugin = (DssiAudioPlugin*)AudioPlugins[plugin_id];

    if (!plugin)
    {
        qCritical("DssiOscGuiThread::run() - invalid plugin");
        return;
    }

    if (plugin->id != plugin_id)
    {
        qCritical("DssiOscGuiThread::run() - plugin mismatch '%i'' != '%i'", plugin->id, plugin_id);
        return;
    }

    QString cmd = QString("%1 '%2/%3' '%4' '%5' '%6 (GUI)'").arg(plugin->gui.name).arg(get_host_osc_url()).arg(plugin_id).arg(plugin->filename).arg(plugin->ldescriptor->Label).arg(plugin->name);

    qDebug("DssiOscGuiThread::run() - command: %s", cmd.toStdString().data());

    int ret;

    while (plugin && plugin->id == plugin_id && carla_is_engine_running())
    {
        if (plugin->gui.show_now)
        {
            ret = system(cmd.toStdString().data());
            plugin->gui.visible  = false;
            plugin->gui.show_now = false;

            if (ret == 0)
            {
                // Hide
                callback_action(CALLBACK_SHOW_GUI, plugin_id, 0, 0, 0.0f);
            }
            else
            {
                // Kill
                callback_action(CALLBACK_SHOW_GUI, plugin_id, -1, 0, 0.0f);
                qWarning("DssiOscGuiThread::run() - DSSI GUI crashed");
                break;
            }
        }
        else
            sleep(1);
    }

    if (plugin->gui.name)
        free((void*)plugin->gui.name);

    plugin->gui.name = nullptr;
    plugin->gui.visible = false;
}

short add_plugin_dssi(const char* filename, const char* label, void* extra_stuff)
{
    qDebug("add_plugin_dssi(%s, %s, %p)", filename, label, extra_stuff);

    short id = get_new_plugin_id();

    if (id >= 0)
    {
        DssiAudioPlugin* plugin = new DssiAudioPlugin;

        if ((plugin->lib_open(filename)))
        {
            DSSI_Descriptor_Function descfn = (DSSI_Descriptor_Function)plugin->lib_symbol("dssi_descriptor");

            if (descfn)
            {
                unsigned long i = 0;
                while ((plugin->descriptor = descfn(i++)))
                {
                    plugin->ldescriptor = plugin->descriptor->LADSPA_Plugin;
                    if (strcmp(plugin->ldescriptor->Label, label) == 0)
                        break;
                }

                if (plugin->descriptor && plugin->ldescriptor)
                {
                    plugin->handle = plugin->ldescriptor->instantiate(plugin->ldescriptor, get_sample_rate());

                    if (plugin->handle)
                    {
                        plugin->name = get_unique_name(plugin->ldescriptor->Name);
                        plugin->filename = strdup(filename);

                        if (carla_options.global_jack_client)
                            plugin->jack_client = carla_jack_client;
                        else
                            carla_jack_register_plugin(plugin);

                        if (plugin->jack_client)
                        {
                            plugin->reload();

                            plugin->id = id;
                            unique_names[id] = plugin->name;
                            AudioPlugins[id] = plugin;

                            // GUI Stuff
                            const char* gui_filename = (char*)extra_stuff;

                            if (gui_filename)
                            {
                                DssiOscGuiThread* dssi_thread = new DssiOscGuiThread();
                                dssi_thread->set_plugin_id(plugin->id);
                                dssi_thread->start();

                                plugin->gui.name   = strdup(gui_filename);
                                plugin->gui.type   = GUI_EXTERNAL_OSC;
                                plugin->osc.thread = dssi_thread;
                            }

                            if (plugin->gui.type != GUI_NONE)
                                plugin->hints |= PLUGIN_HAS_GUI;

                            osc_new_plugin(plugin);
                        }
                        else
                        {
                            set_last_error("Failed to register plugin in JACK");
                            delete plugin;
                            id = -1;
                        }
                    }
                    else
                    {
                        set_last_error("Plugin failed to initialize");
                        delete plugin;
                        id = -1;
                    }
                }
                else
                {
                    set_last_error("Could not find the requested plugin Label in the plugin library");
                    delete plugin;
                    id = -1;
                }
            }
            else
            {
                set_last_error("Could not find the DSSI Descriptor in the plugin library");
                delete plugin;
                id = -1;
            }
        }
        else
        {
            set_last_error(plugin->lib_error());
            delete plugin;
            id = -1;
        }
    }
    else
        set_last_error("Maximum number of plugins reached");

    return id;
}
