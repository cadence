#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QSettings, QString, QTimer, QVariant, SIGNAL, SLOT
from PyQt4.QtGui import QApplication, QDialog, QDialogButtonBox, QFontMetrics, QGraphicsView, QMainWindow, QPainter

# Imports (Custom Stuff)
import ui_catia, ui_settings_app
from shared_canvas import *
from shared_jack import *

try:
  from PyQt4.QtOpenGL import QGLWidget
  hasGL = True
except:
  hasGL = False

# Imports (DBus)
try:
  import dbus
  from dbus.mainloop.qt import DBusQtMainLoop
  haveDBus = True
except:
  haveDBus = False

if (haveDBus):
  DBus.loop = DBusQtMainLoop(set_as_default=True)
  DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)

  try:
    DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
    jacksettings.initBus(DBus.bus)
  except:
    DBus.jack = None

  try:
    DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
    a2j_client_name = DBus.a2j.get_jack_client_name()
  except:
    DBus.a2j = None
    a2j_client_name = None

else:
  DBus.jack = None
  DBus.a2j = None
  a2j_client_name = None

# Configure Claudia Dialog
class CatiaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent=None):
        super(CatiaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.settings = self.parent().settings
        self.loadSettings()

        self.lw_page.hideRow(2)
        self.lw_page.hideRow(3)
        self.lw_page.setCurrentCell(0, 0)
        self.lw_page.item(0, 0).setIcon(QIcon(":/48x48/catia.png"))

        self.group_main_paths.setVisible(False)
        self.group_tray.setVisible(False)

        # Disabled for Alpha release
        self.cb_canvas_eyecandy.setEnabled(False)

        if not hasGL:
          self.cb_canvas_use_opengl.setChecked(False)
          self.cb_canvas_use_opengl.setEnabled(False)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

        #self.connect(self.b_main_def_folder_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_main_def_folder.text(),
                                                       #lineEdit=self.le_main_def_folder: getAndSetPath(parent, current_path, lineEdit))

    def saveSettings(self):
        #self.settings.setValue("Main/DefaultProjectFolder", self.le_main_def_folder.text())
        self.settings.setValue("Main/RefreshInterval", self.sb_gui_refresh.value())
        self.settings.setValue("Main/JackPortAlias", self.cb_jack_port_alias.currentIndex())
        self.settings.setValue("Canvas/Theme", self.cb_canvas_theme.currentText())
        self.settings.setValue("Canvas/BezierLines", self.cb_canvas_bezier_lines.isChecked())
        self.settings.setValue("Canvas/AutoHideGroups", self.cb_canvas_hide_groups.isChecked())
        self.settings.setValue("Canvas/FancyEyeCandy", self.cb_canvas_eyecandy.isChecked())
        self.settings.setValue("Canvas/UseOpenGL", self.cb_canvas_use_opengl.isChecked())
        self.settings.setValue("Canvas/Antialiasing", self.cb_canvas_render_aa.checkState())
        self.settings.setValue("Canvas/TextAntialiasing", self.cb_canvas_render_text_aa.isChecked())
        self.settings.setValue("Canvas/HighQualityAntialiasing", self.cb_canvas_render_hq_aa.isChecked())

    def loadSettings(self):
        #self.le_main_def_folder.setText(self.settings.value("Main/DefaultProjectFolder", os.path.join(os.getenv("HOME"),"jack-projects")).toString())
        self.sb_gui_refresh.setValue(self.settings.value("Main/RefreshInterval", 100).toInt()[0])
        self.cb_jack_port_alias.setCurrentIndex(self.settings.value("Main/JackPortAlias", 2).toInt()[0])
        self.cb_canvas_bezier_lines.setChecked(self.settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(self.settings.value("Canvas/AutoHideGroups", True).toBool())
        self.cb_canvas_eyecandy.setChecked(self.settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_use_opengl.setChecked(self.settings.value("Canvas/UseOpenGL", False).toBool())
        self.cb_canvas_render_aa.setCheckState(self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(self.settings.value("Canvas/TextAntialiasing", True).toBool())
        self.cb_canvas_render_hq_aa.setChecked(self.settings.value("Canvas/HighQualityAntialiasing", False).toBool())

        theme_name = self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme())).toString()

        for i in range(patchcanvas.Theme.THEME_MAX):
          this_theme_name = patchcanvas.getThemeName(i)
          self.cb_canvas_theme.addItem(this_theme_name)
          if (this_theme_name == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

    def resetSettings(self):
        #self.le_main_def_folder.setText(os.path.join(os.getenv("HOME"),"jack-projects"))
        self.sb_gui_refresh.setValue(100)
        self.cb_jack_port_alias.setCurrentIndex(1)
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(True)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_use_opengl.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)
        self.cb_canvas_render_hq_aa.setChecked(False)

# Main Window
class CatiaMainW(QMainWindow, ui_catia.Ui_CatiaMainW):
    def __init__(self, parent=None):
        super(CatiaMainW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Catia")
        self.loadSettings()

        self.timer100 = self.startTimer(self.saved_settings["Main/RefreshInterval"])
        self.timer500 = self.startTimer(self.saved_settings["Main/RefreshInterval"]*5)

        setIcons(self, ["canvas", "jack", "transport", "misc"])

        self.act_quit.setIcon(getIcon("application-exit"))
        self.act_configure.setIcon(getIcon("configure"))

        self.group_list = []
        self.group_split_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_connection_id = 1

        self.buffer_size = 0
        self.sample_rate = 0
        self.last_buffer_size = 0
        self.last_sample_rate = 0
        self.next_sample_rate = 0

        self.last_bpm = None
        self.last_transport_state = None

        self.cb_buffer_size.clear()
        self.cb_sample_rate.clear()

        for i in range(len(buffer_sizes)):
          self.cb_buffer_size.addItem(str(buffer_sizes[i]))

        for i in range(len(sample_rates)):
          self.cb_sample_rate.addItem(str(sample_rates[i]))

        self.act_jack_bf_list = (self.act_jack_bf_16, self.act_jack_bf_32, self.act_jack_bf_64, self.act_jack_bf_128, self.act_jack_bf_256,
                                 self.act_jack_bf_512, self.act_jack_bf_1024, self.act_jack_bf_2048, self.act_jack_bf_4096, self.act_jack_bf_8192)

        self.scene = patchcanvas.PatchScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        if (self.saved_settings["Canvas/UseOpenGL"] and hasGL):
          self.graphicsView.setViewport(QGLWidget(self.graphicsView))
          self.graphicsView.setRenderHint(QPainter.HighQualityAntialiasing, self.saved_settings["Canvas/HighQualityAntialiasing"])

        p_options = patchcanvas.options_t()
        p_options.theme_name       = QString(self.saved_settings["Canvas/Theme"])
        p_options.bezier_lines     = self.saved_settings["Canvas/BezierLines"]
        p_options.antialiasing     = self.saved_settings["Canvas/Antialiasing"]
        p_options.auto_hide_groups = self.saved_settings["Canvas/AutoHideGroups"]
        p_options.fancy_eyecandy   = self.saved_settings["Canvas/FancyEyeCandy"]

        p_features = patchcanvas.features_t()
        p_features.group_info       = False
        p_features.group_rename     = False
        p_features.port_info        = True
        p_features.port_rename      = (self.saved_settings["Main/JackPortAlias"] > 0)
        p_features.handle_group_pos = True

        patchcanvas.set_options(p_options)
        patchcanvas.set_features(p_features)
        patchcanvas.init(self.scene, self.canvas_callback, DEBUG)

        # DBus Stuff
        if (haveDBus):
          if (DBus.jack and DBus.jack.IsStarted()):
            self.jackStarted()
          else:
            self.jackStarted(auto_stop=True)
            if (jack.client):
              self.act_tools_jack_start.setEnabled(False)
              self.act_tools_jack_stop.setEnabled(False)
              self.act_jack_configure.setEnabled(False)
              self.b_jack_configure.setEnabled(False)

          if (DBus.a2j):
            if (DBus.a2j.is_started()):
              self.a2jStarted()
            else:
              self.a2jStopped()
          else:
            self.act_tools_a2j_start.setEnabled(False)
            self.act_tools_a2j_stop.setEnabled(False)
            self.act_tools_a2j_export_hw.setEnabled(False)
            self.menu_A2J_Bridge.setEnabled(False)

        else: #No DBus
          self.act_tools_jack_start.setEnabled(False)
          self.act_tools_jack_stop.setEnabled(False)
          self.act_jack_configure.setEnabled(False)
          self.b_jack_configure.setEnabled(False)
          self.act_tools_a2j_start.setEnabled(False)
          self.act_tools_a2j_stop.setEnabled(False)
          self.act_tools_a2j_export_hw.setEnabled(False)
          self.menu_A2J_Bridge.setEnabled(False)

          self.jackStarted(auto_stop=True)

        self.cb_sample_rate.setEnabled(DBus.jack != None)

        setCanvasConnections(self)
        setJackConnections(self, ["jack", "buffer-size", "transport", "misc"])

        self.connect(self.act_tools_jack_start, SIGNAL("triggered()"), self.JackServerStart)
        self.connect(self.act_tools_jack_stop, SIGNAL("triggered()"), self.JackServerStop)
        self.connect(self.act_tools_a2j_start, SIGNAL("triggered()"), self.A2JBridgeStart)
        self.connect(self.act_tools_a2j_stop, SIGNAL("triggered()"), self.A2JBridgeStop)
        self.connect(self.act_tools_a2j_export_hw, SIGNAL("triggered()"), self.A2JBridgeExportHW)

        self.connect(self.act_configure, SIGNAL("triggered()"), self.configureCatia)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCatia)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("XRunCallback"), self._XRunCallback)
        self.connect(self, SIGNAL("BufferSizeCallback"), self._BufferSizeCallback)
        self.connect(self, SIGNAL("SampleRateCallback"), self._SampleRateCallback)
        self.connect(self, SIGNAL("ClientRegistrationCallback"), self._ClientRegistrationCallback)
        self.connect(self, SIGNAL("PortRegistrationCallback"), self._PortRegistrationCallback)
        self.connect(self, SIGNAL("PortConnectCallback"), self._PortConnectCallback)
        self.connect(self, SIGNAL("PortRenameCallback"), self._PortRenameCallback)
        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)

        if (not LINUX):
          self.act_show_logs.setEnabled(False)

        if (DBus.jack or DBus.a2j):
          DBus.bus.add_signal_receiver(self.DBusSignalReceiver, destination_keyword='dest', path_keyword='path',
                          member_keyword='member', interface_keyword='interface', sender_keyword='sender', )

    def showWindow(self):
        if (self.isMaximized()):
          self.showMaximized()
        else:
          self.showNormal()

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()
        elif (kwds['interface'] == "org.gna.home.a2jmidid.control"):
          if (kwds['member'] == "bridge_started"):
            self.a2jStarted()
          elif (kwds['member'] == "bridge_stopped"):
            self.a2jStopped()

    def DBusReconnect(self):
        DBus.bus = dbus.SessionBus(mainloop=DBus.loop)
        try:
          DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        except:
          DBus.jack = None
        try:
          DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
          a2j_client_name = DBus.a2j.get_jack_client_name()
        except:
          DBus.a2j = None
          a2j_client_name = None

    def jackStarted(self, auto_stop=False):
        if (not jack.client):
          jack.client = jacklib.client_open("catia", jacklib.NoStartServer, 0)
          if (auto_stop and not jack.client):
            self.jackStopped()
            return

        self.act_jack_render.setEnabled(canRender)
        self.b_jack_render.setEnabled(canRender)
        self.menu_Transport.setEnabled(True)
        self.group_transport.setEnabled(True)
        self.menuJackServer(True)

        if (DBus.jack):
          self.cb_sample_rate.setEnabled(True)

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(100)
        self.pb_dsp_load.update()

        self.init_jack()

    def jackStopped(self):
        if (haveDBus):
          self.DBusReconnect()

        if (jack.client):
          # client already closed
          jack.client = None

        self.act_jack_render.setEnabled(False)
        self.b_jack_render.setEnabled(False)
        self.menu_Transport.setEnabled(False)
        self.group_transport.setEnabled(False)
        self.menuJackServer(False)

        if (DBus.jack):
          setBufferSize(self, jacksettings.getBufferSize())
          setSampleRate(self, jacksettings.getSampleRate())
          setRealTime(self, jacksettings.isRealtime())
          setXruns(self, -1)
        else:
          self.cb_buffer_size.setEnabled(False)
          self.cb_sample_rate.setEnabled(False)
          self.menu_Jack_Buffer_Size.setEnabled(False)

        if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
          self.label_time.setText("00:00:00")
        elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
          self.label_time.setText("000|0|0000")
        elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
          self.label_time.setText("000'000'000")

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(0)
        self.pb_dsp_load.update()

        if (self.next_sample_rate):
          jack_sample_rate(self, self.next_sample_rate)

        patchcanvas.clear()

    def a2jStarted(self):
        self.menuA2JBridge(True)

    def a2jStopped(self):
        self.menuA2JBridge(False)

    def JackServerStart(self):
        if (DBus.jack):
          return DBus.jack.StartServer()
        else:
          return False

    def JackServerStop(self):
        if (DBus.jack):
          return DBus.jack.StopServer()
        else:
          return False

    def A2JBridgeStart(self):
        return DBus.a2j.start()

    def A2JBridgeStop(self):
        return DBus.a2j.stop()

    def A2JBridgeExportHW(self):
        ask = QMessageBox.question(self, self.tr("A2J Hardware Export"), self.tr("Enable Hardware Export on the A2J Bridge?"),
            QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel, QMessageBox.No)
        if (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(True)
        elif (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(False)

    def menuJackServer(self, started):
        if (DBus.jack):
          self.act_tools_jack_start.setEnabled(not started)
          self.act_tools_jack_stop.setEnabled(started)
          self.menuA2JBridge(False)

    def menuA2JBridge(self, started):
        if (DBus.jack and DBus.jack.IsStarted()):
          self.act_tools_a2j_start.setEnabled(not started)
          self.act_tools_a2j_stop.setEnabled(started)
          self.act_tools_a2j_export_hw.setEnabled(not started)
        else:
          self.act_tools_a2j_start.setEnabled(False)
          self.act_tools_a2j_stop.setEnabled(False)

    def canvas_callback(self, action, value1, value2, value_str):
        if (action == patchcanvas.ACTION_GROUP_INFO):
          pass

        elif (action == patchcanvas.ACTION_GROUP_RENAME):
          pass

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = value1
          patchcanvas.splitGroup(group_id)

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = value1
          patchcanvas.joinGroup(group_id)

        elif (action == patchcanvas.ACTION_PORT_INFO):
          port_id   = value1
          port_name = jacklib.port_name(port_id)
          port_short_name = jacklib.port_short_name(port_id)
          port_flags = jacklib.port_flags(port_id)
          if (JACK2):
            port_type = jacklib.port_type_id(port_id)
          else:
            port_type = jacklib.AUDIO if ("audio" in jacklib.port_type(port_id)) else jacklib.MIDI

          aliases = jacklib.port_get_aliases(port_id)
          if (aliases[0] == 1):
            alias1text = aliases[1]
            alias2text = "(none)"
          elif (aliases[0] == 2):
            alias1text = aliases[1]
            alias2text = aliases[2]
          else:
            alias1text = "(none)"
            alias2text = "(none)"

          port_latency = jacklib.port_get_latency(port_id)
          port_total_latency = jacklib.port_get_total_latency(jack.client, port_id)

          group_name = port_name.split(":")[0]

          flags = []
          if (port_flags & jacklib.PortIsInput):
            flags.append(self.tr("Input"))
          if (port_flags & jacklib.PortIsOutput):
            flags.append(self.tr("Output"))
          if (port_flags & jacklib.PortIsPhysical):
            flags.append(self.tr("Physical"))
          if (port_flags & jacklib.PortCanMonitor):
            flags.append(self.tr("Can Monitor"))
          if (port_flags & jacklib.PortIsTerminal):
            flags.append(self.tr("Terminal"))

          flags_text = ""
          for i in range(len(flags)):
            if (flags_text): flags_text+= " | "
            flags_text += flags[i]

          if (port_type == jacklib.AUDIO):
            type_text = self.tr("Audio")
          elif (port_type == jacklib.MIDI):
            type_text = self.tr("MIDI")
          else:
            type_text = self.tr("Unknown")

          latency_text = self.tr("%1 ms (%2 frames)").arg(port_latency*1000/self.sample_rate).arg(port_latency)
          latency_total_text = self.tr("%1 ms (%2 frames)").arg(port_total_latency*1000/self.sample_rate).arg(port_total_latency)

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%3</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%4</td></tr>"
                  "<tr><td align='right'><b>Port Alias #1:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td align='right'><b>Port Alias #2:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Flags:</b></td><td>&nbsp;%7</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%8</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Latency:</b></td><td>&nbsp;%9</td></tr>"
                  "<tr><td align='right'><b>Total Port Latency:</b></td><td>&nbsp;%10</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(port_short_name).arg(port_id).arg(port_name).arg(alias1text).arg(alias2text).arg(
                  flags_text).arg(type_text).arg(latency_text).arg(latency_total_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORT_RENAME):
          port_id  = value1
          new_name = value_str

          aliases = jacklib.port_get_aliases(port_id)

          if (aliases[0] == 2):
            # JACK only allows 2 aliases, remove 2nd
            jacklib.port_unset_alias(port_id, aliases[2])

            # If we're going for 1st alias, unset it too
            if (self.saved_settings["Main/JackPortAlias"] == 1):
              jacklib.port_unset_alias(port_id, aliases[1])

          elif (aliases[0] == 1 and self.saved_settings["Main/JackPortAlias"] == 1):
            jacklib.port_unset_alias(port_id, aliases[1])

          elif (aliases[0] == 0 and self.saved_settings["Main/JackPortAlias"] == 2):
            # If 2nd alias is enable and port had no previous aliases, set the 1st alias now
            jacklib.port_set_alias(port_id, new_name)

          if (jacklib.port_set_alias(port_id, new_name) == 0):
            patchcanvas.renamePort(port_id, new_name)

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          port_a = value1
          port_b = value2
          jacklib.connect(jack.client, jacklib.port_name(port_a), jacklib.port_name(port_b))

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = value1

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][0]):
              port_a = self.connection_list[i][1]
              port_b = self.connection_list[i][2]
              break
          else:
            return

          jacklib.disconnect(jack.client, jacklib.port_name(port_a), jacklib.port_name(port_b))

    def canvas_add_group(self, group_name):
        group_id = self.last_group_id
        patchcanvas.addGroup(group_id, group_name)
        self.group_list.append([group_id, group_name])
        self.last_group_id += 1

    def canvas_remove_group(self, group_name):
        group_id = None
        for i in range(len(self.group_list)):
          if (self.group_list[i][1] == group_name):
            group_id = self.group_list[i][0]
            self.group_list.pop(i)
            break
        else:
          return

        patchcanvas.removeGroup(group_id)

    def canvas_add_port(self, port_id, group_name=None):
        group_id = self.last_group_id
        if (not group_name):
          group_name = jacklib.port_name(port_id).split(":")[0]

        if (group_name == a2j_client_name):
          full_name = jacklib.port_short_name(port_id)
          a2j_group_name = full_name.split(" [")[0]
          a2j_port_name  = full_name.split("): ")[1]
          a2j_port_mode  = full_name.split("] (")[1].split("): ")[0]
          if (a2j_group_name): group_name = a2j_group_name
          if (a2j_port_name): port_name = a2j_port_name
          if (a2j_port_mode): port_mode = jacklib.PortIsInput if (a2j_port_mode == "playback") else jacklib.PortIsOutput
          port_type = patchcanvas.PORT_TYPE_MIDI_A2J
          haveA2J = True
          if (DEBUG): print "Emulating a2j port ->", a2j_group_name+":"+a2j_port_name
        else:
          port_name = jacklib.port_short_name(port_id)
          port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
          if (JACK2):
            port_type = patchcanvas.PORT_TYPE_AUDIO_JACK if (jacklib.port_type_id(port_id) == jacklib.AUDIO) else patchcanvas.PORT_TYPE_MIDI_JACK
          else:
            port_type = patchcanvas.PORT_TYPE_AUDIO_JACK if ("audio" in jacklib.port_type(port_id)) else patchcanvas.PORT_TYPE_MIDI_JACK
          haveA2J = False

        for i in range(len(self.group_list)):
          if (haveA2J):
            if (" VST" in group_name and group_name[0].isdigit()):
              group_name = group_name.replace(group_name[0], "", 1) # <- TESTING (useful for vsthost/dssi-vst)

            if (group_name.lower() == self.group_list[i][1].lower() or # <- TESTING (useful for LMMS)
                group_name.split(" ")[0].lower() == self.group_list[i][1].split(" ")[0].lower() or # <- TESTING (useful for Renoise and Loomer plugins)
                "vst_"+group_name.rsplit(" ",1)[0].replace(" ","").lower() == self.group_list[i][1] # <- TESTING (useful for vsthost/dssi-vst)
                ):
              group_id = self.group_list[i][0]
              break
          else:
            if (group_name == self.group_list[i][1]):
              group_id = self.group_list[i][0]
              break
        else: # For clients started with no ports
          patchcanvas.addGroup(group_id, group_name)
          self.group_list.append([group_id, group_name])

        alias_n = self.saved_settings["Main/JackPortAlias"]
        if (0 < alias_n <= 2):
          aliases = jacklib.port_get_aliases(port_id)
          if (aliases[0] == 2 and alias_n == 2):
            port_name = aliases[2]
          elif (aliases[0] >= 1 and alias_n == 1):
            port_name = aliases[1]

        patchcanvas.addPort(group_id, port_id, port_name, port_mode, port_type)
        self.last_group_id += 1

        if not group_id in self.group_split_list:
          if (jacklib.port_flags(port_id) & jacklib.PortIsPhysical):
            patchcanvas.splitGroup(group_id)
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          self.group_split_list.append(group_id)

    def canvas_remove_port(self, port_id):
        patchcanvas.removePort(port_id)

    def canvas_rename_port(self, port_id, port_name):
        patchcanvas.renamePort(port_id, port_name.split(":")[-1])

    def canvas_connect_ports(self, port_a, port_b):
        connection_id = self.last_connection_id
        patchcanvas.connectPorts(connection_id, port_a, port_b)
        self.connection_list.append([connection_id, port_a, port_b])
        self.last_connection_id += 1

    def canvas_disconnect_ports(self, port_a, port_b):
        for i in range(len(self.connection_list)):
          if (self.connection_list[i][1] == port_a and self.connection_list[i][2] == port_b):
            patchcanvas.disconnectPorts(self.connection_list[i][0])
            self.connection_list.pop(i)
            break

    def init_jack(self):
        if (not jack.client): # Jack Crash/Bug ?
          self.menu_Transport.setEnabled(False)
          self.group_transport.setEnabled(False)
          return

        buffer_size = int(jacklib.get_buffer_size(jack.client))
        realtime = int(jacklib.is_realtime(jack.client))
        sample_rate = int(jacklib.get_sample_rate(jack.client))
        self.xruns = 0
        self.last_transport_state = None
        self.last_bpm = 0.0

        setBufferSize(self, buffer_size)
        setRealTime(self, realtime)
        setSampleRate(self, sample_rate)
        setXruns(self, self.xruns)

        refreshDSPLoad(self)
        refreshTransport(self)

        self.init_callbacks()
        self.init_ports()
        self.scene.zoom_fit()
        self.scene.zoom_reset()

        jacklib.activate(jack.client)

    def init_callbacks(self):
        if (not jack.client): return
        jacklib.set_buffer_size_callback(jack.client, self.JackBufferSizeCallback)
        jacklib.set_sample_rate_callback(jack.client, self.JackSampleRateCallback)
        jacklib.set_xrun_callback(jack.client, self.JackXRunCallback)

        jacklib.set_client_registration_callback(jack.client, self.JackClientRegistrationCallback)
        jacklib.set_port_registration_callback(jack.client, self.JackPortRegistrationCallback)
        jacklib.set_port_connect_callback(jack.client, self.JackPortConnectCallback)
        jacklib.on_shutdown(jack.client, self.JackShutdownCallback)

        if (JACK2):
          jacklib.set_port_rename_callback(jack.client, self.JackPortRenameCallback)

    def init_ports_prepare(self):
        pass

    def init_ports(self):
        if (not jack.client): return

        self.group_list = []
        self.group_split_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_connection_id = 1

        a2j_list = []
        port_list = jacklib.get_ports(jack.client, None, None, 0)

        h = 0
        for i in range(len(port_list)):
          if (port_list[i-h].split(":")[0] == a2j_client_name):
            port = port_list.pop(i-h)
            a2j_list.append(port)
            h += 1

        for i in range(len(a2j_list)):
          port_list.append(a2j_list[i])

        for i in range(len(port_list)):
          port_name = port_list[i]
          port_id = jacklib.port_by_name(jack.client, port_name)
          group_name = port_name.split(":")[0]
          self.canvas_add_port(port_id, group_name)

        for i in range(len(port_list)):
          port_id = jacklib.port_by_name(jack.client, port_list[i])

          # Only make connections from an output port
          if (jacklib.port_flags(port_id) & jacklib.PortIsInput):
            continue

          if (JACK2):
            port_connections = jacklib.port_get_connections(port_id)
          else:
            port_connections = jacklib.port_get_all_connections(jack.client, port_id)

          for j in range(len(port_connections)):
            port_b = jacklib.port_by_name(jack.client, port_connections[j])
            self.canvas_connect_ports(port_id, port_b)

    def jack_clear_xruns(self):
        self.xruns = 0
        setXruns(self, self.xruns)

    def JackBufferSizeCallback(self, buffer_size, arg=None):
        if (DEBUG): print "JackBufferSizeCallback", buffer_size
        self.emit(SIGNAL("BufferSizeCallback"), buffer_size)
        return 0

    def JackSampleRateCallback(self, sample_rate, arg=None):
        if (DEBUG): print "JackSampleRateCallback", sample_rate
        self.emit(SIGNAL("SampleRateCallback"), sample_rate)
        return 0

    def JackXRunCallback(self, arg=None):
        if (DEBUG): print "JackXRunCallback", self.xruns+1
        self.emit(SIGNAL("XRunCallback"))
        return 0

    def JackClientRegistrationCallback(self, client_name, register_yesno, arg=None):
        if (DEBUG): print "JackClientRegistrationCallback", client_name, register_yesno
        self.emit(SIGNAL("ClientRegistrationCallback"), client_name, register_yesno)
        return 0

    def JackPortRegistrationCallback(self, port_id, register_yesno, arg=None):
        if (DEBUG): print "JackPortRegistrationCallback", port_id, register_yesno
        self.emit(SIGNAL("PortRegistrationCallback"), port_id, register_yesno)
        return 0

    def JackPortConnectCallback(self, port_a, port_b, connect_yesno, arg=None):
        if (DEBUG): print "JackPortConnectCallback", port_a, port_b, connect_yesno
        self.emit(SIGNAL("PortConnectCallback"), port_a, port_b, connect_yesno)
        return 0

    def JackPortRenameCallback(self, port_id, old_name, new_name, arg=None):
        if (DEBUG): print "JackPortRenameCallback", port_id, old_name, new_name
        self.emit(SIGNAL("PortRenameCallback"), port_id, old_name, new_name)
        return 0

    def JackShutdownCallback(self, arg=None):
        if (DEBUG): print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _BufferSizeCallback(self, buffer_size):
        setBufferSize(self, buffer_size)

    def _SampleRateCallback(self, sample_rate):
        setSampleRate(self, sample_rate)

    def _XRunCallback(self):
        self.xruns += 1
        setXruns(self, self.xruns)

    def _ClientRegistrationCallback(self, client_name, register_yesno):
        if (register_yesno):
          self.canvas_add_group(client_name)
        else:
          self.canvas_remove_group(client_name)

    def _PortRegistrationCallback(self, port_id, register_yesno):
        if (not JACK2):
          port_id = jacklib.port_by_id(jack.client, port_id)

        if (register_yesno):
          self.canvas_add_port(port_id)
        else:
          self.canvas_remove_port(port_id)

    def _PortConnectCallback(self, port_a, port_b, connect_yesno):
        if (not JACK2):
          port_a = jacklib.port_by_id(jack.client, port_a)
          port_b = jacklib.port_by_id(jack.client, port_b)

        if (connect_yesno):
          self.canvas_connect_ports(port_a, port_b)
        else:
          self.canvas_disconnect_ports(port_a, port_b)

    def _PortRenameCallback(self, port_id, old_name, new_name):
        self.canvas_rename_port(port_id, new_name)

    def _ShutdownCallback(self):
        self.jackStopped()
        jack.client = None

    def configureCatia(self):
        CatiaSettingsW(self).exec_()

    def aboutCatia(self):
        QMessageBox.about(self, self.tr("About Catia"), self.tr("<h3>Catia</h3>"
            "<br>Version %1"
            "<br>Catia is a nice JACK Patchbay with A2J Bridge integration.<br>"
            "<br>Copyright (C) 2010-2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())
        self.settings.setValue("ShowStatusbar", self.frame_statusbar.isVisible())
        self.settings.setValue("TransportView", self.selected_transport_view)

    def loadSettings(self, size_too=True):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

        show_toolbar = self.settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        show_statusbar = self.settings.value("ShowStatusbar", True).toBool()
        self.act_settings_show_statusbar.setChecked(show_statusbar)
        self.frame_statusbar.setVisible(show_statusbar)

        transport_set_view(self, self.settings.value("TransportView", TRANSPORT_VIEW_HMS).toInt()[0])

        self.saved_settings = {
          #"Main/DefaultProjectFolder": self.settings.value("Main/DefaultProjectFolder", os.path.join(HOME, "ladish-projects")).toString(),
          "Main/RefreshInterval": self.settings.value("Main/RefreshInterval", 100).toInt()[0],
          "Main/JackPortAlias": self.settings.value("Main/JackPortAlias", 2).toInt()[0],
          "Canvas/Theme": self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme)).toString(),
          "Canvas/BezierLines": self.settings.value("Canvas/BezierLines", True).toBool(),
          "Canvas/AutoHideGroups": self.settings.value("Canvas/AutoHideGroups", True).toBool(),
          "Canvas/FancyEyeCandy": self.settings.value("Canvas/FancyEyeCandy", False).toBool(),
          "Canvas/UseOpenGL": self.settings.value("Canvas/UseOpenGL", False).toBool(),
          "Canvas/Antialiasing": self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": self.settings.value("Canvas/TextAntialiasing", True).toBool(),
          "Canvas/HighQualityAntialiasing": self.settings.value("Canvas/HighQualityAntialiasing", False).toBool()
        }

    def timerEvent(self, event):
        if (event.timerId() == self.timer100):
          if (jack.client):
            refreshTransport(self)
        elif (event.timerId() == self.timer500):
          if (jack.client):
            refreshDSPLoad(self)
          else:
            self.update()
        QMainWindow.timerEvent(self, event)

    def closeEvent(self, event):
        self.saveSettings()
        patchcanvas.clear()
        QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Catia")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/svg/catia.svg"))

    # Show GUI
    gui = CatiaMainW()
    gui.show()

    # Set-up custom signal handling
    set_up_signals(gui)

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
