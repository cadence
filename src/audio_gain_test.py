#!/usr/bin/env python
# -*- coding: utf-8 -*-

# NOTE - This is a test app to see how far jacklib can go

# Imports (Global)
import sys
from PyQt4.QtCore import QTimer, SIGNAL
from PyQt4.QtGui import QApplication, QDialog

# Imports (Custom Stuff)
import jacklib, ui_plugin_audio_gain

global x_left, x_right, v_left, v_right
x_left = x_right = 1.0
v_left = v_right = 0.0

def process_callback(nframes, arg):
  global x_left, x_right, v_left, v_right
  v_leftx = v_rightx = 0.0

  p_in1 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(in_port1, nframes))
  p_in2 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(in_port2, nframes))
  p_out1 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(out_port1, nframes))
  p_out2 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(out_port2, nframes))

  for i in range(nframes):
    p_out1[i] = p_in1[i]*x_left
    p_out2[i] = p_in2[i]*x_right

    if (abs(p_out1[i]) > v_leftx):
      v_leftx = abs(p_out1[i])

    if (abs(p_out2[i]) > v_rightx):
      v_rightx = abs(p_out2[i])

  v_left = v_leftx
  v_right = v_rightx

  return 0

# TESTING!!!!!!!!!
class AudioGainW(QDialog, ui_plugin_audio_gain.Ui_AudioGainW):
    def __init__(self, parent=None):
        super(AudioGainW, self).__init__(parent)
        self.setupUi(self)

        self.refresh = 60 #miliseconds

        self.dial_left.setPixmap(2)
        self.dial_right.setPixmap(2)
        self.dial_left.setLabel("Left")
        self.dial_right.setLabel("Right")
        self.lpeak.setChannels(1)
        self.lpeak.setMaximumWidth(20)
        self.lpeak.setRefreshRate(self.refresh*2/3)
        self.rpeak.setChannels(1)
        self.rpeak.setMaximumWidth(20)
        self.rpeak.setRefreshRate(self.refresh*2/3)

        self.connect(self.dial_left, SIGNAL("valueChanged(int)"), self.checkValueL)
        self.connect(self.dial_right, SIGNAL("valueChanged(int)"), self.checkValueR)

        self.resize(250, 150)
        self.checkPeaks()

    def checkValueL(self, value):
        global x_left
        x_left = (float(value+50)/100)*2

    def checkValueR(self, value):
        global x_right
        x_right = (float(value+50)/100)*2

    def checkPeaks(self):
        global v_left, v_right
        self.lpeak.displayMeter(1, v_left)
        self.rpeak.displayMeter(1, v_right)
        QTimer.singleShot(self.refresh, self.checkPeaks)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)

    client = jacklib.client_open("audio_gain_test", jacklib.NullOption, 0)
    in_port1 = jacklib.port_register(client, "in-left", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
    in_port2 = jacklib.port_register(client, "in-right", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
    out_port1 = jacklib.port_register(client, "out-left", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsOutput, 0)
    out_port2 = jacklib.port_register(client, "out-right", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsOutput, 0)

    jacklib.set_process_callback(client, process_callback, 0)
    jacklib.activate(client)

    # Show GUI
    gui = AudioGainW()
    gui.show()

    # App-Loop
    ret = app.exec_()

    jacklib.deactivate(client)
    jacklib.client_close(client)

    sys.exit(ret)
