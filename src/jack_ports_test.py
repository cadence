#!/usr/bin/env python
# -*- coding: utf-8 -*-

# NOTE - This is a test app to test the JACK's ability for custom port types
# WARNING - Custom port types may confuse your JACK patchbay/connections manager

# Imports (Global)
import sys
from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QApplication, QPushButton

# Imports (Custom Stuff)
import jacklib

class TestGui(QPushButton):
    def __init__(self, parent=None):
        super(TestGui, self).__init__(parent)

        self.setText("Click me to close")
        self.connect(self, SIGNAL("clicked()"), self.close)

        self.resize(200, 100)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)

    client = jacklib.client_open("ports_test", jacklib.NullOption, 0)

    # Audio
    port_audio_in = jacklib.port_register(client, "audio-in", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
    port_audio_out = jacklib.port_register(client, "audio-out", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsOutput, 0)

    # MIDI
    port_midi_in = jacklib.port_register(client, "midi-in", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsInput, 0)
    port_midi_out = jacklib.port_register(client, "midi-out", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsOutput, 0)

    # Custom 1
    port_video_in = jacklib.port_register(client, "video-in", "128 bit video data", jacklib.PortIsPhysical|jacklib.PortIsInput, 128)
    port_video_out = jacklib.port_register(client, "video-out", "128 bit video data", jacklib.PortIsPhysical|jacklib.PortIsOutput, 128)

    # Custom 2
    port_auto_in = jacklib.port_register(client, "auto-in", "32 bit float automation", jacklib.PortIsInput, 25)
    port_auto_out = jacklib.port_register(client, "auto-out", "32 bit float automation", jacklib.PortIsOutput, 25)

    jacklib.activate(client)

    # Show GUI
    gui = TestGui()
    gui.show()

    # App-Loop
    ret = app.exec_()

    jacklib.deactivate(client)
    jacklib.client_close(client)

    sys.exit(ret)
