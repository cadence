#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os, sys
from commands import getoutput
from random import randint
from PyQt4.QtCore import Qt, QVariant, SIGNAL
from PyQt4.QtGui import QIcon, QTableWidgetItem

# Imports (Custom)
import database
from xicon import XIcon

# Debug Mode
SHOW_ALL = False

# Tab Indexes
TAB_INDEX_DAW = 0
TAB_INDEX_HOST = 1
TAB_INDEX_INSTRUMENT = 2
TAB_INDEX_GHOSTESS = 3
TAB_INDEX_BRISTOL = 4
TAB_INDEX_EFFECT = 5
TAB_INDEX_TOOL = 6

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# Launcher object
class KlaudiaLauncher(object):
    def __init__(self, parent=None, settings=None, ladish_only=False):
        super(KlaudiaLauncher, self).__init__()

        self._parent = parent
        self._settings = settings
        self.ladish_only = ladish_only

        # For the custom icons
        self.MyIcons = XIcon()
        self.MyIcons.addIconPath(os.path.join(sys.path[0], "../icons"))
        self.MyIcons.addThemeName(QStringStr(QIcon.themeName()))
        self.MyIcons.addThemeName("oxygen") # Just in case...

        if (self.settings() != None and self.settings().contains("SplitterDAW") and self.settings().contains("SplitterGhostess")):
          self.loadSettings()
        else: # First-Run
          self.parent().splitter_DAW.setSizes([500,200])
          self.parent().splitter_Host.setSizes([500,200])
          self.parent().splitter_Instrument.setSizes([500,200])
          self.parent().splitter_Ghostess.setSizes([500,200])
          self.parent().splitter_Bristol.setSizes([500,200])
          self.parent().splitter_Effect.setSizes([500,200])
          self.parent().splitter_Tool.setSizes([500,200])

        self.parent().listDAW.setColumnWidth(0, 22)
        self.parent().listDAW.setColumnWidth(1, 150)
        self.parent().listDAW.setColumnWidth(2, 125)
        self.parent().listHost.setColumnWidth(0, 22)
        self.parent().listHost.setColumnWidth(1, 150)
        self.parent().listInstrument.setColumnWidth(0, 22)
        self.parent().listInstrument.setColumnWidth(1, 150)
        self.parent().listInstrument.setColumnWidth(2, 125)
        self.parent().listGhostess.setColumnWidth(0, 22)
        self.parent().listGhostess.setColumnWidth(1, 280)
        self.parent().listBristol.setColumnWidth(0, 22)
        self.parent().listBristol.setColumnWidth(1, 100)
        self.parent().listEffect.setColumnWidth(0, 22)
        self.parent().listEffect.setColumnWidth(1, 225)
        self.parent().listEffect.setColumnWidth(2, 125)
        self.parent().listTool.setColumnWidth(0, 22)
        self.parent().listTool.setColumnWidth(1, 225)
        self.parent().listTool.setColumnWidth(2, 125)

        self.clearInfo_DAW()
        self.clearInfo_Host()
        self.clearInfo_Intrument()
        self.clearInfo_Ghostess()
        self.clearInfo_Bristol()
        self.clearInfo_Effect()
        self.clearInfo_Tool()

        self.refreshAll()

        self.connect(self.parent().tabWidget, SIGNAL("currentChanged(int)"), self.checkSelectedTab)

        self.connect(self.parent().listDAW, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedDAW)
        self.connect(self.parent().listHost, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedHost)
        self.connect(self.parent().listInstrument, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedInstrument)
        self.connect(self.parent().listGhostess, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedGhostess)
        self.connect(self.parent().listBristol, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedBristol)
        self.connect(self.parent().listEffect, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedEffect)
        self.connect(self.parent().listTool, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedTool)
        self.connect(self.parent().listDAW, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListDAW)
        self.connect(self.parent().listHost, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListHost)
        self.connect(self.parent().listInstrument, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListInstrument)
        self.connect(self.parent().listGhostess, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListGhostess)
        self.connect(self.parent().listBristol, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListBristol)
        self.connect(self.parent().listEffect, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListEffect)
        self.connect(self.parent().listTool, SIGNAL("cellDoubleClicked(int, int)"), self.doubleClickedListTool)

        self.connect(self.parent().url_documentation_daw, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_daw, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_host, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_host, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_ins, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_ins, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_ghostess, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_ghostess, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_bristol, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_bristol, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_effect, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_effect, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_documentation_tool, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.parent().url_website_tool, SIGNAL("leftClickedUrl(QString)"), self.openUrl)

    def parent(self):
        return self._parent

    def settings(self):
        return self._settings

    def connect(self, widget, signal, func):
        self.parent().connect(widget, signal, func)

    def getIcon(self, icon, size=48):
        return QIcon.fromTheme(icon, QIcon(self.MyIcons.getIconPath(icon, size)))

    def getIconForYesNo(self, yesno, size=16):
        return self.getIcon("dialog-ok-apply" if (yesno) else "dialog-cancel", size)

    def getSelectedApp(self):
        tab_index   = self.parent().tabWidget.currentIndex()
        column_name = 2 if (tab_index == TAB_INDEX_BRISTOL) else 1

        if (tab_index == TAB_INDEX_DAW):
          listSel = self.parent().listDAW
        elif (tab_index == TAB_INDEX_HOST):
          listSel = self.parent().listHost
        elif (tab_index == TAB_INDEX_INSTRUMENT):
          listSel = self.parent().listInstrument
        elif (tab_index == TAB_INDEX_GHOSTESS):
          listSel = self.parent().listGhostess
        elif (tab_index == TAB_INDEX_BRISTOL):
          listSel = self.parent().listBristol
        elif (tab_index == TAB_INDEX_EFFECT):
          listSel = self.parent().listEffect
        elif (tab_index == TAB_INDEX_TOOL):
          listSel = self.parent().listTool
        else:
          return ""

        return QStringStr(listSel.item(listSel.currentRow(), column_name).text())

    def getBinaryFromAppName(self, appname):
        list_DAW = database.list_DAW
        for Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs in list_DAW:
          if (appname == AppName):
              return Binary

        list_Host = database.list_Host
        for Package, AppName, Instruments, Effects, Binary, Icon, Save, Level, License, Features, Docs in list_Host:
          if (appname == AppName):
              return Binary

        list_Instrument = database.list_Instrument
        for Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs in list_Instrument:
          if (appname == AppName):
              return Binary

        list_Ghostess = database.list_Ghostess
        for Package, PluginName, Type, PluginBinaryLabel, Icon, Save, Level, License, Features, Docs in list_Ghostess:
          if (appname == PluginName):
              ghname = PluginBinaryLabel.split(":")[1].replace(" ","_")
              binary = "ghostess "+PluginBinaryLabel+" -hostname "+ghname
              return binary

        list_Bristol = database.list_Bristol
        for Package, AppName, Type, ShortName, Icon, Save, Level, License, Features, Docs in list_Bristol:
          if (appname == AppName):
              binary = "startBristol -audio jack -midi jack -"+ShortName
              return binary

        list_Effect = database.list_Effect
        for Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs in list_Effect:
          if (appname == AppName):
              return Binary

        list_Tool = database.list_Tool
        for Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs in list_Tool:
          if (appname == AppName):
              return Binary

        print "Failed to find binary from App name"
        return ""

    def doubleClickedListDAW(self, row, column):
        app = QStringStr(self.parent().listDAW.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListHost(self, row, column):
        app = QStringStr(self.parent().listHost.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListInstrument(self, row, column):
        app = QStringStr(self.parent().listInstrument.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListGhostess(self, row, column):
        app = QStringStr(self.parent().listGhostess.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListBristol(self, row, column):
        app = QStringStr(self.parent().listBristol.item(row, 2).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListEffect(self, row, column):
        app = QStringStr(self.parent().listEffect.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def doubleClickedListTool(self, row, column):
        app = QStringStr(self.parent().listTool.item(row, 1).text())
        if (self.ladish_only):
          self.addAppToLADISH(app)
        else:
          self.startApp(app)

    def startApp(self, app=None):
        if (not app):
          app = self.getSelectedApp()
        binary = self.getBinaryFromAppName(app)
        os.system("cd '%s' && %s &" % (QStringStr(self.parent().getProjectFolder()), binary))

    def addAppToLADISH(self, app=None):
        if (not app):
          app = self.getSelectedApp()
        binary = self.getBinaryFromAppName(app)

        if (binary.startswith("startBristol")):
          self.createAppTemplate("bristol", app, binary)

        elif (app in ["Ardour", "Ardour (32bit)", "ArdourVST", "ArdourVST (32bit)"]):
          self.createAppTemplate("ardour2", app, binary)

        elif (app == "Calf Jack Host (GIT)"):
          self.createAppTemplate("calfjackhost", app, binary)

        #elif (app == "FeSTige"):
          #self.createAppTemplate("festige", app, binary)

        elif (app in ["Hydrogen", "Hydrogen (SVN)"]):
          self.createAppTemplate("hydrogen", app, binary)

        elif (app == "Jack Mixer"):
          self.createAppTemplate("jack-mixer", app, binary)

        elif (app == "Jack Rack"):
          self.createAppTemplate("jack-rack", app, binary)

        elif (app == "Jacker"):
          self.createAppTemplate("jacker", app, binary)

        elif (app == "Jamin"):
          self.createAppTemplate("jamin", app, binary)

        elif (app in ["MusE", "MusE (SVN)"]):
          self.createAppTemplate("muse", app, binary)

        elif (app == "Non-DAW"):
          self.createAppTemplate("non-daw", app, binary)

        elif (app == "Non-Mixer"):
          self.createAppTemplate("non-mixer", app, binary)

        elif (app == "Non-Sequencer"):
          self.createAppTemplate("non-sequencer", app, binary)

        elif (app in ["Qsampler", "Qsampler (SVN)"]):
          self.createAppTemplate("qsampler", app, binary)

        elif (app in ["Qtractor", "Qtractor (SVN)", "Qtractor (32bit)"]):
          self.createAppTemplate("qtractor", app, binary)

        elif (app in ["Renoise", "Renoise (32bit)"]):
          self.createAppTemplate("renoise", app, binary)

        elif (app == "Rosegarden"):
          self.createAppTemplate("rosegarden", app, binary)

        elif (app == "Seq24"):
          self.createAppTemplate("seq24", app, binary)

        elif (app == "Yoshimi"):
          self.createAppTemplate("yoshimi", app, binary)

        else:
          appBus = self.parent().getAppBus()
          appBus.RunCustom(False, binary, str(app), 0)

    def createAppTemplate(self, app, app_name, binary):
        rand_check  = randint(1, 99999)
        proj_srate  = str(self.parent().getSampleRate())
        proj_bpm    = str(self.parent().getBPM())
        proj_folder = QStringStr(self.parent().getProjectFolder())

        tmplte_file = None
        tmplte_cmd  = ""
        tmplte_lvl  = "0"

        if (not os.path.exists(proj_folder)):
          os.mkdir(proj_folder)

        if (app == "ardour2"):
          tmplte_folder = os.path.join(proj_folder, "Ardour2_%i" % (rand_check))
          tmplte_file   = os.path.join(tmplte_folder, "Ardour2_%i.ardour" % (rand_check))
          os.mkdir(tmplte_folder)

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Ardour2", "Ardour2.ardour"), tmplte_file))
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Ardour2", "instant.xml"), tmplte_folder))
          os.mkdir(os.path.join(tmplte_folder, "analysis"))
          os.mkdir(os.path.join(tmplte_folder, "dead_sounds"))
          os.mkdir(os.path.join(tmplte_folder, "export"))
          os.mkdir(os.path.join(tmplte_folder, "interchange"))
          os.mkdir(os.path.join(tmplte_folder, "interchange", "Ardour"))
          os.mkdir(os.path.join(tmplte_folder, "interchange", "Ardour", "audiofiles"))
          os.mkdir(os.path.join(tmplte_folder, "peaks"))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_folder) if self.parent().isRoom() else tmplte_folder)

          tmplte_lvl = "0" # FIXME - detect this

        elif (app == "bristol"):
          module = binary.replace("startBristol -audio jack -midi jack -", "")
          tmplte_folder = os.path.join(proj_folder, "bristol_%s_%i" % (module, rand_check))
          os.mkdir(tmplte_folder)

          if (self.parent().isRoom()):
            tmplte_folder = os.path.basename(tmplte_folder)

          tmplte_cmd = binary
          tmplte_cmd += " -emulate %s"   % (module)
          tmplte_cmd += " -cache '%s'"   % (tmplte_folder)
          tmplte_cmd += " -memdump '%s'" % (tmplte_folder)
          tmplte_cmd += " -import '%s'"  % (os.path.join(tmplte_folder, "memory"))
          tmplte_cmd += " -exec"

          tmplte_lvl = "1"

        elif (app == "calfjackhost"):
          tmplte_file_r = os.path.join(proj_folder, "CalfJackHost_%i" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "CalfJackHost"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " --load '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

          tmplte_lvl = "1"

        elif (app == "hydrogen"):
          tmplte_file = os.path.join(proj_folder, "Hydrogen_%i.h2song" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Hydrogen.h2song"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " -s '%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

          tmplte_lvl = "1"

        elif (app == "jack-mixer"):
          tmplte_file_r = os.path.join(proj_folder, "Jack-Mixer_%i.xml" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Jack-Mixer.xml"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " -c '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

          tmplte_lvl = "1"

        elif (app == "jack-rack"):
          tmplte_file = os.path.join(proj_folder, "Jack-Rack_%i.xml" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Jack-Rack.xml"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " -s '%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

        elif (app == "jacker"):
          tmplte_file = os.path.join(proj_folder, "Jacker_%i.jsong" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Jacker.jsong"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

          # No decimal bpm support
          proj_bpm = proj_bpm.split(".")[0]

          tmplte_lvl = "1"

        elif (app == "jamin"):
          tmplte_file_r = os.path.join(proj_folder, "Jamin_%i.jam" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Jamin.jam"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " -f '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

        elif (app == "muse"):
          tmplte_file_r = os.path.join(proj_folder, "MusE_%i.med" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "MusE.med"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

        elif (app == "non-daw"):
          tmplte_folder = os.path.join(proj_folder, "Non-DAW_%i" % (rand_check))

          # Create template
          os.system("cp -r '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Non-DAW"), tmplte_folder))
          os.mkdir(os.path.join(tmplte_folder, "sources"))

          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/%s/" "%s"' % (proj_srate, os.path.join(tmplte_folder, "info")))
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/%s/" "%s"' % (proj_bpm, os.path.join(tmplte_folder, "history")))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_folder) if self.parent().isRoom() else tmplte_folder)

        elif (app == "non-mixer"):
          tmplte_folder = os.path.join(proj_folder, "Non-Mixer_%i" % (rand_check))

          # Create template
          os.system("cp -r '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Non-Mixer"), tmplte_folder))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_folder) if self.parent().isRoom() else tmplte_folder)

        elif (app == "non-sequencer"):
          tmplte_file_r = os.path.join(proj_folder, "Non-Sequencer_%i.non" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Non-Sequencer.non"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

        elif (app == "qsampler"):
          tmplte_file_r = os.path.join(proj_folder, "Qsampler_%i.lscp" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Qsampler.lscp"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

          tmplte_lvl = "1" if (app_name == "Qsampler (SVN)") else "0"

        elif (app == "qtractor"):
          tmplte_file = os.path.join(proj_folder, "Qtractor_%i.qtr" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Qtractor.qtr"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

          tmplte_lvl = "1"

        elif (app == "renoise"):
          tmplte_file_r = os.path.join(proj_folder, "Renoise_%i.xrns" % (rand_check))
          tmplte_folder = os.path.join(proj_folder, "tmp_renoise_%i" % (rand_check))

          # Create template
          os.mkdir(tmplte_folder)
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Renoise.xml"), tmplte_folder))
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/%s/" "%s"' % (proj_srate, os.path.join(tmplte_folder, "Renoise.xml")))
          os.system("cd '%s' && mv Renoise.xml Song.xml && zip '%s' Song.xml" % (tmplte_folder, tmplte_file_r))
          os.system("rm -rf '%s'" % (tmplte_folder))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

        elif (app == "rosegarden"):
          tmplte_file = os.path.join(proj_folder, "Rosegarden_%i.rg" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Rosegarden.rg"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

          tmplte_lvl = "1"

        elif (app == "seq24"):
          tmplte_file_r = os.path.join(proj_folder, "Seq24_%i.midi" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Seq24.midi"), tmplte_file_r))

          tmplte_cmd = binary
          tmplte_cmd += " '%s'" % (os.path.basename(tmplte_file_r) if self.parent().isRoom() else tmplte_file_r)

          tmplte_lvl = "1"

        elif (app == "yoshimi"):
          tmplte_file = os.path.join(proj_folder, "Yoshimi_%i.state" % (rand_check))

          # Create template
          os.system("cp '%s' '%s'" % (os.path.join(sys.path[0], "..", "templates", "Yoshimi.state"), tmplte_file))

          tmplte_cmd = binary
          tmplte_cmd += " --state='%s'" % (os.path.basename(tmplte_file) if self.parent().isRoom() else tmplte_file)

          tmplte_lvl = "1"

        else:
          print "ERROR: Failed to parse app name"
          return

        if (tmplte_file != None):
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/%s/" "%s"' % (proj_srate, tmplte_file))
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/%s/" "%s"' % (proj_bpm, tmplte_file))
          os.system('sed -i "s/X_FOLDER_X-KLAUDIA-X_FOLDER_X/%s/" "%s"' % (proj_folder.replace("/", "\/").replace("$", "\$"), tmplte_file))

        appBus = self.parent().getAppBus()
        appBus.RunCustom(False, tmplte_cmd, app_name, int(tmplte_lvl))

    def openUrl(self, qurl):
        os.system("xdg-open '%s' &" % (QStringStr(qurl)))

    def clearInfo_DAW(self):
        self.parent().ico_app_daw.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_daw.setText("App Name")
        self.parent().ico_ladspa_daw.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_dssi_daw.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_lv2_daw.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_vst_daw.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_vst_mode_daw.setText("")
        self.parent().ico_jack_transport_daw.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_midi_mode_daw.setText("---")
        self.parent().label_ladish_level_daw.setText("0")
        self.parent().frame_DAW.setEnabled(False)
        self.showDoc_DAW("", "")

    def clearInfo_Host(self):
        self.parent().ico_app_host.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_host.setText("App Name")
        self.parent().ico_ladspa_host.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_dssi_host.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_lv2_host.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_vst_host.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_vst_mode_host.setText("")
        self.parent().label_midi_mode_host.setText("---")
        self.parent().label_ladish_level_host.setText("0")
        self.parent().frame_Host.setEnabled(False)
        self.showDoc_Host("", "")

    def clearInfo_Intrument(self):
        self.parent().ico_app_ins.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_ins.setText("App Name")
        self.parent().ico_builtin_fx_ins.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_audio_input_ins.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_midi_mode_ins.setText("---")
        self.parent().label_ladish_level_ins.setText("0")
        self.parent().frame_Instrument.setEnabled(False)
        self.showDoc_Instrument("", "")

    def clearInfo_Ghostess(self):
        self.parent().ico_app_ghostess.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_ghostess.setText("Plugin Name")
        self.parent().ico_builtin_fx_ghostess.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_stereo_ghostess.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_midi_mode_ghostess.setText("---")
        self.parent().label_ladish_level_ghostess.setText("0")
        self.parent().frame_Ghostess.setEnabled(False)
        self.showDoc_Ghostess("", "")

    def clearInfo_Bristol(self):
        self.parent().ico_app_bristol.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_bristol.setText("App Name")
        self.parent().ico_builtin_fx_bristol.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().ico_audio_input_bristol.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_midi_mode_bristol.setText("---")
        self.parent().label_ladish_level_bristol.setText("0")
        self.parent().frame_Bristol.setEnabled(False)
        self.showDoc_Bristol("", "")

    def clearInfo_Effect(self):
        self.parent().ico_app_effect.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_effect.setText("App Name")
        self.parent().ico_stereo_effect.setPixmap(self.getIconForYesNo(False).pixmap(16, 16))
        self.parent().label_midi_mode_effect.setText("---")
        self.parent().label_ladish_level_effect.setText("0")
        self.parent().frame_Effect.setEnabled(False)
        self.showDoc_Effect("", "")

    def clearInfo_Tool(self):
        self.parent().ico_app_tool.setPixmap(self.getIcon("start-here").pixmap(48, 48))
        self.parent().label_name_tool.setText("App Name")
        self.parent().label_midi_mode_tool.setText("---")
        self.parent().label_ladish_level_tool.setText("0")
        self.parent().frame_Tool.setEnabled(False)
        self.showDoc_Tool("", "")

    def checkSelectedTab(self, tab_index):
        test_selected = False
        if (tab_index == TAB_INDEX_DAW):
          if (len(self.parent().listDAW.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_HOST):
          if (len(self.parent().listHost.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_INSTRUMENT):
          if (len(self.parent().listInstrument.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_GHOSTESS):
          if (len(self.parent().listGhostess.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_BRISTOL):
          if (len(self.parent().listBristol.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_EFFECT):
          if (len(self.parent().listEffect.selectedItems()) > 0):
            test_selected = True
        elif (tab_index == TAB_INDEX_TOOL):
          if (len(self.parent().listTool.selectedItems()) > 0):
            test_selected = True

        self.parent().checkGUI(test_selected)

    def checkSelectedDAW(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listDAW.item(row, 1).text())
          list_DAW = database.list_DAW

          for i in range(len(list_DAW)):
            if (app_name == list_DAW[i][1]):
              app_info = list_DAW[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_DAW.setEnabled(True)
          self.parent().ico_app_daw.setPixmap(QIcon(self.getIcon(Icon)).pixmap(48, 48))
          self.parent().ico_ladspa_daw.setPixmap(QIcon(self.getIconForYesNo(Features[0])).pixmap(16, 16))
          self.parent().ico_dssi_daw.setPixmap(QIcon(self.getIconForYesNo(Features[1])).pixmap(16, 16))
          self.parent().ico_lv2_daw.setPixmap(QIcon(self.getIconForYesNo(Features[2])).pixmap(16, 16))
          self.parent().ico_vst_daw.setPixmap(QIcon(self.getIconForYesNo(Features[3])).pixmap(16, 16))
          self.parent().ico_jack_transport_daw.setPixmap(QIcon(self.getIconForYesNo(Features[5])).pixmap(16, 16))
          self.parent().label_name_daw.setText(AppName)
          self.parent().label_vst_mode_daw.setText(Features[4])
          self.parent().ico_midi_mode_daw.setPixmap(QIcon(self.getIconForYesNo(Features[6])).pixmap(16, 16))
          self.parent().label_midi_mode_daw.setText(Features[7])
          self.parent().label_ladish_level_daw.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_DAW(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_DAW()

        self.parent().checkGUI(test_selected)

    def checkSelectedHost(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listHost.item(row, 1).text())
          list_Host = database.list_Host

          for i in range(len(list_Host)):
            if (app_name == list_Host[i][1]):
              app_info = list_Host[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Instruments, Effects, Binary, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Host.setEnabled(True)
          self.parent().ico_app_host.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().ico_internal_host.setPixmap(self.getIconForYesNo(Features[0]).pixmap(16, 16))
          self.parent().ico_ladspa_host.setPixmap(self.getIconForYesNo(Features[1]).pixmap(16, 16))
          self.parent().ico_dssi_host.setPixmap(self.getIconForYesNo(Features[2]).pixmap(16, 16))
          self.parent().ico_lv2_host.setPixmap(self.getIconForYesNo(Features[3]).pixmap(16, 16))
          self.parent().ico_vst_host.setPixmap(self.getIconForYesNo(Features[4]).pixmap(16, 16))
          self.parent().label_name_host.setText(AppName)
          self.parent().label_vst_mode_host.setText(Features[5])
          self.parent().label_midi_mode_host.setText(Features[6])
          self.parent().label_ladish_level_host.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Host(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_DAW()

        self.parent().checkGUI(test_selected)

    def checkSelectedInstrument(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listInstrument.item(row, 1).text())
          list_Instrument = database.list_Instrument

          for i in range(len(list_Instrument)):
            if (app_name == list_Instrument[i][1]):
              app_info = list_Instrument[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Instrument.setEnabled(True)
          self.parent().ico_app_ins.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().ico_builtin_fx_ins.setPixmap(self.getIconForYesNo(Features[0]).pixmap(16, 16))
          self.parent().ico_audio_input_ins.setPixmap(self.getIconForYesNo(Features[1]).pixmap(16, 16))
          self.parent().label_name_ins.setText(AppName)
          self.parent().label_midi_mode_ins.setText(Features[2])
          self.parent().label_ladish_level_ins.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Instrument(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_Intrument()

        self.parent().checkGUI(test_selected)

    def checkSelectedGhostess(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listGhostess.item(row, 1).text())
          list_Ghostess = database.list_Ghostess

          for i in range(len(list_Ghostess)):
            if (app_name == list_Ghostess[i][1]):
              app_info = list_Ghostess[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, PluginName, Type, PluginBinaryLabel, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Ghostess.setEnabled(True)
          self.parent().ico_app_ghostess.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().ico_builtin_fx_ghostess.setPixmap(self.getIconForYesNo(Features[0]).pixmap(16, 16))
          self.parent().ico_stereo_ghostess.setPixmap(self.getIconForYesNo(Features[1]).pixmap(16, 16))
          self.parent().label_name_ghostess.setText(PluginName)
          self.parent().label_midi_mode_ghostess.setText(Features[2])
          self.parent().label_ladish_level_ghostess.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Ghostess(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_Ghostess()

        self.parent().checkGUI(test_selected)

    def checkSelectedBristol(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listBristol.item(row, 2).text())
          list_Bristol = database.list_Bristol

          for i in range(len(list_Bristol)):
            if (app_name == list_Bristol[i][1]):
              app_info = list_Bristol[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Type, ShortName, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Bristol.setEnabled(True)
          self.parent().ico_app_bristol.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().ico_builtin_fx_bristol.setPixmap(self.getIconForYesNo(Features[0]).pixmap(16, 16))
          self.parent().ico_audio_input_bristol.setPixmap(self.getIconForYesNo(Features[1]).pixmap(16, 16))
          self.parent().label_name_bristol.setText(AppName)
          self.parent().label_midi_mode_bristol.setText(Features[2])
          self.parent().label_ladish_level_bristol.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Bristol(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_Bristol()

        self.parent().checkGUI(test_selected)

    def checkSelectedEffect(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listEffect.item(row, 1).text())
          list_Effect = database.list_Effect

          for i in range(len(list_Effect)):
            if (app_name == list_Effect[i][1]):
              app_info = list_Effect[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Effect.setEnabled(True)
          self.parent().ico_app_effect.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().ico_stereo_effect.setPixmap(self.getIconForYesNo(Features[0]).pixmap(16, 16))
          self.parent().label_name_effect.setText(AppName)
          self.parent().label_midi_mode_effect.setText(Features[1])
          self.parent().label_ladish_level_effect.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Effect(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_Effect()

        self.parent().checkGUI(test_selected)

    def checkSelectedTool(self, row, column, p_row, p_column):
        if (row >= 0):
          test_selected = True

          app_name = QStringStr(self.parent().listTool.item(row, 1).text())
          list_Tool = database.list_Tool

          for i in range(len(list_Tool)):
            if (app_name == list_Tool[i][1]):
              app_info = list_Tool[i]
              break
          else:
            print "ERROR: Failed to retrieve app info from database"
            return

          Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = app_info

          self.parent().frame_Tool.setEnabled(True)
          self.parent().ico_app_tool.setPixmap(self.getIcon(Icon).pixmap(48, 48))
          self.parent().label_name_tool.setText(AppName)
          self.parent().label_midi_mode_tool.setText(Features[0])
          self.parent().ico_jack_transport_tool.setPixmap(self.getIconForYesNo(Features[1]).pixmap(16, 16))
          self.parent().label_ladish_level_tool.setText(str(Level))

          Docs0 = Docs[0] if (os.path.exists(Docs[0].replace("file://",""))) else ""
          self.showDoc_Tool(Docs0, Docs[1])

        else:
          test_selected = False
          self.clearInfo_Tool()

        self.parent().checkGUI(test_selected)

    def showDoc_DAW(self, doc, web):
        self.parent().url_documentation_daw.setVisible(bool(doc))
        self.parent().url_website_daw.setVisible(bool(web))
        self.parent().label_no_help_daw.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_daw, doc)
        if (web): self.setWebUrl(self.parent().url_website_daw, web)

    def showDoc_Host(self, doc, web):
        self.parent().url_documentation_host.setVisible(bool(doc))
        self.parent().url_website_host.setVisible(bool(web))
        self.parent().label_no_help_host.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_host, doc)
        if (web): self.setWebUrl(self.parent().url_website_host, web)

    def showDoc_Instrument(self, doc, web):
        self.parent().url_documentation_ins.setVisible(bool(doc))
        self.parent().url_website_ins.setVisible(bool(web))
        self.parent().label_no_help_ins.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_ins, doc)
        if (web): self.setWebUrl(self.parent().url_website_ins, web)

    def showDoc_Ghostess(self, doc, web):
        self.parent().url_documentation_ghostess.setVisible(bool(doc))
        self.parent().url_website_ghostess.setVisible(bool(web))
        self.parent().label_no_help_ghostess.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_ghostess, doc)
        if (web): self.setWebUrl(self.parent().url_website_ghostess, web)

    def showDoc_Bristol(self, doc, web):
        self.parent().url_documentation_bristol.setVisible(bool(doc))
        self.parent().url_website_bristol.setVisible(bool(web))
        self.parent().label_no_help_bristol.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_bristol, doc)
        if (web): self.setWebUrl(self.parent().url_website_bristol, web)

    def showDoc_Effect(self, doc, web):
        self.parent().url_documentation_effect.setVisible(bool(doc))
        self.parent().url_website_effect.setVisible(bool(web))
        self.parent().label_no_help_effect.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_effect, doc)
        if (web): self.setWebUrl(self.parent().url_website_effect, web)

    def showDoc_Tool(self, doc, web):
        self.parent().url_documentation_tool.setVisible(bool(doc))
        self.parent().url_website_tool.setVisible(bool(web))
        self.parent().label_no_help_tool.setVisible(not(doc or web))

        if (doc): self.setDocUrl(self.parent().url_documentation_tool, doc)
        if (web): self.setWebUrl(self.parent().url_website_tool, web)

    def setDocUrl(self, item, link):
        item.setText(self.parent().tr("<a href='%1'>Documentation</a>").arg(link))

    def setWebUrl(self, item, link):
        item.setText(self.parent().tr("<a href='%1'>WebSite</a>").arg(link))

    def clearAll(self):
        self.parent().listDAW.clearContents()
        self.parent().listHost.clearContents()
        self.parent().listInstrument.clearContents()
        self.parent().listGhostess.clearContents()
        self.parent().listBristol.clearContents()
        self.parent().listEffect.clearContents()
        self.parent().listTool.clearContents()
        for i in range(self.parent().listDAW.rowCount()):
          self.parent().listDAW.removeRow(0)
        for i in range(self.parent().listHost.rowCount()):
          self.parent().listHost.removeRow(0)
        for i in range(self.parent().listInstrument.rowCount()):
          self.parent().listInstrument.removeRow(0)
        for i in range(self.parent().listGhostess.rowCount()):
          self.parent().listGhostess.removeRow(0)
        for i in range(self.parent().listBristol.rowCount()):
          self.parent().listBristol.removeRow(0)
        for i in range(self.parent().listEffect.rowCount()):
          self.parent().listEffect.removeRow(0)
        for i in range(self.parent().listTool.rowCount()):
          self.parent().listTool.removeRow(0)

    def refreshAll(self):
        self.clearAll()

        if (not SHOW_ALL):
          pkglist = []

          if (os.path.exists("/usr/bin/yaourt")):
            pkg_out = getoutput("/usr/bin/yaourt -Qsq").split("\n")
            for package in pkg_out:
              pkglist.append(package)

          elif (os.path.exists("/usr/bin/pacman")):
            pkg_out = getoutput("/usr/bin/pacman -Qsq").split("\n")
            for package in pkg_out:
              pkglist.append(package)

          elif (os.path.exists("/usr/bin/dpkg-query")):
            pkg_out = getoutput("/usr/bin/dpkg-query -W --showformat='${Package}\n'").split("\n")
            for package in pkg_out:
              pkglist.append(package)

          elif (os.path.exists("/usr/bin/equery")):
            pkg_out = getoutput("/usr/bin/equery list").split("\n")

            for pkg_info in pkg_out:
              if pkg_info.startswith("[I--] "):
                pkg_info_small = pkg_info.replace("[I--] ","",1).split("]",1)[1].split("/",1)[1].split("(",1)[0]

                # Find '-' that splits package's name-version
                pkg_info_split = pkg_info_small.split("-")
                pkg_info_word_count = 0

                for i in range(len(pkg_info_split)):
                  if (i == 0): continue
                  else: pkg_info_word_count += len(pkg_info_split[i-1])

                  if (pkg_info_split[i][0].isdigit()):
                    pkg_info_sep = i + pkg_info_word_count -1
                    break

                else:
                  # Could not find info separator
                  continue

                package = pkg_info_small[0:pkg_info_sep]
                pkglist.append(package)

        if (not SHOW_ALL and not "ghostess" in pkglist):
          self.parent().tabWidget.setTabEnabled(TAB_INDEX_GHOSTESS, False)

        if (not SHOW_ALL and not "bristol" in pkglist):
          self.parent().tabWidget.setTabEnabled(TAB_INDEX_BRISTOL, False)

        last_pos = 0
        list_DAW = database.list_DAW
        for i in range(len(list_DAW)):
          if (SHOW_ALL or list_DAW[i][0] in pkglist):
            Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = list_DAW[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listDAW.insertRow(last_pos)
            self.parent().listDAW.setItem(last_pos, 0, w_icon)
            self.parent().listDAW.setItem(last_pos, 1, w_name)
            self.parent().listDAW.setItem(last_pos, 2, w_type)
            self.parent().listDAW.setItem(last_pos, 3, w_save)
            self.parent().listDAW.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Host = database.list_Host
        for i in range(len(list_Host)):
          if (SHOW_ALL or list_Host[i][0] in pkglist):
            Package, AppName, Instruments, Effects, Binary, Icon, Save, Level, License, Features, Docs = list_Host[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_h_in = QTableWidgetItem(Instruments)
            w_h_ef = QTableWidgetItem(Effects)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listHost.insertRow(last_pos)
            self.parent().listHost.setItem(last_pos, 0, w_icon)
            self.parent().listHost.setItem(last_pos, 1, w_name)
            self.parent().listHost.setItem(last_pos, 2, w_h_in)
            self.parent().listHost.setItem(last_pos, 3, w_h_ef)
            self.parent().listHost.setItem(last_pos, 4, w_save)
            self.parent().listHost.setItem(last_pos, 5, w_licc)

            last_pos += 1

        last_pos = 0
        list_Instrument = database.list_Instrument
        for i in range(len(list_Instrument)):
          if (SHOW_ALL or list_Instrument[i][0] in pkglist):
            Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = list_Instrument[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listInstrument.insertRow(last_pos)
            self.parent().listInstrument.setItem(last_pos, 0, w_icon)
            self.parent().listInstrument.setItem(last_pos, 1, w_name)
            self.parent().listInstrument.setItem(last_pos, 2, w_type)
            self.parent().listInstrument.setItem(last_pos, 3, w_save)
            self.parent().listInstrument.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Ghostess = database.list_Ghostess
        for i in range(len(list_Ghostess)):
          if (SHOW_ALL or list_Ghostess[i][0] in pkglist):
            Package, PluginName, Type, PluginBinaryLabel, Icon, Save, Level, License, Features, Docs = list_Ghostess[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(PluginName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listGhostess.insertRow(last_pos)
            self.parent().listGhostess.setItem(last_pos, 0, w_icon)
            self.parent().listGhostess.setItem(last_pos, 1, w_name)
            self.parent().listGhostess.setItem(last_pos, 2, w_type)
            self.parent().listGhostess.setItem(last_pos, 3, w_save)
            self.parent().listGhostess.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Bristol = database.list_Bristol
        for i in range(len(list_Bristol)):
          if (SHOW_ALL or list_Bristol[i][0] in pkglist):
            Package, FullName, Type, ShortName, Icon, Save, Level, License, Features, Docs = list_Bristol[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_fullname  = QTableWidgetItem(FullName)
            w_shortname = QTableWidgetItem(ShortName)

            self.parent().listBristol.insertRow(last_pos)
            self.parent().listBristol.setItem(last_pos, 0, w_icon)
            self.parent().listBristol.setItem(last_pos, 1, w_shortname)
            self.parent().listBristol.setItem(last_pos, 2, w_fullname)

            last_pos += 1

        last_pos = 0
        list_Effect = database.list_Effect
        for i in range(len(list_Effect)):
          if (SHOW_ALL or list_Effect[i][0] in pkglist):
            Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = list_Effect[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listEffect.insertRow(last_pos)
            self.parent().listEffect.setItem(last_pos, 0, w_icon)
            self.parent().listEffect.setItem(last_pos, 1, w_name)
            self.parent().listEffect.setItem(last_pos, 2, w_type)
            self.parent().listEffect.setItem(last_pos, 3, w_save)
            self.parent().listEffect.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Tool = database.list_Tool
        for i in range(len(list_Tool)):
          if (SHOW_ALL or list_Tool[i][0] in pkglist):
            Package, AppName, Type, Binary, Icon, Save, Level, License, Features, Docs = list_Tool[i]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.getIcon(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(License)

            self.parent().listTool.insertRow(last_pos)
            self.parent().listTool.setItem(last_pos, 0, w_icon)
            self.parent().listTool.setItem(last_pos, 1, w_name)
            self.parent().listTool.setItem(last_pos, 2, w_type)
            self.parent().listTool.setItem(last_pos, 3, w_save)
            self.parent().listTool.setItem(last_pos, 4, w_licc)

            last_pos += 1

        self.parent().listDAW.setCurrentCell(-1, -1)
        self.parent().listHost.setCurrentCell(-1, -1)
        self.parent().listInstrument.setCurrentCell(-1, -1)
        self.parent().listGhostess.setCurrentCell(-1, -1)
        self.parent().listBristol.setCurrentCell(-1, -1)
        self.parent().listEffect.setCurrentCell(-1, -1)
        self.parent().listTool.setCurrentCell(-1, -1)

        self.parent().listDAW.sortByColumn(1, Qt.AscendingOrder)
        self.parent().listHost.sortByColumn(1, Qt.AscendingOrder)
        self.parent().listInstrument.sortByColumn(1, Qt.AscendingOrder)
        self.parent().listGhostess.sortByColumn(1, Qt.AscendingOrder)
        self.parent().listBristol.sortByColumn(2, Qt.AscendingOrder)
        self.parent().listEffect.sortByColumn(1, Qt.AscendingOrder)
        self.parent().listTool.sortByColumn(1, Qt.AscendingOrder)

    def saveSettings(self):
        self.settings().setValue("SplitterDAW", QVariant(self.parent().splitter_DAW.saveState()))
        self.settings().setValue("SplitterHost", QVariant(self.parent().splitter_Host.saveState()))
        self.settings().setValue("SplitterInstrument", QVariant(self.parent().splitter_Instrument.saveState()))
        self.settings().setValue("SplitterGhostess", QVariant(self.parent().splitter_Ghostess.saveState()))
        self.settings().setValue("SplitterBristol", QVariant(self.parent().splitter_Bristol.saveState()))
        self.settings().setValue("SplitterEffect", QVariant(self.parent().splitter_Effect.saveState()))
        self.settings().setValue("SplitterTool", QVariant(self.parent().splitter_Tool.saveState()))

    def loadSettings(self):
        self.parent().splitter_DAW.restoreState(self.settings().value("SplitterDAW").toByteArray())
        self.parent().splitter_Host.restoreState(self.settings().value("SplitterHost").toByteArray())
        self.parent().splitter_Instrument.restoreState(self.settings().value("SplitterInstrument").toByteArray())
        self.parent().splitter_Ghostess.restoreState(self.settings().value("SplitterGhostess").toByteArray())
        self.parent().splitter_Bristol.restoreState(self.settings().value("SplitterBristol").toByteArray())
        self.parent().splitter_Effect.restoreState(self.settings().value("SplitterEffect").toByteArray())
        self.parent().splitter_Tool.restoreState(self.settings().value("SplitterTool").toByteArray())
