#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QTimer, SIGNAL
from PyQt4.QtGui import QFileDialog, QImage, QPainter, QPrinter, QPrintDialog

# Imports (Custom Stuff)
import patchcanvas

# Shared Canvas code
def canvas_arrange(self):
    patchcanvas.Arrange()

    if ("miniCanvasPreview" in dir(self)):
      self.miniCanvasPreview.update()

      items = self.scene.items()
      ret_items = []
      update = False

      for i in range(len(items)):
        if (items[i].isVisible() and items[i].type() == patchcanvas.CanvasBoxType):
          use_xy2 = True if (items[i].splitted and items[i].splitted_mode == patchcanvas.PORT_MODE_INPUT) else False
          ret_items.append((items[i].group_id, items[i].scenePos(), use_xy2))
          update = True

      if (update):
        self.canvas_items_moved(ret_items)

def canvas_refresh(self):
    self.init_ports_prepare()
    patchcanvas.clear()
    self.init_ports()

def canvas_zoom_fit(self):
    self.scene.zoom_fit()

def canvas_zoom_in(self):
    self.scene.zoom_in()

def canvas_zoom_out(self):
    self.scene.zoom_out()

def canvas_zoom_reset(self):
    self.scene.zoom_reset()

def canvas_print(self):
    self.scene.clearSelection()
    self.printer = QPrinter()
    dialog = QPrintDialog(self.printer, self)
    if (dialog.exec_()):
      painter = QPainter(self.printer)
      painter.setRenderHint(QPainter.Antialiasing)
      painter.setRenderHint(QPainter.TextAntialiasing)
      self.scene.render(painter)

def canvas_save_image(self):
    path = QFileDialog.getSaveFileName(self, self.tr("Save Image"), filter=self.tr("PNG Image (*.png);;JPEG Image (*.jpg)"))

    if (not path.isEmpty()):
      self.scene.clearSelection()
      if (path.endsWith(".jpg", Qt.CaseInsensitive)):
        img_format = "JPG"
      else:
        img_format = "PNG"

      self.image = QImage(self.scene.sceneRect().width(), self.scene.sceneRect().height(), QImage.Format_RGB32)
      painter = QPainter(self.image)
      painter.setRenderHint(QPainter.Antialiasing)
      painter.setRenderHint(QPainter.TextAntialiasing)
      self.scene.render(painter)
      self.image.save(path, img_format, 100)

# Shared Connections
def setCanvasConnections(self):
  self.act_canvas_arrange.setEnabled(False)
  #self.connect(self.act_canvas_arrange, SIGNAL("triggered()"), lambda: canvas_arrange(self))
  self.connect(self.act_canvas_refresh, SIGNAL("triggered()"), lambda: canvas_refresh(self))
  self.connect(self.act_canvas_zoom_fit, SIGNAL("triggered()"), lambda: canvas_zoom_fit(self))
  self.connect(self.act_canvas_zoom_in, SIGNAL("triggered()"), lambda: canvas_zoom_in(self))
  self.connect(self.act_canvas_zoom_out, SIGNAL("triggered()"), lambda: canvas_zoom_out(self))
  self.connect(self.act_canvas_zoom_100, SIGNAL("triggered()"), lambda: canvas_zoom_reset(self))
  self.connect(self.act_canvas_print, SIGNAL("triggered()"), lambda: canvas_print(self))
  self.connect(self.act_canvas_save_image, SIGNAL("triggered()"), lambda: canvas_save_image(self))
  self.connect(self.b_canvas_zoom_fit, SIGNAL("clicked()"), lambda: canvas_zoom_fit(self))
  self.connect(self.b_canvas_zoom_in, SIGNAL("clicked()"), lambda: canvas_zoom_in(self))
  self.connect(self.b_canvas_zoom_out, SIGNAL("clicked()"), lambda: canvas_zoom_out(self))
  self.connect(self.b_canvas_zoom_100, SIGNAL("clicked()"), lambda: canvas_zoom_reset(self))
