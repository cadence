#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QSettings, QString, QTimer, QVariant, SLOT
from PyQt4.QtGui import QApplication, QDialog, QDialogButtonBox, QFileDialog, QGraphicsView, QMainWindow, QMessageBox, QPainter, QTableWidgetItem
from PyQt4.QtXml import QDomDocument

# Imports (Custom Stuff)
import patchcanvas
import ui_catarina, ui_settings_app, icons_rc
import ui_catarina_addgroup, ui_catarina_removegroup, ui_catarina_renamegroup
import ui_catarina_addport, ui_catarina_removeport, ui_catarina_renameport
import ui_catarina_connectports, ui_catarina_disconnectports
from shared_canvas import *
from shared import *

try:
  from PyQt4.QtOpenGL import QGLWidget
  hasGL = True
except:
  hasGL = False

iGroupId     = 0
iGroupName   = 1
iGroupSplit  = 2
iGroupIcon   = 3

iGroupPosId  = 0
iGroupPosX_o = 1
iGroupPosY_o = 2
iGroupPosX_i = 3
iGroupPosY_i = 4

iPortGroup   = 0
iPortId      = 1
iPortName    = 2
iPortMode    = 3
iPortType    = 4

iConnId      = 0
iConnOutput  = 1
iConnInput   = 2

# Configure Catarina Dialog
class CatarinaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent):
        super(CatarinaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.settings = self.parent().settings
        self.loadSettings()

        self.lw_page.hideRow(0)
        self.lw_page.hideRow(2)
        self.lw_page.hideRow(3)
        self.lw_page.setCurrentCell(1, 0)

        # Disabled for Alpha release
        self.cb_canvas_eyecandy.setEnabled(False)

        if not hasGL:
          self.cb_canvas_use_opengl.setChecked(False)
          self.cb_canvas_use_opengl.setEnabled(False)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

    def saveSettings(self):
        self.settings.setValue("Canvas/Theme", self.cb_canvas_theme.currentText())
        self.settings.setValue("Canvas/BezierLines", self.cb_canvas_bezier_lines.isChecked())
        self.settings.setValue("Canvas/AutoHideGroups", self.cb_canvas_hide_groups.isChecked())
        self.settings.setValue("Canvas/FancyEyeCandy", self.cb_canvas_eyecandy.isChecked())
        self.settings.setValue("Canvas/UseOpenGL", self.cb_canvas_use_opengl.isChecked())
        self.settings.setValue("Canvas/Antialiasing", self.cb_canvas_render_aa.checkState())
        self.settings.setValue("Canvas/TextAntialiasing", self.cb_canvas_render_text_aa.isChecked())
        self.settings.setValue("Canvas/HighQualityAntialiasing", self.cb_canvas_render_hq_aa.isChecked())

    def loadSettings(self):
        self.cb_canvas_bezier_lines.setChecked(self.settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(self.settings.value("Canvas/AutoHideGroups", False).toBool())
        self.cb_canvas_eyecandy.setChecked(self.settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_use_opengl.setChecked(self.settings.value("Canvas/UseOpenGL", False).toBool())
        self.cb_canvas_render_aa.setCheckState(self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(self.settings.value("Canvas/TextAntialiasing", True).toBool())
        self.cb_canvas_render_hq_aa.setChecked(self.settings.value("Canvas/HighQualityAntialiasing", False).toBool())

        theme_name = self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme())).toString()

        for i in range(patchcanvas.Theme.THEME_MAX):
          this_theme_name = patchcanvas.getThemeName(i)
          self.cb_canvas_theme.addItem(this_theme_name)
          if (this_theme_name == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

    def resetSettings(self):
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(False)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_use_opengl.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)
        self.cb_canvas_render_hq_aa.setChecked(False)

# Add Group Dialog
class CatarinaAddGroupW(QDialog, ui_catarina_addgroup.Ui_CatarinaAddGroupW):
    def __init__(self, parent, group_list):
        super(CatarinaAddGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list_names = []

        for i in range(len(group_list)):
          self.group_list_names.append(group_list[i][iGroupName])

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.le_group_name, SIGNAL("textChanged(QString)"), self.checkText)

        self.ret_group_name  = ""
        self.ret_group_split = ""

    def setReturn(self):
        self.ret_group_name  = self.le_group_name.text()
        self.ret_group_split = self.cb_split.isChecked()

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or text in self.group_list_names) else True)

# Remove Group Dialog
class CatarinaRemoveGroupW(QDialog, ui_catarina_removegroup.Ui_CatarinaRemoveGroupW):
    def __init__(self, parent, group_list):
        super(CatarinaRemoveGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.addGroups(group_list)

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_group_list, SIGNAL("cellDoubleClicked(int, int)"), self.accept)
        self.connect(self.tw_group_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)

        self.ret_group_id = -1

    def addGroups(self, group_list):
        for i in range(len(group_list)):
          twi_group_id    = QTableWidgetItem(str(group_list[i][iGroupId]))
          twi_group_name  = QTableWidgetItem(group_list[i][iGroupName])
          twi_group_split = QTableWidgetItem("Yes" if (group_list[i][iGroupSplit]) else "No")
          self.tw_group_list.insertRow(i)
          self.tw_group_list.setItem(i, 0, twi_group_id)
          self.tw_group_list.setItem(i, 1, twi_group_name)
          self.tw_group_list.setItem(i, 2, twi_group_split)

    def setReturn(self):
        ret_try = self.tw_group_list.item(self.tw_group_list.currentRow(), 0).text().toInt()
        if (ret_try[1]):
          self.ret_group_id = ret_try[0]
        else:
          print "CatarinaRemoveGroupW::setReturn() - failed to get group_id"

    def checkCell(self, row, column, old_row, old_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Rename Group Dialog
class CatarinaRenameGroupW(QDialog, ui_catarina_renamegroup.Ui_CatarinaRenameGroupW):
    def __init__(self, parent, group_list):
        super(CatarinaRenameGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.addGroups(group_list)

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.cb_group_to_rename, SIGNAL("currentIndexChanged(int)"), self.checkItem)
        self.connect(self.le_new_group_name, SIGNAL("textChanged(QString)"), self.checkText)

        self.ret_group_id = -1
        self.ret_new_group_name = ""

    def addGroups(self, group_list):
        for i in range(len(group_list)):
          self.cb_group_to_rename.addItem(str(group_list[i][iGroupId])+" - "+group_list[i][iGroupName])

    def setReturn(self):
        ret_try = self.cb_group_to_rename.currentText().split(" - ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_group_id = ret_try[0]
        else:
          print "CatarinaRenameGroupW::setReturn() - failed to get group_id"
        self.ret_new_group_name = self.le_new_group_name.text()

    def checkItem(self, index):
        self.checkText(self.le_new_group_name.text())

    def checkText(self, text):
        group_name = self.cb_group_to_rename.currentText().split(" - ", 1)[1]
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or text == group_name) else True)

# Add Port Dialog
class CatarinaAddPortW(QDialog, ui_catarina_addport.Ui_CatarinaAddPortW):
    def __init__(self, parent, group_list, port_id):
        super(CatarinaAddPortW, self).__init__(parent)
        self.setupUi(self)

        self.sb_port_id.setValue(port_id)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.addGroups(group_list)

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.le_port_name, SIGNAL("textChanged(QString)"), self.checkText)

        self.ret_group_id = -1
        self.ret_new_port_name = ""
        self.ret_new_port_mode = patchcanvas.PORT_MODE_NULL
        self.ret_new_port_type = patchcanvas.PORT_TYPE_NULL

    def addGroups(self, group_list):
        for i in range(len(group_list)):
          self.cb_group.addItem(QString("%1 - %2").arg(group_list[i][iGroupId]).arg(group_list[i][iGroupName]))

    def setReturn(self):
        ret_try = self.cb_group.currentText().split(" ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_group_id = ret_try[0]
        else:
          print "CatarinaAddPortW::setReturn() - failed to get group_id"
        self.ret_new_port_name = self.le_port_name.text()
        self.ret_new_port_mode = patchcanvas.PORT_MODE_INPUT if (self.rb_flags_input.isChecked()) else patchcanvas.PORT_MODE_OUTPUT
        self.ret_new_port_type = self.cb_port_type.currentIndex()+1 # 1, 2, 3 or 4

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if text.isEmpty() else True)

# Remove Port Dialog
class CatarinaRemovePortW(QDialog, ui_catarina_removeport.Ui_CatarinaRemovePortW):
    def __init__(self, parent, group_list, port_list):
        super(CatarinaRemovePortW, self).__init__(parent)
        self.setupUi(self)

        self.tw_port_list.setColumnWidth(0, 25)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list  = port_list
        self.reAddPorts()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_port_list, SIGNAL("cellDoubleClicked(int, int)"), self.accept)
        self.connect(self.tw_port_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)
        self.connect(self.rb_input, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_output, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_audio_jack, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi_jack, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi_a2j, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi_alsa, SIGNAL("clicked()"), self.reAddPorts)

        self.ret_port_id = -1

    def reAddPorts(self):
        self.tw_port_list.clearContents()
        for i in range(self.tw_port_list.rowCount()):
          self.tw_port_list.removeRow(0)

        port_mode = patchcanvas.PORT_MODE_INPUT if (self.rb_input.isChecked()) else patchcanvas.PORT_MODE_OUTPUT

        if (self.rb_audio_jack.isChecked()):
          port_type = patchcanvas.PORT_TYPE_AUDIO_JACK
        elif (self.rb_midi_jack.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_JACK
        elif (self.rb_midi_a2j.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_A2J
        elif (self.rb_midi_alsa.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_ALSA
        else:
          print "CatarinaRemovePortW::reAddPorts() - Invalid port type"
          return

        index = 0
        for i in range(len(self.port_list)):
          if (self.port_list[i][iPortMode] == port_mode and self.port_list[i][iPortType] == port_type):
            port_id    = self.port_list[i][iPortId]
            port_name  = self.port_list[i][iPortName]
            group_name = self.findPortGroupName(self.port_list[i][iPortGroup])
            tw_port_id   = QTableWidgetItem(str(port_id))
            tw_port_name = QTableWidgetItem(group_name+":"+port_name)
            self.tw_port_list.insertRow(index)
            self.tw_port_list.setItem(index, 0, tw_port_id)
            self.tw_port_list.setItem(index, 1, tw_port_name)
            index += 1

    def findPortGroupName(self, group_id):
      for i in range(len(self.group_list)):
        if (self.group_list[i][iGroupId] == group_id):
          return self.group_list[i][iGroupName]
      return ""

    def setReturn(self):
        ret_try = self.tw_port_list.item(self.tw_port_list.currentRow(), 0).text().toInt()
        if (ret_try[1]):
          self.ret_port_id = ret_try[0]
        else:
          print "CatarinaRemovePortW::setReturn() - failed to get port_id"

    def checkCell(self, row, column, old_row, old_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Rename Port Dialog
class CatarinaRenamePortW(QDialog, ui_catarina_renameport.Ui_CatarinaRenamePortW):
    def __init__(self, parent, group_list, port_list):
        super(CatarinaRenamePortW, self).__init__(parent)
        self.setupUi(self)

        self.tw_port_list.setColumnWidth(0, 25)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list = port_list
        self.reAddPorts()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_port_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)
        self.connect(self.le_new_name, SIGNAL("textChanged(QString)"), self.checkText)

        self.connect(self.rb_input, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_output, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.reAddPorts)

        self.ret_port_id = -1
        self.ret_new_port_name = ""

    def reAddPorts(self):
        self.tw_port_list.clearContents()
        for i in range(self.tw_port_list.rowCount()):
          self.tw_port_list.removeRow(0)

        port_mode = patchcanvas.PORT_MODE_INPUT if (self.rb_input.isChecked()) else patchcanvas.PORT_MODE_OUTPUT

        if (self.rb_audio_jack.isChecked()):
          port_type = patchcanvas.PORT_TYPE_AUDIO_JACK
        elif (self.rb_midi_jack.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_JACK
        elif (self.rb_midi_a2j.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_A2J
        elif (self.rb_midi_alsa.isChecked()):
          port_type = patchcanvas.PORT_TYPE_MIDI_ALSA
        else:
          print "CatarinaRenamePortW::reAddPorts() - Invalid port type"
          return

        index = 0
        for i in range(len(self.port_list)):
          if (self.port_list[i][iPortMode] == port_mode and self.port_list[i][iPortType] == port_type):
            port_id    = self.port_list[i][iPortId]
            port_name  = self.port_list[i][iPortName]
            group_name = self.findPortGroupName(self.port_list[i][iPortGroup])
            tw_port_id   = QTableWidgetItem(str(port_id))
            tw_port_name = QTableWidgetItem(group_name+":"+port_name)
            self.tw_port_list.insertRow(index)
            self.tw_port_list.setItem(index, 0, tw_port_id)
            self.tw_port_list.setItem(index, 1, tw_port_name)
            index += 1

        self.tw_port_list.setCurrentCell(0, 0)

    def findPortGroupName(self, group_id):
      for i in range(len(self.group_list)):
        if (group_id == self.group_list[i][iGroupId]):
          return self.group_list[i][iGroupName]
      return ""

    def setReturn(self):
        ret_try = self.tw_port_list.item(self.tw_port_list.currentRow(), 0).text().toInt()
        if (ret_try[1]):
          self.ret_port_id = ret_try[0]
        else:
          print "CatarinaRenamePortW::setReturn() - failed to get port_id"
        self.ret_new_port_name = self.le_new_name.text()

    def checkCell(self):
        self.checkText(self.le_new_name.text())

    def checkText(self, text=QString("")):
        item = self.tw_port_list.item(self.tw_port_list.currentRow(), 0)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(item and not text.isEmpty())

# Connect Ports Dialog
class CatarinaConnectPortsW(QDialog, ui_catarina_connectports.Ui_CatarinaConnectPortsW):
    def __init__(self, parent, group_list, port_list):
        super(CatarinaConnectPortsW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.ports_audio_jack = []
        self.ports_midi_jack  = []
        self.ports_midi_a2j   = []
        self.ports_midi_alsa  = []
        self.group_list = group_list
        self.port_list  = port_list

        for i in range(len(self.port_list)):
          if (self.port_list[i][iPortType] == patchcanvas.PORT_TYPE_AUDIO_JACK):
            self.ports_audio_jack.append(self.port_list[i])
          elif (self.port_list[i][iPortType] == patchcanvas.PORT_TYPE_MIDI_JACK):
            self.ports_midi_jack.append(self.port_list[i])
          elif (self.port_list[i][iPortType] == patchcanvas.PORT_TYPE_MIDI_A2J):
            self.ports_midi_a2j.append(self.port_list[i])
          elif (self.port_list[i][iPortType] == patchcanvas.PORT_TYPE_MIDI_ALSA):
            self.ports_midi_alsa.append(self.port_list[i])

        self.portTypeChanged()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.lw_outputs, SIGNAL("currentRowChanged(int)"), self.checkOutSelection)
        self.connect(self.lw_inputs, SIGNAL("currentRowChanged(int)"), self.checkInSelection)

        self.ret_port_out_id = -1
        self.ret_port_in_id  = -1

    def portTypeChanged(self):
        if (self.rb_audio.isChecked()):
          ports = self.ports_audio
        elif (self.rb_midi.isChecked()):
          ports = self.ports_midi
        elif (self.rb_outro.isChecked()):
          ports = self.ports_outro
        else:
          print "CatarinaConnectPortstW::portTypeChanged() - Invalid port type"
          return
        self.showPorts(ports)

    def showPorts(self, ports):
        self.lw_outputs.clear()
        self.lw_inputs.clear()

        for i in range(len(ports)):
          if (ports[i][iPortMode] == patchcanvas.PORT_MODE_INPUT):
            self.lw_inputs.addItem(str(ports[i][iPortId])+" - '"+self.findGroupName(ports[i][iPortGroup])+":"+ports[i][iPortName]+"'")
          elif (ports[i][iPortMode] == patchcanvas.PORT_MODE_OUTPUT):
            self.lw_outputs.addItem(str(ports[i][iPortId])+" - '"+self.findGroupName(ports[i][iPortGroup])+":"+ports[i][iPortName]+"'")

    def findGroupName(self, group_id):
        for i in range(len(self.group_list)):
          if (self.group_list[i][iGroupId] == group_id):
            return self.group_list[i][iGroupName]
        return ""

    def setReturn(self):
        ret_try = self.lw_outputs.currentItem().text().split(" - ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_port_out_id = ret_try[0]
        else:
          print "CatarinaConnectPortsW::setReturn() - failed to get port_out_id"

        ret_try = self.lw_inputs.currentItem().text().split(" - ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_port_in_id = ret_try[0]
        else:
          print "CatarinaConnectPortsW::setReturn() - failed to get port_in_id"

    def checkOutSelection(self, row):
        self.checkSelection(row, self.lw_inputs.currentRow())

    def checkInSelection(self, row):
        self.checkSelection(self.lw_outputs.currentRow(), row)

    def checkSelection(self, out_row, in_row):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (out_row >= 0 and in_row >= 0) else False)

# Disconnect Ports Dialog
class CatarinaDisconnectPortsW(QDialog, ui_catarina_disconnectports.Ui_CatarinaDisconnectPortsW):
    def __init__(self, parent, group_list, port_list, connection_list):
        super(CatarinaDisconnectPortsW, self).__init__(parent)
        self.setupUi(self)

        self.tw_connections.setColumnWidth(0, 225)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list  = port_list
        self.connection_list = connection_list

        self.portTypeChanged()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.portTypeChanged)
        self.connect(self.tw_connections, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection)
        self.connect(self.tw_connections, SIGNAL("cellDoubleClicked(int, int)"), self.accept)

        self.ret_port_out_id = -1
        self.ret_port_in_id  = -1

    def portTypeChanged(self):
        if (self.rb_audio_jack.isChecked()):
          ptype = patchcanvas.PORT_TYPE_AUDIO_JACK
        elif (self.rb_midi_jack.isChecked()):
          ptype = patchcanvas.PORT_TYPE_MIDI_JACK
        elif (self.rb_midi_a2j.isChecked()):
          ptype = patchcanvas.PORT_TYPE_MIDI_A2J
        elif (self.rb_midi_alsa.isChecked()):
          ptype = patchcanvas.PORT_TYPE_MIDI_ALSA
        else:
          print "CatarinaDisconnectPortstW::portTypeChanged() - Invalid port type"
          return
        self.showPorts(ptype)

    def showPorts(self, ptype):
        self.tw_connections.clearContents()
        for i in range(self.tw_connections.rowCount()):
          self.tw_connections.removeRow(0)

        index = 0
        for i in range(len(self.connection_list)):
          if (self.findPortType(self.connection_list[i][iConnOutput]) == ptype):
            port_out_id   = self.connection_list[i][iConnOutput]
            port_out_name = self.findPortName(port_out_id)

            port_in_id    = self.connection_list[i][iConnInput]
            port_in_name  = self.findPortName(port_in_id)

            tw_port_out = QTableWidgetItem(str(port_out_id)+" - '"+port_out_name+"'")
            tw_port_in  = QTableWidgetItem(str(port_in_id)+" - '"+port_in_name+"'")

            self.tw_connections.insertRow(index)
            self.tw_connections.setItem(index, 0, tw_port_out)
            self.tw_connections.setItem(index, 1, tw_port_in)
            index += 1

    def findPortName(self, port_id):
        for i in range(len(self.port_list)):
          if (self.port_list[i][iPortId] == port_id):
            return self.findGroupName(self.port_list[i][iPortGroup])+":"+self.port_list[i][iPortName]
        return ""

    def findPortType(self, port_id):
        for i in range(len(self.port_list)):
          if (self.port_list[i][iPortId] == port_id):
            return self.port_list[i][iPortType]
        return -1

    def findGroupName(self, group_id):
        for i in range(len(self.group_list)):
          if (self.group_list[i][iGroupId] == group_id):
            return self.group_list[i][iGroupName]
        return -1

    def setReturn(self):
        ret_try = self.tw_connections.item(self.tw_connections.currentRow(), 0).text().split(" - ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_port_out_id = ret_try[0]
        else:
          print "CatarinaDisconnectPortsW::setReturn() - failed to get port_out_id"

        ret_try = self.tw_connections.item(self.tw_connections.currentRow(), 1).text().split(" - ", 1)[0].toInt()
        if (ret_try[1]):
          self.ret_port_in_id = ret_try[0]
        else:
          print "CatarinaDisconnectPortsW::setReturn() - failed to get port_in_id"

    def checkSelection(self, row, column, prev_row, prev_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Main Window
class CatarinaMainW(QMainWindow, ui_catarina.Ui_CatarinaMainW):
    def __init__(self, parent=None):
        super(CatarinaMainW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Catarina")
        self.loadSettings()

        self.act_project_new.setIcon(getIcon("document-new"))
        self.act_project_open.setIcon(getIcon("document-open"))
        self.act_project_save.setIcon(getIcon("document-save"))
        self.act_project_save_as.setIcon(getIcon("document-save-as"))
        self.b_project_new.setIcon(getIcon("document-new"))
        self.b_project_open.setIcon(getIcon("document-open"))
        self.b_project_save.setIcon(getIcon("document-save"))
        self.b_project_save_as.setIcon(getIcon("document-save-as"))

        self.act_patchbay_add_group.setIcon(getIcon("list-add"))
        self.act_patchbay_remove_group.setIcon(getIcon("edit-delete"))
        self.act_patchbay_rename_group.setIcon(getIcon("edit-rename"))
        self.act_patchbay_add_port.setIcon(getIcon("list-add"))
        self.act_patchbay_remove_port.setIcon(getIcon("list-remove"))
        self.act_patchbay_rename_port.setIcon(getIcon("edit-rename"))
        self.act_patchbay_connect_ports.setIcon(getIcon("network-connect"))
        self.act_patchbay_disconnect_ports.setIcon(getIcon("network-disconnect"))
        self.b_group_add.setIcon(getIcon("list-add"))
        self.b_group_remove.setIcon(getIcon("edit-delete"))
        self.b_group_rename.setIcon(getIcon("edit-rename"))
        self.b_port_add.setIcon(getIcon("list-add"))
        self.b_port_remove.setIcon(getIcon("list-remove"))
        self.b_port_rename.setIcon(getIcon("edit-rename"))
        self.b_ports_connect.setIcon(getIcon("network-connect"))
        self.b_ports_disconnect.setIcon(getIcon("network-disconnect"))

        setIcons(self, ["canvas"])

        self.scene = patchcanvas.PatchScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontSavePainterState, True)
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontAdjustForAntialiasing, not bool(self.saved_settings["Canvas/Antialiasing"]))
        if (self.saved_settings["Canvas/UseOpenGL"] and hasGL):
          self.graphicsView.setViewport(QGLWidget(self.graphicsView))
          self.graphicsView.setRenderHint(QPainter.HighQualityAntialiasing, self.saved_settings["Canvas/HighQualityAntialiasing"])

        p_options = patchcanvas.options_t()
        p_options.theme_name       = QString(self.saved_settings["Canvas/Theme"])
        p_options.bezier_lines     = self.saved_settings["Canvas/BezierLines"]
        p_options.antialiasing     = self.saved_settings["Canvas/Antialiasing"]
        p_options.auto_hide_groups = self.saved_settings["Canvas/AutoHideGroups"]
        p_options.fancy_eyecandy   = self.saved_settings["Canvas/FancyEyeCandy"]

        p_features = patchcanvas.features_t()
        p_features.group_info       = False
        p_features.group_rename     = True
        p_features.port_info        = True
        p_features.port_rename      = True
        p_features.handle_group_pos = False

        patchcanvas.set_options(p_options)
        patchcanvas.set_features(p_features)
        patchcanvas.init(self.scene, self.canvas_callback, DEBUG)

        self.connect(self.act_project_new, SIGNAL("triggered()"), self.func_project_new)
        self.connect(self.act_project_open, SIGNAL("triggered()"), self.func_project_open)
        self.connect(self.act_project_save, SIGNAL("triggered()"), self.func_project_save)
        self.connect(self.act_project_save_as, SIGNAL("triggered()"), self.func_project_save_as)
        self.connect(self.b_project_new, SIGNAL("clicked()"), self.func_project_new)
        self.connect(self.b_project_open, SIGNAL("clicked()"), self.func_project_open)
        self.connect(self.b_project_save, SIGNAL("clicked()"), self.func_project_save)
        self.connect(self.b_project_save_as, SIGNAL("clicked()"), self.func_project_save_as)
        self.connect(self.act_patchbay_add_group, SIGNAL("triggered()"), self.func_group_add)
        self.connect(self.act_patchbay_remove_group, SIGNAL("triggered()"), self.func_group_remove)
        self.connect(self.act_patchbay_rename_group, SIGNAL("triggered()"), self.func_group_rename)
        self.connect(self.act_patchbay_add_port, SIGNAL("triggered()"), self.func_port_add)
        self.connect(self.act_patchbay_remove_port, SIGNAL("triggered()"), self.func_port_remove)
        self.connect(self.act_patchbay_rename_port, SIGNAL("triggered()"), self.func_port_rename)
        self.connect(self.act_patchbay_connect_ports, SIGNAL("triggered()"), self.func_connect_ports)
        self.connect(self.act_patchbay_disconnect_ports, SIGNAL("triggered()"), self.func_disconnect_ports)
        self.connect(self.b_group_add, SIGNAL("clicked()"), self.func_group_add)
        self.connect(self.b_group_remove, SIGNAL("clicked()"), self.func_group_remove)
        self.connect(self.b_group_rename, SIGNAL("clicked()"), self.func_group_rename)
        self.connect(self.b_port_add, SIGNAL("clicked()"), self.func_port_add)
        self.connect(self.b_port_remove, SIGNAL("clicked()"), self.func_port_remove)
        self.connect(self.b_port_rename, SIGNAL("clicked()"), self.func_port_rename)
        self.connect(self.b_ports_connect, SIGNAL("clicked()"), self.func_connect_ports)
        self.connect(self.b_ports_disconnect, SIGNAL("clicked()"), self.func_disconnect_ports)

        setCanvasConnections(self)

        self.connect(self.act_settings_configure, SIGNAL("triggered()"), self.configureCatarina)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCatarina)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("SIGUSR1()"), self.func_project_save)

        # Dummy timer to keep events active
        self.update_timer = self.startTimer(500)

        # Start Empty Project
        self.func_project_new()

    def canvas_callback(self, action, value1, value2, value_str):
        if (action == patchcanvas.ACTION_GROUP_INFO):
          pass

        elif (action == patchcanvas.ACTION_GROUP_RENAME):
          group_id = value1
          new_group_name = value_str
          patchcanvas.renameGroup(group_id, new_group_name)

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              self.group_list[i][iGroupName] = new_group_name
              break

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = value1
          patchcanvas.splitGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              self.group_list[i][iGroupSplit] = True
              break

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = value1
          patchcanvas.joinGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              self.group_list[i][iGroupSplit] = False
              break

        elif (action == patchcanvas.ACTION_PORT_INFO):
          port_id = value1

          for i in range(len(self.port_list)):
            if (self.port_list[i][iPortId] == port_id):
              group_id  = self.port_list[i][iPortGroup]
              port_name = self.port_list[i][iPortName]
              port_mode = self.port_list[i][iPortMode]
              port_type = self.port_list[i][iPortType]
              break

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              group_name = self.group_list[i][iGroupName]
              break

          if (port_mode == patchcanvas.PORT_MODE_INPUT):
            mode_text = self.tr("Input")
          elif (port_mode == patchcanvas.PORT_MODE_OUTPUT):
            mode_text = self.tr("Output")
          else:
            mode_text = self.tr("Unknown")

          if (port_type == patchcanvas.PORT_TYPE_AUDIO_JACK):
            type_text = self.tr("JACK Audio")
          elif (port_type == patchcanvas.PORT_TYPE_MIDI_JACK):
            type_text = self.tr("JACK MIDI")
          elif (port_type == patchcanvas.PORT_TYPE_MIDI_A2J):
            type_text = self.tr("A2J MIDI")
          elif (port_type == patchcanvas.PORT_TYPE_MIDI_ALSA):
            type_text = self.tr("ALSA MIDI")
          else:
            type_text = self.tr("Unknown")

          port_full_name = group_name+":"+port_name

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Group ID:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%3</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%4</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Mode:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%7</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(group_id).arg(port_name).arg(port_id).arg(port_full_name).arg(mode_text).arg(type_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORT_RENAME):
          port_id = value1
          new_port_name = value_str
          patchcanvas.renamePort(port_id, new_port_name)

          for i in range(len(self.port_list)):
            if (self.port_list[i][iPortId] == port_id):
              self.port_list[i][iPortName] = new_port_name
              break

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          connection_id = self.last_connection_id
          port_out_id = value1
          port_in_id  = value2
          patchcanvas.connectPorts(connection_id, port_out_id, port_in_id)

          conn_obj = [None, None, None]
          conn_obj[iConnId]     = connection_id
          conn_obj[iConnOutput] = port_out_id
          conn_obj[iConnInput]  = port_in_id

          self.connection_list.append(conn_obj)
          self.last_connection_id += 1

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = value1
          patchcanvas.disconnectPorts(connection_id)

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][iConnId]):
              self.connection_list.pop(i)
              break

    def init_ports_prepare(self):
        self.group_list_pos = []
        for i in range(len(self.group_list)):
          group_id = self.group_list[i][iGroupId]
          group_pos_i = patchcanvas.getGroupPos(group_id, patchcanvas.PORT_MODE_INPUT)
          group_pos_o = patchcanvas.getGroupPos(group_id, patchcanvas.PORT_MODE_OUTPUT)
          group_pos_obj = [None, None, None, None, None]
          group_pos_obj[iGroupPosId]  = group_id
          group_pos_obj[iGroupPosX_o] = group_pos_o.x()
          group_pos_obj[iGroupPosY_o] = group_pos_o.y()
          group_pos_obj[iGroupPosX_i] = group_pos_i.x()
          group_pos_obj[iGroupPosY_i] = group_pos_i.y()
          self.group_list_pos.append(group_pos_obj)

    def init_ports(self):
        for i in range(len(self.group_list)):
          patchcanvas.addGroup(self.group_list[i][iGroupId], self.group_list[i][iGroupName], patchcanvas.SPLIT_YES if (self.group_list[i][iGroupSplit]) else patchcanvas.SPLIT_NO, self.group_list[i][iGroupIcon])

        for i in range(len(self.group_list_pos)):
          patchcanvas.setGroupPos(self.group_list_pos[i][iGroupPosId], self.group_list_pos[i][iGroupPosX_o], self.group_list_pos[i][iGroupPosY_o], self.group_list_pos[i][iGroupPosX_i], self.group_list_pos[i][iGroupPosY_i])

        for i in range(len(self.port_list)):
          patchcanvas.addPort(self.port_list[i][iPortGroup], self.port_list[i][iPortId], self.port_list[i][iPortName], self.port_list[i][iPortMode], self.port_list[i][iPortType])

        for i in range(len(self.connection_list)):
          patchcanvas.connectPorts(self.connection_list[i][iConnId], self.connection_list[i][iConnOutput], self.connection_list[i][iConnInput])

        self.group_list_pos = []

    def func_project_new(self):
        self.group_list = []
        self.group_list_pos = []
        self.port_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_port_id = 1
        self.last_connection_id = 1
        self.save_path = None
        patchcanvas.clear()

    def func_project_open(self):
        path = QFileDialog.getOpenFileName(self, self.tr("Load State"), filter=self.tr("Catarina XML Document (*.xml)"))
        if (not path.isEmpty()):
          patchcanvas.clear()
          self.save_path = path
          self.loadFile(path)

    def func_project_save(self):
        if (self.save_path):
          self.saveFile(self.save_path)
        else:
          self.func_project_save_as()

    def func_project_save_as(self):
        path = QFileDialog.getSaveFileName(self, self.tr("Save State"), filter=self.tr("Catarina XML Document (*.xml)"))
        if (not path.isEmpty()):
          self.save_path = path
          self.saveFile(path)

    def func_group_add(self):
        dialog = CatarinaAddGroupW(self, self.group_list)
        if (dialog.exec_()):
          group_id    = self.last_group_id
          group_name  = dialog.ret_group_name
          group_split = dialog.ret_group_split
          group_icon  = patchcanvas.ICON_HARDWARE if (group_split) else patchcanvas.ICON_APPLICATION
          patchcanvas.addGroup(group_id, group_name, group_split, group_icon)

          group_obj = [None, None, None, None]
          group_obj[iGroupId]    = group_id
          group_obj[iGroupName]  = group_name
          group_obj[iGroupSplit] = group_split
          group_obj[iGroupIcon]  = group_icon

          self.group_list.append(group_obj)
          self.last_group_id += 1

    def func_group_remove(self):
        dialog = CatarinaRemoveGroupW(self, self.group_list)
        if (dialog.exec_()):
          group_id = dialog.ret_group_id

          for i in range(len(self.port_list)):
            if (group_id == self.port_list[i][iPortGroup]):
              port_id = self.port_list[i][iPortId]

              h = 0
              for j in range(len(self.connection_list)):
                if (self.connection_list[j-h][iConnOutput] == port_id or self.connection_list[j-h][iConnInput] == port_id):
                  patchcanvas.disconnectPorts(self.connection_list[j-h][iConnId])
                  self.connection_list.pop(j-h)
                  h += 1

          h = 0
          for i in range(len(self.port_list)):
            if (self.port_list[i-h][iPortGroup] == group_id):
              port_id = self.port_list[i-h][iPortId]
              patchcanvas.removePort(port_id)
              self.port_list.pop(i-h)
              h += 1

          patchcanvas.removeGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              self.group_list.pop(i)
              break

    def func_group_rename(self):
        dialog = CatarinaRenameGroupW(self, self.group_list)
        if (dialog.exec_()):
          group_id       = dialog.ret_group_id
          new_group_name = dialog.ret_new_group_name
          patchcanvas.renameGroup(group_id, new_group_name)

          for i in range(len(self.group_list)):
            if (self.group_list[i][iGroupId] == group_id):
              self.group_list[i][iGroupName] = new_group_name
              break

    def func_port_add(self):
        if (len(self.group_list) > 0):
          dialog = CatarinaAddPortW(self, self.group_list, self.last_port_id)
          if (dialog.exec_()):
            group_id = dialog.ret_group_id
            new_port_name = dialog.ret_new_port_name
            new_port_mode = dialog.ret_new_port_mode
            new_port_type = dialog.ret_new_port_type
            patchcanvas.addPort(group_id, self.last_port_id, new_port_name, new_port_mode, new_port_type)

            new_port = [None, None, None, None, None]
            new_port[iPortGroup] = group_id
            new_port[iPortId]    = self.last_port_id
            new_port[iPortName]  = new_port_name
            new_port[iPortMode]  = new_port_mode
            new_port[iPortType]  = new_port_type

            self.port_list.append(new_port)
            self.last_port_id += 1

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Group first!"))

    def func_port_remove(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaRemovePortW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            port_id = dialog.ret_port_id

            h = 0
            for i in range(len(self.connection_list)):
              if (self.connection_list[i-h][iConnOutput] == port_id or self.connection_list[i-h][iConnInput] == port_id):
                patchcanvas.disconnectPorts(self.connection_list[i-h][iConnId])
                self.connection_list.pop(i-h)
                h += 1

            patchcanvas.removePort(port_id)

            for i in range(len(self.port_list)):
              if (self.port_list[i][iPortId] == port_id):
                self.port_list.pop(i)
                break

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Port first!"))

    def func_port_rename(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaRenamePortW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            port_id       = dialog.ret_port_id
            new_port_name = dialog.ret_new_port_name
            patchcanvas.renamePort(port_id, new_port_name)

            for i in range(len(self.port_list)):
              if (self.port_list[i][iPortId] == port_id):
                self.port_list[i][iPortName] = new_port_name
                break

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Port first!"))

    def func_connect_ports(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaConnectPortsW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            connection_id = self.last_connection_id
            port_out_id   = dialog.ret_port_out_id
            port_in_id    = dialog.ret_port_in_id

            for i in range(len(self.connection_list)):
              if (self.connection_list[i][iConnOutput] == port_out_id and self.connection_list[i][iConnInput] == port_in_id):
                QMessageBox.warning(self, self.tr("Warning"), self.tr("Ports already connected!"))
                return

            patchcanvas.connectPorts(connection_id, port_out_id, port_in_id)

            conn_obj = [None, None, None]
            conn_obj[iConnId]     = connection_id
            conn_obj[iConnOutput] = port_out_id
            conn_obj[iConnInput]  = port_in_id

            self.connection_list.append(conn_obj)
            self.last_connection_id += 1

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add some Ports first!"))

    def func_disconnect_ports(self):
        if (len(self.connection_list) > 0):
          dialog = CatarinaDisconnectPortsW(self, self.group_list, self.port_list, self.connection_list)
          if (dialog.exec_()):
            connection_id = 0
            port_out_id   = dialog.ret_port_out_id
            port_in_id    = dialog.ret_port_in_id

            for i in range(len(self.connection_list)):
              if (self.connection_list[i][iConnOutput] == port_out_id and self.connection_list[i][iConnInput] == port_in_id):
                connection_id = self.connection_list[i][iConnId]
                self.connection_list.pop(i)
                break

            patchcanvas.disconnectPorts(connection_id)

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please make some Connections first!"))

    def saveFile(self, path):
        content = ("<?xml version='1.0' encoding='UTF-8'?>\n"
                   "<!DOCTYPE CATARINA>\n"
                   "<CATARINA VERSION='%s'>\n") % (VERSION)

        content += " <Groups>\n"
        for i in range(len(self.group_list)):
          group_id    = self.group_list[i][iGroupId]
          group_name  = QStringStr(self.group_list[i][iGroupName])
          group_split = self.group_list[i][iGroupSplit]
          group_icon  = self.group_list[i][iGroupIcon]
          group_pos_i = patchcanvas.getGroupPos(group_id, patchcanvas.PORT_MODE_INPUT)
          group_pos_o = patchcanvas.getGroupPos(group_id, patchcanvas.PORT_MODE_OUTPUT)
          content += "  <g%i> <name>%s</name> <data>%i:%i:%i:%f:%f:%f:%f</data> </g%i>\n" % (i, group_name, group_id, group_split, group_icon,
                      group_pos_o.x(), group_pos_o.y(), group_pos_i.x(), group_pos_i.y(), i)
        content += " </Groups>\n"

        content += " <Ports>\n"
        for i in range(len(self.port_list)):
          content += "  <p%i> <name>%s</name> <data>%i:%i:%i:%i</data> </p%i>\n" % (i,
            QStringStr(self.port_list[i][iPortName]), self.port_list[i][iPortGroup], self.port_list[i][iPortId], self.port_list[i][iPortMode], self.port_list[i][iPortType], i)
        content += " </Ports>\n"

        content += " <Connections>\n"
        for i in range(len(self.connection_list)):
          content += "  <c%i>%i:%i:%i</c%i>\n" % (i, self.connection_list[i][iConnId], self.connection_list[i][iConnOutput], self.connection_list[i][iConnInput], i)
        content += " </Connections>\n"

        content += "</CATARINA>\n"

        if (open(path, "w").write(content)):
          QMessageBox.critical(self, self.tr("Error"), self.tr("Failed to save file"))

    def prepareToloadFile(self):
        QTimer.singleShot(0, self.loadFile)

    def loadFile(self, path=None):
        if (path == None):
          path = self.save_path

        if not os.path.exists(path):
          QMessageBox.critical(self, self.tr("Error"), self.tr("The file '%1' does not exist").arg(path))
          return

        read = open(path, "r").read()

        if not read:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Failed to load file"))
          return

        self.group_list = []
        self.group_list_pos = []
        self.port_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_port_id = 1
        self.last_connection_id = 1

        xml = QDomDocument()
        xml.setContent(read)

        content = xml.documentElement()
        if (content.tagName() != "CATARINA"):
          QMessageBox.critical(self, self.tr("Error"), self.tr("Not a valid Catarina file"))
          return

        # Get values from XML - the big code
        node = content.firstChild()
        while not node.isNull():
          if (node.toElement().tagName() == "Groups"):
            group_name = ""
            groups = node.toElement().firstChild()
            while not groups.isNull():
              group = groups.toElement().firstChild()
              while not group.isNull():
                tag  = group.toElement().tagName()
                text = group.toElement().text()
                if (tag == "name"):
                  group_name = text
                elif (tag == "data"):
                  group_data   = text.split(":")

                  group_obj = [None, None, None, None]
                  group_obj[iGroupId]    = int(group_data[0])
                  group_obj[iGroupName]  = group_name
                  group_obj[iGroupSplit] = int(group_data[1])
                  group_obj[iGroupIcon]  = int(group_data[2])

                  group_pos_obj = [None, None, None, None, None]
                  group_pos_obj[iGroupPosId]  = int(group_data[0])
                  group_pos_obj[iGroupPosX_o] = float(group_data[3])
                  group_pos_obj[iGroupPosY_o] = float(group_data[4])
                  group_pos_obj[iGroupPosX_i] = float(group_data[5])
                  group_pos_obj[iGroupPosY_i] = float(group_data[6])

                  self.group_list.append(group_obj)
                  self.group_list_pos.append(group_pos_obj)

                  group_id = group_obj[iGroupId]
                  if (group_id > self.last_group_id):
                    self.last_group_id = group_id+1

                group = group.nextSibling()
              groups = groups.nextSibling()

          elif (node.toElement().tagName() == "Ports"):
            port_id   = 0
            port_name = ""
            ports = node.toElement().firstChild()
            while not ports.isNull():
              port = ports.toElement().firstChild()
              while not port.isNull():
                tag  = port.toElement().tagName()
                text = port.toElement().text()
                if (tag == "name"):
                  port_name = text
                elif (tag == "data"):
                  port_data = text.split(":")
                  new_port  = [None, None, None, None, None]
                  new_port[iPortGroup] = int(port_data[0])
                  new_port[iPortId]    = int(port_data[1])
                  new_port[iPortName]  = port_name
                  new_port[iPortMode]  = int(port_data[2])
                  new_port[iPortType]  = int(port_data[3])

                  port_id = new_port[iPortId]
                  self.port_list.append(new_port)

                  if (port_id > self.last_port_id):
                    self.last_port_id = port_id+1

                port = port.nextSibling()
              ports = ports.nextSibling()

          elif (node.toElement().tagName() == "Connections"):
            conns = node.toElement().firstChild()
            while not conns.isNull():
              conn_data = conns.toElement().text().split(":")
              if (not conn_data[0].toInt()[0]):
                conns = conns.nextSibling()
                continue

              conn_obj = [None, None, None]
              conn_obj[iConnId]     = int(conn_data[0])
              conn_obj[iConnOutput] = int(conn_data[1])
              conn_obj[iConnInput]  = int(conn_data[2])

              connection_id = conn_obj[iConnId]
              self.connection_list.append(conn_obj)

              if (connection_id >= self.last_connection_id):
                self.last_connection_id = connection_id+1

              conns = conns.nextSibling()
          node = node.nextSibling()

        self.last_group_id += 1
        self.last_port_id += 1
        self.last_connection_id += 1

        self.init_ports()
        self.scene.zoom_fit()
        self.scene.zoom_reset()

    def configureCatarina(self):
        CatarinaSettingsW(self).exec_()

    def aboutCatarina(self):
        QMessageBox.about(self, self.tr("About Catarina"), self.tr("<h3>Catarina</h3>"
            "<br>Version %1"
            "<br>Catarina is a testing ground for the 'PatchCanvas' module.<br>"
            "<br>Copyright (C) 2010-2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

        show_toolbar = self.settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        self.saved_settings = {
          "Canvas/Theme": self.settings.value("Canvas/Theme", patchcanvas.getThemeName(patchcanvas.getDefaultTheme)).toString(),
          "Canvas/BezierLines": self.settings.value("Canvas/BezierLines", True).toBool(),
          "Canvas/AutoHideGroups": self.settings.value("Canvas/AutoHideGroups", False).toBool(),
          "Canvas/FancyEyeCandy": self.settings.value("Canvas/FancyEyeCandy", False).toBool(),
          "Canvas/UseOpenGL": self.settings.value("Canvas/UseOpenGL", False).toBool(),
          "Canvas/Antialiasing": self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": self.settings.value("Canvas/TextAntialiasing", True).toBool(),
          "Canvas/HighQualityAntialiasing": self.settings.value("Canvas/HighQualityAntialiasing", False).toBool()
        }

    def timerEvent(self, event):
        if (event.timerId() == self.update_timer):
          self.update()
        QMainWindow.timerEvent(self, event)

    def closeEvent(self, event):
        self.saveSettings()
        QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Catarina")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/svg/catarina.svg"))

    # Show GUI
    gui = CatarinaMainW()

    # Set-up custom signal handling
    set_up_signals(gui)

    if (app.arguments().count() > 1):
      gui.save_path = QStringStr(app.arguments()[1])
      gui.prepareToloadFile()

    gui.show()

    # App-Loop
    sys.exit(app.exec_())
