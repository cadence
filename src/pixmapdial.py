#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QPointF, QRectF, QSize, QString
from PyQt4.QtGui import QColor, QDial, QFontMetrics, QLinearGradient, QPainter, QPixmap

# Imports (Custom Stuff)
import icons_rc

# Custom Dial, using a pixmap for paiting
class PixmapDial(QDial):
    HORIZONTAL = 0
    VERTICAL   = 1

    def __init__(self, parent):
        super(PixmapDial, self).__init__(parent)

        self.pixmap_n_str = "01"
        self.pixmap = QPixmap(":/bitmaps/dial_01d.png")

        if (self.pixmap.width() > self.pixmap.height()):
          self.orientation = self.HORIZONTAL
        else:
          self.orientation = self.VERTICAL

        self.label = QString("")
        self.label_pos = QPointF(0.0, 0.0)
        self.label_width = 0
        self.label_height = 0
        self.label_gradient = QLinearGradient(0, 0, 0, 1)

        #if (self.palette().window().color().lightness() > 100):
          ## Light background
          #self.color1 = self.palette().dark().color()
          #self.color2 = QColor(0, 0, 0, 0)
          #self.tcolor = [self.palette().text().color(), self.palette().mid().color()]
        #else:
          # Dark background
        self.color1 = QColor(0, 0, 0, 255)
        self.color2 = QColor(0, 0, 0, 0)
        self.tcolor = [Qt.white, Qt.darkGray]

        self.updateSizes()

    def setEnabled(self, enabled):
        if (self.isEnabled() != enabled):
          self.pixmap.load(":/bitmaps/dial_%s%s.png" % (self.pixmap_n_str, "" if enabled else "d"))
          self.updateSizes()
          self.update()
        QDial.setEnabled(self, enabled)

    def setLabel(self, label):
        self.label = QString(label)

        self.label_width  = QFontMetrics(self.font()).width(label)
        self.label_height = QFontMetrics(self.font()).height()

        self.label_pos.setX((self.p_size/2)-(self.label_width/2))
        self.label_pos.setY(self.p_size+self.label_height)

        self.label_gradient.setColorAt(0.0, self.color1)
        self.label_gradient.setColorAt(0.6, self.color1)
        self.label_gradient.setColorAt(1.0, self.color2)

        self.label_gradient.setStart(0, self.p_size/2)
        self.label_gradient.setFinalStop(0, self.p_size+self.label_height+5)

        self.label_gradient_rect = QRectF(self.p_size*1/8, self.p_size/2, self.p_size*6/8, self.p_size+self.label_height+5)
        self.update()

    def setPixmap(self, pixmap_id):
        if (pixmap_id > 10):
          self.pixmap_n_str = str(pixmap_id)
        else:
          self.pixmap_n_str = "0%i" % (pixmap_id)

        self.pixmap.load(":/bitmaps/dial_%s%s.png" % (self.pixmap_n_str, "" if self.isEnabled() else "d"))

        if (self.pixmap.width() > self.pixmap.height()):
          self.orientation = self.HORIZONTAL
        else:
          self.orientation = self.VERTICAL

        self.updateSizes()
        self.update()

    def getSize(self):
        return self.p_size

    def minimumSizeHint(self):
        return QSize(self.p_size, self.p_size)

    def sizeHint(self):
        return QSize(self.p_size, self.p_size)

    def updateSizes(self):
        self.p_width = self.pixmap.width()
        self.p_height = self.pixmap.height()

        if (self.p_width < 1):
          self.p_width = 1

        if (self.p_height < 1):
          self.p_height = 1

        if (self.orientation == self.HORIZONTAL):
          self.p_size = self.p_height
          self.p_count = self.p_width/self.p_height
        else:
          self.p_size = self.p_width
          self.p_count = self.p_height/self.p_width

        self.setMinimumSize(self.p_size, self.p_size+self.label_height+5)
        self.setMaximumSize(self.p_size, self.p_size+self.label_height+5)

    def paintEvent(self, event):
        painter = QPainter(self)

        if (not self.label.isEmpty()):
          painter.setPen(self.color2)
          painter.setBrush(self.label_gradient)
          painter.drawRect(self.label_gradient_rect)

          painter.setPen(self.tcolor[0] if self.isEnabled() else self.tcolor[1])
          painter.drawText(self.label_pos, self.label)

        if (self.isEnabled()):
          current = float(self.value()-self.minimum())
          divider = float(self.maximum()-self.minimum())

          if (divider == 0.0):
            return

          target = QRectF(0.0, 0.0, self.p_size, self.p_size)
          per = int((self.p_count-1)*(current/divider))

          if (self.orientation == self.HORIZONTAL):
            xpos = self.p_size*per
            ypos = 0.0
          else:
            xpos = 0.0
            ypos = self.p_size*per

          source = QRectF(xpos, ypos, self.p_size, self.p_size)

        else:
          target = QRectF(0.0, 0.0, self.p_size, self.p_size)
          source = target

        painter.drawPixmap(target, self.pixmap, source)

    def resizeEvent(self, event):
        self.updateSizes()
        return QDial.resizeEvent(self, event)
