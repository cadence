#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from os import path
from xdg import IconTheme

# TODO - cache icons for faster lookup

EXTRA_PATHS = [
  "/usr/share/icons",
  "/usr/share/pixmaps",
  "/usr/local/share/pixmaps"
]

class XIcon(object):
    def __init__(self, *args):
      self.extra_themes = []

    def addIconPath(self, path):
      IconTheme.icondirs.append(path)

    def addThemeName(self, theme_name):
      for icondir in IconTheme.icondirs:
        theme_path = path.join(icondir, theme_name)
        if (path.exists(theme_path)):
          self.extra_themes.append(theme_name)
          break

    def getIconPath(self, name, size=48):
      for extra_theme in self.extra_themes:
        icon = IconTheme.getIconPath(name, size, extra_theme)
        if (icon != None):
          break
      else:
        icon = IconTheme.getIconPath(name, size)

      if (icon == None):
        for i in range(len(EXTRA_PATHS)):
          if path.exists(path.join(EXTRA_PATHS[i], name, ".png")):
            icon = path.join(EXTRA_PATHS[i], name, ".png")
            break
          elif path.exists(path.join(EXTRA_PATHS[i], name, ".svg")):
            icon = path.join(EXTRA_PATHS[i], name, ".svg")
            break
          elif path.exists(path.join(EXTRA_PATHS[i], name, ".xpm")):
            icon = path.join(EXTRA_PATHS[i], name, ".xpm")
            break
        else:
          print "XIcon::Failed to find icon for", name
          icon = ""

      return icon
