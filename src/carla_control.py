#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import sys
from PyQt4.QtCore import QSettings, QString, QVariant, SIGNAL, SLOT
from PyQt4.QtGui import QApplication, QColor, QCursor, QDialog, QFontMetrics, QFrame, QInputDialog, QMainWindow, QMenu, QPainter, QVBoxLayout, QWidget
from liblo import make_method, Address, ServerError, ServerThread
from liblo import send as lo_send

# Imports (Custom Stuff)
import ui_carla_control, ui_carla_edit, ui_carla_parameter, ui_carla_plugin, ui_carla_about
from palette import *
from shared import *

# static max values
MAX_PLUGINS     = 99
MAX_PARAMETERS  = 200
MAX_MIDI_EVENTS = 512

# plugin hints
PLUGIN_HAS_GUI     = 0x01
PLUGIN_IS_BRIDGE   = 0x02
PLUGIN_IS_SYNTH    = 0x04
PLUGIN_USES_CHUNKS = 0x08
PLUGIN_CAN_DRYWET  = 0x10
PLUGIN_CAN_VOL     = 0x20
PLUGIN_CAN_BALANCE = 0x40

# parameter hints
PARAMETER_IS_ENABLED        = 0x01
PARAMETER_IS_AUTOMABLE      = 0x02
PARAMETER_HAS_STRICT_BOUNDS = 0x04
PARAMETER_USES_SCALEPOINTS  = 0x08
PARAMETER_USES_SAMPLERATE   = 0x10

# enum PluginType
PLUGIN_NONE   = 0
PLUGIN_LADSPA = 1
PLUGIN_DSSI   = 2
PLUGIN_LV2    = 3
PLUGIN_VST    = 4
PLUGIN_WINVST = 5
PLUGIN_SF2    = 6

# enum ParameterType
PARAMETER_UNKNOWN = 0
PARAMETER_INPUT   = 1
PARAMETER_OUTPUT  = 2

# enum PluginInfoCategory
PLUGIN_CATEGORY_NONE      = 0
PLUGIN_CATEGORY_SYNTH     = 1
PLUGIN_CATEGORY_DELAY     = 2 # also Reverb
PLUGIN_CATEGORY_EQ        = 3
PLUGIN_CATEGORY_FILTER    = 4
PLUGIN_CATEGORY_DYNAMICS  = 5 # Amplifier, Compressor, Gate
PLUGIN_CATEGORY_MODULATOR = 6 # Chorus, Flanger, Phaser
PLUGIN_CATEGORY_UTILITY   = 7 # Analyzer, Converter, Mixer
PLUGIN_CATEGORY_OUTRO     = 8 # used to check if a plugin has a category

# enum InternalParametersIndex
PARAMETER_ACTIVE = -1
PARAMETER_DRYWET = -2
PARAMETER_VOLUME = -3
PARAMETER_BALANCE_LEFT  = -4
PARAMETER_BALANCE_RIGHT = -5

global carla_name, lo_target
carla_name = ""
lo_target = None

ICON_STATE_WAIT = 0
ICON_STATE_OFF  = 1
ICON_STATE_ON   = 2

PALETTE_COLOR_NONE   = 0
PALETTE_COLOR_WHITE  = 1
PALETTE_COLOR_RED    = 2
PALETTE_COLOR_GREEN  = 3
PALETTE_COLOR_BLUE   = 4
PALETTE_COLOR_YELLOW = 5
PALETTE_COLOR_ORANGE = 6
PALETTE_COLOR_BROWN  = 7
PALETTE_COLOR_PINK   = 8

# OSC Control server
class ControlServer(ServerThread):
    def __init__(self, parent):
        ServerThread.__init__(self, 8087)

        self.parent = parent

    def get_full_url(self):
        return self.get_url()+"carla-control"

    @make_method('/carla-control/add_plugin', 'is')
    def add_plugin_callback(self, path, args):
        plugin_id, plugin_name = args
        q_plugin_name = QString(plugin_name)
        self.parent.emit(SIGNAL("AddPluginCallback(int, QString)"), plugin_id, q_plugin_name)

    @make_method('/carla-control/remove_plugin', 'i')
    def remove_plugin_callback(self, path, args):
        plugin_id = args[0]
        self.parent.emit(SIGNAL("RemovePluginCallback(int)"), plugin_id)

    @make_method('/carla-control/set_plugin_data', 'iiiissssi')
    def set_plugin_data_callback(self, path, args):
        plugin_id, ptype, category, hints, name, label, maker, copyright, unique_id = args
        q_name  = QString(name)
        q_label = QString(label)
        q_maker = QString(maker)
        q_copyright = QString(copyright)
        self.parent.emit(SIGNAL("SetPluginDataCallback(int, int, int, int, QString, QString, QString, QString, long)"),
                                  plugin_id, ptype, category, hints, q_name, q_label, q_maker, q_copyright, unique_id)

    @make_method('/carla-control/set_plugin_ports', 'iiiiiiii')
    def set_plugin_ports_callback(self, path, args):
        plugin_id, ains, aouts, mins, mouts, cins, couts, ctotals = args
        self.parent.emit(SIGNAL("SetPluginPortsCallback(int, int, int, int, int, int, int, int)"),
                                  plugin_id, ains, aouts, mins, mouts, cins, couts, ctotals)

    @make_method('/carla-control/set_parameter_value', 'iif')
    def set_parameter_value_callback(self, path, args):
        plugin_id, param_id, value = args
        self.parent.emit(SIGNAL("SetParameterValueCallback(int, int, double)"), plugin_id, param_id, value)

    @make_method('/carla-control/set_parameter_data', 'iiiissfffffff')
    def set_parameter_data_callback(self, path, args):
        plugin_id, param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large = args
        q_name  = QString(name)
        q_label = QString(label)
        self.parent.emit(SIGNAL("SetParameterDataCallback(int, int, int, int, QString, QString, double, double, double, double, double, double, double)"),
                                  plugin_id, param_id, ptype, hints, q_name, q_label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large)

    @make_method('/carla-control/set_default_value', 'iif')
    def set_default_value_callback(self, path, args):
        plugin_id, param_id, value = args
        self.parent.emit(SIGNAL("SetDefaultValueCallback(int, int, double)"), plugin_id, param_id, value)

    @make_method('/carla-control/set_input_peak_value', 'iif')
    def set_input_peak_value_callback(self, path, args):
        plugin_id, port_id, value = args
        self.parent.emit(SIGNAL("SetInputPeakValueCallback(int, int, double)"), plugin_id, port_id, value)

    @make_method('/carla-control/set_output_peak_value', 'iif')
    def set_output_peak_value_callback(self, path, args):
        plugin_id, port_id, value = args
        self.parent.emit(SIGNAL("SetOutputPeakValueCallback(int, int, double)"), plugin_id, port_id, value)

    @make_method('/carla-control/set_program', 'ii')
    def set_program_callback(self, path, args):
        plugin_id, program_id = args
        self.parent.emit(SIGNAL("SetProgramCallback(int, int)"), plugin_id, program_id)

    @make_method('/carla-control/set_program_count', 'ii')
    def set_program_count_callback(self, path, args):
        plugin_id, program_count = args
        self.parent.emit(SIGNAL("SetProgramCountCallback(int, int)"), plugin_id, program_count)

    @make_method('/carla-control/set_program_name', 'iis')
    def set_program_name_callback(self, path, args):
        plugin_id, program_id, program_name = args
        q_program_name = QString(program_name)
        self.parent.emit(SIGNAL("SetProgramNameCallback(int, int, QString)"), plugin_id, program_id, q_program_name)

    @make_method('/carla-control/set_midi_program', 'ii')
    def set_midi_program_callback(self, path, args):
        plugin_id, midi_program_id = args
        self.parent.emit(SIGNAL("SetMidiProgramCallback(int, int)"), plugin_id, midi_program_id)

    @make_method('/carla-control/set_midi_program_count', 'ii')
    def set_midi_program_count_callback(self, path, args):
        plugin_id, midi_program_count = args
        self.parent.emit(SIGNAL("SetMidiProgramCountCallback(int, int)"), plugin_id, midi_program_count)

    @make_method('/carla-control/set_midi_program_data', 'iiiiis')
    def set_midi_program_data(self, path, args):
        plugin_id, midi_program_id, bank_id, program_id, program_name = args
        q_program_name = QString(program_name)
        self.parent.emit(SIGNAL("SetMidiProgramNameCallback(int, int, int, int, int, QString)"), plugin_id, midi_program_id, bank_id, program_id, q_program_name)

    @make_method('/carla-control/note_on', 'iii')
    def note_on_callback(self, path, args):
        plugin_id, note, velo = args
        self.parent.emit(SIGNAL("NoteOnCallback(int, int, int)"), plugin_id, note, velo)

    @make_method('/carla-control/note_off', 'iii')
    def note_off_callback(self, path, args):
        plugin_id, note, velo = args
        self.parent.emit(SIGNAL("NoteOffCallback(int, int, int)"), plugin_id, note, velo)

    @make_method('/carla-control/exit', '')
    def exit_callback(self, path, args):
        self.parent.emit(SIGNAL("ExitCallback()"))

    @make_method(None, None)
    def fallback(self, path, args):
        print "received unknown message '%s'" % path, ":", len(args), ":", args

# Python Object class compatible to 'Host' on the Carla Backend code
class Host(object):
    def __init__(self):
        super(Host, self).__init__()

    def set_active(self, plugin_id, active):
        global carla_name, to_target
        osc_control = "/%s/%i/set_active" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, 1 if (active) else 0)

    def set_drywet(self, plugin_id, value):
        global carla_name, to_target
        osc_control = "/%s/%i/set_drywet" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, value)

    def set_vol(self, plugin_id, value):
        global carla_name, to_target
        osc_control = "/%s/%i/set_vol" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, value)

    def set_balance_left(self, plugin_id, value):
        global carla_name, to_target
        osc_control = "/%s/%i/set_balance_left" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, value)

    def set_balance_right(self, plugin_id, value):
        global carla_name, to_target
        osc_control = "/%s/%i/set_balance_right" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, value)

    def set_parameter_value(self, plugin_id, param_id, value):
        global carla_name, to_target
        osc_control = "/%s/%i/set_parameter" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, param_id, value)

    def set_program(self, plugin_id, program_id):
        global carla_name, to_target
        osc_control = "/%s/%i/set_program" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, program_id)

    def set_midi_program(self, plugin_id, midi_program_id):
        global carla_name, to_target
        osc_control = "/%s/%i/set_midi_program" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, midi_program_id)

    def note_on(self, plugin_id, note, velo):
        global carla_name, to_target
        osc_control = "/%s/%i/note_on" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, note, velo)

    def note_off(self, plugin_id, note, velo):
        global carla_name, to_target
        osc_control = "/%s/%i/note_off" % (carla_name, plugin_id)
        lo_send(lo_target, osc_control, note, velo)

# Single Plugin Parameter
class PluginParameter(QWidget, ui_carla_parameter.Ui_PluginParameter):
    def __init__(self, parent, pinfo, plugin_id):
        super(PluginParameter, self).__init__(parent)
        self.setupUi(self)

        # Not available on OSC
        self.combo.setVisible(False)
        self.sb_channel.setVisible(False)

        self.ptype = pinfo['type']
        self.parameter_id = pinfo['index']
        self.hints = pinfo['hints']

        self.midi_cc = -1
        self.midi_channel = 1
        self.plugin_id = plugin_id

        self.label.setText(pinfo['name'])

        if (self.ptype == PARAMETER_INPUT):
          self.widget.set_minimum(pinfo['minimum'])
          self.widget.set_maximum(pinfo['maximum'])
          self.widget.set_default(pinfo['default'])
          self.widget.set_value(pinfo['current'], False)
          self.widget.set_label(pinfo['label'])
          self.widget.set_step(pinfo['step'])
          self.widget.set_step_small(pinfo['step_small'])
          self.widget.set_step_large(pinfo['step_large'])
          self.widget.set_scalepoints(pinfo['scalepoints'], (pinfo['hints'] & PARAMETER_USES_SCALEPOINTS))

          if (not self.hints & PARAMETER_IS_ENABLED):
            self.widget.set_read_only(True)

        elif (self.ptype == PARAMETER_OUTPUT):
          self.widget.set_minimum(pinfo['minimum'])
          self.widget.set_maximum(pinfo['maximum'])
          self.widget.set_value(pinfo['current'], False)
          self.widget.set_label(pinfo['label'])
          self.widget.set_read_only(True)

        else:
          self.widget.setVisible(False)
          self.combo.setVisible(False)
          self.sb_channel.setVisible(False)

        self.connect(self.widget, SIGNAL("valueChanged(float)"), self.handleValueChanged)

        if force_parameters_style:
          self.widget.force_plastique_style()

        self.widget.updateAll()

    def set_default_value(self, value):
        self.widget.set_default(value)

    def set_parameter_value(self, value, send=True):
        self.widget.set_value(value, send)

    #def set_extra_info(self):
        #extra_param_info = NativeHost.get_parameter_extra_info(self.plugin_id, self.parameter_id)
        #if (extra_param_info['valid']):
          #self.widget.set_fake_minimum(extra_param_info['x_min'])
          #self.widget.set_fake_maximum(extra_param_info['x_max'])
          #self.widget.updateAll()

    def handleValueChanged(self, value):
        self.emit(SIGNAL("valueChanged(int, float)"), self.parameter_id, value)

# Plugin Editor (Built-in)
class PluginEdit(QDialog, ui_carla_edit.Ui_PluginEdit):
    def __init__(self, parent, plugin_id, plugin_name):
        super(PluginEdit, self).__init__(parent)
        self.setupUi(self)

        # Not available for OSC
        self.b_save_state.setEnabled(False)
        self.b_load_state.setEnabled(False)

        # FIXME - this too?
        self.b_reload_program.setEnabled(False)
        self.b_reload_midi_program.setEnabled(False)

        self.pinfo = None
        self.ptype = PLUGIN_NONE
        self.hints = 0
        self.plugin_id = plugin_id
        self.plugin_name = plugin_name

        self.parameter_count = 0
        self.parameter_list = []
        self.parameter_list_data = []
        self.parameter_list_to_update = []

        self.cur_program_index = -1
        self.cur_midi_program_index = -1

        self.tab_icon_off = QIcon(":/bitmaps/led_off.png")
        self.tab_icon_on  = QIcon(":/bitmaps/led_yellow.png")
        self.tab_icon_count = 0
        self.tab_icon_timers = []

        self.connect(self.keyboard, SIGNAL("noteOn(int)"), self.handleNoteOn)
        self.connect(self.keyboard, SIGNAL("noteOff(int)"), self.handleNoteOff)

        self.connect(self.keyboard, SIGNAL("notesOn()"), self.handleNotesOn)
        self.connect(self.keyboard, SIGNAL("notesOff()"), self.handleNotesOff)

        self.connect(self.cb_programs, SIGNAL("currentIndexChanged(int)"),  self.handleProgramIndexChanged)
        self.connect(self.cb_midi_programs, SIGNAL("currentIndexChanged(int)"),  self.handleMidiProgramIndexChanged)

        self.keyboard.setMode(self.keyboard.HORIZONTAL)
        self.keyboard.setOctaves(6)
        self.scrollArea.ensureVisible(self.keyboard.width()*1/5, 0)
        self.scrollArea.setVisible(False)

        self.setWindowTitle(self.plugin_name)

        self.label_plugin.setText("\n%s\n" % (self.plugin_name))

        self.TIMER_GUI_STUFF = self.startTimer(100)

    def set_data(self, ptype, category, hints, name, label, maker, copyright, unique_id):
        if (self.ptype == PLUGIN_NONE and ptype in (PLUGIN_DSSI, PLUGIN_SF2)):
          self.tab_programs.setCurrentIndex(1)

        self.ptype = ptype
        self.hints = hints

        self.le_name.setText(name)
        self.le_name.setToolTip(name)
        self.le_label.setText(label)
        self.le_label.setToolTip(label)
        self.le_maker.setText(maker)
        self.le_maker.setToolTip(maker)
        self.le_copyright.setText(copyright)
        self.le_copyright.setToolTip(copyright)
        self.le_unique_id.setText(str(unique_id))
        self.le_unique_id.setToolTip(str(unique_id))

        if (self.ptype == PLUGIN_LADSPA):
          self.le_type.setText("LADSPA")
        elif (self.ptype == PLUGIN_DSSI):
          self.le_type.setText("DSSI")
        elif (self.ptype == PLUGIN_LV2):
          self.le_type.setText("LV2")
        elif (self.ptype == PLUGIN_VST):
          self.le_type.setText("VST")
        elif (self.ptype == PLUGIN_WINVST):
          self.le_type.setText("Windows VST")
        else:
          self.le_type.setText(self.tr("Unknown"))

        self.le_is_synth.setText(self.tr("Yes") if (self.hints & PLUGIN_IS_SYNTH) else (self.tr("No")))
        self.le_has_gui.setText(self.tr("Yes") if (self.hints & PLUGIN_HAS_GUI) else (self.tr("No")))

        self.scrollArea.setVisible(self.hints & PLUGIN_IS_SYNTH)

    def set_ports(self, ains, aouts, mins, mouts, cins, couts, ctotals):
        self.le_ains.setText(str(ains))
        self.le_aouts.setText(str(aouts))
        self.le_params.setText(str(cins))
        self.le_couts.setText(str(couts))

        self.parameter_count = ctotals
        self.parameter_list = []
        self.parameter_list_data = []
        self.parameter_list_to_update = []

        self.tab_icon_count = 0
        self.tab_icon_timers = []

        for i in range(self.tabWidget.count()):
          if (i == 0): continue
          self.tabWidget.widget(1).deleteLater()
          self.tabWidget.removeTab(1)

    def set_parameter_value(self, param_id, value):
        for param in self.parameter_list_to_update:
          if (param[0] == param_id):
            param[1] = value
            break
        else:
          self.parameter_list_to_update.append([param_id, value])

    def set_parameter_data(self, param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large):
        if (self.parameter_count < 200):
          self.parameter_list_data.append([param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large])

        if (len(self.parameter_list_data) == self.parameter_count):

          # Got all parameters, create widgets now

          if (self.parameter_count <= 0):
            pass

          elif (self.parameter_count <= MAX_PARAMETERS):
            p_in = []
            p_in_tmp = []
            p_in_index = 0
            p_in_width = 0

            p_out = []
            p_out_tmp = []
            p_out_index = 0
            p_out_width = 0

            for i in range(self.parameter_count):
              param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large = self.parameter_list_data[i]

              parameter = {
                'type':  ptype,
                'hints': hints,
                'name':  QStringStr(name),
                'label': QStringStr(label),
                'scalepoints': [],

                'index':   i,
                'default': x_def,
                'minimum': x_min,
                'maximum': x_max,
                'step':       x_step,
                'step_small': x_step_small,
                'step_large': x_step_large,
                'midi_channel': 1,
                'midi_cc': -1,

                'current': current
              }

              # -----------------------------------------------------------------
              # Get width values, in packs of 10

              if (parameter['type'] == PARAMETER_INPUT):
                p_in_tmp.append(parameter)
                p_in_width_tmp = QFontMetrics(self.font()).width(parameter['name'])

                if (p_in_width_tmp > p_in_width):
                  p_in_width = p_in_width_tmp

                if (len(p_in_tmp) == 10):
                  p_in.append((p_in_tmp, p_in_width))
                  p_in_tmp = []
                  p_in_index = 0
                  p_in_width = 0
                else:
                  p_in_index += 1

              elif (parameter['type'] == PARAMETER_OUTPUT):
                p_out_tmp.append(parameter)
                p_out_width_tmp = QFontMetrics(self.font()).width(parameter['name'])

                if (p_out_width_tmp > p_out_width):
                  p_out_width = p_out_width_tmp

                if (len(p_out_tmp) == 10):
                  p_out.append((p_out_tmp, p_out_width))
                  p_out_tmp = []
                  p_out_index = 0
                  p_out_width = 0
                else:
                  p_out_index += 1

            else:
              # Final page width values
              if (len(p_in_tmp) > 0 and len(p_in_tmp) < 10):
                p_in.append((p_in_tmp, p_in_width))

              if (len(p_out_tmp) > 0 and len(p_out_tmp) < 10):
                p_out.append((p_out_tmp, p_out_width))

            # -----------------------------------------------------------------
            # Create parameter widgets

            if (len(p_in) > 0):
              self.createParameterWidgets(p_in, self.tr("Parameters"), PARAMETER_INPUT)

            if (len(p_out) > 0):
              self.createParameterWidgets(p_out, self.tr("Outputs"), PARAMETER_OUTPUT)

          else: # > MAX_PARAMETERS
            fake_name = "This plugin has too many parameters to display here!"

            p_fake = []
            p_fake_tmp = []
            p_fake_width = QFontMetrics(self.font()).width(fake_name)

            parameter = {
              'type':  PARAMETER_UNKNOWN,
              'hints': 0,
              'name':  fake_name,
              'label': "",
              'scalepoints': [],

              'index':   0,
              'default': 0,
              'minimum': 0,
              'maximum': 0,
              'step':       0,
              'step_small': 0,
              'step_large': 0,
              'midi_channel': 0,
              'midi_cc': -1,

              'current': 0.0
            }

            p_fake_tmp.append(parameter)
            p_fake.append((p_fake_tmp, p_fake_width))

            self.createParameterWidgets(p_fake, self.tr("Information"), PARAMETER_UNKNOWN)

    def set_default_value(self, param_id, value):
        for param in self.parameter_list:
            if (param[1] == param_id):
              param[2].set_default_value(value)
              break

    def set_program(self, program_id):
        self.cur_program_index = program_id
        self.cb_programs.setCurrentIndex(program_id)

    def set_midi_program(self, midi_program_id):
        self.cur_midi_program_index = midi_program_id
        self.cb_midi_programs.setCurrentIndex(midi_program_id)

    def set_program_count(self, program_count):
        old_current = self.cur_program_index

        if (self.cb_programs.count() > 0):
          self.cur_program_index = -1
          self.set_program(-1)
          self.cb_programs.clear()

        if (program_count > 0):
          self.cb_programs.setEnabled(True)
          self.cur_program_index = 0

          for i in range(program_count):
            self.cb_programs.addItem("")

          if (old_current < 0):
            old_current = 0

          self.cur_program_index = old_current
          self.set_program(old_current)

        else:
          self.cb_programs.setEnabled(False)

    def set_midi_program_count(self, midi_program_count):
        old_midi_current = self.cur_midi_program_index

        if (self.cb_midi_programs.count() > 0):
          self.cur_midi_program_index = -1
          self.set_midi_program(-1)
          self.cb_midi_programs.clear()

        if (midi_program_count > 0):
          self.cb_midi_programs.setEnabled(True)
          self.cur_midi_program_index = 0

          for i in range(midi_program_count):
            self.cb_midi_programs.addItem("")

          if (old_midi_current < 0):
            old_midi_current = 0

          self.cur_midi_program_index = old_midi_current
          self.set_midi_program(old_midi_current)

        else:
          self.cb_midi_programs.setEnabled(False)

    def set_program_name(self, program_id, program_name):
        self.cb_programs.setItemText(program_id, program_name)

    def set_midi_program_data(self, midi_program_id, bank_id, program_id, midi_program_name):
        self.cb_midi_programs.setItemText(midi_program_id, midi_program_name)
        # TODO

    def handleParameterValueChanged(self, parameter_id, value):
        NativeHost.set_parameter_value(self.plugin_id, parameter_id, value)

    def handleProgramIndexChanged(self, index):
        if (self.cur_program_index != index):
          NativeHost.set_program(self.plugin_id, index)
        self.cur_program_index = index

    def handleMidiProgramIndexChanged(self, index):
        if (self.cur_midi_program_index != index):
          NativeHost.set_midi_program(self.plugin_id, index)
        self.cur_midi_program_index = index

    def handleNoteOn(self, note):
        NativeHost.note_on(self.plugin_id, note, 100)

    def handleNoteOff(self, note):
        NativeHost.note_off(self.plugin_id, note, 100)

    def handleNotesOn(self):
        self.parent().led_midi.setChecked(True)

    def handleNotesOff(self):
        self.parent().led_midi.setChecked(False)

    def createParameterWidgets(self, p_list_full, tab_name, ptype):
        for i in range(len(p_list_full)):
          p_list = p_list_full[i][0]
          width  = p_list_full[i][1]

          if (len(p_list) > 0):
            container = QWidget(self.tabWidget)
            layout = QVBoxLayout(container)
            container.setLayout(layout)

            for j in range(len(p_list)):
              pwidget = PluginParameter(container, p_list[j], self.plugin_id)
              pwidget.label.setMinimumWidth(width)
              pwidget.label.setMaximumWidth(width)
              pwidget.tab_index = self.tabWidget.count()
              layout.addWidget(pwidget)

              self.parameter_list.append((ptype, p_list[j]['index'], pwidget))

              if (ptype == PARAMETER_INPUT):
                self.connect(pwidget, SIGNAL("valueChanged(int, float)"),  self.handleParameterValueChanged)

            layout.addStretch()

            self.tabWidget.addTab(container, "%s (%i)" % (tab_name, i+1))

            if (ptype == PARAMETER_INPUT):
              self.tabWidget.setTabIcon(pwidget.tab_index, self.tab_icon_off)
              self.tab_icon_timers.append(ICON_STATE_OFF)
            else:
              self.tab_icon_timers.append(None)


    def animateTab(self, index):
        if (self.tab_icon_timers[index-1] == None):
          self.tabWidget.setTabIcon(index, self.tab_icon_on)
        self.tab_icon_timers[index-1] = ICON_STATE_ON

    def checkTabIcons(self):
        for i in range(len(self.tab_icon_timers)):
          if (self.tab_icon_timers[i] == ICON_STATE_ON):
            self.tab_icon_timers[i] = ICON_STATE_WAIT
          elif (self.tab_icon_timers[i] == ICON_STATE_WAIT):
            self.tab_icon_timers[i] = ICON_STATE_OFF
          elif (self.tab_icon_timers[i] == ICON_STATE_OFF):
            self.tabWidget.setTabIcon(i+1, self.tab_icon_off)
            self.tab_icon_timers[i] = None

    def checkUpdatedParameters(self):
        for parameter_id, value in self.parameter_list_to_update:
          for parameter in self.parameter_list:
            x_parameter_type   = parameter[0]
            x_parameter_id     = parameter[1]
            x_parameter_widget = parameter[2]

            if (x_parameter_id == parameter_id):
              x_parameter_widget.set_parameter_value(value, False)

              if (x_parameter_type == PARAMETER_INPUT):
                self.animateTab(x_parameter_widget.tab_index)

              break

        for i in self.parameter_list_to_update:
          self.parameter_list_to_update.pop(0)

    def timerEvent(self, event):
        if (event.timerId() == self.TIMER_GUI_STUFF):
          self.checkTabIcons()
          self.checkUpdatedParameters()
        return QDialog.timerEvent(self, event)

# (New) Plugin Widget
class PluginWidget(QFrame, ui_carla_plugin.Ui_PluginWidget):
    def __init__(self, parent, plugin_id, plugin_name):
        super(PluginWidget, self).__init__(parent)
        self.setupUi(self)

        self.plugin_id = plugin_id
        self.plugin_name = plugin_name
        self.parameter_activity_timer = None

        self.ptype = PLUGIN_NONE
        self.ains = 0
        self.aouts = 0
        self.atotals = 0

        # Fake effect
        self.color_1 = QColor( 0,  0,  0, 220)
        self.color_2 = QColor( 0,  0,  0, 170)
        self.color_3 = QColor( 7,  7,  7, 250)
        self.color_4 = QColor(14, 14, 14, 255)

        self.led_enable.setColor(self.led_enable.BIG_RED)
        self.led_enable.setChecked(True)

        self.led_control.setColor(self.led_control.YELLOW)
        self.led_control.setEnabled(False)

        self.led_midi.setColor(self.led_midi.RED)
        self.led_midi.setEnabled(False)

        self.led_audio_in.setColor(self.led_audio_in.GREEN)
        self.led_audio_in.setEnabled(False)

        self.led_audio_out.setColor(self.led_audio_out.BLUE)
        self.led_audio_out.setEnabled(False)

        self.dial_drywet.setPixmap(1)
        self.dial_vol.setPixmap(2)
        self.dial_b_left.setPixmap(1)
        self.dial_b_right.setPixmap(1)

        self.dial_drywet.setLabel("Wet")
        self.dial_vol.setLabel("Vol")
        self.dial_b_left.setLabel("L")
        self.dial_b_right.setLabel("R")

        self.peak_in.setColor(self.peak_in.GREEN)
        self.peak_out.setColor(self.peak_in.BLUE)

        self.peak_in.setOrientation(self.peak_in.HORIZONTAL)
        self.peak_out.setOrientation(self.peak_out.HORIZONTAL)

        self.peak_in.setChannels(0)
        self.peak_out.setChannels(0)

        self.peak_in_values = [None, None]
        self.peak_out_values = [None, None]

        self.stackedWidget.setCurrentIndex(1)

        self.label_name.setText(self.plugin_name)

        self.dial_drywet.setEnabled(False)
        self.dial_vol.setEnabled(False)
        self.dial_b_left.setEnabled(False)
        self.dial_b_right.setEnabled(False)
        self.b_gui.setEnabled(False)
        self.b_remove.setEnabled(False)

        self.set_plugin_widget_color(PALETTE_COLOR_NONE)

        self.led_midi.setVisible(False)

        self.edit_dialog = PluginEdit(self, self.plugin_id, self.plugin_name)
        self.edit_dialog.hide()
        self.edit_dialog_geometry = QVariant(self.edit_dialog.saveGeometry())

        self.connect(self.led_enable, SIGNAL("clicked(bool)"), self.set_active)
        self.connect(self.dial_drywet, SIGNAL("sliderMoved(int)"), self.set_drywet)
        self.connect(self.dial_vol, SIGNAL("sliderMoved(int)"), self.set_vol)
        self.connect(self.dial_b_left, SIGNAL("sliderMoved(int)"), self.set_balance_left)
        self.connect(self.dial_b_right, SIGNAL("sliderMoved(int)"), self.set_balance_right)
        self.connect(self.b_edit, SIGNAL("clicked(bool)"), self.handleEdit)

        self.connect(self.dial_drywet, SIGNAL("customContextMenuRequested(QPoint)"), self.showCustomDialMenu)
        self.connect(self.dial_vol, SIGNAL("customContextMenuRequested(QPoint)"), self.showCustomDialMenu)
        self.connect(self.dial_b_left, SIGNAL("customContextMenuRequested(QPoint)"), self.showCustomDialMenu)
        self.connect(self.dial_b_right, SIGNAL("customContextMenuRequested(QPoint)"), self.showCustomDialMenu)

        self.connect(self.edit_dialog, SIGNAL("finished(int)"), self.edit_dialog_closed)

        self.TIMER_GUI_STUFF = self.startTimer(50)

    def set_data(self, ptype, category, hints, name, label, maker, copyright, unique_id):
        self.edit_dialog.set_data(ptype, category, hints, name, label, maker, copyright, unique_id)

        self.ptype = ptype

        self.dial_drywet.setEnabled(hints & PLUGIN_CAN_DRYWET)
        self.dial_vol.setEnabled(hints & PLUGIN_CAN_VOL)
        self.dial_b_left.setEnabled(hints & PLUGIN_CAN_BALANCE)
        self.dial_b_right.setEnabled(hints & PLUGIN_CAN_BALANCE)

        # Colorify
        if (category == PLUGIN_CATEGORY_SYNTH):
          self.set_plugin_widget_color(PALETTE_COLOR_WHITE)
        elif (category == PLUGIN_CATEGORY_DELAY):
          self.set_plugin_widget_color(PALETTE_COLOR_ORANGE)
        elif (category == PLUGIN_CATEGORY_EQ):
          self.set_plugin_widget_color(PALETTE_COLOR_GREEN)
        elif (category == PLUGIN_CATEGORY_FILTER):
          self.set_plugin_widget_color(PALETTE_COLOR_BLUE)
        elif (category == PLUGIN_CATEGORY_DYNAMICS):
          self.set_plugin_widget_color(PALETTE_COLOR_PINK)
        elif (category == PLUGIN_CATEGORY_MODULATOR):
          self.set_plugin_widget_color(PALETTE_COLOR_RED)
        elif (category == PLUGIN_CATEGORY_UTILITY):
          self.set_plugin_widget_color(PALETTE_COLOR_YELLOW)
        elif (category == PLUGIN_CATEGORY_OUTRO):
          self.set_plugin_widget_color(PALETTE_COLOR_BROWN)
        else:
          self.set_plugin_widget_color(PALETTE_COLOR_NONE)

        if (hints & PLUGIN_IS_SYNTH):
          self.led_audio_in.setVisible(False)
        else:
          self.led_midi.setVisible(False)

    def set_ports(self, ains, aouts, mins, mouts, cins, couts, ctotals):
        self.edit_dialog.set_ports(ains, aouts, mins, mouts, cins, couts, ctotals)

        self.ains = ains
        self.aouts = aouts
        self.atotals = ains+aouts

        peaks_in  = ains
        peaks_out = aouts

        if (ains > 2):
          peaks_in = 2

        if (aouts > 2):
          peaks_out = 2

        self.peak_in.setChannels(peaks_in)
        self.peak_out.setChannels(peaks_out)

        if (self.atotals > 0):
          self.stackedWidget.setCurrentIndex(0)

    def set_active(self, active, gui_send=False, callback_send=True):
        if (gui_send): self.led_enable.setChecked(active)
        if (callback_send): NativeHost.set_active(self.plugin_id, active)

    def set_drywet(self, value, gui_send=False, callback_send=True):
        if (gui_send): self.dial_drywet.setValue(value)
        if (callback_send): NativeHost.set_drywet(self.plugin_id, float(value)/1000)

        message = self.tr("Output dry/wet (%1%)").arg(value/10)
        self.dial_drywet.setStatusTip(message)
        gui.statusBar().showMessage(message)

    def set_vol(self, value, gui_send=False, callback_send=True):
        if (gui_send): self.dial_vol.setValue(value)
        if (callback_send): NativeHost.set_vol(self.plugin_id, float(value)/1000)

        message = self.tr("Output volume (%1%)").arg(value/10)
        self.dial_vol.setStatusTip(message)
        gui.statusBar().showMessage(message)

    def set_balance_left(self, value, gui_send=False, callback_send=True):
        if (gui_send): self.dial_b_left.setValue(value)
        if (callback_send): NativeHost.set_balance_left(self.plugin_id, float(value)/1000)

        if (value == 0):
          message = self.tr("Left Panning (Center)")
        elif (value < 0):
          message = self.tr("Left Panning (%1% Left)").arg(-value/10)
        else:
          message = self.tr("Left Panning (%1% Right)").arg(value/10)
        self.dial_b_left.setStatusTip(message)
        gui.statusBar().showMessage(message)

    def set_balance_right(self, value, gui_send=False, callback_send=True):
        if (gui_send): self.dial_b_right.setValue(value)
        if (callback_send): NativeHost.set_balance_right(self.plugin_id, float(value)/1000)
        if (value == 0):
          message = self.tr("Right Panning (Center)")
        elif (value < 0):
          message = self.tr("Right Panning (%1%) Left").arg(-value/10)
        else:
          message = self.tr("Right Panning (%1% Right)").arg(value/10)
        self.dial_b_right.setStatusTip(message)
        gui.statusBar().showMessage(message)

    def edit_dialog_closed(self):
        self.b_edit.setChecked(False)

    def set_input_peak_value(self, port_id, value):
        self.peak_in_values[port_id-1] = value

    def set_output_peak_value(self, port_id, value):
        self.peak_out_values[port_id-1] = value

    def check_gui_stuff(self):
        # Parameter Activity LED
        if (self.parameter_activity_timer == ICON_STATE_ON):
          self.led_control.setChecked(True)
          self.parameter_activity_timer = ICON_STATE_WAIT
        elif (self.parameter_activity_timer == ICON_STATE_WAIT):
          self.parameter_activity_timer = ICON_STATE_OFF
        elif (self.parameter_activity_timer == ICON_STATE_OFF):
          self.led_control.setChecked(False)
          self.parameter_activity_timer = None

        if (self.peak_in_values[0] != None and self.ains >= 1):
          peak1_in = self.peak_in_values[0]
          self.peak_in.displayMeter(1, peak1_in)
        else:
          peak1_in = 0.0

        if (self.peak_in_values[1] != None and self.ains >= 2):
          peak2_in = self.peak_in_values[1]
          self.peak_in.displayMeter(2, peak2_in)
        else:
          peak2_in = 0.0

        if (self.peak_out_values[0] != None and self.aouts >= 1):
          peak1_out = self.peak_out_values[0]
          self.peak_out.displayMeter(1, peak1_out)
        else:
          peak1_out = 0.0

        if (self.peak_out_values[1] != None and self.aouts >= 2):
          peak2_out = self.peak_out_values[1]
          self.peak_out.displayMeter(2, peak2_out)
        else:
          peak2_out = 0.0

        self.led_audio_in.setChecked((peak1_in != 0.0 or peak2_in != 0.0))
        self.led_audio_out.setChecked((peak1_out != 0.0 or peak2_out != 0.0))

    def set_plugin_widget_color(self, color):
      if (color == PALETTE_COLOR_WHITE):
        r = 110
        g = 110
        b = 110
        texture = 7
      elif (color == PALETTE_COLOR_RED):
        r = 110
        g = 15
        b = 15
        texture = 3
      elif (color == PALETTE_COLOR_GREEN):
        r = 15
        g = 110
        b = 15
        w = 15
        texture = 6
      elif (color == PALETTE_COLOR_BLUE):
        r = 15
        g = 15
        b = 110
        texture = 4
      elif (color == PALETTE_COLOR_YELLOW):
        r = 110
        g = 110
        b = 15
        texture = 2
      elif (color == PALETTE_COLOR_ORANGE):
        r = 180
        g = 110
        b = 15
        texture = 5
      elif (color == PALETTE_COLOR_BROWN):
        r = 110
        g = 35
        b = 15
        texture = 1
      elif (color == PALETTE_COLOR_PINK):
        r = 110
        g = 15
        b = 110
        texture = 8
      else:
        r = 35
        g = 35
        b = 35
        texture = 4

      self.setStyleSheet("""
        QFrame#PluginWidget {
                  background-image: url(:/bitmaps/textures/metal_%i-512px.jpg);
                  background-repeat: repeat-x;
                  background-position: top left;
                }
        QLabel#label_name {
          color: white;
        }
        QFrame#frame_name {
          background-image: url(:/bitmaps/glass.png);
          background-color: rgba(%i, %i, %i);
          border: 2px outset;
          border-color: rgb(%i, %i, %i);
        }
        QFrame#frame_controls {
          background-image: url(:/bitmaps/glass2.png);
          background-color: rgb(30, 30, 30);
          border: 2px outset;
          border-color: rgb(30, 30, 30);
        }
        QFrame#frame_peaks {
          background-color: rgba(30, 30, 30, 200);
          border: 2px inset;
          border-color: rgba(30, 30, 30, 255);
        }
      """ % (texture, r, g, b, r, g, b))

    def handleEdit(self, show):
        if (show):
          self.edit_dialog.restoreGeometry(self.edit_dialog_geometry.toByteArray())
        else:
          self.edit_dialog_geometry = QVariant(self.edit_dialog.saveGeometry())

        self.edit_dialog.setVisible(show)

    def showCustomDialMenu(self, pos):
        dial_name = QStringStr(self.sender().objectName())
        if (dial_name == "dial_drywet"):
          minimum = 0
          maximum = 100
          default = 100
          label = "Dry/Wet"
        elif (dial_name == "dial_vol"):
          minimum = 0
          maximum = 127
          default = 100
          label = "Volume"
        elif (dial_name == "dial_b_left"):
          minimum = -100
          maximum = 100
          default = -100
          label = "Balance-Left"
        elif (dial_name == "dial_b_right"):
          minimum = -100
          maximum = 100
          default = 100
          label = "Balance-Right"
        else:
          minimum = 0
          maximum = 100
          default = 100
          label = "Unknown"

        current = self.sender().value()/10

        menu = QMenu(self)
        act_x_reset = menu.addAction(self.tr("Reset (%1%)").arg(default))
        menu.addSeparator()
        act_x_min = menu.addAction(self.tr("Set to Minimum (%1%)").arg(minimum))
        act_x_cen = menu.addAction(self.tr("Set to Center"))
        act_x_max = menu.addAction(self.tr("Set to Maximum (%1%)").arg(maximum))
        menu.addSeparator()
        act_x_set = menu.addAction(self.tr("Set value..."))

        if (label not in ("Balance-Left", "Balance-Right")):
          menu.removeAction(act_x_cen)

        act_x_sel = menu.exec_(QCursor.pos())

        if (act_x_sel == act_x_set):
          ans_value = QInputDialog.getInteger(self, self.tr("Set value"), label, current, minimum, maximum, 1)
          if (ans_value[1]):
            value = ans_value[0]*10
          else:
            value = None

        elif (act_x_sel == act_x_min):
          value = minimum*10
        elif (act_x_sel == act_x_max):
          value = maximum*10
        elif (act_x_sel == act_x_reset):
          value = default*10
        elif (act_x_sel == act_x_cen):
          value = 0
        else:
          value = None

        if (value != None):
          if (label == "Dry/Wet"):
            self.set_drywet(value, True, True)
          elif (label == "Volume"):
            self.set_vol(value, True, True)
          elif (label == "Balance-Left"):
            self.set_balance_left(value, True, True)
          elif (label == "Balance-Right"):
            self.set_balance_right(value, True, True)

    def timerEvent(self, event):
        if (event.timerId() == self.TIMER_GUI_STUFF):
          self.check_gui_stuff()
        return QFrame.timerEvent(self, event)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(self.color_1)
        painter.drawLine(0, 3, self.width(), 3)
        painter.drawLine(0, self.height()-4, self.width(), self.height()-4)
        painter.setPen(self.color_2)
        painter.drawLine(0, 2, self.width(), 2)
        painter.drawLine(0, self.height()-3, self.width(), self.height()-3)
        painter.setPen(self.color_3)
        painter.drawLine(0, 1, self.width(), 1)
        painter.drawLine(0, self.height()-2, self.width(), self.height()-2)
        painter.setPen(self.color_4)
        painter.drawLine(0, 0, self.width(), 0)
        painter.drawLine(0, self.height()-1, self.width(), self.height()-1)
        QFrame.paintEvent(self, event)

# About Carla Dialog
class AboutW(QDialog, ui_carla_about.Ui_AboutW):
    def __init__(self, parent=None):
        super(AboutW, self).__init__(parent)
        self.setupUi(self)

        self.l_about.setText(self.tr(""
            "<br>Version %1"
            "<br>Carla is a Multi-Plugin Host for JACK - <b>OSC Bridge Version</b>.<br>"
            "<br>Copyright (C) 2011 falkTX<br>"
            "<br><i>VST is a trademark of Steinberg Media Technologies GmbH.</i>"
            "").arg(VERSION))

        self.tabWidget.removeTab(1)
        self.tabWidget.removeTab(1)

# Main Window
class CarlaControlW(QMainWindow, ui_carla_control.Ui_CarlaControlW):
    def __init__(self, parent=None):
        super(CarlaControlW, self).__init__(parent)
        self.setupUi(self)

        self.settings = QSettings("Cadence", "Carla-Control")
        self.loadSettings()

        self.lo_address = ""
        self.lo_server = None

        self.setStyleSheet("""
          QWidget#centralwidget {
            background-color: qlineargradient(spread:pad,
                x1:0.0, y1:0.0,
                x2:0.2, y2:1.0,
                stop:0 rgb( 7,  7,  7),
                stop:1 rgb(28, 28, 28)
            );
          }
        """)

        self.plugin_list = []
        for x in range(MAX_PLUGINS):
          self.plugin_list.append(None)

        self.act_file_refresh.setEnabled(False)

        self.resize(self.width(), 0)

        self.connect(self.act_file_connect, SIGNAL("triggered()"), self.do_connect)
        self.connect(self.act_file_refresh, SIGNAL("triggered()"), self.do_refresh)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCarla)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("AddPluginCallback(int, QString)"), self.handleAddPluginCallback)
        self.connect(self, SIGNAL("RemovePluginCallback(int)"), self.handleRemovePluginCallback)
        self.connect(self, SIGNAL("SetPluginDataCallback(int, int, int, int, QString, QString, QString, QString, long)"), self.handleSetPluginDataCallback)
        self.connect(self, SIGNAL("SetPluginPortsCallback(int, int, int, int, int, int, int, int)"), self.handleSetPluginPortsCallback)
        self.connect(self, SIGNAL("SetParameterValueCallback(int, int, double)"), self.handleSetParameterCallback)
        self.connect(self, SIGNAL("SetParameterDataCallback(int, int, int, int, QString, QString, double, double, double, double, double, double, double)"), self.handleSetParameterDataCallback)
        self.connect(self, SIGNAL("SetDefaultValueCallback(int, int, double)"), self.handleSetDefaultValueCallback)
        self.connect(self, SIGNAL("SetInputPeakValueCallback(int, int, double)"), self.handleSetInputPeakValueCallback)
        self.connect(self, SIGNAL("SetOutputPeakValueCallback(int, int, double)"), self.handleSetOutputPeakValueCallback)
        self.connect(self, SIGNAL("SetProgramCallback(int, int)"), self.handleSetProgramCallback)
        self.connect(self, SIGNAL("SetProgramCountCallback(int, int)"), self.handleSetProgramCountCallback)
        self.connect(self, SIGNAL("SetProgramNameCallback(int, int, QString)"), self.handleSetProgramNameCallback)
        self.connect(self, SIGNAL("SetMidiProgramCallback(int, int)"), self.handleSetMidiProgramCallback)
        self.connect(self, SIGNAL("SetMidiProgramCountCallback(int, int)"), self.handleSetMidiProgramCountCallback)
        self.connect(self, SIGNAL("SetMidiProgramDataCallback(int, int, QString)"), self.handleSetMidiProgramDataCallback)
        self.connect(self, SIGNAL("NoteOnCallback(int, int, int)"), self.handleNoteOnCallback)
        self.connect(self, SIGNAL("NoteOffCallback(int, int, int)"), self.handleNoteOffCallback)
        self.connect(self, SIGNAL("ExitCallback()"), self.handleExitCallback)

    def do_connect(self):
        global carla_name, lo_target
        if (lo_target and self.lo_server):
          url_text = self.lo_address
        else:
          url_text = "osc.udp://127.0.0.1:19000/Carla"

        ans_value = QInputDialog.getText(self, self.tr("Carla Control - Connect"), self.tr("Address"), text=url_text)

        if (ans_value[1]):
          if (lo_target and self.lo_server):
            lo_send(lo_target, "unregister")

          self.act_file_refresh.setEnabled(True)
          self.lo_address = QStringStr(ans_value[0])
          lo_target = Address(self.lo_address)

          carla_name = self.lo_address.rsplit("/", 1)[1]
          print "connected to", self.lo_address, "as", carla_name

          try:
            self.lo_server = ControlServer(self)
          except ServerError, err:
            print str(err)

          if (self.lo_server):
            self.lo_server.start()
            self.do_refresh()
          else:
            pass

    def do_refresh(self):
        global lo_target
        if (lo_target and self.lo_server):
          self.func_remove_all()
          lo_send(lo_target, "unregister")
          lo_send(lo_target, "register", self.lo_server.get_full_url())

    def func_remove_all(self):
        for i in range(MAX_PLUGINS):
          if (self.plugin_list[i] != None):
            self.handleRemovePluginCallback(i)

    def handleAddPluginCallback(self, plugin_id, plugin_name):
        pwidget = PluginWidget(self, plugin_id, plugin_name)
        self.w_plugins.layout().addWidget(pwidget)
        self.plugin_list[plugin_id] = pwidget

    def handleRemovePluginCallback(self, plugin_id):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.close()
          pwidget.close()
          pwidget.deleteLater()
          self.w_plugins.layout().removeWidget(pwidget)
          self.plugin_list[plugin_id] = None

    def handleSetPluginDataCallback(self, plugin_id, ptype, category, hints, name, label, maker, copyright, unique_id):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.set_data(ptype, category, hints, name, label, maker, copyright, unique_id)

    def handleSetPluginPortsCallback(self, plugin_id, ains, aouts, mins, mouts, cins, couts, ctotals):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.set_ports(ains, aouts, mins, mouts, cins, couts, ctotals)

    def handleSetParameterCallback(self, plugin_id, parameter_id, value):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          if (parameter_id < 0):
            pwidget.parameter_activity_timer = ICON_STATE_ON
          else:
            for param in pwidget.edit_dialog.parameter_list:
              if (param[1] == parameter_id):
                if (param[0] == PARAMETER_INPUT):
                  pwidget.parameter_activity_timer = ICON_STATE_ON
                break

          if (parameter_id == PARAMETER_ACTIVE):
            pwidget.set_active((value > 0.0), True, False)
          elif (parameter_id == PARAMETER_DRYWET):
            pwidget.set_drywet(value*1000, True, False)
          elif (parameter_id == PARAMETER_VOLUME):
            pwidget.set_vol(value*1000, True, False)
          elif (parameter_id == PARAMETER_BALANCE_LEFT):
            pwidget.set_balance_left(value*1000, True, False)
          elif (parameter_id == PARAMETER_BALANCE_RIGHT):
            pwidget.set_balance_right(value*1000, True, False)
          elif (parameter_id >= 0):
            pwidget.edit_dialog.set_parameter_value(parameter_id, value)

    def handleSetParameterDataCallback(self, plugin_id, param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_parameter_data(param_id, ptype, hints, name, label, current, x_min, x_max, x_def, x_step, x_step_small, x_step_large)

    def handleSetDefaultValueCallback(self, plugin_id, param_id, value):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_parameter_default_value(param_id, value)

    def handleSetInputPeakValueCallback(self, plugin_id, port_id, value):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.set_input_peak_value(port_id, value)

    def handleSetOutputPeakValueCallback(self, plugin_id, port_id, value):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.set_output_peak_value(port_id, value)

    def handleSetProgramCallback(self, plugin_id, program_id):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_program(program_id)

    def handleSetProgramCountCallback(self, plugin_id, program_count):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_program_count(program_count)

    def handleSetProgramNameCallback(self, plugin_id, program_id, program_name):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_program_name(program_id, program_name)

    def handleSetMidiProgramCallback(self, plugin_id, midi_program_id):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_midi_program(midi_program_id)

    def handleSetMidiProgramCountCallback(self, plugin_id, midi_program_count):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_midi_program_count(midi_program_count)

    def handleSetMidiProgramDataCallback(self, plugin_id, midi_program_id, bank_id, program_id, midi_program_name):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.set_midi_program_data(midi_program_id, bank_id, program_id, midi_program_name)

    def handleNoteOnCallback(self, plugin_id, note, velo):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.keyboard.noteOn(note, False)

    def handleNoteOffCallback(self, plugin_id, note, velo):
        pwidget = self.plugin_list[plugin_id]
        if (pwidget):
          pwidget.edit_dialog.keyboard.noteOff(note, False)

    def handleExitCallback(self):
        self.func_remove_all()
        self.act_file_refresh.setEnabled(False)

        global carla_name, lo_target
        carla_name = ""
        lo_target = None
        self.lo_address = ""

    def aboutCarla(self):
        AboutW(self).exec_()

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

    def closeEvent(self, event):
        self.saveSettings()

        global lo_target
        if (lo_target and self.lo_server):
          lo_send(lo_target, "unregister")

        QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Carla-Control")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/48x48/carla-control.png"))

    style = app.style().metaObject().className()
    force_parameters_style = (style in ["Bespin::Style"])

    NativeHost = Host()

    # Show GUI
    gui = CarlaControlW()
    gui.show()

    # Set-up custom signal handling
    set_up_signals(gui)

    # App-Loop
    sys.exit(app.exec_())
