#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtGui import QPalette, QColor

def rgb_max(value):
  if (value < 0):
    value = 0
  elif (value > 255):
    value = 255
  return value

def to_disabled(r, g, b):
  r = rgb_max((r*0.75)+(120*0.25))
  g = rgb_max((g*0.75)+(120*0.25))
  b = rgb_max((b*0.75)+(110*0.25))
  return (r, g, b)

def to_disabled_text(r, g, b):
  r = rgb_max((r*0.75)-(120*0.25))
  g = rgb_max((g*0.75)-(120*0.25))
  b = rgb_max((b*0.75)-(110*0.25))
  return (r, g, b)

def set_kxstudio_palette(app):
  kxPal = QPalette()

  # Window
  r, g, b = to_disabled(17,17,17)
  kxPal.setColor(QPalette.Window, QColor(17,17,17))
  kxPal.setColor(QPalette.Disabled, QPalette.Window, QColor(r, g, b))

  # Window Text
  kxPal.setColor(QPalette.WindowText, QColor(240,240,240))
  kxPal.setColor(QPalette.Disabled, QPalette.WindowText, QColor(100,100,100))

  # Base
  r, g, b = to_disabled(7,7,7)
  kxPal.setColor(QPalette.Base, QColor(7,7,7))
  kxPal.setColor(QPalette.Disabled, QPalette.Base, QColor(r, g, b))

  # Base Alternate
  r, g, b = to_disabled(14,14,14)
  kxPal.setColor(QPalette.AlternateBase, QColor(14,14,14))
  kxPal.setColor(QPalette.Disabled, QPalette.AlternateBase, QColor(r, g, b))

  # ToolTip
  kxPal.setColor(QPalette.ToolTipBase, QColor(4,4,4))
  kxPal.setColor(QPalette.ToolTipText, QColor(230,230,230))
  kxPal.setColor(QPalette.Disabled, QPalette.ToolTipText, QColor(100,100,100))

  # Text
  kxPal.setColor(QPalette.Text, QColor(240,240,240))
  kxPal.setColor(QPalette.Disabled, QPalette.Text, QColor(100,100,100))

  # Button
  r, g, b = to_disabled(28,28,28)
  kxPal.setColor(QPalette.Button, QColor(28,28,28))
  kxPal.setColor(QPalette.Disabled, QPalette.Button, QColor(r, g, b))

  # Button Text
  kxPal.setColor(QPalette.ButtonText, QColor(240,240,240))
  kxPal.setColor(QPalette.Disabled, QPalette.ButtonText, QColor(100,100,100))

  # Bright Text
  kxPal.setColor(QPalette.BrightText, QColor(255,255,255))

  kxPal.setColor(QPalette.Light,    QColor(11,11,11))
  kxPal.setColor(QPalette.Midlight, QColor(14,14,14))
  kxPal.setColor(QPalette.Mid,      QColor(48,48,48))
  kxPal.setColor(QPalette.Dark,     QColor(80,80,80))
  kxPal.setColor(QPalette.Shadow,   QColor(150,150,150))

  # Highlight
  r, g, b = to_disabled(60,60,60)
  kxPal.setColor(QPalette.Highlight, QColor(60,60,60))
  kxPal.setColor(QPalette.Disabled, QPalette.Highlight, QColor(r, g, b))

  # Highlight Text
  r, g, b = to_disabled_text(255,255,255)
  kxPal.setColor(QPalette.HighlightedText, QColor(255,255,255))
  kxPal.setColor(QPalette.Disabled, QPalette.HighlightedText, QColor(r, g, b))

  # Link Text
  kxPal.setColor(QPalette.Link, QColor(100,100,230))
  kxPal.setColor(QPalette.LinkVisited, QColor(230,100,230))

  app.setPalette(kxPal)
