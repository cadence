#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QTimer, SIGNAL
from PyQt4.QtGui import QLabel

# Simple Clickable Label (for screenshots -> apps)
class ClickableLabel(QLabel):
    def __init__(self, parent=None):
        super(ClickableLabel, self).__init__(parent)

        self.setCursor(Qt.PointingHandCursor)

    def mousePressEvent(self, event):
        # Use busy cursor for 2 secs
        self.setCursor(Qt.WaitCursor)
        QTimer.singleShot(2000, self.setNormalCursor)

        self.emit(SIGNAL("clicked()"))
        QLabel.mousePressEvent(self, event)

    def setNormalCursor(self):
        self.setCursor(Qt.PointingHandCursor)
