#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import sys
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QApplication, QWidget

# Imports (Custom Stuff)
import jacklib
from digitalpeakmeter import DigitalPeakMeter

global x_port1, x_port2, need_reconnect
x_port1 = 0.0
x_port2 = 0.0
need_reconnect = False

def process_callback(nframes, arg=None):
  global x_port1, x_port2

  p_out1 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(port_1, nframes))
  p_out2 = jacklib.translate_audio_port_buffer(jacklib.port_get_buffer(port_2, nframes))

  for i in range(nframes):
    if (abs(p_out1[i]) > x_port1):
      x_port1 = abs(p_out1[i])

    if (abs(p_out2[i]) > x_port2):
      x_port2 = abs(p_out2[i])

  return 0

def port_callback(port_a, port_b, connect_yesno, arg=None):
  global need_reconnect
  need_reconnect = True
  return 0

def reconnect_inputs():
  play_port_1 = jacklib.port_by_name(client, "system:playback_1")
  play_port_2 = jacklib.port_by_name(client, "system:playback_2")
  list_port_1 = jacklib.port_get_all_connections(client, play_port_1)
  list_port_2 = jacklib.port_get_all_connections(client, play_port_2)

  for port in list_port_1:
    this_port = jacklib.port_by_name(client, port)
    if not jacklib.port_is_mine(client, this_port):
      jacklib.connect(client, port, "jackmeter:in1")

  for port in list_port_2:
    this_port = jacklib.port_by_name(client, port)
    if not jacklib.port_is_mine(client, this_port):
      jacklib.connect(client, port, "jackmeter:in2")

class MeterW(DigitalPeakMeter):
    def __init__(self, parent=None):
        super(MeterW, self).__init__(parent)

        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint);
        self.setWindowTitle("M")
        self.setChannels(2)
        self.setOrientation(self.VERTICAL)

        self.displayMeter(1, x_port1)
        self.displayMeter(2, x_port2)

        self.setRefreshRate(30)
        self.peakTimer = self.startTimer(60)

    def timerEvent(self, event):
        if (event.timerId() == self.peakTimer):
          global x_port1, x_port2, need_reconnect
          self.displayMeter(1, x_port1)
          self.displayMeter(2, x_port2)
          x_port1 = 0.0
          x_port2 = 0.0

          if (need_reconnect):
            reconnect_inputs()
            need_reconnect = False
        QWidget.timerEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)

    client = jacklib.client_open("jackmeter", jacklib.NullOption, 0)

    port_1 = jacklib.port_register(client, "in1", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
    port_2 = jacklib.port_register(client, "in2", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)

    jacklib.set_process_callback(client, process_callback, None)
    jacklib.set_port_connect_callback(client, port_callback, None)
    jacklib.activate(client)

    reconnect_inputs()

    # Show GUI
    gui = MeterW()
    gui.resize(50, 500)
    gui.show()

    # App-Loop
    ret = app.exec_()

    jacklib.deactivate(client)
    jacklib.client_close(client)

    sys.exit(ret)
