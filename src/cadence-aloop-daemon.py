#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from os import system
from signal import signal, SIGINT, SIGTERM
from time import sleep

# Imports (Custom Stuff)
import jacklib

# Global loop check
global doRun
doRun = True

# Global JACK variables
sample_rate = 44100
buffer_size = 1024
nperiods = 2

def signal_handler(sig, frame=0):
  global doRun
  doRun = False

def run_alsa_bridge(x_sample_rate, x_buffer_size, x_nperiods):
  system("killall alsa_in alsa_out pulseaudio")
  system("env JACK_SAMPLE_RATE=%i JACK_PERIOD_SIZE=%i JACK_PERIODS=%i alsa_in -j alsa_in -dcloop -q 1 2>&1 1> /dev/null &" % (x_sample_rate, x_buffer_size, x_nperiods))
  system("env JACK_SAMPLE_RATE=%i JACK_PERIOD_SIZE=%i JACK_PERIODS=%i alsa_out -j alsa_out -dploop -q 1 2>&1 1> /dev/null &" % (x_sample_rate, x_buffer_size, x_nperiods))

def buffer_size_callback(new_buffer_size, arg=None):
  buffer_size = new_buffer_size
  run_alsa_bridge(sample_rate, new_buffer_size, nperiods)
  return 0

#--------------- main ------------------
if __name__ == '__main__':

  # Init JACK client
  client = jacklib.client_open("cadence-aloop-daemon", jacklib.NullOption, 0)

  if (client == None):
    print "Could not connect to JACK"
    quit()

  jacklib.set_buffer_size_callback(client, buffer_size_callback, None)
  jacklib.activate(client)

  # Quit when requested
  signal(SIGINT, signal_handler)
  signal(SIGTERM, signal_handler)

  # Get initial values
  sample_rate = jacklib.get_sample_rate(client)
  buffer_size = jacklib.get_buffer_size(client)
  nperiods = 2  # TODO - get nperiods value (DBus?)

  # Activate alsa_in/out
  run_alsa_bridge(sample_rate, buffer_size, nperiods)

  # Pause it for a bit, and connect to the proper system ports
  sleep(1)
  jacklib.connect(client, "alsa_in:capture_1", "system:playback_1")
  jacklib.connect(client, "alsa_in:capture_2", "system:playback_2")
  jacklib.connect(client, "system:capture_1", "alsa_out:playback_1")
  jacklib.connect(client, "system:capture_2", "alsa_out:playback_2")

  # Keep running until told otherwise
  while (doRun):
    sleep(1)

  # Close JACK client
  jacklib.deactivate(client)
  jacklib.client_close(client)
