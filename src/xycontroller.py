#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import sys
from PyQt4.QtCore import Qt, QPointF, QRectF, QSettings, QTimer, QVariant, SIGNAL
from PyQt4.QtGui import QApplication, QColor, QIcon, QPainter, QPen, QGraphicsItem, QGraphicsScene, QMainWindow, QMessageBox
from Queue import Queue, Empty as QuequeEmpty

# Imports (Plugins and Resources)
import jacklib, ui_xycontroller
from shared import *

# Globals
global jack_client, jack_midi_in_port, jack_midi_out_port, jack_midi_in_data, jack_midi_out_data
jack_client = None
jack_midi_in_port  = None
jack_midi_out_port = None
jack_midi_in_data  = Queue(512)
jack_midi_out_data = Queue(512)

# XY Controller Scene
class XYGraphicsScene(QGraphicsScene):
    def __init__(self, parent=None):
        super(XYGraphicsScene, self).__init__(parent)

        self.setBackgroundBrush(QColor(0,0,0))

        cursor_pen = QPen(QColor(255,255,255), 2)
        cursor_brush = QColor(255,255,255,50)
        self.xy_cursor = self.addEllipse(QRectF(-10, -10, 20, 20), cursor_pen, cursor_brush)

        line_pen = QPen(QColor(200,200,200,100), 1, Qt.DashLine)
        self.hline = self.addLine(-9999, 0, 9999, 0, line_pen)
        self.vline = self.addLine(0, -9999, 0, 9999, line_pen)

        self.rect_size = QRectF(-100, -100, 100, 100)
        self.bounds = self.addRect(self.rect_size, Qt.white)

        self.x_cc = self.parent().x_cc
        self.y_cc = self.parent().y_cc
        self.channels = self.parent().channels

        self.lock = False

    def setPosX(self, x, forward=True):
        if not self.lock:
          pos_x = x*(self.rect_size.x()+self.rect_size.width())
          self.xy_cursor.setPos(pos_x, self.xy_cursor.y())
          self.vline.setX(pos_x)

          if (forward):
            self.handleMIDI(pos_x/(self.rect_size.x()+self.rect_size.width()))

    def setPosY(self, y, forward=True):
        if not self.lock:
          pos_y = y*(self.rect_size.y()+self.rect_size.height())
          self.xy_cursor.setPos(self.xy_cursor.x(), pos_y)
          self.hline.setY(pos_y)

          if (forward):
            self.handleMIDI(None, pos_y/(self.rect_size.y()+self.rect_size.height()))

    def handleCCfromJACK(self, param, value):
        if (param == self.x_cc):
          xp = (float(value)/63)-1.025
          yp = self.xy_cursor.y()/(self.rect_size.y()+self.rect_size.height())
          self.setPosX(xp, False)

        elif (param == self.y_cc):
          xp = self.xy_cursor.x()/(self.rect_size.x()+self.rect_size.width())
          yp = (float(value)/63)-1.025
          self.setPosY(yp, False)

        else:
          return

        self.emit(SIGNAL("cursorMoved(float, float)"), xp, yp)

    def handleMousePos(self, pos):
        if (not self.rect_size.contains(pos)):
          if (pos.x() < self.rect_size.x()):
            pos.setX(self.rect_size.x())
          elif (pos.x() > self.rect_size.x()+self.rect_size.width()):
            pos.setX(self.rect_size.x()+self.rect_size.width())

          if (pos.y() < self.rect_size.y()):
            pos.setY(self.rect_size.y())
          elif (pos.y() > self.rect_size.y()+self.rect_size.height()):
            pos.setY(self.rect_size.y()+self.rect_size.height())

        self.xy_cursor.setPos(pos)
        self.hline.setY(pos.y())
        self.vline.setX(pos.x())

        xp = pos.x()/(self.rect_size.x()+self.rect_size.width())
        yp = pos.y()/(self.rect_size.y()+self.rect_size.height())

        self.emit(SIGNAL("cursorMoved(float, float)"), xp, yp)
        self.handleMIDI(xp, yp)

    def handleMIDI(self, xp=None, yp=None):
        global jack_midi_out_data
        rate = float(0xff)/4

        if (xp != None):
          value = int((xp*rate)+rate)
          for channel in self.channels:
            jack_midi_out_data.put_nowait((0xB0+channel-1, self.x_cc, value))

        if (yp != None):
          value = int((yp*rate)+rate)
          for channel in self.channels:
            jack_midi_out_data.put_nowait((0xB0+channel-1, self.y_cc, value))

    def keyPressEvent(self, event):
        event.accept()

    def wheelEvent(self, event):
        event.accept()

    def mousePressEvent(self, event):
        self.lock = True
        self.handleMousePos(event.scenePos())
        QGraphicsScene.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        self.handleMousePos(event.scenePos())
        QGraphicsScene.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        self.lock = False
        QGraphicsScene.mouseReleaseEvent(self, event)

    def updateSize(self, size):
        self.rect_size.setRect(-(size.width()/2), -(size.height()/2), size.width(), size.height())
        self.bounds.setRect(self.rect_size)

# XY Controller Window
class XYControllerW(QMainWindow, ui_xycontroller.Ui_XYControllerW):
    def __init__(self, parent=None):
        super(XYControllerW, self).__init__(parent)
        self.setupUi(self)

        # TODO - NOT IMPLEMENTED YET
        self.cb_smooth.setEnabled(False)

        # Pre-Initial setup
        self.dial_x.setPixmap(2)
        self.dial_y.setPixmap(2)
        self.dial_x.setLabel("X")
        self.dial_y.setLabel("Y")
        self.keyboard.setOctaves(6)

        self.x_cc = 1
        self.y_cc = 2
        self.channels = []

        ## Load App Settings
        self.settings = QSettings("Cadence", "XY-Controller")
        self.loadSettings()

        for i in range(len(MIDI_CC_LIST)):
          self.cb_control_x.addItem(MIDI_CC_LIST[i])
          self.cb_control_y.addItem(MIDI_CC_LIST[i])

          cc = int(MIDI_CC_LIST[i].split(" ")[0], 16)
          if (self.x_cc == cc):
            self.cb_control_x.setCurrentIndex(i)
          if (self.y_cc == cc):
            self.cb_control_y.setCurrentIndex(i)

        self.midiInTimer = self.startTimer(100)

        self.scene = XYGraphicsScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHints(QPainter.Antialiasing)

        # Connect actions to functions
        self.connect(self.act_show_keyboard, SIGNAL("triggered(bool)"), self.showKeyboard)
        self.connect(self.act_about, SIGNAL("triggered()"), self.aboutXYController)

        self.connect(self.act_ch_01, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_02, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_03, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_04, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_05, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_06, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_07, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_08, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_09, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_10, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_11, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_12, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_13, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_14, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_15, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_16, SIGNAL("triggered(bool)"), self.checkChannel)
        self.connect(self.act_ch_all, SIGNAL("triggered()"), self.checkChannel_all)
        self.connect(self.act_ch_none, SIGNAL("triggered()"), self.checkChannel_none)

        self.connect(self.keyboard, SIGNAL("noteOn(int)"), self.handleNoteOn)
        self.connect(self.keyboard, SIGNAL("noteOff(int)"), self.handleNoteOff)

        self.connect(self.cb_control_x, SIGNAL("currentIndexChanged(QString)"), self.checkCC_X)
        self.connect(self.cb_control_y, SIGNAL("currentIndexChanged(QString)"), self.checkCC_Y)

        self.connect(self.dial_x, SIGNAL("valueChanged(int)"), self.updateX)
        self.connect(self.dial_y, SIGNAL("valueChanged(int)"), self.updateY)

        self.connect(self.scene, SIGNAL("cursorMoved(float, float)"), self.updateDials)

        self.connect(self, SIGNAL("SIGTERM()"), self.close)

    def showKeyboard(self, show):
        self.scrollArea.setVisible(show)
        QTimer.singleShot(0, self.updateScreen)

    def updateDials(self, xp, yp):
        self.dial_x.setValue(xp*100)
        self.dial_y.setValue(yp*100)

    def updateScreen(self):
        self.scene.updateSize(self.graphicsView.size())
        self.graphicsView.centerOn(0, 0)
        self.updateXY()

    def updateX(self, x):
        self.scene.setPosX(float(x)/100)

    def updateY(self, y):
        self.scene.setPosY(float(y)/100)

    def updateXY(self):
        self.updateX(self.dial_x.value())
        self.updateY(self.dial_y.value())

    def checkChannel(self, clicked):
        channel = int(str(self.sender().text()))
        if (clicked and not channel in self.channels):
          self.channels.append(channel)
        elif (not clicked and channel in self.channels):
          self.channels.remove(channel)
        self.scene.channels = self.channels

    def checkChannel_all(self):
        self.act_ch_01.setChecked(True)
        self.act_ch_02.setChecked(True)
        self.act_ch_03.setChecked(True)
        self.act_ch_04.setChecked(True)
        self.act_ch_05.setChecked(True)
        self.act_ch_06.setChecked(True)
        self.act_ch_07.setChecked(True)
        self.act_ch_08.setChecked(True)
        self.act_ch_09.setChecked(True)
        self.act_ch_10.setChecked(True)
        self.act_ch_11.setChecked(True)
        self.act_ch_12.setChecked(True)
        self.act_ch_13.setChecked(True)
        self.act_ch_14.setChecked(True)
        self.act_ch_15.setChecked(True)
        self.act_ch_16.setChecked(True)
        self.channels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
        self.scene.channels = self.channels

    def checkChannel_none(self):
        self.act_ch_01.setChecked(False)
        self.act_ch_02.setChecked(False)
        self.act_ch_03.setChecked(False)
        self.act_ch_04.setChecked(False)
        self.act_ch_05.setChecked(False)
        self.act_ch_06.setChecked(False)
        self.act_ch_07.setChecked(False)
        self.act_ch_08.setChecked(False)
        self.act_ch_09.setChecked(False)
        self.act_ch_10.setChecked(False)
        self.act_ch_11.setChecked(False)
        self.act_ch_12.setChecked(False)
        self.act_ch_13.setChecked(False)
        self.act_ch_14.setChecked(False)
        self.act_ch_15.setChecked(False)
        self.act_ch_16.setChecked(False)
        self.channels = []
        self.scene.channels = self.channels

    def checkCC_X(self, text):
        if (not text.isEmpty()):
          self.x_cc = int(str(text).split(" ")[0], 16)
          self.scene.x_cc = self.x_cc

    def checkCC_Y(self, text):
        if (not text.isEmpty()):
          self.y_cc = int(str(text).split(" ")[0], 16)
          self.scene.y_cc = self.y_cc

    def handleNoteOn(self, note):
        for channel in self.channels:
          jack_midi_out_data.put_nowait((0x90+channel-1, note, 100))

    def handleNoteOff(self, note):
        for channel in self.channels:
          jack_midi_out_data.put_nowait((0x80+channel-1, note, 100))

    def aboutXYController(self):
        QMessageBox.about(self, self.tr("About XY Controller"), self.tr("<h3>XY Controller</h3>"
            "<br>Version %1"
            "<br>XY Controller is a simple XY widget that sends and receives data from Jack MIDI.<br>"
            "<br>Copyright (C) 2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.settings.setValue("ShowKeyboard", self.act_show_keyboard.isChecked())
        self.settings.setValue("DialX", self.dial_x.value())
        self.settings.setValue("DialY", self.dial_y.value())
        self.settings.setValue("ControlX", self.x_cc)
        self.settings.setValue("ControlY", self.y_cc)
        self.settings.setValue("Channels", self.channels)

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())
        self.act_show_keyboard.setChecked(self.settings.value("ShowKeyboard", True).toBool())
        self.showKeyboard(self.settings.value("ShowKeyboard", True).toBool()) # signal is not yet connected
        self.dial_x.setValue(self.settings.value("DialX", 50).toInt()[0])
        self.dial_y.setValue(self.settings.value("DialY", 50).toInt()[0])
        self.x_cc = self.settings.value("ControlX", 1).toInt()[0]
        self.y_cc = self.settings.value("ControlY", 2).toInt()[0]
        self.channels = QVariantPyObjectList(self.settings.value("Channels", [1]).toList())

        for i in range(len(self.channels)):
          self.channels[i] = int(self.channels[i])

        if (1 in self.channels):
          self.act_ch_01.setChecked(True)
        if (2 in self.channels):
          self.act_ch_02.setChecked(True)
        if (3 in self.channels):
          self.act_ch_03.setChecked(True)
        if (4 in self.channels):
          self.act_ch_04.setChecked(True)
        if (5 in self.channels):
          self.act_ch_05.setChecked(True)
        if (6 in self.channels):
          self.act_ch_06.setChecked(True)
        if (7 in self.channels):
          self.act_ch_07.setChecked(True)
        if (8 in self.channels):
          self.act_ch_08.setChecked(True)
        if (9 in self.channels):
          self.act_ch_09.setChecked(True)
        if (10 in self.channels):
          self.act_ch_10.setChecked(True)
        if (11 in self.channels):
          self.act_ch_11.setChecked(True)
        if (12 in self.channels):
          self.act_ch_12.setChecked(True)
        if (13 in self.channels):
          self.act_ch_13.setChecked(True)
        if (14 in self.channels):
          self.act_ch_14.setChecked(True)
        if (15 in self.channels):
          self.act_ch_15.setChecked(True)
        if (16 in self.channels):
          self.act_ch_16.setChecked(True)

    def timerEvent(self, event):
        if (event.timerId() == self.midiInTimer):
          global jack_midi_in_data
          if (not jack_midi_in_data.empty()):
            while (True):
              try:
                mode, note, velo = jack_midi_in_data.get_nowait()
              except QuequeEmpty:
                break

              if (0x80 <= mode and mode <= 0x8F):
                self.keyboard.noteOff(note, False)
              elif (0x90 <= mode and mode < 0x9F):
                self.keyboard.noteOn(note, False)
              elif (0xB0 <= mode and mode < 0xBF):
                channel = mode - 0xB0+1
                if (channel in self.channels):
                  self.scene.handleCCfromJACK(note, velo)

              jack_midi_in_data.task_done()

        QMainWindow.timerEvent(self, event)

    def closeEvent(self, event):
        self.saveSettings()
        QMainWindow.closeEvent(self, event)

    def resizeEvent(self, event):
        QTimer.singleShot(0, self.updateScreen)
        QMainWindow.resizeEvent(self, event)


static_event = jacklib.jack_midi_event_t()

def jack_process_callback(nframes, arg):
    global jack_midi_in_port, jack_midi_out_port, jack_midi_in_data, jack_midi_out_data

    # MIDI In
    midi_in_buffer = jacklib.port_get_buffer(jack_midi_in_port, nframes)
    event_count = jacklib.midi_get_event_count(midi_in_buffer)

    if (event_count > 0):
      for i in range(event_count):
        if (jacklib.midi_event_get(jacklib.pointer(static_event), midi_in_buffer, i) == 0):
          data = jacklib.translate_midi_event_buffer(static_event.buffer)

          if (len(data) == 1):
            jack_midi_in_data.put_nowait((data[0], 0, 0))

          elif (len(data) == 2):
            jack_midi_in_data.put_nowait((data[0], data[1], 0))

          elif (len(data) == 3):
            jack_midi_in_data.put_nowait((data[0], data[1], data[2]))

          if (jack_midi_in_data.full()):
            break

    # MIDI Out
    midi_out_buffer = jacklib.port_get_buffer(jack_midi_out_port, nframes)
    jacklib.midi_clear_buffer(midi_out_buffer)

    if (not jack_midi_out_data.empty()):
      while (True):
        try:
          mode, note, velo = jack_midi_out_data.get_nowait()
        except QuequeEmpty:
          break

        data = jacklib.encode_midi_data(mode, note, velo)
        jacklib.midi_event_write(midi_out_buffer, 0, data, 3)

        jack_midi_out_data.task_done()

    return 0


#--------------- main ------------------
if __name__ == '__main__':
    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("XY-Controller")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    #app.setWindowIcon(QIcon(":/48x48/xy-controller.png"))

    # Start jack
    jack_client = jacklib.client_open("XY-Controller", jacklib.NullOption, 0)
    jack_midi_in_port = jacklib.port_register(jack_client, "midi_in", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsInput, 0)
    jack_midi_out_port = jacklib.port_register(jack_client, "midi_out", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsOutput, 0)
    jacklib.set_process_callback(jack_client, jack_process_callback, 0)

    # Show GUI
    gui = XYControllerW()
    gui.show()

    # Set-up custom signal handling
    set_up_signals(gui)

    jacklib.activate(jack_client)

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack_client):
      jacklib.deactivate(jack_client)
      jacklib.client_close(jack_client)

    # Exit properly
    sys.exit(ret)
