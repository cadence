#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import QRectF, QSize
from PyQt4.QtGui import QPainter, QPixmap, QPushButton

# Imports (Custom Stuff)
import icons_rc

# LED Button
class LEDButton(QPushButton):

    BLUE    = 1
    GREEN   = 2
    RED     = 3
    YELLOW  = 4
    BIG_RED = 5

    def __init__(self, parent=None):
        super(LEDButton, self).__init__(parent)

        self.setCheckable(True)
        self.setText("")

        self.setColor(self.GREEN)

    def minimumSizeHint(self):
        return QSize(self.pixmap_size, self.pixmap_size)

    def sizeHint(self):
        return QSize(self.pixmap_size, self.pixmap_size)

    def setColor(self, color):
        self.color = color

        if (color in [self.BLUE, self.GREEN, self.RED, self.YELLOW]):
          size = 14
        elif (color in [self.BIG_RED]):
          size = 64
        else:
          size = 16

        self.setPixmapSize(size)

    def setPixmapSize(self, size):
        self.pixmap_size = size
        self.pixmap_rect = QRectF(0, 0, self.pixmap_size, self.pixmap_size)

        self.setMinimumWidth(self.pixmap_size)
        self.setMaximumWidth(self.pixmap_size)
        self.setMinimumHeight(self.pixmap_size)
        self.setMaximumHeight(self.pixmap_size)

    def paintEvent(self, event):
        painter = QPainter(self)

        pixmap = QPixmap()
        if (self.isChecked()):
          if (self.color == self.BLUE):
            pixmap.load(":/bitmaps/led_blue.png")
          elif (self.color == self.GREEN):
            pixmap.load(":/bitmaps/led_green.png")
          elif (self.color == self.RED):
            pixmap.load(":/bitmaps/led_red.png")
          elif (self.color == self.YELLOW):
            pixmap.load(":/bitmaps/led_yellow.png")
          elif (self.color == self.BIG_RED):
            pixmap.load(":/bitmaps/led-big_on.png")
          else:
            pixmap.load(":/bitmaps/led_off.png")
        else:
          if (self.color == self.BIG_RED):
            pixmap.load(":/bitmaps/led-big_off.png")
          else:
            pixmap.load(":/bitmaps/led_off.png")

        painter.drawPixmap(self.pixmap_rect, pixmap, self.pixmap_rect)
